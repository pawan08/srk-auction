package com.tons.srkauction.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.tons.srkauction.R;
import com.tons.srkauction.SrkAuction;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.storage.FCMData;
import com.tons.srkauction.view.ScrHome;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;
import static androidx.core.app.NotificationCompat.DEFAULT_VIBRATE;
import static androidx.core.app.NotificationCompat.PRIORITY_HIGH;

public class MyFirebaseMessaging extends FirebaseMessagingService {

    private NotificationManagerCompat notificationManager;
    String notification1, title, message, image_url;
    String channelId = "channel1";

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {

            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    return;
                }

                Log.e("TAG", "onComplete: Task " + task);
                String token = task.getResult().getToken();
                Log.e("TAG", "onComplete: " + token);
                FCMData.getInstance().setFCMToken(token);
            }
        });
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        notificationManager = NotificationManagerCompat.from(SrkAuction.appContext);

        try {
            title = remoteMessage.getData().get("title");
            message = remoteMessage.getData().get("message");
            image_url = remoteMessage.getData().get("image_url");
            String click_action = remoteMessage.getData().get("click_action");
            String position = remoteMessage.getData().get("position");

            Log.d("TAG", "onMessageReceived:pos" + position);
            Intent intent = new Intent(click_action);
            intent.putExtra("position",Integer.parseInt(position));
            intent.setFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            // Intent resultIntent = new Intent(BaseApplication.appContext,ScrHome.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(SrkAuction.appContext);
            stackBuilder.addNextIntentWithParentStack(new Intent(SrkAuction.appContext, ScrHome.class));
            PendingIntent pendingIntent = PendingIntent.getActivity(SrkAuction.appContext, 0, intent, PendingIntent.FLAG_ONE_SHOT);

            final NotificationCompat.Builder builder = new NotificationCompat.Builder(SrkAuction.appContext, channelId)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.drawable.ic_camera)
                    .setAutoCancel(true)
                    .setPriority(PRIORITY_HIGH)
                    .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                    .setContentIntent(pendingIntent);

            ImageRequest imageRequest = new ImageRequest(image_url, new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {

                    Log.d("TAG", "onResponse: " + "Success");
                    builder.setStyle(new NotificationCompat.BigPictureStyle().bigPicture(response));

                    // if the image is attached with notification
                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel notificationChannel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_HIGH);

                        notificationManager.createNotificationChannel(notificationChannel);
                    }

                    notificationManager.notify(999, builder.build());

                }
            }, 0, 0, null, Bitmap.Config.ALPHA_8, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    // if the image is not  attached with notification
                    Log.d("TAG", "onResponse: " + "Failed");
                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        NotificationChannel notificationChannel = new NotificationChannel(channelId, "Default channel", NotificationManager.IMPORTANCE_HIGH);

                        notificationManager.createNotificationChannel(notificationChannel);
                    }

                    notificationManager.notify(999, builder.build());

                }
            });

            VolleySingleton.getInstance(this).addToRequestQueue(imageRequest);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
