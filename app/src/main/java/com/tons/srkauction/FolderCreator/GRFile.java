package com.tons.srkauction.FolderCreator;

import android.os.Environment;
import android.util.Log;

import java.io.File;

import static com.tons.srkauction.FolderCreator.FileUtils.folder_document;
import static com.tons.srkauction.FolderCreator.FileUtils.folder_image;
import static com.tons.srkauction.FolderCreator.FileUtils.folder_main;
import static com.tons.srkauction.FolderCreator.FileUtils.folder_pdf;



public class GRFile {
    private static String IMAGE_FOLDER_ON_SD_CARD = folder_main + "/" + folder_image;
    private static String DOC_FOLDER_ON_SD_CARD = folder_main + "/" + folder_document;
    private static String PDF_FOLDER_ON_SD_CARD = folder_main + "/" + folder_pdf;

    public static String getPDfFilePath() {
        String filePath = null;
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            filePath = sdCard.getAbsolutePath() + File.separator + PDF_FOLDER_ON_SD_CARD + File.separator;
            File basedir = new File(filePath);
            if (!basedir.isDirectory()) {
                basedir.mkdirs();
                Log.d("TAG", "pdf Path: folder created ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }

    public static String getIMAGEFilePath() {
        String filePath = null;
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            filePath = sdCard.getAbsolutePath() + File.separator + IMAGE_FOLDER_ON_SD_CARD + File.separator;
            File basedir = new File(filePath);
            if (!basedir.isDirectory()) {
                basedir.mkdirs();
                Log.d("TAG", "pdf Path: folder created ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }


    public static String getDOCFilePath() {
        String filePath = null;
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            filePath = sdCard.getAbsolutePath() + File.separator + DOC_FOLDER_ON_SD_CARD + File.separator;
            File basedir = new File(filePath);
            if (!basedir.isDirectory()) {
                basedir.mkdirs();
                Log.d("TAG", "pdf Path: folder created ");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return filePath;
    }
}