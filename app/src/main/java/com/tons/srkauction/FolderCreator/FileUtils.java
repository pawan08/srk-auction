package com.tons.srkauction.FolderCreator;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Environment;

import java.io.File;

public class FileUtils {
    public static String folder_main = "Srk Auction";
    public static String folder_pdf = "PDF";
    public static String folder_image = "Images";
    public static String folder_document = "Documents";
    public static File f;
    public static File FILE_PDF;


    public static void createFolder(Context context) {

        int code = context.getPackageManager().checkPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                context.getPackageName());
        int code2 = context.getPackageManager().checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, context.getPackageName());
        if (code == PackageManager.PERMISSION_GRANTED) {
            // todo create directory


            f = new File(Environment.getExternalStorageDirectory(), folder_main);
            if (!f.exists()) {
                f.mkdirs();
            }
            FILE_PDF = new File(Environment.getExternalStorageDirectory() + "/" + folder_main, folder_pdf);
            if (!FILE_PDF.isDirectory()) {
                FILE_PDF.mkdirs();
            }

        }

    }
}
