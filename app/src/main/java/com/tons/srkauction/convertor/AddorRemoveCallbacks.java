package com.tons.srkauction.convertor;

public interface AddorRemoveCallbacks {
    public void onAddProduct();
    public void onRemoveProduct();
}
