package com.tons.srkauction.storage;


import android.content.Context;
import android.content.SharedPreferences;

import com.tons.srkauction.SrkAuction;


public class BrokerData {

    private static BrokerData _instance = null;
    private static SharedPreferences _sharedPreferences = null;
    private static SharedPreferences.Editor _sharedPrefEditor = null;
    private final String SHARED_PREFERENCE_NAME = "user_data";


    private final String BROKER_ID = "broker_id";
    private final String BROKER_NAME = "broker_name";
    private final String BROKER_MOBILE = "broker_mobile";
    private final String BROKER_EMAIL = "broker_email";
    private final String BROKER_PASSWORD = "broker_password";
    private final String BROKER_PiC_PROFILE = "broker_pic_profile";
    private final String BROKER_GENDER = "broker_gender";
    private final String BROKER_AGE = "broker_age";
    private final String BROKER_ADDRESS = "broker_address";
    private final String BROKER_WORK_EXPERINCE = "broker_experience";
    private final String BROKER_DEALING_IN = "broker_dealing_in";
    private final String BROKER_UOM = "broker_uom";


    private BrokerData() {

    }

    public static BrokerData getInstance() {
        if (_instance == null) {
            _instance = new BrokerData();
            _instance._initSharedPreferences();
        }
        return _instance;
    }

    /**
     * This method is used to initialized {@link SharedPreferences} and
     * {@link SharedPreferences.Editor}
     */
    public void _initSharedPreferences() {
        _sharedPreferences = _getSharedPref();
        _sharedPrefEditor = _getSharedPrefEditor();
    }

    /**
     * Method to get the SharedPreferences.
     *
     * @return the {@link SharedPreferences} object.
     */
    private SharedPreferences _getSharedPref() {
        if (_sharedPreferences == null) {
            _sharedPreferences = SrkAuction.appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return _sharedPreferences;
    }

    /**
     * Method to get the {@link SharedPreferences.Editor} for writing values to {@link SharedPreferences}.
     *
     * @return the {@link SharedPreferences.Editor} object.
     */
    private SharedPreferences.Editor _getSharedPrefEditor() {
        if (_sharedPrefEditor == null) {
            _sharedPrefEditor = _getSharedPref().edit();
        }
        return _sharedPrefEditor;
    }

    public void setBrokerId(int type) {
        _getSharedPrefEditor().putInt(BROKER_ID, type).commit();
    }

    public int getBrokerId() {
        return _getSharedPref().getInt(BROKER_ID, -1);
    }

    public void setBrokerName(String type) {
        _getSharedPrefEditor().putString(BROKER_NAME, type).commit();
    }

    public String getBrokerName() {
        return _getSharedPref().getString(BROKER_NAME, null);
    }

    public void setBrokerMobile(String mobile) {
        _getSharedPrefEditor().putString(BROKER_MOBILE, mobile).commit();
    }

    public String getBrokerMobile() {
        return _getSharedPref().getString(BROKER_MOBILE, null);
    }

    public void setBrokerEmail(String email) {
        _getSharedPrefEditor().putString(BROKER_EMAIL, email).commit();
    }

    public String getBrokerEmail() {
        return _getSharedPref().getString(BROKER_EMAIL, null);
    }

    public void setBrokerPassword(String password) {
        _getSharedPrefEditor().putString(BROKER_PASSWORD, password).commit();
    }

    public String getBrokerPassword() {
        return _getSharedPref().getString(BROKER_PASSWORD, null);
    }

    public void setBrokerProfile(String url) {
        _getSharedPrefEditor().putString(BROKER_PiC_PROFILE, url).commit();
    }

    public String getBrokerProfile() {
        return _getSharedPref().getString(BROKER_PiC_PROFILE, null);
    }

    public void setBrokerGender(String brokerGender) {
        _getSharedPrefEditor().putString(BROKER_GENDER, brokerGender).commit();
    }

    public String getBrokerGender() {
        return _getSharedPref().getString(BROKER_GENDER, null);
    }


    public void setBrokerAge(String brokerAge) {
        _getSharedPrefEditor().putString(BROKER_AGE, brokerAge).commit();
    }

    public String getBROKERAge() {
        return _getSharedPref().getString(BROKER_AGE, null);
    }

    public void setBrokerAddress(String brokerAddress) {
        _getSharedPrefEditor().putString(BROKER_ADDRESS, brokerAddress).commit();
    }

    public String getBrokerAddress() {
        return _getSharedPref().getString(BROKER_ADDRESS, null);
    }

    public void setBROKER_DEALING_IN(String dealing_in) {
        _getSharedPrefEditor().putString(BROKER_DEALING_IN, dealing_in).commit();
    }

    public String getBROKER_DEALING_IN() {
        return _getSharedPref().getString(BROKER_DEALING_IN, null);
    }

    public void setBrokerWorkExperience(String brokerWorkExperience) {
        _getSharedPrefEditor().putString(BROKER_WORK_EXPERINCE, brokerWorkExperience).commit();
    }

    public String getBrokerWorkExperience() {
        return _getSharedPref().getString(BROKER_WORK_EXPERINCE, null);
    }

    public void setBrokerUOM(String uom) {
        _getSharedPrefEditor().putString(BROKER_UOM, uom).commit();
    }

    public String getBrokerUOM() {
        return _getSharedPref().getString(BROKER_UOM, null);
    }

    public void clearData() {
        setBrokerId(-1);
        setBrokerName(null);
        setBrokerMobile(null);
        setBrokerEmail(null);
        setBrokerPassword(null);
        setBrokerProfile(null);
        setBrokerAddress(null);
        setBrokerAge(null);
        setBrokerGender(null);
        setBrokerWorkExperience(null);
    }

    public void remove() {
        _getSharedPrefEditor().clear();
        _getSharedPrefEditor().commit();
    }


}
