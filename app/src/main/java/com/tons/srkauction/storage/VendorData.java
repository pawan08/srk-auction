package com.tons.srkauction.storage;


import android.content.Context;
import android.content.SharedPreferences;

import com.tons.srkauction.SrkAuction;


public class VendorData {

    private static VendorData _instance = null;
    private static SharedPreferences _sharedPreferences = null;
    private static SharedPreferences.Editor _sharedPrefEditor = null;
    private final String SHARED_PREFERENCE_NAME = "user_data";


    private final String VENDOR_ID = "vendor_id";
    private final String VENDOR_NAME = "vendor_name";
    private final String VENDOR_MOBILE = "vendor_mobile";
    private final String VENDOR_MOBILE_WHTSAPP = "vendor_mobile_whtsapp";

    private final String VENDOR_REQUEST_COMMODITY = "vendor_request_commodity";

    private final String VENDOR_EMAIL = "vendor_email";
    private final String VENDOR_PASSWORD = "vendor_password";
    private final String VENDOR_PiC_PROFILE = "vendor_pic_profile";
    private final String VENDOR_PiC_AADHAR = "vendor_pic_aadhar";
    private final String VENDOR_PiC_GST = "vendor_pic_gst";
    private final String VENDOR_PiC_PAN = "vendor_pic_pan";
    private final String VENDOR_PiC_MANDI = "vendor_pic_mandi_license";
    private final String VENDOR_ADDRESS = "vendor_addr";
    private final String VENDOR_BUSIESS_TYPE = "business_type";
    private final String COMPANY_NAME = "company_name";
    private final String COMPANY_ADDRESS = "company_addr";
    private final String PAN_CARD = "pan_card";
    private final String GST_NO = "gst_no";
    private final String MANDI_LICENSE_NUMBER = "mandi_license_no";
    private final String VENDOR_DEALING_IN = "vendor_dealing_in";
    private final String VENDOR_UOM = "vendor_uom";

    private VendorData() {

    }

    public static VendorData getInstance() {
        if (_instance == null) {
            _instance = new VendorData();
            _instance._initSharedPreferences();
        }
        return _instance;
    }

    /**
     * This method is used to initialized {@link SharedPreferences} and
     * {@link SharedPreferences.Editor}
     */
    public void _initSharedPreferences() {
        _sharedPreferences = _getSharedPref();
        _sharedPrefEditor = _getSharedPrefEditor();
    }

    /**
     * Method to get the SharedPreferences.
     *
     * @return the {@link SharedPreferences} object.
     */
    private SharedPreferences _getSharedPref() {
        if (_sharedPreferences == null) {
            _sharedPreferences = SrkAuction.appContext.getSharedPreferences(SHARED_PREFERENCE_NAME, Context.MODE_PRIVATE);
        }
        return _sharedPreferences;
    }

    /**
     * Method to get the {@link SharedPreferences.Editor} for writing values to {@link SharedPreferences}.
     *
     * @return the {@link SharedPreferences.Editor} object.
     */
    private SharedPreferences.Editor _getSharedPrefEditor() {
        if (_sharedPrefEditor == null) {
            _sharedPrefEditor = _getSharedPref().edit();
        }
        return _sharedPrefEditor;
    }

    public void setVendorId(int type) {
        _getSharedPrefEditor().putInt(VENDOR_ID, type).commit();
    }

    public int getVendorId() {
        return _getSharedPref().getInt(VENDOR_ID, -1);
    }

    public void setVendorName(String type) {
        _getSharedPrefEditor().putString(VENDOR_NAME, type).commit();
    }

    public String getVendorName() {
        return _getSharedPref().getString(VENDOR_NAME, null);
    }

    public void setVendorMobile(String mobile) {
        _getSharedPrefEditor().putString(VENDOR_MOBILE, mobile).commit();
    }

    public String getVendorMobile() {
        return _getSharedPref().getString(VENDOR_MOBILE, null);
    }


    public void setVENDOR_MOBILE_WHTSAPP(String whtsapp) {
        _getSharedPrefEditor().putString(VENDOR_MOBILE_WHTSAPP, whtsapp).commit();
    }

    public String getVENDOR_MOBILE_WHTSAPP() {
        return _getSharedPref().getString(VENDOR_MOBILE_WHTSAPP, null);
    }

    public void setVendorEmail(String email) {
        _getSharedPrefEditor().putString(VENDOR_EMAIL, email).commit();
    }

    public String getVendorEmail() {
        return _getSharedPref().getString(VENDOR_EMAIL, null);
    }

    public String getVENDOR_REQUEST_COMMODITY() {
        return _getSharedPref().getString(VENDOR_REQUEST_COMMODITY, null);
    }

    public String setVendorRequestCommoddity(String commodity) {
        return _getSharedPref().getString(VENDOR_REQUEST_COMMODITY, commodity);
    }


    public void setVendorPassword(String password) {
        _getSharedPrefEditor().putString(VENDOR_PASSWORD, password).commit();
    }

    public String getVendorPassword() {
        return _getSharedPref().getString(VENDOR_PASSWORD, null);
    }

    public void setVendorProfile(String url) {
        _getSharedPrefEditor().putString(VENDOR_PiC_PROFILE, url).commit();
    }

    public String getVendorProfile() {
        return _getSharedPref().getString(VENDOR_PiC_PROFILE, null);
    }


    public void setVendorAddress(String address) {
        _getSharedPrefEditor().putString(VENDOR_ADDRESS, address).commit();
    }

    public String getVendorAddress() {
        return _getSharedPref().getString(VENDOR_ADDRESS, null);
    }

    public void setVendorBusinessType(String vendorBusinessType) {
        _getSharedPrefEditor().putString(VENDOR_BUSIESS_TYPE, vendorBusinessType).commit();
    }

    public String getVendorBusinessType() {
        return _getSharedPref().getString(VENDOR_BUSIESS_TYPE, null);
    }


    public void setVendorCompanyName(String vendorCompanyName) {
        _getSharedPrefEditor().putString(COMPANY_NAME, vendorCompanyName).commit();
    }

    public String getVendorCompanyName() {
        return _getSharedPref().getString(COMPANY_NAME, null);
    }

    public void setVendorCompanyAddress(String vendorCompanyAddress) {
        _getSharedPrefEditor().putString(COMPANY_ADDRESS, vendorCompanyAddress).commit();
    }

    public String getVendorCompanyAddress() {
        return _getSharedPref().getString(COMPANY_ADDRESS, null);
    }

    public void setVendorPanCard(String vendorPanCard) {
        _getSharedPrefEditor().putString(PAN_CARD, vendorPanCard).commit();
    }

    public String getVendorPanCard() {
        return _getSharedPref().getString(PAN_CARD, null);
    }

    public void setGSTNO(String gst) {
        _getSharedPrefEditor().putString(GST_NO, gst).commit();
    }

    public String getGSTNO() {
        return _getSharedPref().getString(GST_NO, null);
    }


    public void setMandiLicenseNumber(String mandiLicenseNumber) {
        _getSharedPrefEditor().putString(MANDI_LICENSE_NUMBER, mandiLicenseNumber).commit();
    }

    public String getMandiLicenseNumber() {
        return _getSharedPref().getString(MANDI_LICENSE_NUMBER, null);
    }

    public void setVendorDealingIn(String vendorDealingIn) {
        _getSharedPrefEditor().putString(VENDOR_DEALING_IN, vendorDealingIn).commit();
    }

    public String getVendorDealingIn() {
        return _getSharedPref().getString(VENDOR_DEALING_IN, null);
    }

    public void setAadharPic(String aadhar_pic) {
        _getSharedPrefEditor().putString(VENDOR_PiC_AADHAR, aadhar_pic).commit();
    }

    public String getAadharPic() {
        return _getSharedPref().getString(VENDOR_PiC_AADHAR, null);
    }

    public void setPanpic(String url) {
        _getSharedPrefEditor().putString(VENDOR_PiC_PAN, url).commit();
    }

    public String getPanPic() {
        return _getSharedPref().getString(VENDOR_PiC_PAN, null);
    }

    public void setGSTPic(String gst) {
        _getSharedPrefEditor().putString(VENDOR_PiC_GST, gst).commit();
    }

    public String getGSTPic() {
        return _getSharedPref().getString(VENDOR_PiC_GST, null);
    }

    public void setMandiPic(String mandiPic) {
        _getSharedPrefEditor().putString(VENDOR_PiC_MANDI, mandiPic).commit();
    }

    public String getMandiPic() {
        return _getSharedPref().getString(VENDOR_PiC_MANDI, null);
    }

    public void setUOM(String uom) {
        _getSharedPrefEditor().putString(VENDOR_UOM, uom).commit();
    }

    public String getUOM() {
        return _getSharedPref().getString(VENDOR_UOM, null);
    }

    public void clearData() {
        setVendorId(-1);
        setVendorName(null);
        setVendorMobile(null);
        setVendorEmail(null);
        setVendorPassword(null);
        setVendorProfile(null);
        setVendorAddress(null);
        setVendorBusinessType(null);
        setVendorCompanyName(null);
        setVendorCompanyAddress(null);
        setVendorPanCard(null);
        setGSTNO(null);
        setMandiLicenseNumber(null);
        setVendorDealingIn(null);
    }

    public void remove() {
        _getSharedPrefEditor().clear();
        _getSharedPrefEditor().commit();
    }

}
