package com.tons.srkauction.view.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.DashboardSubAuction;
import com.tons.srkauction.model.OfflineAuctionModel;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.view.ScrHome;
import com.tons.srkauction.view.ScrOffflineAuctionDetails;

import java.util.ArrayList;

public class OfflineAuctionAdapter extends RecyclerView.Adapter<OfflineAuctionAdapter.ViewHolderBid> {

    private ArrayList<OfflineAuctionModel> list;
    private Context context;
    private ApiInterface apiInterface;

    public OfflineAuctionAdapter(Context context) {
        this.context = context;
    }

    public void setList(ArrayList<OfflineAuctionModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_offline_auction, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {

        OfflineAuctionModel offlineAuctionModel = list.get(i);
        holder.tv_auction_code.setText(offlineAuctionModel.getAuction_code());
        holder.tv_commodity.setText(offlineAuctionModel.getCommodity());
        holder.tv_price.setText(offlineAuctionModel.getPrice());
        holder.tv_qty.setText(offlineAuctionModel.getQuantity());

        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomIntent.startActivity((Activity) context, ScrOffflineAuctionDetails.class,false);
            }
        });
    }



    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder {

        private TextView tv_auction_code, tv_commodity, tv_qty, tv_price;
        private Button bt_view_details;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_commodity = itemView.findViewById(R.id.tv_commodity);
            tv_auction_code = itemView.findViewById(R.id.tv_auction_code);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_price = itemView.findViewById(R.id.tv_price);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
        }
    }
}
