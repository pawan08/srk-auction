package com.tons.srkauction.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.communication.volley_communication.VolleyMultipartRequest;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.communication.volley_communication.datapart.DataPart;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

public class ScrAddMoneyToWallet extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener {

    private ApiInterface apiInterface;
    private EditText et_amount, et_desc;
    private Button bt_submit;
    private LinearLayout ll_image;
    private ImageView iv_image;

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;
    private String x;
    private String amount;
    private String description;
    private Uri imageUri;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_add_money_to_wallet);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        initUI();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }

    public void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Add Money into wallet");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        et_amount = findViewById(R.id.et_amount);
        et_desc = findViewById(R.id.et_desc);
        bt_submit = findViewById(R.id.bt_submit);
        iv_image = findViewById(R.id.iv_image);
        ll_image = findViewById(R.id.ll_image);
        bt_submit.setOnClickListener(this);
        ll_image.setOnClickListener(this);
    }

    public void doValidate() {

        amount = et_amount.getText().toString();
        description = et_desc.getText().toString();


        if (bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select image", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(amount)) {
            MyCustomDialog.showValidationDialog("Please enter amount", "Oop's", this);
            return;
        }

        if (amount.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter amount greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(description)) {
            MyCustomDialog.showValidationDialog("Please enter description", "Oop's", this);
            return;
        }

        Map<String, String> params = new HashMap<>();
        params.put("wallet_vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("wallet_credit", amount);
        params.put("wallet_vendor_desc", description);
        call_add_money_api(params);


    }


    private void call_add_money_api(final Map<String, String> params) {


        final String url = CommunicationConstant.ADD_MONEY_INTO_WALLET;

        CustomDialog.showDialog(ScrAddMoneyToWallet.this, Constants.PROGRESS_MSG);

        VolleyMultipartRequest postRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        CustomDialog.closeDialog(ScrAddMoneyToWallet.this);
                        Log.d("TAG", "onResponse: call_add_money_api " + new String(response.data));
                        Log.d("TAG", "onResponse: call_add_money_api url " + url);
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            boolean status = jsonObject.getBoolean("status");
                            int message = jsonObject.getInt("message");
                            if (message == Messages.SUCCESS) {
                                CustomToast.showToast(ScrAddMoneyToWallet.this, "Request has been sent. Once approved it will reflect into your wallet.");
                                Intent rejected = new Intent(ScrAddMoneyToWallet.this, ScrPendingRequestTab.class);
                                rejected.putExtra("position", Constants.PENDING_EMD);
                                startActivity(rejected);
                                finish();
                            } else {
                                CustomToast.showToast(ScrAddMoneyToWallet.this, Constants.WENT_WRONG);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("TAG", "onResponse: call_add_money_api " + e.getMessage());
                        }

                        Log.d("REGISTER_RESPONSE", "call_add_money_api : " + response.data.toString());

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomDialog.closeDialog(ScrAddMoneyToWallet.this);
                        Log.d("TAG", String.valueOf(error));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("wallet_vendor_attachment", new DataPart("wallet_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        long lengthbmp = imageInByte.length;
        Log.d("SIZETAG", String.valueOf(lengthbmp));
        return byteArrayOutputStream.toByteArray();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_submit:
                doValidate();
                break;

            case R.id.ll_image:
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "image");
                break;
        }
    }

    private void launchCameraIntent1() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }

    @Override
    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                x = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchCameraIntent1();
                }
                break;

            case 2:
                x = "2";
                launchGalleryIntent();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: x = " + x);
                switch (x) {

                    case "1":
                        Log.d("TAG", "onActivityResult: inside case 1 ");
                        String url1 = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                            // url1 = getRealPathFromURI(imageUri1);
                            InputStream input2 = this.getContentResolver().openInputStream(imageUri);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(input2);
                                    int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: camera id proof ");
                            break;

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                            Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                        }
                        break;

                    case "2":
                        Log.d("TAG", "onActivityResult: inside case 2 ");
                        try {

                            imageUri = data.getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                            bitmap = BitmapFactory.decodeStream(imageStream);
                            ExifInterface exif3 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif3 = new ExifInterface(imageStream);
                                    int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: gallery id proof ");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrAddMoneyToWallet.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }
}