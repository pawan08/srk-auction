package com.tons.srkauction.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.communication.volley_communication.VolleyMultipartRequest;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.communication.volley_communication.datapart.DataPart;
import com.tons.srkauction.model.Broker;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrUpdateBrokerProfile extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener {
    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;

    private ArrayList<String> gender_list;
    private ArrayAdapter<String> gender_adapter;


    private EditText et_name;
    private EditText et_contact;
    private EditText et_email;
    private EditText et_age;
    private EditText et_address;
    private EditText et_work_experience;
    private EditText et_password;

    private AppCompatSpinner sp_gender;

    private Button bt_update_broker_profile;

    //  private RelativeLayout ll_profile_image;
    private CircleImageView civ_profile_image;
    private ImageView tv_profile_image;

    private Bitmap bitmap;
    private Uri imageUri;
    private String x;
    private SearchableSpinner sp_broker_dealing_in;
    private SearchableSpinner sp_uom;
    private ApiInterface apiInterface;

    String name, contact, email, age, address, work_experience, passwword, gender, getUOM;

    MultipartBody.Part profile_pic;

    private String broker_dealing_in = null;
    private String broker_profile_pic = null;
    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_update_broker_profile);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        initUI();

    }


    public void initUI() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Update Profile");

        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);

        et_name = findViewById(R.id.et_name);
        et_contact = findViewById(R.id.et_contact);
        et_email = findViewById(R.id.et_email);
        et_age = findViewById(R.id.et_age);
        et_address = findViewById(R.id.et_address);
        et_work_experience = findViewById(R.id.et_work_experience);
        et_password = findViewById(R.id.et_password);

        sp_gender = findViewById(R.id.sp_gender);
        sp_uom = findViewById(R.id.sp_uom);
        sp_broker_dealing_in = findViewById(R.id.sp_broker_dealing_in);

        bt_update_broker_profile = findViewById(R.id.bt_update_broker_profile);
        bt_update_broker_profile.setOnClickListener(this);

        /*ll_profile_image = findViewById(R.id.ll_profile_image);
        ll_profile_image.setOnClickListener(this);*/
        civ_profile_image = findViewById(R.id.civ_profile_image);
        civ_profile_image.setOnClickListener(this);
        tv_profile_image = findViewById(R.id.tv_profile_image);
        tv_profile_image.setOnClickListener(this);

        // called other init functions blow
        initGenderData();
        getBrokerProfileDetails();
        getBrokerDealingIn();
        initUom();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void getBrokerProfileDetails() {

        CustomDialog.showDialog(ScrUpdateBrokerProfile.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        Call<String> call = apiInterface.bro_profile_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                CustomDialog.closeDialog(ScrUpdateBrokerProfile.this);

                if (!response.isSuccessful()) {
                    return;
                }
                Log.d("TAG", "onResponse: getBrokerDetails : " + response.body());
                Log.d("TAG", "onResponse: getBrokerDetails url : " + call.request().url().toString());

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String broker_name = jsonObject1.getString("broker_name");
                            String broker_mobile = jsonObject1.getString("broker_mobile");
                            String broker_email = jsonObject1.getString("broker_email");
                            String broker_gender = jsonObject1.getString("broker_gender");
                            String broker_age = jsonObject1.getString("broker_age");
                            String broker_address = jsonObject1.getString("broker_address");
                            String broker_work_exp = jsonObject1.getString("broker_work_exp");
                            broker_dealing_in = jsonObject1.getString("broker_dealing_in");
                            broker_profile_pic = jsonObject1.getString("broker_profile_pic");
                            String uom = jsonObject1.getString("broker_uom");

                            sp_gender.setSelection(gender_adapter.getPosition(broker_gender));

                            int spinnerPosition = uom_adapter.getPosition(uom);
                            Log.d("TAG", "initUom: " + spinnerPosition);
                            Log.d("TAG", "initUom: " + uom);
                            sp_uom.setSelection(spinnerPosition);

                            if (broker_name.equals("") || broker_name.equals("null") || broker_name == null) {
                                et_name.setText("");
                                et_name.setHint("Enter Your Name");


                            } else {

                                et_name.setText(broker_name);
                            }

                            if (broker_mobile.equals("") || broker_mobile.equals("null") || broker_mobile == null) {
                                et_contact.setText("");
                                et_contact.setHint("Enter Mobile Number");


                            } else {
                                et_contact.setText(broker_mobile);
                            }


                            if (broker_email.equals("") || broker_email.equals("null") || broker_email == null) {
                                et_email.setText("");
                                et_email.setHint("Enter Email");

                            } else {
                                et_email.setText(broker_email);
                            }

                            if (broker_age.equals("") || broker_age.equals("null") || broker_age == null) {
                                et_age.setText("");
                                et_age.setHint("Enter Age");

                            } else {
                                et_age.setText(broker_age);
                            }


                            if (broker_address.equals("") || broker_address.equals("null") || broker_address == null) {
                                et_address.setText("");
                                et_address.setHint("Enter Address");

                            } else {
                                et_address.setText(broker_address);
                            }

                            if (broker_work_exp.equals("") || broker_work_exp.equals("null") || broker_work_exp == null) {
                                et_work_experience.setText("");
                                et_work_experience.setHint("Enter Work Experience");

                            } else {
                                et_work_experience.setText(broker_work_exp);

                            }


                            Picasso.with(ScrUpdateBrokerProfile.this).load(CommunicationConstant.BROKER_PROFILE + broker_profile_pic)
                                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .into(civ_profile_image);


                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void initGenderData() {
        gender_list = new ArrayList<>();
        gender_list.add(0, "Select Gender");
        gender_list.add(1, "Male");
        gender_list.add(2, "Female");
        gender_list.add(3, "Transgender");
        gender_adapter = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, R.id.tv_text, gender_list);
        sp_gender.setAdapter(gender_adapter);
        gender_adapter.notifyDataSetChanged();

    }

    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "1 KG");
        uom_list.add(2, "QUINTAL");
        uom_list.add(3, "METRIC TON");
        uom_list.add(4, "20 KG PRICE");

        uom_adapter = new ArrayAdapter<String>(ScrUpdateBrokerProfile.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);
        uom_adapter.notifyDataSetChanged();
    }

    public void getBrokerDealingIn() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("TAG", "onResponse: getBrokerDealingIn : " + response.body());
                Log.d("TAG", "onResponse: getBrokerDealingIn url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            ArrayAdapter<String> commodity_adapter = new ArrayAdapter<>(ScrUpdateBrokerProfile.this, android.R.layout.simple_list_item_1, CommodityList.getInstance().getNames());
                            sp_broker_dealing_in.setAdapter(commodity_adapter);

                            int spinnerItem = commodity_adapter.getPosition(broker_dealing_in);
                            sp_broker_dealing_in.setSelection(spinnerItem);
                            commodity_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    public void doValidate() {
        if (broker_profile_pic != null && !broker_profile_pic.equals("") && !broker_profile_pic.equals("null")) {
            civ_profile_image.invalidate();
            BitmapDrawable drawable1 = (BitmapDrawable) civ_profile_image.getDrawable();
            bitmap = drawable1.getBitmap();
        }

        name = et_name.getText().toString();
        contact = et_contact.getText().toString();
        email = et_email.getText().toString();
        age = et_age.getText().toString();
        address = et_address.getText().toString();
        work_experience = et_work_experience.getText().toString();
        gender = sp_gender.getSelectedItem().toString();
        if (TextUtils.isEmpty(name)) {
            MyCustomDialog.showValidationDialog("Please enter your name", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(contact)) {
            MyCustomDialog.showValidationDialog("Please enter your contact", "Oop's", this);
            return;
        } else if (!contact.matches("[0-9]{10}")) {
            MyCustomDialog.showValidationDialog("Contact number should have 10 digits.", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(email)) {
            MyCustomDialog.showValidationDialog("Please enter your email", "Oop's", this);
            return;
        } else if (!email.matches(String.valueOf(Patterns.EMAIL_ADDRESS))) {
            MyCustomDialog.showValidationDialog("Please enter valid email", "Oop's", this);
            return;
        }
        if (sp_gender.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select gender", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(age)) {
            MyCustomDialog.showValidationDialog("Please enter your age", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(address)) {
            MyCustomDialog.showValidationDialog("Please enter address", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(work_experience)) {
            MyCustomDialog.showValidationDialog("Please enter work experience", "Oop's", this);
            return;
        }
        if (sp_broker_dealing_in.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select dealing in", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }


        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        params.put("broker_name", name);
        params.put("broker_mobile", contact);
        params.put("broker_email", email);
        //params.put("broker_password", passwword);
        params.put("broker_gender", gender);
        params.put("broker_age", age);
        params.put("broker_address", address);
        params.put("broker_work_exp", work_experience);
        params.put("broker_dealing_in", sp_broker_dealing_in.getSelectedItem().toString());
        params.put("broker_uom", sp_uom.getSelectedItem().toString());
        call_Vendor_Update_Profile(params);
    }


    private void call_Vendor_Update_Profile(final Map<String, String> params) {

        final String url = CommunicationConstant.UPDATE_BROKER_PROFILE;

        CustomDialog.showDialog(ScrUpdateBrokerProfile.this, Constants.PROGRESS_MSG);

        VolleyMultipartRequest postRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        CustomDialog.closeDialog(ScrUpdateBrokerProfile.this);
                        Log.d("TAG", "onResponse: vendor profile " + new String(response.data));
                        Log.d("TAG", "onResponse: vendor profile url " + url);
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            boolean status = jsonObject.getBoolean("status");
                            int message = jsonObject.getInt("message");
                            if (message == Messages.SUCCESS) {

                                VendorData.getInstance().setVendorName(name);
                                VendorData.getInstance().setVendorEmail(email);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String broker_profile_pic = jsonObject1.getString("broker_profile_pic");
                                    BrokerData.getInstance().setBrokerProfile(broker_profile_pic);
                                }

                                CustomToast.showToast(ScrUpdateBrokerProfile.this, "Profile Updated Successfully");
                                /*CustomIntent.startActivity(ScrUpdateBrokerProfile.this, ScrHome.class);*/
                                finish();

                            } else {

                                CustomToast.showToast(ScrUpdateBrokerProfile.this, "Profile Update Failed");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("REGISTER_RESPONSE", response.data.toString());

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomDialog.closeDialog(ScrUpdateBrokerProfile.this);
                        Log.d("TAG", String.valueOf(error));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("broker_profile_pic", new DataPart("prfile_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        long lengthbmp = imageInByte.length;
        Log.d("SIZETAG", String.valueOf(lengthbmp));
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.civ_profile_image:
            case R.id.tv_profile_image:
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.bt_update_broker_profile:
                doValidate();
                break;


        }
    }

    @Override

    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                x = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchCameraIntent1();
                }
                break;

            case 2:
                x = "2";
                launchGalleryIntent();
                break;
        }
    }

    private void launchCameraIntent1() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: x = " + x);
                switch (x) {

                    case "1":
                        Log.d("TAG", "onActivityResult: inside case 1 ");
                        String url1 = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                            // url1 = getRealPathFromURI(imageUri1);
                            InputStream input2 = this.getContentResolver().openInputStream(imageUri);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(input2);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            assert exif2 != null;
                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                            civ_profile_image.setImageBitmap(bitmap);
                            Log.d("TAG", "onActivityResult: camera id proof ");
                            break;

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                            Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                        }
                        break;

                    case "2":
                        Log.d("TAG", "onActivityResult: inside case 2 ");
                        try {

                            imageUri = data.getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                            bitmap = BitmapFactory.decodeStream(imageStream);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(imageStream);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            assert exif2 != null;
                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                            bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                            civ_profile_image.setImageBitmap(bitmap);
                            Log.d("TAG", "onActivityResult: gallery id proof ");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrUpdateBrokerProfile.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }


}

