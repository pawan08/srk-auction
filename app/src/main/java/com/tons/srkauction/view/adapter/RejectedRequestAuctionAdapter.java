package com.tons.srkauction.view.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestAuction;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrEditBrokerAuctionRequest;
import com.tons.srkauction.view.ScrEditVendorAuctionRequest;
import com.tons.srkauction.view.ScrRequestAuctionDetails;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RejectedRequestAuctionAdapter extends RecyclerView.Adapter<RejectedRequestAuctionAdapter.ViewHolderBid> {


    private ApiInterface apiInterface;
    private ArrayList<RequestAuction> list;
    private Context context;

    public RejectedRequestAuctionAdapter(Context context) {
        this.context = context;

    }

    public void setList(ArrayList<RequestAuction> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_rejected_requested_auction, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        RequestAuction requestedAuction = list.get(i);
        holder.tv_auction_type.setText(requestedAuction.getAuction_type() + " Auction");
        holder.tv_auction_commomditity.setText(requestedAuction.getAuction_commodity());
        holder.tv_uom.setText(requestedAuction.getUom());
        holder.tv_qty.setText(requestedAuction.getQty());
        holder.tv_remark.setText(requestedAuction.getRequest_auction_remark());
        holder.tv_unit_price.setText("₹" + requestedAuction.getUnit_price());
        holder.tv_date.setText(DateToTimeStamp.getDate(requestedAuction.getDate()));

        final String req_auction = requestedAuction.getBroker_name();

        if (req_auction.contains("null")) {
            holder.ll_requested_by.setVisibility(View.GONE);
        } else {
            holder.ll_requested_by.setVisibility(View.VISIBLE);
            holder.tv_added_by.setText(requestedAuction.getBroker_name());

        }

        holder.cv_main.setTag(requestedAuction);
        holder.cv_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestAuction requestAuction = (RequestAuction) view.getTag();
                Intent intent = new Intent(context, ScrRequestAuctionDetails.class);
                intent.putExtra("auction_id", String.valueOf(requestAuction.getRequest_auction_id()));
                intent.putExtra("auction_id", String.valueOf(requestAuction.getRequest_auction_id()));
                context.startActivity(intent);
            }
        });



        holder.bt_delete.setTag(requestedAuction);
        holder.bt_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RequestAuction requestAuction = (RequestAuction) view.getTag();
                deleteDialog(requestAuction.getRequest_auction_id(),i);
            }
        });
    }



    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    private void deleteDialog(final int request_auction_id, final int position) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure you want to delete this auction?")
                .setTitle("Delete!")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        deleteAuctionRequest(request_auction_id,position);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    private void deleteAuctionRequest(int request_auction_id, final int position){

       // CustomDialog.showDialog(SrkAuction.appContext,"Please Wait..!");
        Map<String,String> params = new HashMap<>();
        params.put("request_auction_id",String.valueOf(request_auction_id));
        Call<String> call = apiInterface.delete_requested_auction(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
               // CustomDialog.closeDialog(context);
                Log.d("TAG", "onResponse: deleteAcution : " + response.body());
                Log.d("TAG", "onResponse: deleteAcution url : " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try{

                    JSONObject jsonObject1 = new JSONObject(response.body());
                    int message = jsonObject1.getInt("message");
                    if (message == Messages.SUCCESS){

                        CustomToast.showToast(context,"Auction Request Deleted Successfully");
                        list.remove(position);
                        notifyItemRemoved(position);
                        if (getItemCount() == 0) {
                           // context.showEmptyLayout();
                        }

                    }else{

                        CustomToast.showToast(context,"Failed To Delete Auction Request");
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
              //  CustomDialog.closeDialog(context);
                CustomToast.showToast(context, Constants.WENT_WRONG);
            }
        });
    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_date, tv_unit_price, tv_uom, tv_added_by, tv_qty,tv_remark;
        private TextView tv_auction_commomditity, tv_auction_type;
        private CardView cv_main;
        private LinearLayout ll_requested_by;
        private Button bt_edit, bt_delete;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_auction_type = itemView.findViewById(R.id.tv_auction_type);
            tv_auction_commomditity = itemView.findViewById(R.id.tv_auction_commomditity);
            tv_uom = itemView.findViewById(R.id.tv_uom);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_added_by = itemView.findViewById(R.id.tv_added_by);
            tv_qty = itemView.findViewById(R.id.tv_qty);
            tv_remark = itemView.findViewById(R.id.tv_remark);
            tv_date = itemView.findViewById(R.id.tv_date);
            cv_main = itemView.findViewById(R.id.cv_main);
            ll_requested_by = itemView.findViewById(R.id.ll_requested_by);
            bt_edit = itemView.findViewById(R.id.bt_edit);
            bt_delete = itemView.findViewById(R.id.bt_delete);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }


}
