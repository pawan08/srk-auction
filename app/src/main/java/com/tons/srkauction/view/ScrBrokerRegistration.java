package com.tons.srkauction.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrBrokerRegistration extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener {

    private String TAG = ScrBrokerRegistration.class.getName();
    private ApiInterface apiInterface;

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;

    private ArrayList<String> gender_list;
    private ArrayAdapter<String> gender_adapter;

    private ArrayAdapter<String> commodity_adapter;

    private EditText et_name;
    private EditText et_contact;
    private EditText et_email;
    private EditText et_age;
    private EditText et_address;
    private EditText et_work_experience;
    private EditText et_password;

    private AppCompatSpinner sp_gender;
    private SearchableSpinner sp_broker_dealing_in;

    private Button bt_submit;

    private LinearLayout ll_profile_image;
    private CircleImageView civ_profile_image;
    private TextView tv_profile_image;
    private SearchableSpinner sp_uom;
    private Bitmap bitmap;
    private Uri imageUri;
    private String x;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.cross_line));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_broker_registration);

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        initUI();
        getBrokerDealingIn();
    }

    public void initUI() {
        et_name = findViewById(R.id.et_name);
        et_contact = findViewById(R.id.et_contact);
        et_email = findViewById(R.id.et_email);
        et_age = findViewById(R.id.et_age);
        et_address = findViewById(R.id.et_address);
        et_work_experience = findViewById(R.id.et_work_experience);
        et_password = findViewById(R.id.et_password);

        sp_gender = findViewById(R.id.sp_gender);
        sp_uom = findViewById(R.id.sp_uom);
        sp_broker_dealing_in = findViewById(R.id.sp_broker_dealing_in);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        ll_profile_image = findViewById(R.id.ll_profile_image);
        ll_profile_image.setOnClickListener(this);
        civ_profile_image = findViewById(R.id.civ_profile_image);
        civ_profile_image.setOnClickListener(this);
        tv_profile_image = findViewById(R.id.tv_profile_image);
        tv_profile_image.setOnClickListener(this);

        // called other init functions blow
        initGenderData();
    }


    public void initGenderData() {
        gender_list = new ArrayList<>();
        gender_list.add(0, "Select Gender");
        gender_list.add(1, "Male");
        gender_list.add(2, "Female");
        gender_list.add(3, "Transgender");
        gender_adapter = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, R.id.tv_text, gender_list);
        sp_gender.setAdapter(gender_adapter);
        gender_adapter.notifyDataSetChanged();

    }

    public void getBrokerDealingIn() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getBrokerDealingIn : " + response.body());
                Log.d(TAG, "onResponse: getBrokerDealingIn url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            commodity_adapter = new ArrayAdapter<>(ScrBrokerRegistration.this, android.R.layout.simple_list_item_1, CommodityList.getInstance().getNames());
                            sp_broker_dealing_in.setAdapter(commodity_adapter);
                            commodity_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_login_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }

    public void doValidate() {
        String name = et_name.getText().toString();
        String contact = et_contact.getText().toString();
        String email = et_email.getText().toString();
        String age = et_age.getText().toString();
        String address = et_address.getText().toString();
        String work_experience = et_work_experience.getText().toString();
        String passwword = et_password.getText().toString();
        String broker_dealing_in = sp_broker_dealing_in.getSelectedItem().toString();
        String broker_uom = sp_uom.getSelectedItem().toString();

        String gender = sp_gender.getSelectedItem().toString();
        if (TextUtils.isEmpty(name)) {
            MyCustomDialog.showValidationDialog("Please enter your name", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(contact)) {
            MyCustomDialog.showValidationDialog("Please enter your contact", "Oop's", this);
            return;
        } else if (!contact.matches("[0-9]{10}")) {
            MyCustomDialog.showValidationDialog("Contact number should have 10 digits.", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(email)) {
            MyCustomDialog.showValidationDialog("Please enter your email", "Oop's", this);
            return;
        } else if (!email.matches(String.valueOf(Patterns.EMAIL_ADDRESS))) {
            MyCustomDialog.showValidationDialog("Please enter valid email", "Oop's", this);
            return;
        }
        if (sp_gender.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select gender", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(age)) {
            MyCustomDialog.showValidationDialog("Please enter your age", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(address)) {
            MyCustomDialog.showValidationDialog("Please enter address", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(work_experience)) {
            MyCustomDialog.showValidationDialog("Please enter work experience", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(passwword)) {
            MyCustomDialog.showValidationDialog("Please enter password", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(broker_dealing_in)) {
            MyCustomDialog.showValidationDialog("Please select broker dealing in", "Oop's", this);
            return;
        }

        if (bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select profile photo", "Oop's", this);
            return;
        }


        Map<String, String> params = new HashMap<>();
        params.put("broker_name", name);
        params.put("broker_mobile", contact);
        params.put("broker_email", email);
        params.put("broker_password", passwword);
        params.put("broker_gender", gender);
        params.put("broker_age", age);
        params.put("broker_address", address);
        params.put("broker_work_exp", work_experience);
        params.put("broker_dealing_in", broker_dealing_in);
        params.put("broker_uom", broker_uom);

        call_broker_registerAPI(params);
    }

    public void call_broker_registerAPI(Map<String, String> params) {
        CustomDialog.showDialog(ScrBrokerRegistration.this, "Please Wait");
        File file_profile = new File(getRealPathFromURI(imageUri));
        Log.d(TAG, "call_broker_registerAPI: file_profile : " + file_profile.getPath().toString());
        RequestBody broker_profile_pic = RequestBody.create(MediaType.parse("*/*"), file_profile);
        MultipartBody.Part profile_pic = MultipartBody.Part.createFormData("broker_profile_pic", file_profile.getName(), broker_profile_pic);
        Call<String> call = apiInterface.bro_register(params, profile_pic);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrBrokerRegistration.this);
                Log.d(TAG, "onResponse: call_broker_registerAPI ; " + response.body());
                Log.d(TAG, "onResponse: call_broker_registerAPI url ; " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject object = new JSONObject(response.body());
                    int message = object.getInt("message");
                    switch (message) {

                        case Messages.SUCCESS:
                            showSuccessDialog();
                            break;

                        case Messages.ALREADY_EXIST:
                            CustomToast.showToast(ScrBrokerRegistration.this, "User already registered with this contact");
                            break;

                        case Messages.FAILED:
                            CustomToast.showToast(ScrBrokerRegistration.this, "Failed to register");
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrBrokerRegistration.this);
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private String getRealPathFromURI(Uri contentUri) {   // This method can also be used to get the file name from uri
        String[] proj = {MediaStore.MediaColumns.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_profile_image:
            case R.id.civ_profile_image:
            case R.id.tv_profile_image:
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.bt_submit:
                doValidate();
                break;
        }
    }


    @Override

    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                x = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchCameraIntent1();
                }
                break;

            case 2:
                x = "2";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchGalleryIntent();
                }
                break;
        }
    }

    private void launchCameraIntent1() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: x = " + x);
                switch (x) {

                    case "1":
                        Log.d("TAG", "onActivityResult: inside case 1 ");
                        String url1 = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                            // url1 = getRealPathFromURI(imageUri1);
                            InputStream input2 = this.getContentResolver().openInputStream(imageUri);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(input2);
                                    int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    civ_profile_image.setImageBitmap(bitmap);
                                } else {
                                    civ_profile_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: camera id proof ");
                            break;

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                            Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                        }
                        break;

                    case "2":
                        Log.d("TAG", "onActivityResult: inside case 2 ");
                        try {

                            imageUri = data.getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                            bitmap = BitmapFactory.decodeStream(imageStream);
                            ExifInterface exif3 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif3 = new ExifInterface(imageStream);
                                    int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    civ_profile_image.setImageBitmap(bitmap);
                                } else {
                                    civ_profile_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: gallery id proof ");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrBrokerRegistration.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }
}