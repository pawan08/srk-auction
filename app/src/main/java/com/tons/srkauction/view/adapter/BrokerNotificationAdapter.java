package com.tons.srkauction.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.BrokerNotification;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrBrokerNotification;

import java.util.ArrayList;

public class BrokerNotificationAdapter extends RecyclerView.Adapter<BrokerNotificationAdapter.ViewHolderBid> {


    private ArrayList<BrokerNotification> list;
    private ScrBrokerNotification context;

    public BrokerNotificationAdapter(ScrBrokerNotification context) {
        this.context = context;

    }

    public void setList(ArrayList<BrokerNotification> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_broker_notification, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        BrokerNotification brokerNotification = list.get(i);
        holder.tv_notification_title.setText(brokerNotification.getNb_title());
        holder.tv_des.setText(brokerNotification.getNb_desc());
        holder.tv_time.setText(DateToTimeStamp.getDateForNotification(brokerNotification.getNb_added_time()));

    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_notification_title, tv_des, tv_show_more_less;
        private TextView tv_time;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_notification_title = itemView.findViewById(R.id.tv_notification_title);
            tv_des = itemView.findViewById(R.id.text_view);
            tv_time = itemView.findViewById(R.id.tv_time);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
