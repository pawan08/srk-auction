package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.Vendor;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;


import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.Vendor;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrVendorProfile extends AppCompatActivity implements View.OnClickListener {

    private CircleImageView civ_vendor_profile_image;
    private TextView tv_vendor_name, tv_uom,tv_vendor_mobile, tv_vendor_email, tv_vendor_whts_app_number, tv_vendor_req_commodity, tv_vendor_address, tv_vendor_b_type;
    private TextView tv_vendor_comp_name, tv_vendor_comp_address, tv_vendor_pan, tv_vendor_gst, tv_vendor_mandi_no, tv_vendor_dealing_in,tv_state;
    private ImageView civ_aadhar_card;
    private ImageView civ_pan_card;
    private ImageView civ_gst_card;
    private ImageView civ_mandilicense_card;
    private Button bt_edit_profile;
    ApiInterface apiInterface;

    private LinearLayout ll_root;
    private LinearLayout ll_mandi_pic;
    private LinearLayout ll_gst;
    private TextView tv_account_no,tv_branch_name,tv_bank_name,tv_ifsc_code;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
                //  getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_vendor_profile);
        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getVendorProfileDetails();
    }

    public void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Profile");

        civ_vendor_profile_image = findViewById(R.id.civ_vendor_profile_image);
        civ_aadhar_card = findViewById(R.id.civ_aadhar_card);
        civ_pan_card = findViewById(R.id.civ_pan_card);
        civ_gst_card = findViewById(R.id.civ_gst_card);
        civ_mandilicense_card = findViewById(R.id.civ_mandilicense_card);

        bt_edit_profile = findViewById(R.id.bt_edit_profile);
        bt_edit_profile.setOnClickListener(this);

        tv_state = findViewById(R.id.tv_state);
        tv_vendor_name = findViewById(R.id.tv_vendor_name);
        tv_vendor_mobile = findViewById(R.id.tv_vendor_mobile);
        tv_vendor_email = findViewById(R.id.tv_vendor_email);
        tv_vendor_address = findViewById(R.id.tv_vendor_address);
        tv_vendor_b_type = findViewById(R.id.tv_vendor_b_type);
        tv_vendor_comp_name = findViewById(R.id.tv_vendor_comp_name);
        tv_vendor_comp_address = findViewById(R.id.tv_vendor_comp_address);
        tv_vendor_comp_address = findViewById(R.id.tv_vendor_comp_address);
        tv_vendor_pan = findViewById(R.id.tv_vendor_pan);
        tv_vendor_gst = findViewById(R.id.tv_vendor_gst);
        tv_vendor_mandi_no = findViewById(R.id.tv_vendor_mandi_no);
        tv_vendor_dealing_in = findViewById(R.id.tv_vendor_dealing_in);
        tv_vendor_whts_app_number = findViewById(R.id.tv_vendor_whts_app_number);
        tv_vendor_req_commodity = findViewById(R.id.tv_vendor_req_commodity);
        tv_uom = findViewById(R.id.tv_uom);
        tv_branch_name = findViewById(R.id.tv_branch_name);
        tv_bank_name = findViewById(R.id.tv_bank_name);
        tv_account_no = findViewById(R.id.tv_account_no);
        tv_ifsc_code = findViewById(R.id.tv_ifsc_code);
        ll_root = findViewById(R.id.ll_root);
        ll_gst = findViewById(R.id.ll_gst);
        ll_mandi_pic = findViewById(R.id.ll_mandi_pic);


        civ_vendor_profile_image.setOnClickListener(this);
        civ_aadhar_card.setOnClickListener(this);
        civ_pan_card.setOnClickListener(this);
    }


    public void getVendorProfileDetails() {

        CustomDialog.showDialog(ScrVendorProfile.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        //  params.put("vendor_id","6");
        Call<String> call = apiInterface.ven_profile_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrVendorProfile.this);

                Log.d("TAG", "onResponse: getVendorDetails : " + response.body());
                Log.d("TAG", "onResponse: getVendorDetails url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        ll_root.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String vendor_name = jsonObject1.getString("vendor_name");
                            String vendor_mobile = jsonObject1.getString("vendor_mobile");
                            String vendor_email = jsonObject1.getString("vendor_email");
                            String vendor_address = jsonObject1.getString("vendor_address");
                            String vendor_business_type = jsonObject1.getString("vendor_business_type");
                            String vendor_company_shop_name = jsonObject1.getString("vendor_company_shop_name");
                            String vendor_company_shop_address = jsonObject1.getString("vendor_company_shop_address");
                            String vendor_pan_shopact = jsonObject1.getString("vendor_pan_shopact");
                            String vendor_gst = jsonObject1.getString("vendor_gst");
                            String vendor_mandi = jsonObject1.getString("vendor_mandi");
                            String vendor_dealing_in = jsonObject1.getString("vendor_dealing_in");
                            final String vendor_pic_aadhar = jsonObject1.getString("vendor_pic_aadhar");
                            final String vendor_pic_pan = jsonObject1.getString("vendor_pic_pan");
                            final String vendor_pic_gst = jsonObject1.getString("vendor_pic_gst");
                            final String vendor_pic_mandi_license = jsonObject1.getString("vendor_pic_mandi_license");
                            final String vendor_pic_profile = jsonObject1.getString("vendor_pic_profile");
                            String vendor_ref_name = jsonObject1.getString("vendor_ref_name");
                            String vendor_ref_mobile = jsonObject1.getString("vendor_ref_mobile");
                            String vendor_ref_address = jsonObject1.getString("vendor_ref_address");
                            String state = jsonObject1.getString("vendor_state");
                            String uom = jsonObject1.getString("vendor_uom");
                            String bank_name = jsonObject1.getString("vendor_bank_name");
                            String branch_name = jsonObject1.getString("vendor_branch_name");
                            String acc_no = jsonObject1.getString("vendor_ac_no");
                            String ifsc_code = jsonObject1.getString("vendor_ifsc_code");

                            String whatsappnumber = jsonObject1.getString("vendor_whatsapp_no");
                            String requestedcommodity = jsonObject1.getString("vendor_requested_commodity");


                            tv_bank_name.setText(bank_name);
                            tv_branch_name.setText(branch_name);
                            tv_account_no.setText(acc_no);
                            tv_ifsc_code.setText(ifsc_code);


                            if (vendor_mandi.equals("") || vendor_mandi.equals("null") || vendor_mandi == null) {
                                tv_vendor_mandi_no.setText("-");

                            } else {
                                tv_vendor_mandi_no.setText(vendor_mandi);
                            }


                            if (uom.equals("") || uom.equals("null") || uom == null) {
                                tv_uom.setText("-");

                            } else {
                                tv_uom.setText(uom);
                            }


                            if (state.equals("") || state.equals("null") || state == null) {
                                tv_state.setText("-");
                            } else {
                                tv_state.setText(state);
                            }

                            if (whatsappnumber.equals("") || whatsappnumber.equals("null") || whatsappnumber == null) {
                                tv_vendor_whts_app_number.setText("-");

                            } else {
                                tv_vendor_whts_app_number.setText(whatsappnumber);
                            }

                            if (requestedcommodity.equals("") || requestedcommodity.equals("null") || requestedcommodity == null) {
                                tv_vendor_req_commodity.setText("-");
                            } else {
                                tv_vendor_req_commodity.setText(requestedcommodity);
                            }

                            if (vendor_name.equals("") || vendor_name.equals("null") || vendor_name == null) {
                                tv_vendor_name.setText("-");
                            } else {
                                tv_vendor_name.setText(vendor_name);
                            }

                            if (vendor_mobile.equals("") || vendor_mobile.equals("null") || vendor_mobile == null) {
                                tv_vendor_mobile.setText("-");
                            } else {
                                tv_vendor_mobile.setText(vendor_mobile);
                            }


                            if (vendor_email.equals("") || vendor_email.equals("null") || vendor_email == null) {
                                tv_vendor_email.setText("-");
                            } else {
                                tv_vendor_email.setText(vendor_email);
                            }

                            if (vendor_address.equals("") || vendor_address.equals("null") || vendor_address == null) {
                                tv_vendor_address.setText("-");
                            } else {
                                tv_vendor_address.setText(vendor_address);
                            }

                            if (vendor_business_type.equals("") || vendor_business_type.equals("null") || vendor_business_type == null) {
                                tv_vendor_b_type.setText("-");
                            } else {
                                tv_vendor_b_type.setText(vendor_business_type);
                            }


                            if (vendor_company_shop_name.equals("") || vendor_company_shop_name.equals("null") || vendor_company_shop_name == null) {
                                tv_vendor_comp_name.setText("-");
                            } else {
                                tv_vendor_comp_name.setText(vendor_company_shop_name);
                            }

                            if (vendor_company_shop_address.equals("") || vendor_company_shop_address.equals("null") || vendor_company_shop_address == null) {
                                tv_vendor_comp_address.setText("-");
                            } else {
                                tv_vendor_comp_address.setText(vendor_company_shop_address);
                            }


                            if (vendor_pan_shopact.equals("") || vendor_pan_shopact.equals("null") || vendor_pan_shopact == null) {
                                tv_vendor_pan.setText("-");
                            } else {
                                tv_vendor_pan.setText(vendor_pan_shopact);
                            }

                            if (vendor_gst.equals("") || vendor_gst.equals("null") || vendor_gst == null) {
                                tv_vendor_gst.setText("-");
                            } else {
                                tv_vendor_gst.setText(vendor_gst);
                            }

                            if (vendor_dealing_in.equals("") || vendor_dealing_in.equals("null") || vendor_dealing_in == null) {
                                tv_vendor_dealing_in.setText("-");
                            } else {
                                tv_vendor_dealing_in.setText(vendor_dealing_in);
                            }


                            Log.d("TAG", "onResponse: Url " + CommunicationConstant.VENDOR_PAN + vendor_pic_pan);

                            Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_AADHAR + vendor_pic_aadhar)
                                    .placeholder(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_aadhar_card, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("TAG", "onSuccess: " + "Success");
                                            civ_aadhar_card.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(ScrVendorProfile.this, ScrViewProfileImage.class);
                                                    intent.putExtra("image", CommunicationConstant.VENDOR_AADHAR + vendor_pic_aadhar);
                                                    startActivity(intent);
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d("TAG", "onError: " + "Failed to load the data");
                                            ll_gst.setVisibility(View.GONE);
                                        }
                                    });

                            Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_PAN + vendor_pic_pan)
                                    .placeholder(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_pan_card, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("TAG", "onSuccess: " + "Success");
                                            ll_gst.setVisibility(View.VISIBLE);
                                            civ_pan_card.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(ScrVendorProfile.this, ScrViewProfileImage.class);
                                                    intent.putExtra("image", CommunicationConstant.VENDOR_PAN + vendor_pic_pan);
                                                    startActivity(intent);
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d("TAG", "onError: " + "Failed to load the data");
                                            ll_gst.setVisibility(View.GONE);
                                        }
                                    });

                            Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_GST + vendor_pic_gst)
                                    .placeholder(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_gst_card, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("TAG", "onSuccess: " + "Success");
                                            ll_gst.setVisibility(View.VISIBLE);
                                            civ_gst_card.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(ScrVendorProfile.this, ScrViewProfileImage.class);
                                                    intent.putExtra("image", CommunicationConstant.VENDOR_GST + vendor_pic_gst);
                                                    startActivity(intent);
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d("TAG", "onError: " + "Failed to load the data");
                                            ll_gst.setVisibility(View.GONE);
                                        }
                                    });


                            Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_MANDI_LICENSE + vendor_pic_mandi_license)
                                    .placeholder(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_mandilicense_card, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("TAG", "onSuccess: " + "Success");
                                            ll_mandi_pic.setVisibility(View.VISIBLE);
                                            civ_mandilicense_card.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(ScrVendorProfile.this, ScrViewProfileImage.class);
                                                    intent.putExtra("image", CommunicationConstant.VENDOR_MANDI_LICENSE + vendor_pic_mandi_license);
                                                    startActivity(intent);
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d("TAG", "onError: " + "Failed to load the data");
                                            ll_mandi_pic.setVisibility(View.GONE);
                                        }
                                    });

                            // Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_MANDI_LICENSE+vendor_pic_mandi_license).into(civ_mandilicense_card);

                            VendorData.getInstance().setVendorProfile(vendor_pic_profile);
                            Picasso.with(ScrVendorProfile.this).load(CommunicationConstant.VENDOR_PROFILE + vendor_pic_profile)
                                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .into(civ_vendor_profile_image, new com.squareup.picasso.Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Log.d("TAG", "onSuccess: " + "Success");
                                            civ_vendor_profile_image.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(ScrVendorProfile.this, ScrViewProfileImage.class);
                                                    intent.putExtra("image", CommunicationConstant.VENDOR_PROFILE + vendor_pic_profile);
                                                    startActivity(intent);
                                                }
                                            });

                                        }

                                        @Override
                                        public void onError() {
                                            Log.d("TAG", "onError: " + "Failed to load the data");
                                        }
                                    });
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.bt_edit_profile:
                CustomIntent.startActivity(ScrVendorProfile.this, ScrVendorUpdateProfile.class, false);
                break;
        }

    }
}