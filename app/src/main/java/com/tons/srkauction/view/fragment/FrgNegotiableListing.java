package com.tons.srkauction.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.OfflineAuctionModel;
import com.tons.srkauction.model.PendingWalletEMD;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.view.adapter.LiveAuctionAdapter;
import com.tons.srkauction.view.adapter.OfflineAuctionAdapter;
import com.tons.srkauction.view.adapter.PendingWalletEMDAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgNegotiableListing extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_negotiable;
    private OfflineAuctionAdapter offlineAuctionAdapter;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout ll_no_data;
    private ArrayList<OfflineAuctionModel> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frg_negotiable_listing, null);
        inItui();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return view;
    }

    private void inItui() {

        rv_negotiable = view.findViewById(R.id.rv_negotiable);
        offlineAuctionAdapter = new OfflineAuctionAdapter(getActivity());
        rv_negotiable.setAdapter(offlineAuctionAdapter);
        rv_negotiable.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        ll_no_data = view.findViewById(R.id.ll_no_data);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);
        list.add(new OfflineAuctionModel("AUC1234","Wheat","500","25"));
        list.add(new OfflineAuctionModel("AUC1224","Rice","50","50"));
        offlineAuctionAdapter.setList(list);
        offlineAuctionAdapter.notifyDataSetChanged();


    }

    public void showEmptyLayout() {
        if (offlineAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    private void call_get_negotiable_lis() {
        swipe_refresh_layout.setRefreshing(true);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_pending_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Get Pending List" + response.body());
                Log.d("TAG", "Get Pending List url " + call.request().url().toString());

                list.clear();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetPendingWallet" + response.body());
                    Log.d("TAG", "GetPendingWallet url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                           /* int wallet_id = jsonObject1.getInt("wallet_id");
                            String wallet_credit = jsonObject1.getString("wallet_credit");
                            String wallet_debit = jsonObject1.getString("wallet_debit");
                            String wallet_added_time = jsonObject1.getString("wallet_added_time");
                            int reqsub_auc_id = jsonObject1.getInt("reqsub_auc_id");
                            String reqsub_auc_code = jsonObject1.getString("reqsub_auc_code");
                            OfflineAuctionModel offlineAuctionModel = new OfflineAuctionModel(wallet_id, wallet_vendor_id, wallet_credit, wallet_debit, wallet_added_time, reqsub_auc_id, reqsub_auc_code);
                            list.add(offlineAuctionModel);*/

                        }

                    }
                    swipe_refresh_layout.setRefreshing(false);
                    offlineAuctionAdapter.setList(list);
                    offlineAuctionAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onRefresh() {

       // call_get_negotiable_lis();
    }
}
