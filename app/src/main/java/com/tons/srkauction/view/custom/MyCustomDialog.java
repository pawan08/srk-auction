package com.tons.srkauction.view.custom;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.tons.srkauction.R;

public class MyCustomDialog {

    public static void showValidationDialog(String message, final String title, Context context) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dlg_validation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText(title + "");
        tv_text.setText(message + "");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Okay");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();

    }
}
