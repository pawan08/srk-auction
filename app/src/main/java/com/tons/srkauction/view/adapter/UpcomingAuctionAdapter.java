package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.UpcomingAuction;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrAuctionDetails;
import com.tons.srkauction.view.fragment.FrgAuctionUpcoming;

import java.util.ArrayList;

public class UpcomingAuctionAdapter extends RecyclerView.Adapter<UpcomingAuctionAdapter.ViewHolderBid> {

    private ArrayList<UpcomingAuction> list;
    private FrgAuctionUpcoming context;

    public UpcomingAuctionAdapter(FrgAuctionUpcoming context) {
        this.context = context;

    }

    public void setList(ArrayList<UpcomingAuction> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_auction2, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        UpcomingAuction upcomingAuction = list.get(i);
        holder.tv_auction_type.setText(upcomingAuction.getAuction_type());
        holder.tv_auction_commomditity.setText(upcomingAuction.getAuction_code());
        holder.tv_uom.setText("Commodity : " + upcomingAuction.getAuction_commodity());
        holder.tv_date.setText(DateToTimeStamp.getDate(upcomingAuction.getDate())+" "+DateToTimeStamp.getTime(upcomingAuction.getAuction_start_time()+":00"));

        holder.bt_view_details.setTag(upcomingAuction);
        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                UpcomingAuction upcomingAuction1 =(UpcomingAuction) view.getTag();
                Intent intent=new Intent(context.getActivity(), ScrAuctionDetails.class);
                intent.putExtra("auction_id", String.valueOf(upcomingAuction1.getAuction_id()));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public void filteredList(ArrayList<UpcomingAuction> filteredList)
    {
        list = filteredList;
        notifyDataSetChanged();
    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_date, tv_unit_price, tv_uom;
        private TextView tv_auction_commomditity, tv_auction_type;
        private CardView cv_main;
        private Button bt_view_details;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_auction_type = itemView.findViewById(R.id.tv_auction_type);
            tv_auction_commomditity = itemView.findViewById(R.id.tv_auction_commomditity);
            tv_uom = itemView.findViewById(R.id.tv_uom);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            cv_main = itemView.findViewById(R.id.cv_main);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
