package com.tons.srkauction.view;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.view.adapter.AuctionRequestTabAdapter;
import com.tons.srkauction.view.adapter.PendingRequestTabAdapter;

public class ScrRequestAuctionTab extends AppCompatActivity {

    int postiion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_pending_request_tab);
        ViewPager viewPager = findViewById(R.id.view_pager_tab);
        AuctionRequestTabAdapter pendingRequestTabAdapter = new AuctionRequestTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pendingRequestTabAdapter);
        viewPager.setOffscreenPageLimit(2);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            postiion = b.getInt("position");
            Log.d("TAG", "onCreate:position if" + postiion);
            switch (postiion) {

                case Constants.AUCTION_APPROVED:
                    viewPager.setCurrentItem(0);
                    break;

                case Constants.AUCTION_PENDING:
                    viewPager.setCurrentItem(1);
                    break;

                case Constants.AUCTION_REJECTED:
                    viewPager.setCurrentItem(2);
                    break;
            }
        } else {
            Log.d("TAG", "onCreate:position else" + postiion);
            viewPager.setCurrentItem(0);
        }

        TabLayout tabLayout = findViewById(R.id.pending_request_tab);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTaskRoot()) {
                    finish();
                    CustomIntent.startActivity(ScrRequestAuctionTab.this, ScrHome.class, true);
                } else {
                    ScrRequestAuctionTab.super.onBackPressed();
                }
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Requested Auctions");
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (isTaskRoot()) {
            finish();
            CustomIntent.startActivity(ScrRequestAuctionTab.this, ScrHome.class, true);
        } else {
            ScrRequestAuctionTab.super.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            finish();
            CustomIntent.startActivity(ScrRequestAuctionTab.this, ScrHome.class, true);
        } else {
            super.onBackPressed();
        }
    }


}
