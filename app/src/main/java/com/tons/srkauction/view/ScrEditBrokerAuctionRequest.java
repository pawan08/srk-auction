package com.tons.srkauction.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.model.Vendor;
import com.tons.srkauction.model.VendorList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrEditBrokerAuctionRequest extends AppCompatActivity implements View.OnClickListener {

    private String TAG = ScrEditBrokerAuctionRequest.class.getName();

    private ArrayList<String> auction_type_list;
    private ArrayAdapter<String> auction_type_adapter;

    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    private ArrayList<String> auction_commodity_list;
    private ArrayAdapter<String> auction_commodity_adapter;

    private String auction_id;
    private String auction_type;
    private String commodity;
    private String uom;

    //private AppCompatSpinner sp_auction_type;
    private SearchableSpinner sp_auction_commodity;
    private SearchableSpinner sp_uom;
    private AppCompatSpinner sp_vendor;
    private EditText et_unit_price;
    private EditText et_location;
    private EditText et_commodity_desc;
    private EditText et_qty;
    private Button bt_submit;

    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_edit_broker_auction_request);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        initUi();

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            auction_id = bundle.getString("auction_id");
        }

        getAuctionRequestDetails();
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Edit Requested Auction");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sp_uom = findViewById(R.id.sp_uom);
        sp_vendor = findViewById(R.id.sp_vendor);
        et_qty = findViewById(R.id.et_qty);
        et_unit_price = findViewById(R.id.et_unit_price);
        et_location = findViewById(R.id.et_location);
        et_commodity_desc = findViewById(R.id.et_commodity_desc);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        sp_auction_commodity = findViewById(R.id.sp_auction_commodity);
        //sp_auction_type = findViewById(R.id.sp_auction_type);
    }

    public void getAuctionRequestDetails() {

        CustomDialog.showDialog(ScrEditBrokerAuctionRequest.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("request_auction_id", String.valueOf(auction_id));
        Call<String> call = apiInterface.get_requested_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrEditBrokerAuctionRequest.this);
                Log.d("TAG", "Get Auction Details" + response.body());
                Log.d("TAG", "Get_Auction_Details url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            auction_type = jsonObject1.getString("request_auction_type");
                            commodity = jsonObject1.getString("request_auction_commodity");
                            uom = jsonObject1.getString("request_auction_uom");

                            String commodity_des = jsonObject1.getString("request_auction_commodity_desc");
                            String location = jsonObject1.getString("request_auction_pickup_location");
                            String price = jsonObject1.getString("request_auction_unit_price");
                            String qty = jsonObject1.getString("request_auction_qty");
                            String vendor_name = jsonObject1.getString("vendor_name");

                            if (!qty.equals("null") && !qty.equals("")) {
                                et_qty.setText(qty);
                            }
                            if (!price.equals("null") && !price.equals("")) {
                                et_unit_price.setText(price);
                            }
                            if (!location.equals("null") && !location.equals("")) {
                                et_location.setText(location);
                            }
                            if (!commodity_des.equals("null") && !commodity_des.equals("")) {
                                et_commodity_desc.setText(commodity_des);
                            }

                            initUom();
                            initVendorList(vendor_name);
                            initAuctioncommodity(commodity);
                        }
                    } else {
                        Log.d("TAG", "onResponse: " + Constants.FETCH_ERROR);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrEditBrokerAuctionRequest.this);
                Log.d("TAG", "onResponse: " + Constants.WENT_WRONG);
            }
        });
    }

    public void initVendorList(final String vendor_name_selected) {

        Call<String> call = apiInterface.ven_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getVendorList : " + response.body());
                Log.d(TAG, "onResponse: getVendorList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    VendorList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int vendor_id = jsonObject1.getInt("vendor_id");
                            String vendor_name = jsonObject1.getString("vendor_name");
                            Vendor Vendor = new Vendor(vendor_id, vendor_name);
                            VendorList.getInstance().add(Vendor);
                            ArrayAdapter<String> vendor_adapter = new ArrayAdapter<>(ScrEditBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, VendorList.getInstance().getNames());
                            sp_vendor.setAdapter(vendor_adapter);
                            vendor_adapter.notifyDataSetChanged();

                            int spinnerposition = vendor_adapter.getPosition(vendor_name_selected);
                            sp_vendor.setSelection(spinnerposition);

                            Log.d(TAG, "onResponse: " + String.valueOf(VendorList.getInstance().getVendorsID(vendor_name)));
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

   /* private void initAuctiontype() {

        auction_type_list = new ArrayList<>();
        auction_type_list.add(0, "Select auction type");
        auction_type_list.add(1, "Forward");
        auction_type_list.add(2, "Reverse");


        auction_type_adapter = new ArrayAdapter<String>(ScrEditBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, auction_type_list);
        sp_auction_type.setAdapter(auction_type_adapter);
        auction_type_adapter.notifyDataSetChanged();

        try {
            if (auction_type.equals("F")) {

                Intent intent = getIntent();
                intent.getStringExtra("auction_type");
                int spinnerPosition = auction_type_adapter.getPosition("Forward");
                sp_auction_type.setSelection(spinnerPosition);
            } else {

                Intent intent = getIntent();
                intent.getStringExtra("auction_type");
                int spinnerPosition = auction_type_adapter.getPosition("Reverse");

                sp_auction_type.setSelection(spinnerPosition);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "initAuctiontype: null pointer Exception : " + e.getMessage());
        }

    }*/


    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "KG");
        uom_list.add(2, "LTR");

        uom_adapter = new ArrayAdapter<String>(ScrEditBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);
        uom_adapter.notifyDataSetChanged();

        Log.d(TAG, "initUom: " + uom);

       /* Intent intent = getIntent();
        intent.getStringExtra("auction_uom");*/
        int spinnerPosition = uom_adapter.getPosition(uom);
        sp_uom.setSelection(spinnerPosition);
        Log.d("TAG", "onResponsenewumo: " + spinnerPosition);


    }


    public void initAuctioncommodity(final String commodity) {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getCommodityList : " + response.body());
                Log.d(TAG, "onResponse: getCommodityList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();

                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);

                            //   auction_commodity_list.add(commodity_name);


                            auction_commodity_adapter = new ArrayAdapter<String>(ScrEditBrokerAuctionRequest.this, android.R.layout.simple_spinner_item, CommodityList.getInstance().getCommodityNames());
                            sp_auction_commodity.setAdapter(auction_commodity_adapter);
                            auction_commodity_adapter.notifyDataSetChanged();


                            Intent intent = getIntent();
                            intent.getStringExtra("auction_commodity");
                            //  String auction_commodity = String.valueOf(getIntent());
                            int spinnerPosition = auction_commodity_adapter.getPosition(commodity);
                            sp_auction_commodity.setSelection(spinnerPosition);
                            Log.d("TAG", "onResponsenew: " + spinnerPosition);


                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }


    private void validate() {
        String unit_price = et_unit_price.getText().toString();
        String qty = et_qty.getText().toString();
        String location = et_location.getText().toString();
        String et_com_desc = et_commodity_desc.getText().toString();


        if (sp_auction_commodity.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select auction commodity", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(qty)) {
            MyCustomDialog.showValidationDialog("Please enter quantity", "Oop's", this);
            return;
        }

        if (qty.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter quantity greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(unit_price)) {
            MyCustomDialog.showValidationDialog("Please enter unit price", "Oop's", this);
            return;
        }

        if (unit_price.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter unit price greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(location)) {
            MyCustomDialog.showValidationDialog("Please enter pick up location", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(et_com_desc)) {
            MyCustomDialog.showValidationDialog("Please enter commodity description", "Oop's", this);
            return;
        }


        updateAuctionRequest();

    }

    public void updateAuctionRequest() {

        CustomDialog.showDialog(ScrEditBrokerAuctionRequest.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();

        params.put("request_auction_id", auction_id);
        params.put("request_auction_type", "FORWARD");
        params.put("request_auction_commodity", sp_auction_commodity.getSelectedItem().toString());
        params.put("request_auction_uom", sp_uom.getSelectedItem().toString());
        params.put("request_auction_commodity_desc", et_commodity_desc.getText().toString());
        params.put("request_auction_qty", et_qty.getText().toString());
        params.put("request_auction_unit_price", et_unit_price.getText().toString());
        params.put("request_auction_pickup_location", et_location.getText().toString());
        params.put("request_auction_vendor", String.valueOf(VendorList.getInstance().getVendorsID(sp_vendor.getSelectedItem().toString())));

        Log.d(TAG, "updateAuctionRequest: " + String.valueOf(VendorList.getInstance().getVendorsID(sp_vendor.getSelectedItem().toString())));

        params.put("request_auction_broker", String.valueOf(BrokerData.getInstance().getBrokerId()));

        Call<String> call = apiInterface.update_requested_auction(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrEditBrokerAuctionRequest.this);
                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        CustomToast.showToast(ScrEditBrokerAuctionRequest.this, "Auction updated successfully");
                        finish();
                    } else {
                        CustomToast.showToast(ScrEditBrokerAuctionRequest.this, "Failed to update auction request");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: Vendor add Auction " + t.getMessage());
                CustomToast.showToast(ScrEditBrokerAuctionRequest.this, Constants.WENT_WRONG);
                CustomDialog.closeDialog(ScrEditBrokerAuctionRequest.this);
            }
        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_submit:
                validate();
                break;
        }
    }
}