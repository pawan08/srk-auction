package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrRequestAuctionDetails extends AppCompatActivity {

    private String auction_id;
    private ApiInterface apiInterface;
    private TextView tv_auction_date,tv_auction_type,tv_commodity,tv_uom,tv_qty,tv_price,tv_location,tv_commodity_des,tv_auction_code;
    private LinearLayout ll_root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_request_auction_details);

        initUI();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        getAuctionRequestDetails();
    }

    public void initUI(){

        Intent intent = getIntent();
        auction_id = intent.getStringExtra("auction_id");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView headerText = toolbar.findViewById(R.id.tv_header);
        ImageView headerImage = toolbar.findViewById(R.id.iv_back);
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerText.setText("Request Auction Details");

        tv_auction_date = findViewById(R.id.tv_auction_date);
        tv_commodity_des = findViewById(R.id.tv_commodity_des);
        tv_location = findViewById(R.id.tv_location);
        tv_price = findViewById(R.id.tv_price);
        tv_qty = findViewById(R.id.tv_qty);
        tv_uom = findViewById(R.id.tv_uom);
        tv_commodity = findViewById(R.id.tv_commodity);
        tv_auction_type = findViewById(R.id.tv_auction_type);
        tv_auction_code = findViewById(R.id.tv_auction_code);
        ll_root = findViewById(R.id.ll_root);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void getAuctionRequestDetails(){

        CustomDialog.showDialog(ScrRequestAuctionDetails.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("request_auction_id", String.valueOf(auction_id));
        Call<String> call = apiInterface.get_requested_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                CustomDialog.closeDialog(ScrRequestAuctionDetails.this);
                Log.d("TAG", "Get Auction Details" + response.body());
                Log.d("TAG", "Get_Auction_Details url " + call.request().url().toString());
                if (!response.isSuccessful()){
                    return;
                }

                try{

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS){
                        ll_root.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i=0;i < jsonArray.length();i++){

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            tv_auction_date.setText(jsonObject1.getString("request_auction_added_time"));
                            tv_auction_type.setText(jsonObject1.getString("request_auction_type"));
                            tv_commodity.setText(jsonObject1.getString("request_auction_commodity"));
                            tv_commodity_des.setText(jsonObject1.getString("request_auction_commodity_desc"));
                            tv_location.setText(jsonObject1.getString("request_auction_pickup_location"));
                            tv_price.setText(jsonObject1.getString("request_auction_unit_price"));
                            tv_qty.setText(jsonObject1.getString("request_auction_qty"));
                            tv_uom.setText(jsonObject1.getString("request_auction_uom"));
                        }
                    }else{
                        Log.d("TAG", "onResponse: " + Constants.FETCH_ERROR);
                    }

                }catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrRequestAuctionDetails.this);
                Log.d("TAG", "onResponse: " + Constants.WENT_WRONG);
            }
        });
    }


}