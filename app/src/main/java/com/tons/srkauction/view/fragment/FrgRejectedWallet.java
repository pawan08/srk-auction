package com.tons.srkauction.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RejectedWalletHistory;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.adapter.RejetedWalletHistoryAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgRejectedWallet extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_rejected_emd;
    private SwipeRefreshLayout swipe_refresh_layout;
    private ArrayList<RejectedWalletHistory> list = new ArrayList<>();
    private RejetedWalletHistoryAdapter rejetedWalletHistoryAdapter;
    private LinearLayout ll_no_data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frg_rejected_emd, null);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        initUi();
        return  view;
    }

    private void initUi(){

        ll_no_data = view.findViewById(R.id.ll_no_data);
        rv_rejected_emd = view.findViewById(R.id.rv_rejected_emd);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);
        rejetedWalletHistoryAdapter = new RejetedWalletHistoryAdapter(getActivity());
        rv_rejected_emd.setLayoutManager(new LinearLayoutManager(getActivity()));
        rv_rejected_emd.setAdapter(rejetedWalletHistoryAdapter);
    }

    @Override
    public void onResume() {
        super.onResume();
        getRejectedEMDDetails();
    }

    private void showEmptyLayout(){

        if (rejetedWalletHistoryAdapter.getItemCount() == 0){
            ll_no_data.setVisibility(View.VISIBLE);
        }else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    private void getRejectedEMDDetails(){

        swipe_refresh_layout.setRefreshing(true);
        Map<String,String> params = new HashMap<>();
        params.put("vendor_id",String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("wallet_type", Constants.WT_ADD);
        params.put("wallet_status", String.valueOf(Constants.WALLET_REJECT));
        Call<String> call = apiInterface.get_pending_reject_requests(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                swipe_refresh_layout.setRefreshing(false);
                Log.d("TAG", "onResponse:get_details "+response.body());
                if (!response.isSuccessful()){
                    return;
                }

                try{
                    list.clear();
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS){

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for(int i=0;i<jsonArray.length();i++){

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                             int  wallet_id = jsonObject1.getInt("wallet_id");
                             int  wallet_vendor_id = jsonObject1.getInt("wallet_vendor_id");
                             String  wallet_vendor_attachment = jsonObject1.getString("wallet_vendor_attachment");
                             String  wallet_credit = jsonObject1.getString("wallet_credit");
                             String  wallet_debit = jsonObject1.getString("wallet_debit");
                             String  wallet_admin_remark = jsonObject1.getString("wallet_admin_remark");
                             String  wallet_changed_time = jsonObject1.getString("wallet_changed_time");

                             RejectedWalletHistory rejectedWalletHistory = new RejectedWalletHistory(wallet_id,wallet_vendor_id,wallet_vendor_attachment,wallet_credit,wallet_debit,wallet_admin_remark,wallet_changed_time);
                             list.add(rejectedWalletHistory);
                        }


                    }
                    swipe_refresh_layout.setRefreshing(false);
                    rejetedWalletHistoryAdapter.setList(list);
                    rejetedWalletHistoryAdapter.notifyDataSetChanged();
                    showEmptyLayout();
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);
                CustomToast.showToast(getActivity(),Constants.WENT_WRONG);
            }
        });
    }

    @Override
    public void onRefresh() {

        getRejectedEMDDetails();
    }
}
