package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.DashboardSubAuction;
import com.tons.srkauction.model.SubAuction;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.view.ScrBrokerSubAuctionDetails;
import com.tons.srkauction.view.ScrHome;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionListing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

import static android.view.View.VISIBLE;

public class DashboardSubAuctionAdapter extends RecyclerView.Adapter<DashboardSubAuctionAdapter.ViewHolderBid> {

    private ArrayList<DashboardSubAuction> list;
    private ScrHome context;
    private ApiInterface apiInterface;
    private View view;

    public DashboardSubAuctionAdapter(ScrHome context) {
        this.context = context;
    }

    public void setList(ArrayList<DashboardSubAuction> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.item_dashboard_sub_auction, parent, false);
      /*  ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
        layoutParams.width = (int) (parent.getWidth() * 0.3);
        itemView.setLayoutParams(layoutParams);*/
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        final DashboardSubAuction subAuction = list.get(i);
        holder.tv_location.setText(subAuction.getLocation());
        holder.tv_quantity.setText(subAuction.getQuantity());
        holder.tv_emd_amount.setText(subAuction.getEmd_amount());
        holder.tv_vendor_name.setText(subAuction.getVendor_name());
        holder.tv_request_code.setText(subAuction.getSub_auction_code());
        holder.tv_auction_code.setText(subAuction.getAuction_code());
        holder.tv_commodity.setText(subAuction.getCommodity());

       /* holder.bt_view_details.setTag(subAuction);
        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubAuction subAuction1 = (SubAuction) view.getTag();
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {

                    Intent intent = new Intent(context, ScrSubAuctionDetails.class);
                    intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                    intent.putExtra("reqsub_auc_status", subAuction1.getReqsub_auc_status());
                    intent.putExtra("auction_id", subAuction1.getAuction_id());
                    intent.putExtra("vendor_id", subAuction1.getReqsub_auc_id_vendor_id());
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, ScrBrokerSubAuctionDetails.class);
                    intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                    intent.putExtra("auction_id", subAuction1.getAuction_id());
                    intent.putExtra("reqsub_auc_status", subAuction1.getReqsub_auc_status());
                    context.startActivity(intent);
                }
            }
        });*/

    }



    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_location, tv_quantity, tv_emd_amount, tv_vendor_name,tv_request_code,tv_commodity,tv_auction_code;
        private Button bt_view_batch,bt_view_details;
        private LinearLayout ll_main;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_request_code = itemView.findViewById(R.id.tv_request_code);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_emd_amount = itemView.findViewById(R.id.tv_emd_amount);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_commodity = itemView.findViewById(R.id.tv_commodity);
            tv_auction_code = itemView.findViewById(R.id.tv_auction_code);
            bt_view_batch = itemView.findViewById(R.id.bt_view_batch);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
            ll_main = itemView.findViewById(R.id.ll_main);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }
}
