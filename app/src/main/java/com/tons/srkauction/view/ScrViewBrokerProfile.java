package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrViewBrokerProfile extends AppCompatActivity implements View.OnClickListener {

    private Button bt_edit_broker_profile;
    private CircleImageView civ_broker_profile;
    private TextView tv_work_exp;
    private TextView tv_address;
    private TextView tv_age;
    private TextView tv_gender;
    private TextView tv_email;
    private TextView tv_mobile;
    private TextView tv_name;
    private TextView tv_bro_dealing_in;
    private TextView tv_uom;

    private ApiInterface apiInterface;
    String broker_profile_pic;
    private LinearLayout ll_root;
    ///

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_view_broker_profile);
        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getBrokerProfileDetails();
    }

    public void initUi() {


        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Profile");

        civ_broker_profile = findViewById(R.id.civ_broker_profile);
        civ_broker_profile.setOnClickListener(this);

        tv_bro_dealing_in = findViewById(R.id.tv_bro_dealing_in);
        tv_work_exp = findViewById(R.id.tv_work_exp);
        tv_address = findViewById(R.id.tv_address);
        tv_age = findViewById(R.id.tv_age);
        tv_gender = findViewById(R.id.tv_gender);
        tv_email = findViewById(R.id.tv_email);
        tv_mobile = findViewById(R.id.tv_mobile);
        tv_name = findViewById(R.id.tv_name);
        tv_uom = findViewById(R.id.tv_uom);
        ll_root = findViewById(R.id.ll_root);
        bt_edit_broker_profile = findViewById(R.id.bt_edit_broker_profile);
        bt_edit_broker_profile.setOnClickListener(this);
    }

    public void getBrokerProfileDetails() {

        CustomDialog.showDialog(ScrViewBrokerProfile.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        Call<String> call = apiInterface.bro_profile_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                CustomDialog.closeDialog(ScrViewBrokerProfile.this);

                if (!response.isSuccessful()) {
                    return;
                }

                Log.d("TAG", "onResponse: getBrokerDetails : " + response.body());
                Log.d("TAG", "onResponse: getBrokerDetails url : " + call.request().url().toString());

                try {
                    ll_root.setVisibility(View.VISIBLE);
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String broker_name = jsonObject1.getString("broker_name");
                            String broker_mobile = jsonObject1.getString("broker_mobile");
                            String broker_email = jsonObject1.getString("broker_email");
                            String broker_gender = jsonObject1.getString("broker_gender");
                            String broker_age = jsonObject1.getString("broker_age");
                            String broker_address = jsonObject1.getString("broker_address");
                            String broker_work_exp = jsonObject1.getString("broker_work_exp");
                            String broker_dealing_in = jsonObject1.getString("broker_dealing_in");
                            String uom = jsonObject1.getString("broker_uom");
                            broker_profile_pic = jsonObject1.getString("broker_profile_pic");

                            BrokerData.getInstance().setBrokerProfile(broker_profile_pic);

                            if (broker_name.equals("") || broker_name.equals("null") || broker_name == null) {
                                tv_name.setText("-");

                            } else {
                                tv_name.setText(broker_name);
                            }

                            if (uom.equals("") || uom.equals("null") || uom == null) {
                                tv_uom.setText("-");

                            } else {
                                tv_uom.setText(uom);
                            }


                            if (broker_mobile.equals("") || broker_mobile.equals("null") || broker_mobile == null) {
                                tv_mobile.setText("-");

                            } else {
                                tv_mobile.setText(broker_mobile);
                            }

                            if (broker_email.equals("") || broker_email.equals("null") || broker_email == null) {
                                tv_email.setText("-");

                            } else {
                                tv_email.setText(broker_email);
                            }


                            if (broker_gender.equals("") || broker_gender.equals("null") || broker_gender == null) {
                                tv_gender.setText("-");

                            } else {
                                tv_gender.setText(broker_gender);
                            }


                            if (broker_age.equals("") || broker_age.equals("null") || broker_age == null) {
                                tv_age.setText("-");

                            } else {
                                tv_age.setText(broker_age);
                            }

                            if (broker_address.equals("") || broker_address.equals("null") || broker_address == null) {
                                tv_address.setText("-");

                            } else {
                                tv_address.setText(broker_address);
                            }

                            if (broker_work_exp.equals("") || broker_work_exp.equals("null") || broker_work_exp == null) {
                                tv_work_exp.setText("-");

                            } else {
                                tv_work_exp.setText(broker_work_exp);
                            }

                            if (broker_dealing_in.equals("") || broker_dealing_in.equals("null") || broker_dealing_in == null) {
                                tv_bro_dealing_in.setText("-");

                            } else {
                                tv_bro_dealing_in.setText(broker_dealing_in);
                            }

                            Picasso.with(ScrViewBrokerProfile.this).load(CommunicationConstant.BROKER_PROFILE + broker_profile_pic)
                                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .into(civ_broker_profile);


                        }
                    }

                } catch (JSONException e) {

                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_edit_broker_profile:
                CustomIntent.startActivity(ScrViewBrokerProfile.this, ScrUpdateBrokerProfile.class, false);
                break;
            case R.id.civ_broker_profile:
                //  CustomIntent.startActivity(ScrViewBrokerProfile.this,ScrViewProfileImage.class);
                Intent intent = new Intent(ScrViewBrokerProfile.this, ScrViewProfileImage.class);
                intent.putExtra("image", CommunicationConstant.BROKER_PROFILE + broker_profile_pic);
                startActivity(intent);
                break;
        }
    }
}
