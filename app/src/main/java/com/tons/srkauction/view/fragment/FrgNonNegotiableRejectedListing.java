package com.tons.srkauction.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.view.adapter.LiveAuctionAdapter;

public class FrgNonNegotiableRejectedListing extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_pending_list;
    private LiveAuctionAdapter liveAuctionAdapter;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout ll_no_data;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frg_rejected_non_negotiablr, null);
        inItui();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return view;
    }

    private void inItui() {


    }

    @Override
    public void onRefresh() {

    }
}
