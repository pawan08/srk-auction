package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgApprovedAuctionRequest;
import com.tons.srkauction.view.fragment.FrgEmd;
import com.tons.srkauction.view.fragment.FrgPendingAuctionRequest;
import com.tons.srkauction.view.fragment.FrgRejectedAuctionRequest;
import com.tons.srkauction.view.fragment.FrgWallet;
import com.tons.srkauction.view.fragment.FrgWithdrawal;

public class AuctionRequestTabAdapter extends FragmentStatePagerAdapter {


    public AuctionRequestTabAdapter(FragmentManager fm){
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FrgApprovedAuctionRequest();
        } else if (position == 1){
            return new FrgPendingAuctionRequest();
        }else{
            return new FrgRejectedAuctionRequest();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Approved";

            case 1:
                return "Pending";

            case 2:
                return "Rejected";
            default:
                return null;
        }
    }
}
