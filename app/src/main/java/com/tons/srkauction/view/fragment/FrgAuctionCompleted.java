package com.tons.srkauction.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CompletedAuction;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.view.adapter.CompletedAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgAuctionCompleted extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_requested_auction;
    private CompletedAuctionAdapter completedAuctionAdapter;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout ll_no_data;
    private ArrayList<CompletedAuction> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frg_completed_auction, null);
        inItui();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return view;
    }

    private void inItui() {

        rv_requested_auction = view.findViewById(R.id.rv_requested_auction);
        completedAuctionAdapter = new CompletedAuctionAdapter(this);
        rv_requested_auction.setAdapter(completedAuctionAdapter);
        rv_requested_auction.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        ll_no_data = view.findViewById(R.id.ll_no_data);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();
        call_get_Auction_List_Api();
    }

    public void showEmptyLayout() {
        if (completedAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    private void call_get_Auction_List_Api() {
        swipe_refresh_layout.setRefreshing(true);
        Map<String, String> params = new HashMap<>();
        params.put("auction_status", Constants.STATUS_COMPLETED);
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));

        Call<String> call = apiInterface.get_auction_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", " " + response.body());
                Log.d("TAG", "Get Auction List url " + call.request().url().toString());
                swipe_refresh_layout.setRefreshing(false);
                list.clear();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        // runLayoutAnimation(rv_requested_auction);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int auction_id = jsonObject1.getInt("auction_id");
                            String auction_code = jsonObject1.getString("auction_code");
                            String auction_type = jsonObject1.getString("auction_type");
                            String auction_commodity = jsonObject1.getString("auction_commodity");
                            String auction_uom = jsonObject1.getString("auction_uom");
                            // String auction_unit_price = jsonObject1.getString("auction_unit_price");
                            String auction_start_date = jsonObject1.getString("auction_start_date");
                            String auction_start_time = jsonObject1.getString("auction_start_time");
                            String is_winner = jsonObject1.getString("is_winner");
                            String is_mine_auction = jsonObject1.getString("is_mine_auction");

                            CompletedAuction completedAuction = new CompletedAuction(auction_id, auction_code, auction_type, auction_commodity, auction_uom, auction_start_date, auction_start_time, is_winner, is_mine_auction);
                            list.add(completedAuction);

                        }
                    }
                    swipe_refresh_layout.setRefreshing(false);
                    completedAuctionAdapter.setList(list);
                    completedAuctionAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: Exception" + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onRefresh() {
        call_get_Auction_List_Api();
    }
}
