package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;


import com.tons.srkauction.view.fragment.FrgEmd;
import com.tons.srkauction.view.fragment.FrgWallet;
import com.tons.srkauction.view.fragment.FrgWithdrawal;

public class PendingRequestTabAdapter extends FragmentStatePagerAdapter {


    public PendingRequestTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        /*if (position == 0) {
            return new FrgEmd();
        } else*/
        if (position == 0) {
            return new FrgWallet();
        } else {
            return new FrgWithdrawal();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

          /*  case 0:
                return "EMD";*/

            case 0:
                return "Wallet";

            case 1:
                return "Withdrawal";
            default:
                return null;
        }
    }
}
