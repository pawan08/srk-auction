package com.tons.srkauction.view.adapter;

import android.Manifest;
import android.app.DownloadManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.VendorNotification;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrVendorNotification;

import java.io.File;
import java.util.ArrayList;

import static android.content.Context.DOWNLOAD_SERVICE;

public class VendorNotificationAdapter extends RecyclerView.Adapter<VendorNotificationAdapter.ViewHolderBid> {


    private ArrayList<VendorNotification> list;
    private ScrVendorNotification context;

    public VendorNotificationAdapter(ScrVendorNotification context) {
        this.context = context;

    }

    public void setList(ArrayList<VendorNotification> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_vendor_notification, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        VendorNotification vendorNotification = list.get(i);
        holder.tv_notification_title.setText(vendorNotification.getNv_title());
        holder.tv_des.setText(vendorNotification.getNv_desc());
        holder.tv_time.setText(DateToTimeStamp.getDateForNotification(vendorNotification.getNv_added_time()));
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_notification_title, tv_des;
        private TextView tv_time;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_notification_title = itemView.findViewById(R.id.tv_notification_title);
            tv_des = itemView.findViewById(R.id.text_view);
            tv_time = itemView.findViewById(R.id.tv_time);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }


}
