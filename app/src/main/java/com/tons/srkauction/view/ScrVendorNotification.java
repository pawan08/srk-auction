package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.VendorNotification;
import com.tons.srkauction.model.VendorNotificationList;
import com.tons.srkauction.view.adapter.VendorNotificationAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ScrVendorNotification extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private RecyclerView rv_vendor_notification;
    private VendorNotificationAdapter vendorNotificationAdapter;
    private ApiInterface apiInterface;

    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView iv_back;
    private TextView tv_header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_vendor_notification);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }

    public void initUi() {
        ll_no_data = findViewById(R.id.ll_no_data);

        rv_vendor_notification = findViewById(R.id.rv_vendor_notification);
        rv_vendor_notification.setLayoutManager(new GridLayoutManager(this, 1));

        vendorNotificationAdapter = new VendorNotificationAdapter(this);
        rv_vendor_notification.setAdapter(vendorNotificationAdapter);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Notification");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);
    }

    public void showEmptyLayout() {
        if (vendorNotificationAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_Vendor_Notifications_Api();
                                    }
                                }
        );
    }


    private void call_Vendor_Notifications_Api() {
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(1));
        Call<String> call = apiInterface.notification_vendor_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                VendorNotificationList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetVendorNotification" + response.body());
                    Log.d("TAG", "GetVendorNotification url : " + call.request().url().toString());

                    boolean status = jsonObject.getBoolean("status");

                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int nv_id = jsonObject1.getInt("nv_id");
                            String nv_title = jsonObject1.getString("nv_title");
                            String nv_desc = jsonObject1.getString("nv_desc");
                            String nv_added_time = jsonObject1.getString("nv_added_time");

                            VendorNotification vendorNotification = new VendorNotification(nv_id, nv_title, nv_desc, nv_added_time);
                            VendorNotificationList.getInstance().add(vendorNotification);
                        }
                        vendorNotificationAdapter.setList(VendorNotificationList.getInstance().getList());
                        vendorNotificationAdapter.notifyDataSetChanged();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_Vendor_Notifications_Api();
                                    }
                                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
