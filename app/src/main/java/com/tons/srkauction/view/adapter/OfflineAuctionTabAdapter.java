package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgApprovedAuctionRequest;
import com.tons.srkauction.view.fragment.FrgNegotiableListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiableListing;
import com.tons.srkauction.view.fragment.FrgPendingAuctionRequest;
import com.tons.srkauction.view.fragment.FrgRejectedAuctionRequest;

public class OfflineAuctionTabAdapter extends FragmentStatePagerAdapter {


    public OfflineAuctionTabAdapter(FragmentManager fm){
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FrgNonNegotiableListing();
        }
        else{
            return new FrgNegotiableListing();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Non Negotiable";

            case 1:
                return "Negotiable";

            default:
                return null;
        }
    }
}
