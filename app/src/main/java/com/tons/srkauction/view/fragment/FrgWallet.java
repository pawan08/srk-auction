package com.tons.srkauction.view.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.PendingWalletHistory;
import com.tons.srkauction.model.PendingWalletHistoryList;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.view.adapter.PendingWalletAdapter;
import com.tons.srkauction.view.adapter.PendingWalletEMDAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgWallet extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_requested_auction;
    private PendingWalletAdapter pendingWalletAdapter;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout ll_no_data;
    private TextView tv_amount;
    String wallet_vendor_attachment;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        view = inflater.inflate(R.layout.frg_wallet, null);
        inItui();
        Intent intent = getActivity().getIntent();
        wallet_vendor_attachment = intent.getStringExtra("wallet_vendor_attachment");

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return view;
    }

    private void inItui() {

        rv_requested_auction = view.findViewById(R.id.rv_requested_auction);
        pendingWalletAdapter = new PendingWalletAdapter(this);
        rv_requested_auction.setAdapter(pendingWalletAdapter);
        rv_requested_auction.setLayoutManager(new GridLayoutManager(getActivity(), 1));

        ll_no_data = view.findViewById(R.id.ll_no_data);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);


    }

    @Override
    public void onResume() {
        super.onResume();
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipe_refresh_layout.setRefreshing(true);
                call_get_Pending_Wallet_List_Api();
            }
        });
    }


    public void showEmptyLayout() {
        if (pendingWalletAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    private void call_get_Pending_Wallet_List_Api() {
        swipe_refresh_layout.setRefreshing(true);
        Map<String, String> params = new HashMap<>();
        params.put("wallet_type", Constants.WT_ADD);
        params.put("wallet_status", String.valueOf(Constants.WALLET_PENDING));
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_pending_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "GetPendingWallet" + response.body());
                Log.d("TAG", "GetPendingWallet url : " + call.request().url().toString());

                PendingWalletHistoryList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONObject objectZero = jsonObject.getJSONObject("data1");
                        String wallet_amount = objectZero.getString("wallet_amount");
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int wallet_id = jsonObject1.getInt("wallet_id");
                            int wallet_vendor_id = jsonObject1.getInt("wallet_vendor_id");
                            String wallet_vendor_attachment = jsonObject1.getString("wallet_vendor_attachment");
                            String wallet_vendor_desc = jsonObject1.getString("wallet_vendor_desc");
                            String wallet_credit = jsonObject1.getString("wallet_credit");
                            String wallet_debit = jsonObject1.getString("wallet_debit");
                            String wallet_admin_remark = jsonObject1.getString("wallet_admin_remark");
                            String wallet_added_time = jsonObject1.getString("wallet_added_time");
                            PendingWalletHistory pendingWalletHistory = new PendingWalletHistory(wallet_id, wallet_vendor_id, wallet_vendor_attachment, wallet_vendor_desc, wallet_credit, wallet_debit, wallet_admin_remark, wallet_added_time);
                            PendingWalletHistoryList.getInstance().add(pendingWalletHistory);
                        }
                    }

                    swipe_refresh_layout.setRefreshing(false);
                    pendingWalletAdapter.setList(PendingWalletHistoryList.getInstance().getList());
                    pendingWalletAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: JSON Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);
                Log.d("TAG", "onFailure: " + t.getMessage());
            }

        });
    }


    @Override
    public void onRefresh() {
        call_get_Pending_Wallet_List_Api();
    }
}
