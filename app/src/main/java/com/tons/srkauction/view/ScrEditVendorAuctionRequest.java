package com.tons.srkauction.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.communication.volley_communication.VolleyMultipartRequest;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.communication.volley_communication.datapart.DataPart;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.model.RequestAuction;
import com.tons.srkauction.model.RequestAuctionList;
import com.tons.srkauction.model.UMO;
import com.tons.srkauction.model.UMOList;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrEditVendorAuctionRequest extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener {

    private String TAG = ScrEditVendorAuctionRequest.class.getName();

    private ArrayList<String> auction_type_list;
    private ArrayAdapter<String> auction_type_adapter;

    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    private ArrayList<String> auction_commodity_list;
    private ArrayAdapter<String> auction_commodity_adapter;

    private String auction_id;
    private String auction_type;
    private String commodity;
    private String uom;

    private String image_type = null;
    // private AppCompatSpinner sp_auction_type;
    private SearchableSpinner sp_auction_commodity;
    private SearchableSpinner sp_uom;
    private EditText et_unit_price;
    private EditText et_location;
    private EditText et_commodity_desc;
    private EditText et_qty;
    private Button bt_submit;
    private ImageView iv_image;
    private LinearLayout ll_image;

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;
    private String x;
    private Uri imageUri;
    private Bitmap bitmap;
    String request_auction_image = null;


    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_edit_vendor_auction_request);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        initUi();

        Intent intent = getIntent();
        auction_id = intent.getStringExtra("auction_id");
        //  request_auction_image=intent.getStringExtra("request_auction_image");

        getAuctionRequestDetails();
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }

    private void initUi() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Edit Requested Auction");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        iv_image = findViewById(R.id.iv_image);

        sp_uom = findViewById(R.id.sp_uom);
        et_qty = findViewById(R.id.et_qty);
        et_unit_price = findViewById(R.id.et_unit_price);
        et_location = findViewById(R.id.et_location);
        et_commodity_desc = findViewById(R.id.et_commodity_desc);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        sp_auction_commodity = findViewById(R.id.sp_auction_commodity);
        //   sp_auction_type = findViewById(R.id.sp_auction_type);
        iv_image = findViewById(R.id.iv_image);
        ll_image = findViewById(R.id.ll_image);
        bt_submit.setOnClickListener(this);
        ll_image.setOnClickListener(this);
    }

    public void getAuctionRequestDetails() {

        CustomDialog.showDialog(ScrEditVendorAuctionRequest.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("request_auction_id", String.valueOf(auction_id));
        Call<String> call = apiInterface.get_requested_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrEditVendorAuctionRequest.this);
                Log.d("TAG", "Get Auction Details" + response.body());
                Log.d("TAG", "Get_Auction_Details url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            auction_type = jsonObject1.getString("request_auction_type");
                            commodity = jsonObject1.getString("request_auction_commodity");
                            uom = jsonObject1.getString("request_auction_uom");

                            String commodity_des = jsonObject1.getString("request_auction_commodity_desc");
                            String location = jsonObject1.getString("request_auction_pickup_location");
                            String price = jsonObject1.getString("request_auction_unit_price");
                            String qty = jsonObject1.getString("request_auction_qty");
                            request_auction_image = jsonObject1.getString("request_auction_image");

                            if (!qty.equals("null") && !qty.equals("")) {
                                et_qty.setText(qty);
                            }
                            if (!price.equals("null") && !price.equals("")) {
                                et_unit_price.setText(price);
                            }
                            if (!location.equals("null") && !location.equals("")) {
                                et_location.setText(location);
                            }
                            if (!commodity_des.equals("null") && !commodity_des.equals("")) {
                                et_commodity_desc.setText(commodity_des);
                            }
                            Picasso.with(ScrEditVendorAuctionRequest.this).load(CommunicationConstant.EDIT_REQUEST_ATTACHMENT + request_auction_image)
                                    .placeholder(ScrEditVendorAuctionRequest.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrEditVendorAuctionRequest.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(iv_image);


                            initUom();
                            initAuctiontype();
                            initAuctioncommodity(commodity);
                        }
                    } else {
                        Log.d("TAG", "onResponse: " + Constants.FETCH_ERROR);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrEditVendorAuctionRequest.this);
                Log.d("TAG", "onResponse: " + Constants.WENT_WRONG);
            }
        });
    }


    private void initAuctiontype() {
        /*  auction_type_list = new ArrayList<>();
        auction_type_list.add(0, "Select auction type");
        auction_type_list.add(1, "Forward");
        auction_type_list.add(2, "Reverse");


        auction_type_adapter = new ArrayAdapter<String>(ScrEditVendorAuctionRequest.this, android.R.layout.simple_list_item_1, auction_type_list);
        sp_auction_type.setAdapter(auction_type_adapter);
        auction_type_adapter.notifyDataSetChanged();*/

        /*try {
            if (auction_type.equals("F")) {

                Intent intent = getIntent();
                intent.getStringExtra("auction_type");
                int spinnerPosition = auction_type_adapter.getPosition("Forward");
                sp_auction_type.setSelection(spinnerPosition);
            } else {

                Intent intent = getIntent();
                intent.getStringExtra("auction_type");
                int spinnerPosition = auction_type_adapter.getPosition("Reverse");

                sp_auction_type.setSelection(spinnerPosition);
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            Log.d(TAG, "initAuctiontype: null pointer Exception : " + e.getMessage());
        }*/

    }


    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "KG");
        uom_list.add(2, "LTR");

        uom_adapter = new ArrayAdapter<String>(ScrEditVendorAuctionRequest.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);
        uom_adapter.notifyDataSetChanged();

        Log.d(TAG, "initUom: " + uom);

       /* Intent intent = getIntent();
        intent.getStringExtra("auction_uom");*/
        int spinnerPosition = uom_adapter.getPosition(uom);
        sp_uom.setSelection(spinnerPosition);
        Log.d("TAG", "onResponsenewumo: " + spinnerPosition);


    }


    public void initAuctioncommodity(final String commodity) {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(TAG, "onResponse: getCommodityList : " + response.body());
                Log.d(TAG, "onResponse: getCommodityList url : " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    CommodityList.getInstance().clearList();

                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);

                            auction_commodity_adapter = new ArrayAdapter<String>(ScrEditVendorAuctionRequest.this, android.R.layout.simple_spinner_item, CommodityList.getInstance().getCommodityNames());
                            sp_auction_commodity.setAdapter(auction_commodity_adapter);
                            auction_commodity_adapter.notifyDataSetChanged();

                            int spinnerPosition = auction_commodity_adapter.getPosition(commodity);
                            sp_auction_commodity.setSelection(spinnerPosition);
                            Log.d("TAG", "onResponsenew: " + spinnerPosition);


                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }


    private void validate() {
        String unit_price = et_unit_price.getText().toString();
        String qty = et_qty.getText().toString();
        String location = et_location.getText().toString();
        String et_com_desc = et_commodity_desc.getText().toString();

        if (request_auction_image != null && !request_auction_image.equals("") && !request_auction_image.equals("null")) {
            iv_image.invalidate();
            BitmapDrawable drawable1 = (BitmapDrawable) iv_image.getDrawable();
            bitmap = drawable1.getBitmap();
        }

        if (sp_auction_commodity.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select auction commodity", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(qty)) {
            MyCustomDialog.showValidationDialog("Please enter quantity", "Oop's", this);
            return;
        }

        if (qty.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter quantity greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(unit_price)) {
            MyCustomDialog.showValidationDialog("Please enter unit price", "Oop's", this);
            return;
        }

        if (unit_price.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter unit price greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(location)) {
            MyCustomDialog.showValidationDialog("Please enter pick up location", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(et_com_desc)) {
            MyCustomDialog.showValidationDialog("Please enter commodity description", "Oop's", this);
            return;
        }

        if (bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select image", "Oop's", this);
            return;
        }

        Map<String, String> params = new HashMap<>();

        params.put("request_auction_id", auction_id);
        params.put("request_auction_type", "FORWARD");
        params.put("request_auction_commodity", sp_auction_commodity.getSelectedItem().toString());
        params.put("request_auction_uom", sp_uom.getSelectedItem().toString());
        params.put("request_auction_commodity_desc", et_commodity_desc.getText().toString());
        params.put("request_auction_qty", et_qty.getText().toString());
        params.put("request_auction_unit_price", et_unit_price.getText().toString());
        params.put("request_auction_pickup_location", et_location.getText().toString());
        params.put("request_auction_vendor", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("request_auction_broker", "0");


        call_add_money_api(params);
        //call_Vendor_Update_Profile(params);

    }

    private void call_add_money_api(final Map<String, String> params) {


        final String url = CommunicationConstant.EDIT_REQUET;

        CustomDialog.showDialog(ScrEditVendorAuctionRequest.this, Constants.PROGRESS_MSG);

        VolleyMultipartRequest postRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        CustomDialog.closeDialog(ScrEditVendorAuctionRequest.this);
                        Log.d("TAG", "onResponse: call_add_request_api " + new String(response.data));
                        Log.d("TAG", "onResponse: call_add_request_api url " + url);
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            boolean status = jsonObject.getBoolean("status");
                            int message = jsonObject.getInt("message");
                            if (message == Messages.SUCCESS) {
                                CustomToast.showToast(ScrEditVendorAuctionRequest.this, "Successfully added");
                                finish();

                            } else {
                                CustomToast.showToast(ScrEditVendorAuctionRequest.this, Constants.WENT_WRONG);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.d("TAG", "onResponse: call_add_request_api " + e.getMessage());
                        }

                        Log.d("REGISTER_RESPONSE", "call_add_request_api : " + response.data.toString());

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomDialog.closeDialog(ScrEditVendorAuctionRequest.this);
                        Log.d("TAG", String.valueOf(error));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("request_auction_image", new DataPart("image_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(bitmap)));
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        long lengthbmp = imageInByte.length;
        Log.d("SIZETAG", String.valueOf(lengthbmp));
        return byteArrayOutputStream.toByteArray();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_submit:
                validate();
                break;

            case R.id.ll_image:
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "image");
                break;
        }
    }

    private void launchCameraIntent1() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }

    @Override
    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                x = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchCameraIntent1();
                }
                break;

            case 2:
                x = "2";
                launchGalleryIntent();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: x = " + x);
                switch (x) {

                    case "1":
                        Log.d("TAG", "onActivityResult: inside case 1 ");
                        String url1 = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                            // url1 = getRealPathFromURI(imageUri1);
                            InputStream input2 = this.getContentResolver().openInputStream(imageUri);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(input2);
                                    int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: camera id proof ");
                            break;

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                            Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                        }
                        break;

                    case "2":
                        Log.d("TAG", "onActivityResult: inside case 2 ");
                        try {

                            imageUri = data.getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                            bitmap = BitmapFactory.decodeStream(imageStream);
                            ExifInterface exif3 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif3 = new ExifInterface(imageStream);
                                    int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: gallery id proof ");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrEditVendorAuctionRequest.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }


}
