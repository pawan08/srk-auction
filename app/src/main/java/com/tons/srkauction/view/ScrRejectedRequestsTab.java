package com.tons.srkauction.view;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.view.adapter.AuctionTabAdapter;
import com.tons.srkauction.view.adapter.RejectedWalletTabAdapter;

public class ScrRejectedRequestsTab extends AppCompatActivity {

    int postiion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_auction_tab);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        ViewPager viewPager = findViewById(R.id.view_pager_tab);
        RejectedWalletTabAdapter rejectedWalletTabAdapter = new RejectedWalletTabAdapter(getSupportFragmentManager());
        viewPager.setAdapter(rejectedWalletTabAdapter);
        viewPager.setOffscreenPageLimit(2);

        Bundle b = getIntent().getExtras();
        if (b != null) {
            postiion = b.getInt("position");
            Log.d("TAG", "onCreate:position if" + postiion);
            switch (postiion) {

               /* case Constants.REJECTED_EMD:
                    viewPager.setCurrentItem(0);
                    break;*/

                case Constants.REJECTED_WALLET:
                    viewPager.setCurrentItem(0);
                    break;

                case Constants.REJECTED_WITHDRAWAL:
                    viewPager.setCurrentItem(1);
                    break;
            }
        } else {
            Log.d("TAG", "onCreate:position else" + postiion);
            viewPager.setCurrentItem(0);
        }

        TabLayout tabLayout = findViewById(R.id.auction_tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabMode(TabLayout.MODE_FIXED);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isTaskRoot()) {
                    finish();
                    CustomIntent.startActivity(ScrRejectedRequestsTab.this, ScrHome.class, true);
                } else {
                    ScrRejectedRequestsTab.super.onBackPressed();
                }
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Rejected Request");
    }

    @Override
    public boolean onSupportNavigateUp() {
        if (isTaskRoot()) {
            finish();
            CustomIntent.startActivity(ScrRejectedRequestsTab.this, ScrHome.class, true);
        } else {
            super.onBackPressed();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            finish();
            CustomIntent.startActivity(ScrRejectedRequestsTab.this, ScrHome.class, true);
        } else {
            super.onBackPressed();
        }
    }

}
