package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.CompletedAuction;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrAuctionDetails;
import com.tons.srkauction.view.fragment.FrgAuctionCompleted;

import java.util.ArrayList;

public class CompletedAuctionAdapter extends RecyclerView.Adapter<CompletedAuctionAdapter.ViewHolderBid> {

    private ArrayList<CompletedAuction> list;
    private FrgAuctionCompleted context;

    public CompletedAuctionAdapter(FrgAuctionCompleted context) {
        this.context = context;

    }

    public void setList(ArrayList<CompletedAuction> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_completed_auction, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        CompletedAuction completedAuction = list.get(i);
        holder.tv_auction_type.setText(completedAuction.getAuction_type());
        holder.tv_auction_commomditity.setText(completedAuction.getAuction_code());
        holder.tv_uom.setText("Commodity : " + completedAuction.getAuction_commodity());
        holder.tv_date.setText(DateToTimeStamp.getDate(completedAuction.getDate()) + " " + DateToTimeStamp.getTime(completedAuction.getAuction_start_time() + ":00"));

        holder.bt_view_details.setTag(completedAuction);
        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CompletedAuction completedAuction1 = (CompletedAuction) view.getTag();
                Intent intent = new Intent(context.getActivity(), ScrAuctionDetails.class);
                intent.putExtra("auction_id", String.valueOf(completedAuction1.getAuction_id()));
                context.startActivity(intent);
            }
        });

        if (completedAuction.getIs_winner().equals("YES")){
            blinkTextView(holder.text_blink);
        }else{
            holder.text_blink.setVisibility(View.GONE);
        }


    }

    private void blinkTextView(final TextView textView) {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                int timeToBlink = 500;
                try {
                    Thread.sleep(timeToBlink);
                } catch (Exception e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (textView.getVisibility() == View.VISIBLE) {
                            textView.setVisibility(View.INVISIBLE);
                        } else {
                            textView.setVisibility(View.VISIBLE);
                        }
                        blinkTextView(textView);
                    }
                });
            }
        }).start();
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public void filteredList(ArrayList<CompletedAuction> filteredList) {
        list = filteredList;
        notifyDataSetChanged();
    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_date, tv_unit_price, tv_uom,text_blink;
        private TextView tv_auction_commomditity, tv_auction_type, auction_timer;
        private CardView cv_main;
        private Button bt_view_details;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            text_blink = itemView.findViewById(R.id.text_blink);
            tv_auction_type = itemView.findViewById(R.id.tv_auction_type);
            tv_auction_commomditity = itemView.findViewById(R.id.tv_auction_commomditity);
            tv_uom = itemView.findViewById(R.id.tv_uom);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            cv_main = itemView.findViewById(R.id.cv_main);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
