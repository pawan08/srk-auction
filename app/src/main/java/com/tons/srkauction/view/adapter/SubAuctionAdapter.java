package com.tons.srkauction.view.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.SubAuction;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.view.ScrBrokerSubAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionListing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;

import static android.view.View.VISIBLE;

public class SubAuctionAdapter extends RecyclerView.Adapter<SubAuctionAdapter.ViewHolderBid> {

    private Timer timer;
    private ArrayList<SubAuction> list;
    private ScrSubAuctionListing context;
    private ApiInterface apiInterface;


    public SubAuctionAdapter(ScrSubAuctionListing context) {
        this.context = context;
    }

    public void setList(ArrayList<SubAuction> list) {
        this.list = list;
        timer = new Timer();
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sub_auction, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        final SubAuction subAuction = list.get(i);
        holder.tv_location.setText(subAuction.getReqsub_auc_location());
        holder.tv_unit_price.setText(subAuction.getReqsub_auc_unit_price());
        holder.tv_quantity.setText(subAuction.getReqsub_auc_qty());
        holder.tv_emd_amount.setText(subAuction.getReqsub_auc_emd());
        holder.tv_vendor_name.setText(subAuction.getVendor_name());
        holder.tv_request_code.setText(subAuction.getReqsub_auc_code());

        Log.d("TAG", "request_auction_status: " + subAuction.getReqsub_auc_status());

        if (subAuction.getReqsub_auc_status() == Constants.SUB_AUCTION_PENDING ) {
            holder.chronometer.setText("Upcoming Auction");
            holder.ll_main.setBackground(context.getResources().getDrawable(R.drawable.layout_background));
            holder.chronometer.setTextColor(context.getResources().getColor(R.color.colorPrimary));
        } else {
            holder.chronometer.setText("Auction Live");
            holder.ll_main.setBackground(context.getResources().getDrawable(R.drawable.layout_background_green));
            holder.chronometer.setTextColor(context.getResources().getColor(R.color.button_dark_green));
        }

        holder.bt_participate.setTag(subAuction);
        holder.bt_participate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubAuction subAuction1 = (SubAuction) view.getTag();
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {


                    Intent intent = new Intent(context, ScrSubAuctionDetails.class);
                    intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                    intent.putExtra("reqsub_auc_status", subAuction1.getReqsub_auc_status());
                    intent.putExtra("auction_id", subAuction1.getAuction_id());
                    intent.putExtra("vendor_id", subAuction1.getReqsub_auc_id_vendor_id());
                    context.startActivity(intent);
                }else{
                    Intent intent = new Intent(context, ScrBrokerSubAuctionDetails.class);
                    intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                    intent.putExtra("auction_id", subAuction1.getAuction_id());
                    intent.putExtra("reqsub_auc_status", subAuction1.getReqsub_auc_status());
                    context.startActivity(intent);
                }
            }
        });
/*
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                //Do something after 1s
                Log.d("TAG", "Updating");
                Log.d("TAG", "run:current " + subAuction.getCurrent_time());
                Log.d("TAG", "run:end " + subAuction.getEnd_date_time() + ":00");
                upcomingTimerCalculation(holder.chronometer, subAuction.getCurrent_time(), subAuction.getEnd_date_time() + ":00");
                handler.postDelayed(this, 1000);
            }
        }, 1000);*/
    }



    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    private void upcomingTimerCalculation(final TextView txtCurrentTime, final String current_time, final String end_date_time) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {
                //Date currentTime = Calendar.getInstance().getTime();
                futureDate = dateFormat.parse(current_time);
                //futureDate = dateFormat.parse(CurrentTime.getInstance().getKEY_CURRENT_TIME());
                futureDate1 = dateFormat.parse(end_date_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                txtCurrentTime.setText(String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                    txtCurrentTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                } else {
                    txtCurrentTime.setVisibility(VISIBLE);
                    txtCurrentTime.setText("Finished!");

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG", "upcomingTimerCalculation: Exception " + e.getMessage());
        }

    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView chronometer;
        private TextView tv_location, tv_quantity, tv_unit_price, tv_view_details, tv_emd_amount, tv_vendor_name,tv_request_code;
        private Button bt_participate;
        private LinearLayout ll_main;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            chronometer = itemView.findViewById(R.id.chronometer);
            tv_request_code = itemView.findViewById(R.id.tv_request_code);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_view_details = itemView.findViewById(R.id.tv_view_details);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_emd_amount = itemView.findViewById(R.id.tv_emd_amount);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            bt_participate = itemView.findViewById(R.id.bt_participate);
            ll_main = itemView.findViewById(R.id.ll_main);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }
}
