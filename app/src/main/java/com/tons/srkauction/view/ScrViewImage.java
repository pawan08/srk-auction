package com.tons.srkauction.view;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.FolderCreator.FileUtils;
import com.tons.srkauction.FolderCreator.GRFile;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrViewImage extends AppCompatActivity implements View.OnClickListener {

    private String TAG = ScrViewImage.class.getName();
    private static final int PERMISSION_REQUEST_CODE = 1;

    private ApiInterface apiInterface;

    public static String image_name = null;
    public static String type = null;

    private String url = null;

    private ZoomageView photoView;
    private ImageView iv_back;
    private Button bt_download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_view_image);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        initUI();
    }

    public void initUI() {
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        photoView = (ZoomageView) findViewById(R.id.photo_view);

        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        bt_download = findViewById(R.id.bt_download);
        bt_download.setOnClickListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView headerText = toolbar.findViewById(R.id.tv_header);
        headerText.setText("QR Code");
        ImageView headerImage = toolbar.findViewById(R.id.iv_back);
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    public void loadImage() {


        Bundle b = getIntent().getExtras();
        if (b !=null){

            image_name = b.getString("image");
            url = CommunicationConstant.QR_CODE+image_name;
            Log.d("TAG", "onCreate: image name : " + image_name);
        }else{
            Log.d("TAG", "onCreate: "+b);
        }
        Log.d(TAG, "onCreate: image url : " + url);

        Picasso.with(this).load(url)
                .placeholder(getResources().getDrawable(R.drawable.ic_baseline_image))
                .error(getResources().getDrawable(R.drawable.ic_baseline_image))
                .into(photoView);

        if (!checkImageDownloadedOrNot(image_name)) {
            bt_download.setText("Download");
        } else {
            bt_download.setText("Downloaded");
        }
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_download:

                bt_download.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startDownload(image_name, bt_download);
                        // dialog.dismiss();
                    }
                });
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        loadImage();
        if (isStoragePermissionGranted(this)) {
            FileUtils.createFolder(this);
        }
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 1 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    FileUtils.createFolder(this);
                }
                break;
            default:
                break;
        }
    }


    private boolean isFileExists(String image_name) {
        String path = GRFile.getIMAGEFilePath() + image_name;
        File file = new File(path);
        return file.exists();
    }

    public boolean checkImageDownloadedOrNot(String image_name) {
        boolean flag;
        if (!isFileExists(image_name)) {
            flag = false;
            Log.d(TAG, "checkImageDownloadedOrNot: download image");
        } else {
            flag = true;
            Log.d(TAG, "checkImageDownloadedOrNot: already download image");
            // tv_download_image.setText("Downloaded");
        }
        return flag;
    }


    private void startDownload(String image_name, Button bt_download) {

        String path = GRFile.getIMAGEFilePath() + image_name;
        if (!isFileExists(image_name)) {
            bt_download.setText("Download");
            call_api_for_downloadPDF(image_name, path, bt_download);
        } else {
            bt_download.setText("Downloaded");
            Log.d(TAG, "startDownload: image already downloaded");
            CustomToast.showToast(ScrViewImage.this, "Already Downloaded");
        }
    }

    public void call_api_for_downloadPDF(final String file_name, final String path, final Button bt_download) {
        CustomDialog.showDialog(ScrViewImage.this, "Downloading Image...");
        Log.d(TAG, "call_api_for_downloadPDF: image url : " + CommunicationConstant.QR_CODE + file_name);
        Call<ResponseBody> call = apiInterface.dowloadAnyFile(CommunicationConstant.QR_CODE + file_name);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                CustomDialog.closeDialog(ScrViewImage.this);
                if (response.isSuccessful()) {
                    Log.d(TAG, "onResponse: server contacted and has file");

                    new AsyncTask<Void, Void, Void>() {
                        @SuppressLint("StaticFieldLeak")
                        @Override
                        protected Void doInBackground(Void... voids) {
                            boolean writtenToDisk = writeResponseBodyToDisk(path, response.body());

                            Log.d(TAG, "onResponse: file download was a success? " + writtenToDisk);
                            CustomToast.showToast(ScrViewImage.this,"File Downloaded Successfully");
                            if (writtenToDisk) {

                            }
                            return null;
                        }
                    }.execute();

                    if (isFileExists(file_name)) {
                        bt_download.setText("Downloaded");
                    } else {
                        bt_download.setText("Download");
                    }
                } else {
                    CustomToast.showToast(ScrViewImage.this,"Download Failed ..Try again!");
                    Log.d(TAG, "onResponse:server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onResponse: failed");
                CustomDialog.closeDialog(ScrViewImage.this);
            }
        });
    }

    private boolean writeResponseBodyToDisk(String path, ResponseBody body) {
        try {
            // todo change the file location/name according to your needs

            File futureStudioIconFile = new File(path);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        super.onBackPressed();
        return true;
    }


}
