package com.tons.srkauction.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestWithdrawWalletModel;
import com.tons.srkauction.view.ScrRequestWithdrawalWallet;

import java.util.ArrayList;

public class RequestWithdrawalFromWalletAdapter extends RecyclerView.Adapter<RequestWithdrawalFromWalletAdapter.ViewHolderBid> {


    private ApiInterface apiInterface;
    private ArrayList<RequestWithdrawWalletModel> list;
    private ScrRequestWithdrawalWallet context;

    public RequestWithdrawalFromWalletAdapter(ScrRequestWithdrawalWallet context) {
        this.context = context;

    }

    public void setList(ArrayList<RequestWithdrawWalletModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_requested_withdrawal_wallet, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBid holder, int position) {
        RequestWithdrawWalletModel requestWithdrawWalletModel = list.get(position);
        final String date = requestWithdrawWalletModel.getDate();
        final String amt = requestWithdrawWalletModel.getAmt();
      // holder.tv_requested_amt.setText(requestWithdrawWalletModel.getAmt());
        //holder.tv_requested_amt.setText(requestWithdrawWalletModel.getDate());

        holder.tv_requested_amt.setText("Requested On" +"  "+ (date) +" " +"  " +" "+ " "+"  "+ " " +(amt));

    }









    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }





    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_requested_amt;



        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);


            tv_requested_amt = itemView.findViewById(R.id.tv_requested_amt);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }

}
