package com.tons.srkauction.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.custom.MyCustomDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrWithdrawMoneyFromWallet extends AppCompatActivity implements View.OnClickListener {

    private ApiInterface apiInterface;
    private EditText et_amount, et_desc;
    private Button bt_request;
    private TextView tv_amt;
    private TextView tv_bank_name, tv_branch_name, tv_account_no, tv_ifsc_code;
    private String amount;
    private String description;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_withdraw_money_from_wallet);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        initUI();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        call_get_available_amt();


    }

    public void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Withdrawal wallet amount");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        et_amount = findViewById(R.id.et_amount);
        et_desc = findViewById(R.id.et_desc);
        bt_request = findViewById(R.id.bt_request);
        tv_amt = findViewById(R.id.tv_amt);
        tv_branch_name = findViewById(R.id.tv_branch_name);
        tv_bank_name = findViewById(R.id.tv_bank_name);
        tv_account_no = findViewById(R.id.tv_account_no);
        tv_ifsc_code = findViewById(R.id.tv_ifsc_code);
        bt_request.setOnClickListener(this);

    }

    public void doValidate() {

        amount = et_amount.getText().toString();
        description = et_desc.getText().toString();

        if (TextUtils.isEmpty(amount)) {
            MyCustomDialog.showValidationDialog("Please enter amount", "Oop's", this);
            return;
        }

        if (amount.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter amount greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(description)) {
            MyCustomDialog.showValidationDialog("Please enter description", "Oop's", this);
            return;
        }

        call_withdraw_amt();

    }


    @Override
    protected void onResume() {

        super.onResume();
    }

    private void call_withdraw_amt() {
        CustomDialog.showDialog(ScrWithdrawMoneyFromWallet.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("amount", et_amount.getText().toString());
        params.put("description", et_desc.getText().toString());

        Call<String> call = apiInterface.wallet_withdrwl(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrWithdrawMoneyFromWallet.this);
                Log.d("TAG", "GetWalletWithdraw" + response.body());
                Log.d("TAG", "GetWalletWithdraw url : " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        CustomToast.showToast(ScrWithdrawMoneyFromWallet.this, "Withdrawal request sent");
                        Intent rejected = new Intent(ScrWithdrawMoneyFromWallet.this, ScrPendingRequestTab.class);
                        rejected.putExtra("position", Constants.PENDING_WITHDRAWAL);
                        startActivity(rejected);
                        finish();
                    } else if (message == Messages.INVALID_WALLET_AMOUNT) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        String available_wallet_amount = data.getString("available_wallet_amount");
                        MyCustomDialog.showValidationDialog("Your available wallet amount is " + available_wallet_amount + " please enter amount less than wallet amount.", "Oop's", ScrWithdrawMoneyFromWallet.this);
                    } else if (message == Messages.ALREADY_EXIST) {
                        CustomToast.showToast(ScrWithdrawMoneyFromWallet.this, "Your Withdrawal Request is already in queue.");
                    } else {
                        CustomToast.showToast(ScrWithdrawMoneyFromWallet.this, "Failed to Request");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: JSON Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomToast.showToast(ScrWithdrawMoneyFromWallet.this, Constants.WENT_WRONG);
            }
        });
    }


    private void call_get_available_amt() {
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_available_amt(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetAvailableAmt" + response.body());
                    Log.d("TAG", "GetAvailableAmt url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            tv_bank_name.setText(jsonObject1.getString("vendor_bank_name"));
                            tv_branch_name.setText("Branch: " + jsonObject1.getString("vendor_branch_name"));
                            tv_account_no.setText("Account No: " + jsonObject1.getString("vendor_ac_no"));
                            tv_ifsc_code.setText("IFSC Code: " + jsonObject1.getString("vendor_ifsc_code"));

                        }

                        JSONObject jsonObject1 = jsonObject.getJSONObject("data1");
                        String wallet_amount = jsonObject1.getString("wallet_amount");
                        tv_amt.setText(wallet_amount);
                    }

                } catch (JSONException ex) {

                    Log.e("TAG", "exception", ex);
                    ex.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }


        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_request:
                doValidate();
                break;

        }
    }


}
