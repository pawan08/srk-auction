package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgNegotiableApprovedListing;
import com.tons.srkauction.view.fragment.FrgNegotiablePendingListing;
import com.tons.srkauction.view.fragment.FrgNegotiableRejectedListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiableApprovedListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiablePendingListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiableRejectedListing;

public class NegotiableTabAdapter extends FragmentStatePagerAdapter {


    public NegotiableTabAdapter(FragmentManager fm){
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FrgNegotiableApprovedListing();
        } else if (position == 1){
            return new FrgNegotiablePendingListing();
        }else{
            return new FrgNegotiableRejectedListing();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Approved";

            case 1:
                return "Pending";

            case 2:
                return "Rejected";
            default:
                return null;
        }
    }
}
