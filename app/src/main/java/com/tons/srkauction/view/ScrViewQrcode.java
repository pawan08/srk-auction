package com.tons.srkauction.view;

import android.Manifest;
import android.app.DownloadManager;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jsibbold.zoomage.ZoomageView;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.CommunicationConstant;

import java.io.File;

public class ScrViewQrcode extends AppCompatActivity {

    private String title = "";
    private DownloadManager downloadManager;
    private String image_path;
    private FloatingActionButton fb_download;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_view_profile_image);

        final ZoomageView photoView = (ZoomageView) findViewById(R.id.photo_view);
        fb_download = findViewById(R.id.fb_download);
        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);

        Bundle b = getIntent().getExtras();
        if (b !=null){
            title = b.getString("title");
            Log.d("TAG", "onCreate:title "+ title);
            if (title.equals("QR Code")){
                fb_download.setVisibility(View.VISIBLE);
            }else{
                fb_download.setVisibility(View.GONE);
            }

            image_path = b.getString("image");
            tv_header.setText(title);
        }else{
            Log.d("TAG", "onCreate: "+b);
            tv_header.setText("Image");
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Picasso.with(this).load(CommunicationConstant.QR_CODE + image_path).placeholder(getResources().getDrawable(R.drawable.progress_animation))
                .into(photoView);


        fb_download.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.M)
            @Override
            public void onClick(View view) {

                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(ScrViewQrcode.this, "Download permission...", Toast.LENGTH_LONG).show();
                    ActivityCompat.requestPermissions(ScrViewQrcode.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);

                } else {

                    String uri = CommunicationConstant.QR_CODE + image_path;
                    Log.d("TAG", "onClick: " + uri);
                    long downloadFileRef = downloadFile(Uri.parse(uri), "/SRK Auction", image_path);

                    if (downloadFileRef != 0) {

                        Toast.makeText(ScrViewQrcode.this, "Starting download...", Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(ScrViewQrcode.this, "File is not available for download", Toast.LENGTH_LONG).show();

                    }
                }

            }
        });
    }


    private long downloadFile(Uri uri, String fileStorageDestinationUri, String ses_assignment_file) {


        File docFolder = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + fileStorageDestinationUri);
        if ((!docFolder.exists())) {

            docFolder.mkdir();
            Log.i("TAG", "Created a new directory for PDF");
        }


        File file_nmae = new File(docFolder.getAbsolutePath(), ses_assignment_file);

        long downloadReference = 0;

        downloadManager = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
        try {
            DownloadManager.Request request = new DownloadManager.Request(uri);

            //Setting title of request
            request.setTitle(ses_assignment_file);

            //Setting description of request
            request.setDescription("Your file is downloading");

            //set notification when download completed
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

            //Set the local destination for the downloaded file to a path within the application's external files directory
            request.setDestinationUri(Uri.fromFile(file_nmae));
            request.allowScanningByMediaScanner();

            //Enqueue download and save the referenceId
            downloadReference = downloadManager.enqueue(request);
        } catch (IllegalArgumentException e) {
            Toast.makeText(ScrViewQrcode.this, "Download link is broken or not availale for download", Toast.LENGTH_LONG).show();
            Log.e("TAG", "Line no: 455,Method: downloadFile: Download link is broken");

        }
        return downloadReference;
    }
}
