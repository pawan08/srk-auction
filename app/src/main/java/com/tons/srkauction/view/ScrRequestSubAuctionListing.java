package com.tons.srkauction.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestSubAuction;
import com.tons.srkauction.model.RequestSubAuctionList;
import com.tons.srkauction.model.SubAuction;
import com.tons.srkauction.view.adapter.RequestSubAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrRequestSubAuctionListing extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private RequestSubAuctionAdapter subAuctionAdapter;
    private RecyclerView rv_sub_auction;
    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipe_refresh_layout;
    private ApiInterface apiInterface;
    private String reqsub_auc_req_id;
    private TextView tv_auction_date, tv_commodity, tv_qty, tv_unit_price, tv_auction_code;
    private LinearLayout ll_root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_src_req_sub_auction_details);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        Intent intent = getIntent();
        reqsub_auc_req_id = intent.getStringExtra("reqsub_auc_req_id");
    }

    public void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Sub Auction Details");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);


        rv_sub_auction = findViewById(R.id.rv_sub_auction);
        subAuctionAdapter = new RequestSubAuctionAdapter(ScrRequestSubAuctionListing.this);
        rv_sub_auction.setAdapter(subAuctionAdapter);
        rv_sub_auction.setLayoutManager(new LinearLayoutManager(this));

        ll_no_data = findViewById(R.id.ll_no_data);
        tv_auction_date = findViewById(R.id.tv_auction_date);
        ll_root = findViewById(R.id.ll_root);

        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          call_SubAuctionDetailsApi();
                                      }
                                  }
        );
    }

    public void showEmptyLayout() {
        if (subAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    private void call_SubAuctionDetailsApi() {
        Map<String, String> params = new HashMap<>();
        params.put("reqsub_auc_req_id", String.valueOf(reqsub_auc_req_id));

        Call<String> call = apiInterface.get_sub_auc_list_by_request_id(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "onResponse:Sub Auction Details" + response.body());
                Log.d("TAG", "onResponse:Sub Auction Details Url " + call.request().url().toString());

                RequestSubAuctionList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray1 = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray1.length(); i++) {

                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            int reqsub_auc_id = jsonObject1.getInt("reqsub_auc_id");
                            int reqsub_auc_vendor_id = jsonObject1.getInt("reqsub_auc_vendor_id");
                            String reqsub_auc_qty = jsonObject1.getString("reqsub_auc_qty");
                            String reqsub_auc_location = jsonObject1.getString("reqsub_auc_location");
                            String reqsub_auc_unit_price = jsonObject1.getString("reqsub_auc_unit_price");
                            String reqsub_auc_emd = jsonObject1.getString("reqsub_auc_emd");
                           /* String current_time = jsonObject1.getString("current_time");
                            String end_date_time = jsonObject1.getString("end_date_time");
                            String vendor_name = jsonObject1.getString("vendor_name");*/
                            int reqsub_auc_status = jsonObject1.getInt("reqsub_auc_status");

                            RequestSubAuction requestSubAuction = new RequestSubAuction(reqsub_auc_id,reqsub_auc_vendor_id,reqsub_auc_qty,reqsub_auc_location,reqsub_auc_unit_price,reqsub_auc_emd,"","","",reqsub_auc_status);
                            RequestSubAuctionList.getInstance().add(requestSubAuction);
                            subAuctionAdapter.setList(RequestSubAuctionList.getInstance().getList());
                            subAuctionAdapter.notifyDataSetChanged();
                        }


                    }
                    swipe_refresh_layout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse:Exception " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);
                Log.d("TAG", "onFailure:Exception "+t.getMessage());

            }

        });
    }


    @Override
    public void onRefresh() {
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          call_SubAuctionDetailsApi();
                                      }
                                  }
        );
    }
}
