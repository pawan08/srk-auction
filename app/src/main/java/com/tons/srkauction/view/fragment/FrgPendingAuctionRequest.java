package com.tons.srkauction.view.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestAuction;
import com.tons.srkauction.model.RequestAuctionList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.view.adapter.PendingRequestAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FrgPendingAuctionRequest extends Fragment implements SwipeRefreshLayout.OnRefreshListener {

    private View view;
    private ApiInterface apiInterface;
    private RecyclerView rv_auction_request;
    private PendingRequestAuctionAdapter pendingRequestAuctionAdapter;
    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipe_refresh_layout;
    private Timer timer;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.frg_auction_request_pending, null);
        inItui();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return view;
    }

    private void inItui() {

        rv_auction_request = view.findViewById(R.id.rv_auction_request);
        pendingRequestAuctionAdapter = new PendingRequestAuctionAdapter(this);
        rv_auction_request.setAdapter(pendingRequestAuctionAdapter);
        rv_auction_request.setLayoutManager(new LinearLayoutManager(getActivity()));

        ll_no_data = view.findViewById(R.id.ll_no_data);
        swipe_refresh_layout = view.findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

    }

    @Override
    public void onResume() {
        super.onResume();

        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
                    call_Vendor_Auction_Request_Api();
                } else {
                    call_Broker_Auction_Request_Api();
                }
                Log.d("TIMER", "Updating");
            }
        };
        timer.schedule(timerTask, 2, 1000);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            Log.d("TIMER", "stopTimer: timer stopped.");
        }
        Log.d("TIMER", "stopTimer: end line");
    }

    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopTimer();
    }

    public void showEmptyLayout() {
        if (pendingRequestAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    private void call_Vendor_Auction_Request_Api() {
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("auction_status", String.valueOf(Constants.STATUS_INACTIVE));
        Call<String> call = apiInterface.ven_auction_request_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Vendor_Auction_Request_List" + response.body());
                Log.d("TAG", "Vendor_Auction_Request_List url " + call.request().url().toString());

                RequestAuctionList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int request_auction_id = jsonObject1.getInt("request_auction_id");
                            String request_auction_type = jsonObject1.getString("request_auction_type");
                            String request_auction_commodity = jsonObject1.getString("request_auction_commodity");
                            String request_auction_uom = jsonObject1.getString("request_auction_uom");
                            String request_auction_qty = jsonObject1.getString("request_auction_qty");
                            String request_auction_unit_price = jsonObject1.getString("request_auction_unit_price");
                            String request_auction_added_time = jsonObject1.getString("request_auction_added_time");
                            String request_auction_vendor = jsonObject1.getString("request_auction_vendor");
                            String request_auction_broker = jsonObject1.getString("request_auction_broker");
                            String broker_name = jsonObject1.getString("broker_name");
                            String request_auction_status = jsonObject1.getString("request_auction_status");
                            String request_auction_remark = jsonObject1.getString("request_auction_remark");
                            String sub_auction_count = jsonObject1.getString("sub_auction_count");
                            RequestAuction requestAuction = new RequestAuction(request_auction_id, request_auction_type, request_auction_commodity, request_auction_uom, request_auction_unit_price, request_auction_qty, request_auction_added_time, request_auction_vendor, request_auction_broker, broker_name, request_auction_status, request_auction_remark, sub_auction_count);
                            RequestAuctionList.getInstance().add(requestAuction);

                        }
                    }
                    swipe_refresh_layout.setRefreshing(false);
                    pendingRequestAuctionAdapter.setList(RequestAuctionList.getInstance().getList());
                    pendingRequestAuctionAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }

    private void call_Broker_Auction_Request_Api() {
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        params.put("auction_status", String.valueOf(Constants.WALLET_PENDING));
        Call<String> call = apiInterface.bro_auction_request_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Broker_Auction_Request_List" + response.body());
                Log.d("TAG", "Broker_Auction_Request_List url " + call.request().url().toString());

                RequestAuctionList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int request_auction_id = jsonObject1.getInt("request_auction_id");
                            String request_auction_type = jsonObject1.getString("request_auction_type");
                            String request_auction_commodity = jsonObject1.getString("request_auction_commodity");
                            String request_auction_uom = jsonObject1.getString("request_auction_uom");
                            String request_auction_qty = jsonObject1.getString("request_auction_qty");
                            String request_auction_unit_price = jsonObject1.getString("request_auction_unit_price");
                            String request_auction_added_time = jsonObject1.getString("request_auction_added_time");
                            String request_auction_vendor = jsonObject1.getString("request_auction_vendor");
                            String request_auction_broker = jsonObject1.getString("request_auction_broker");
                            String broker_name;

                            if (jsonObject1.has("broker_name")) {
                                broker_name = jsonObject1.getString("broker_name");

                            } else {
                                broker_name = "null";
                                Log.d("TAG", "broker_name" + broker_name);
                            }

                            String request_auction_status = jsonObject1.getString("request_auction_status");
                            String request_auction_remark = jsonObject1.getString("request_auction_remark");
                            RequestAuction requestAuction = new RequestAuction(request_auction_id, request_auction_type, request_auction_commodity, request_auction_uom, request_auction_unit_price, request_auction_qty, request_auction_added_time, request_auction_vendor, request_auction_broker, broker_name, request_auction_status, request_auction_remark, "");
                            RequestAuctionList.getInstance().add(requestAuction);
                        }
                    }
                    swipe_refresh_layout.setRefreshing(false);
                    pendingRequestAuctionAdapter.setList(RequestAuctionList.getInstance().getList());
                    pendingRequestAuctionAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onRefresh() {

        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            call_Vendor_Auction_Request_Api();
        } else {
            call_Broker_Auction_Request_Api();
        }
    }
}
