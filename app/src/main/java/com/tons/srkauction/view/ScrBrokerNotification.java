package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.BrokerNotification;
import com.tons.srkauction.model.BrokerNotificationList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.view.adapter.BrokerNotificationAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrBrokerNotification extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {

    private RecyclerView rv_broker_notification;
    private BrokerNotificationAdapter brokerNotificationAdapter;
    private ApiInterface apiInterface;

    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView iv_back;
    private TextView tv_header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_broker_notification);

        initUi();

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

    }

    public void initUi() {
        ll_no_data = findViewById(R.id.ll_no_data);

        rv_broker_notification = findViewById(R.id.rv_broker_notification);
        brokerNotificationAdapter = new BrokerNotificationAdapter(this);

        rv_broker_notification.setAdapter(brokerNotificationAdapter);
        rv_broker_notification.setLayoutManager(new GridLayoutManager(this, 1));

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        iv_back = findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_header = findViewById(R.id.tv_header);
        tv_header.setText("Notification");
    }

    public void showEmptyLayout() {
        if (brokerNotificationAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_Broker_Notifications_Api();
                                    }
                                }
        );
    }

    private void call_Broker_Notifications_Api() {
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        Call<String> call = apiInterface.notification_broker_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "GetBrokerNotification" + response.body());
                Log.d("TAG", "GetBrokerNotification url " + call.request().url().toString());

                BrokerNotificationList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int nb_id = jsonObject1.getInt("nb_id");
                            String nb_title = jsonObject1.getString("nb_title");
                            String nb_desc = jsonObject1.getString("nb_desc");
                            String nb_added_time = jsonObject1.getString("nb_added_time");

                            BrokerNotification brokerNotification = new BrokerNotification(nb_id, nb_title, nb_desc, nb_added_time);
                            BrokerNotificationList.getInstance().add(brokerNotification);

                            brokerNotificationAdapter.setList(BrokerNotificationList.getInstance().getList());
                            brokerNotificationAdapter.notifyDataSetChanged();
                        }
                    }
                    swipeRefreshLayout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);

            }

        });
    }


    @Override
    public void onRefresh() {
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_Broker_Notifications_Api();
                                    }
                                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
