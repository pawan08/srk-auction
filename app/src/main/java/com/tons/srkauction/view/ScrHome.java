package com.tons.srkauction.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.convertor.Convertor;
import com.tons.srkauction.model.DashboardSubAuction;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.adapter.DashboardSubAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrHome extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private String TAG = ScrHome.class.getName();

    private ApiInterface apiInterface;
    //qr code scanner object
    private IntentIntegrator qrScan;
    private DashboardSubAuctionAdapter dashboardSubAuctionAdapter;
    private ActionBarDrawerToggle drawerToggle;
    private NavigationView navigationView;

    private CardView cv_live_auctions;
    private CardView cv_upcoming_auctions;
    private CardView cv_requested_auctions;
    private CardView cv_completed_auctions;
    private CardView cv_approved_auctions;
    private CardView cv_pending_auctions;
    private CardView cv_rejected_auctions;
    private TextView tv_wallet_amt, tv_live_count, tv_upcoming_count, tv_complete_count;
    private TextView tv_approved_count, tv_pending_count, tv_rejected_count;
    private static int notification_count = 0;
    private RecyclerView rv_sub_auction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_scr_home);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("SRK Auction");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);

        drawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawerToggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.white));
        drawer.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        initDrawerItems();
        initUI();


    }

    public void initUI() {
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        qrScan = new IntentIntegrator(this);

        rv_sub_auction = findViewById(R.id.rv_sub_auction);
        cv_live_auctions = findViewById(R.id.cv_live_auctions);
        cv_live_auctions.setOnClickListener(this);
        cv_upcoming_auctions = findViewById(R.id.cv_upcoming_auctions);
        cv_upcoming_auctions.setOnClickListener(this);
        cv_requested_auctions = findViewById(R.id.cv_requested_auctions);
        cv_requested_auctions.setOnClickListener(this);
        cv_completed_auctions = findViewById(R.id.cv_completed_auctions);
        cv_completed_auctions.setOnClickListener(this);

        cv_approved_auctions = findViewById(R.id.cv_approved_auctions);
        cv_approved_auctions.setOnClickListener(this);
        cv_pending_auctions = findViewById(R.id.cv_pending_auctions);
        cv_pending_auctions.setOnClickListener(this);
        cv_rejected_auctions = findViewById(R.id.cv_rejected_auctions);
        cv_rejected_auctions.setOnClickListener(this);

        tv_wallet_amt = findViewById(R.id.tv_wallet_amt);

        tv_live_count = findViewById(R.id.tv_live_count);
        tv_upcoming_count = findViewById(R.id.tv_upcoming_count);
        tv_complete_count = findViewById(R.id.tv_complete_count);

        tv_approved_count = findViewById(R.id.tv_approved_count);
        tv_pending_count = findViewById(R.id.tv_pending_count);
        tv_rejected_count = findViewById(R.id.tv_rejected_count);

        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            cv_requested_auctions.setVisibility(View.VISIBLE);
        } else {
            cv_requested_auctions.setVisibility(View.GONE);
        }

        rv_sub_auction.setLayoutManager(new LinearLayoutManager(this));
        dashboardSubAuctionAdapter = new DashboardSubAuctionAdapter(this);
        ArrayList<DashboardSubAuction> list = new ArrayList<>();
        list.clear();
        list.add(new DashboardSubAuction("AUC12340", "REQ-SUB1230", "Wheat"
                , "100", "Pawan Chintala", "Pune", "5000"));
        list.add(new DashboardSubAuction("AUC12340", "REQ-SUB1230", "Rice"
                , "100", "Tabish Shaikh", "Mumbai", "10000"));
        list.add(new DashboardSubAuction("AUC12340", "REQ-SUB1230", "Wheat"
                , "100", "Pawan Chintala", "Pune", "5000"));
        list.add(new DashboardSubAuction("AUC12340", "REQ-SUB1230", "Rice"
                , "100", "Tabish Shaikh", "Mumbai", "10000"));

        rv_sub_auction.setAdapter(dashboardSubAuctionAdapter);
        rv_sub_auction.setNestedScrollingEnabled(false);
        dashboardSubAuctionAdapter.setList(list);
        dashboardSubAuctionAdapter.notifyDataSetChanged();
        Log.d(TAG, "initUI: itemcount" + dashboardSubAuctionAdapter.getItemCount());
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            call_get_dashboard_data();
        } else {
            call_get_dashboard_data_for_broker();
        }

        setNavHeader();
    }

    public void setNavHeader() {

        View header = navigationView.getHeaderView(0);
        TextView tv_name = header.findViewById(R.id.tv_name);
        TextView textView = header.findViewById(R.id.textView);
        TextView tv_login_type = header.findViewById(R.id.tv_login_type);
        ImageView imageView = header.findViewById(R.id.imageView);


        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            tv_name.setText(VendorData.getInstance().getVendorName());
            textView.setText(VendorData.getInstance().getVendorEmail());
            tv_login_type.setText("Login as vendor");
            Picasso.with(ScrHome.this).load(CommunicationConstant.VENDOR_PROFILE + VendorData.getInstance().getVendorProfile())
                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                    .into(imageView);

        } else if (UserData.getInstance().getUserType() == UserData.ROLE_BROKER) {
            tv_name.setText(BrokerData.getInstance().getBrokerName());
            textView.setText(BrokerData.getInstance().getBrokerEmail());
            tv_login_type.setText("Login as Broker");
            Picasso.with(ScrHome.this).load(CommunicationConstant.BROKER_PROFILE + BrokerData.getInstance().getBrokerProfile())
                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                    .into(imageView);

        }

    }

    private void initDrawerItems() {
        int userType = UserData.getInstance().getUserType();
        Menu menu = navigationView.getMenu();

        switch (userType) {
            case UserData.ROLE_VENDOR:
                menu.findItem(R.id.nav_auction_request).setVisible(true);
                menu.findItem(R.id.nav_scan_qr_code).setVisible(true);
                menu.findItem(R.id.nav_my_wallet).setVisible(true);
                menu.findItem(R.id.nav_my_wallet_withdraw).setVisible(true);
                menu.findItem(R.id.nav_pending_request).setVisible(true);
                menu.findItem(R.id.nav_rejected_requests).setVisible(true);
                menu.findItem(R.id.nav_view_sample).setVisible(true);
                menu.findItem(R.id.non_negotiable).setVisible(true);
                menu.findItem(R.id.negotiable).setVisible(true);
                menu.findItem(R.id.nav_offline_auction).setVisible(true);
                break;

            case UserData.ROLE_BROKER:
                menu.findItem(R.id.nav_auction_request).setVisible(true);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:

                break;

            case R.id.nav_profile:
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
                    CustomIntent.startActivity(ScrHome.this, ScrVendorProfile.class, false);
                } else {
                    CustomIntent.startActivity(ScrHome.this, ScrViewBrokerProfile.class, false);
                }
                break;

            case R.id.nav_auction:
                CustomIntent.startActivity(ScrHome.this, ScrAuctionTab.class, false);
                break;

            case R.id.nav_notifications:
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
                    CustomIntent.startActivity(ScrHome.this, ScrVendorNotification.class, false);
                } else {
                    CustomIntent.startActivity(ScrHome.this, ScrBrokerNotification.class, false);
                }
                break;

            case R.id.nav_auction_request:
                CustomIntent.startActivity(ScrHome.this, ScrRequestAuctionTab.class, false);
                break;

            case R.id.nav_scan_qr_code:
                qrScan.initiateScan();
                break;
            case R.id.nav_my_wallet:
                CustomIntent.startActivity(ScrHome.this, ScrWallet.class, false);
                break;


            case R.id.nav_my_wallet_withdraw:
                CustomIntent.startActivity(ScrHome.this, ScrWithdrawMoneyFromWallet.class, false);
                break;

            case R.id.nav_pending_request:
                CustomIntent.startActivity(ScrHome.this, ScrPendingRequestTab.class, false);
                break;
            case R.id.nav_rejected_requests:
                CustomIntent.startActivity(ScrHome.this, ScrRejectedRequestsTab.class, false);
                break;

            case R.id.nav_view_sample:
                CustomIntent.startActivity(ScrHome.this, ScrViewSample.class, false);
                break;

            case R.id.nav_request_offline_auction:
                CustomIntent.startActivity(ScrHome.this, ScrOfflineRequestAdd.class, false);
                break;

            case R.id.nav_offline_auction:
                CustomIntent.startActivity(ScrHome.this, ScrOfflineAuctionTab.class, false);
                break;

            case R.id.non_negotiable:
                CustomIntent.startActivity(ScrHome.this, ScrNonNegotiableTab.class, false);
                break;

            case R.id.negotiable:
                CustomIntent.startActivity(ScrHome.this, ScrNegotiableTab.class, false);
                break;

            case R.id.nav_logout:
                logoutDialog();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }


    public void call_logout_api() {
        Log.d(TAG, "call_logout_api: inside logout api");
        CustomDialog.showDialog(ScrHome.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();

        int id = 0;
        int type = 0;

        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            id = VendorData.getInstance().getVendorId();
            type = UserData.ROLE_VENDOR;
        } else {
            id = BrokerData.getInstance().getBrokerId();
            type = UserData.ROLE_BROKER;
        }

        params.put("id", id + "");
        params.put("type", type + "");

        Call<String> call = apiInterface.logout(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrHome.this);
                Log.d(TAG, "onResponse: logout " + response.body());
                Log.d(TAG, "onResponse: logout url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(response.body());
                    boolean status = object.getBoolean("status");
                    int message = object.getInt("message");

                    if (message == Messages.SUCCESS) {
                        VendorData.getInstance().remove();
                        BrokerData.getInstance().remove();
                        CustomIntent.startActivity(ScrHome.this, ScrSelectRole.class);
                    } else if (message == Messages.FAILED) {
                        CustomToast.showToast(ScrHome.this, "Failed to logout.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: JSONException : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrHome.this);
                Log.d(TAG, "onFailure: res " + t.getMessage());
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_op_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.notification);
        menuItem.setIcon(Convertor.convertLayoutToImage(ScrHome.this, notification_count, R.drawable.ic_baseline_notifications));
        Log.d(TAG, "onCreateOptionsMenu: " + notification_count);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        boolean val = false;
        switch (item.getItemId()) {
            case R.id.notification:
                if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
                    CustomIntent.startActivity(ScrHome.this, ScrVendorNotification.class, false);
                } else {
                    CustomIntent.startActivity(ScrHome.this, ScrBrokerNotification.class, false);
                }
                val = true;
                break;
            default:
                val = super.onOptionsItemSelected(item);
        }
        return val;
    }

    private void logoutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(ScrHome.this);
        builder.setMessage("Are you sure you want to logout?")
                .setTitle("Logout!")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        call_logout_api();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            //if qrcode has nothing in it
            if (result.getContents() == null) {
                Toast.makeText(this, "Result Not Found", Toast.LENGTH_LONG).show();
            } else {
                //if qr contains data
                Log.d(TAG, "onActivityResult: data : " + result.getContents());
                CustomToast.showToast(ScrHome.this, result.getContents() + "");
                Intent intent = new Intent(ScrHome.this, ScrSubAuctionDetails.class);
                intent.putExtra("reqsub_auc_id", result.getContents());
                startActivity(intent);

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }


    private void call_get_dashboard_data() {
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_dashboard_data(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetDahboardHistory" + response.body());
                    Log.d("TAG", "GetDahboardHistory url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String wallet_amount = jsonObject1.getString("wallet_amount");
                            String status_upcoming = jsonObject1.getString("status_upcoming");
                            String status_live = jsonObject1.getString("status_live");
                            String status_completed = jsonObject1.getString("status_completed");

                            String status_request_approved = jsonObject1.getString("status_request_approved");
                            String status_request_pending = jsonObject1.getString("status_request_pending");
                            String status_request_rejected = jsonObject1.getString("status_request_rejected");


                            notification_count = jsonObject1.getInt("notification_count");
                            Log.d(TAG, "onResponse: " + notification_count);
                            tv_wallet_amt.setText(wallet_amount);
                            tv_live_count.setText(status_live);
                            tv_upcoming_count.setText(status_upcoming);
                            tv_complete_count.setText(status_completed);

                            tv_approved_count.setText(status_request_approved);
                            tv_pending_count.setText(status_request_pending);
                            tv_rejected_count.setText(status_request_rejected);
                            invalidateOptionsMenu();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //swipeRefreshLayout.setRefreshing(false);

            }

        });
    }


    private void call_get_dashboard_data_for_broker() {
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        Call<String> call = apiInterface.get_dashboard_data_for_broker(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetDahboardHistory" + response.body());
                    Log.d("TAG", "GetDahboardHistory url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String wallet_amount = jsonObject1.getString("wallet_amount");
                            String status_upcoming = jsonObject1.getString("status_upcoming");
                            String status_live = jsonObject1.getString("status_live");
                            String status_completed = jsonObject1.getString("status_completed");

                            String status_request_approved = jsonObject1.getString("status_request_approved");
                            String status_request_pending = jsonObject1.getString("status_request_pending");
                            String status_request_rejected = jsonObject1.getString("status_request_rejected");


                            notification_count = jsonObject1.getInt("notification_count");
                            Log.d(TAG, "onResponse: " + notification_count);
                            tv_wallet_amt.setText(wallet_amount);
                            tv_live_count.setText(status_live);
                            tv_upcoming_count.setText(status_upcoming);
                            tv_complete_count.setText(status_completed);

                            tv_approved_count.setText(status_request_approved);
                            tv_pending_count.setText(status_request_pending);
                            tv_rejected_count.setText(status_request_rejected);
                            invalidateOptionsMenu();
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //swipeRefreshLayout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cv_live_auctions:
                Intent intent1 = new Intent(ScrHome.this, ScrAuctionTab.class);
                intent1.putExtra("position", Constants.AUCTION_LIVE);
                startActivity(intent1);
                break;

            case R.id.cv_upcoming_auctions:
                Intent i = new Intent(ScrHome.this, ScrAuctionTab.class);
                i.putExtra("position", Constants.AUCTION_UPCOMING);
                startActivity(i);
                break;

            case R.id.cv_requested_auctions:
                CustomIntent.startActivity(ScrHome.this, ScrWallet.class, false);
                break;

            case R.id.cv_completed_auctions:
                Intent intent = new Intent(ScrHome.this, ScrAuctionTab.class);
                intent.putExtra("position", Constants.AUCTION_COMPLETED);
                startActivity(intent);
                break;

            case R.id.cv_approved_auctions:
                Intent approved = new Intent(ScrHome.this, ScrRequestAuctionTab.class);
                approved.putExtra("position", Constants.AUCTION_APPROVED);
                startActivity(approved);
                break;

            case R.id.cv_pending_auctions:
                Intent pending = new Intent(ScrHome.this, ScrRequestAuctionTab.class);
                pending.putExtra("position", Constants.AUCTION_PENDING);
                startActivity(pending);
                break;

            case R.id.cv_rejected_auctions:
                Intent rejected = new Intent(ScrHome.this, ScrRequestAuctionTab.class);
                rejected.putExtra("position", Constants.AUCTION_REJECTED);
                startActivity(rejected);
                break;

            default:
                throw new IllegalStateException("Unexpected value: " + view.getId());
        }

    }
}