package com.tons.srkauction.view;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tons.srkauction.FolderCreator.FileUtils;
import com.tons.srkauction.FolderCreator.GRFile;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.Broker;
import com.tons.srkauction.model.BrokerList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.UOMCalculation;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class ScrSubAuctionDetails extends AppCompatActivity implements View.OnClickListener {

    private String TAG = ScrSubAuctionDetails.class.getName();
    private static final int PERMISSION_REQUEST_CODE = 1;

    private ApiInterface apiInterface;

    private ImageView iv_qr_code_image;
    private ImageView iv_click_qr;
    private ImageView headerImage;
    private TextView tv_vendor_name, tv_commodity, tv_emd, tv_auction_code, tv_bag_type, tv_no_of_bags, tv_qty, tv_location, tv_warehouse, tv_unit_price;
    private TextView headerText;
    private TextView tv_technical_doc;
    private TextView tv_time_ending;
    private TextView tv_timer;
    private TextView tv_assign_broker;
    private Button bt_participate;
    private Button bt_download;
    private LinearLayout ll_technical_doc;
    private DownloadManager downloadManager;
    private String reqsub_auc_id;
    private String auction_qr_code, add_money_into_wallet, pay_emd, reqsub_auc_auction_id, emd;
    private String auction_status, technical_doc, assigned_broker_name;
    private int wallet_status, vendor_id;
    int reqsub_auc_status;
    int auction_id;
    public Timer timer;
    boolean isFirstTimeForDialoge = true;
    boolean isFirstTimeForTimer = true;
    private String new_end_date_time = null;
    private Dialog dialog;
    private ImageView iv_assign_edit;
    private ArrayList<Broker> list = new ArrayList<>();
    private TextView tv_mine_auction, tv_request_code, tv_desc;
    private LinearLayout ll_assign_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_sub_auction_details);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        getIntentData();
        initUI();

    }

    public void getIntentData() {
        Bundle b = getIntent().getExtras();
        if (b != null) {
            reqsub_auc_id = b.getString("reqsub_auc_id");
            vendor_id = b.getInt("vendor_id");
            reqsub_auc_status = b.getInt("reqsub_auc_status");
            auction_id = b.getInt("auction_id");
        }
        Log.d("TAG", "Data vendor_id: " + vendor_id);
        Log.d("TAG", "Data reqsub_auc_status: " + reqsub_auc_status);
        Log.d("TAG", "Data auction_id: " + auction_id);
    }


    private void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        headerText = toolbar.findViewById(R.id.tv_header);
        headerImage = toolbar.findViewById(R.id.iv_back);
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerText.setText("Sub-Auction Details");

        tv_desc = findViewById(R.id.tv_desc);
        tv_request_code = findViewById(R.id.tv_request_code);
        tv_mine_auction = findViewById(R.id.tv_mine_auction);
        ll_assign_layout = findViewById(R.id.ll_assign_layout);
        tv_time_ending = findViewById(R.id.tv_time_ending);
        tv_timer = findViewById(R.id.tv_timer);
        tv_vendor_name = findViewById(R.id.tv_vendor_name);
        tv_assign_broker = findViewById(R.id.tv_assign_broker);
        tv_commodity = findViewById(R.id.tv_commodity);
        tv_emd = findViewById(R.id.tv_emd);
        tv_auction_code = findViewById(R.id.tv_auction_code);
        tv_bag_type = findViewById(R.id.tv_bag_type);
        tv_no_of_bags = findViewById(R.id.tv_no_of_bags);
        tv_qty = findViewById(R.id.tv_qty);
        tv_location = findViewById(R.id.tv_location);
        tv_warehouse = findViewById(R.id.tv_warehouse);
        tv_unit_price = findViewById(R.id.tv_unit_price);
        tv_technical_doc = findViewById(R.id.tv_technical_doc);
        iv_assign_edit = findViewById(R.id.iv_assign_edit);

        bt_participate = findViewById(R.id.bt_participate);
        bt_download = findViewById(R.id.bt_download);
        ll_technical_doc = findViewById(R.id.ll_technical_doc);
        iv_click_qr = findViewById(R.id.iv_click_qr);
        bt_participate.setOnClickListener(this);
        iv_click_qr.setOnClickListener(this);
        bt_download.setOnClickListener(this);
        tv_assign_broker.setOnClickListener(this);
        iv_assign_edit.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSubAuctionDetails();
        if (isStoragePermissionGranted(this)) {
            FileUtils.createFolder(this);
        }
        startTimer();
        //getAuctionEndTime();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
    }

    private void startTimer() {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                getAuctionEndTime();
                Log.d("TIMER", "Updating");
            }
        };
        timer.schedule(timerTask, 1, 1000);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            Log.d("TIMER", "stopTimer: timer stopped.");
        }
        Log.d("TIMER", "stopTimer: end line");
    }

    public void showDialogBroker() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_assign_broker);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setCancelable(true);
        final SearchableSpinner sp_broker = dialog.findViewById(R.id.sp_broker);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        final TextView tv_error = dialog.findViewById(R.id.tv_error);
        Button tv_submit = dialog.findViewById(R.id.tv_submit);
        initBrokerList(sp_broker);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sp_broker.getSelectedItemPosition() == 0) {
                    tv_error.setVisibility(VISIBLE);
                    tv_error.setText("Please Select Broker");
                    return;

                }
                tv_assign_broker.setClickable(false);
                tv_assign_broker.setText("Broker Assigned : - " + sp_broker.getSelectedItem().toString());

                assignBrokerApi(BrokerList.getInstance().getVendorsID(sp_broker.getSelectedItem().toString()), tv_error);
            }
        });
        dialog.setCancelable(true);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                getSubAuctionDetails();
            }
        });
    }

    public void showupdateDialogBroker() {

        dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_assign_broker);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        dialog.setCancelable(true);
        final SearchableSpinner sp_broker = dialog.findViewById(R.id.sp_broker);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        final TextView tv_error = dialog.findViewById(R.id.tv_error);
        Button tv_submit = dialog.findViewById(R.id.tv_submit);
        initBrokerList(sp_broker);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (sp_broker.getSelectedItemPosition() == 0) {
                    tv_error.setVisibility(VISIBLE);
                    tv_error.setText("Please Select Broker");
                    return;

                }
                tv_assign_broker.setClickable(false);
                tv_assign_broker.setText("Broker Assigned : - " + sp_broker.getSelectedItem().toString());

                updateBrokerApi(BrokerList.getInstance().getVendorsID(sp_broker.getSelectedItem().toString()));
            }
        });
        dialog.setCancelable(true);
        dialog.show();
        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                getSubAuctionDetails();
            }
        });
    }

    public void assignBrokerApi(int broker_id, final TextView tv_error) {

        CustomDialog.showDialog(ScrSubAuctionDetails.this, "Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("br_auction_id", String.valueOf(auction_id));
        params.put("br_broker_id", String.valueOf(broker_id));
        params.put("br_vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("br_sub_auction_id", reqsub_auc_id);
        Call<String> call = apiInterface.assign_broker_to_auction(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                Log.d("TAG", "Assign_broker" + response.body());
                Log.d("TAG", "Assign_broker url " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        CustomToast.showToast(ScrSubAuctionDetails.this, "Broker Assigned Successfully");
                        dialog.dismiss();
                    } else if (message == Messages.ALREADY_EXIST) {
                        tv_error.setVisibility(VISIBLE);
                        tv_error.setText("Selected broker is already assigned");
                    } else {
                        CustomToast.showToast(ScrSubAuctionDetails.this, "Failed TO Assigned Broker");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
            }
        });
    }

    public void updateBrokerApi(int broker_id) {

        CustomDialog.showDialog(ScrSubAuctionDetails.this, "Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("broker_id", String.valueOf(broker_id));
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("sub_auction_id", reqsub_auc_id);
        Call<String> call = apiInterface.update_assigned_broker(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                Log.d("TAG", "update_broker" + response.body());
                Log.d("TAG", "update_broker url " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        CustomToast.showToast(ScrSubAuctionDetails.this, "Broker Assigned Successfully");
                        dialog.dismiss();
                        iv_assign_edit.setVisibility(VISIBLE);
                    } else {
                        CustomToast.showToast(ScrSubAuctionDetails.this, "Failed TO Assigned Broker");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
            }
        });
    }

    public void initBrokerList(final SearchableSpinner sp_broker) {

        Call<String> call = apiInterface.bro_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("TAG", "onResponse: getVendorList : " + response.body());
                Log.d("TAG", "onResponse: getVendorList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    BrokerList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int broker_id = jsonObject1.getInt("broker_id");
                            String broker_name = jsonObject1.getString("broker_name");
                            Broker broker = new Broker(broker_id, broker_name);
                            BrokerList.getInstance().add(broker);
                            ArrayAdapter<String> vendor_adapter = new ArrayAdapter<>(ScrSubAuctionDetails.this, android.R.layout.simple_list_item_1, BrokerList.getInstance().getNames());
                            sp_broker.setAdapter(vendor_adapter);
                            vendor_adapter.notifyDataSetChanged();

                            Log.d(TAG, "onResponse: broker_name" + assigned_broker_name);
                            int position = vendor_adapter.getPosition(assigned_broker_name);
                            sp_broker.setSelection(position);
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }


    private void getSubAuctionDetails() {
        CustomDialog.showDialog(ScrSubAuctionDetails.this, "Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("reqsub_auc_id", reqsub_auc_id);
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_sub_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                Log.d("TAG", "onResponse: subauction " + response.body());
                Log.d("TAG", "onResponse: subauction url  " + call.request().url());
                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            tv_vendor_name.setText(jsonObject1.getString("vendor_name"));
                            tv_emd.setText(jsonObject1.getString("reqsub_auc_emd"));
                            tv_auction_code.setText(jsonObject1.getString("auction_code"));
                            tv_bag_type.setText(jsonObject1.getString("reqsub_auc_bag_type"));
                            tv_commodity.setText(jsonObject1.getString("auction_commodity"));
                            tv_no_of_bags.setText(jsonObject1.getString("reqsub_auc_no_of_bag"));
                            tv_qty.setText(jsonObject1.getString("reqsub_auc_qty"));
                            tv_request_code.setText(jsonObject1.getString("reqsub_auc_code"));
                            tv_desc.setText(jsonObject1.getString("reqsub_auc_desc"));
                            tv_warehouse.setText(jsonObject1.getString("reqsub_auc_warehouse"));
                            tv_location.setText(jsonObject1.getString("reqsub_auc_location"));
                            tv_unit_price.setText(UOMCalculation.calculateUOM(jsonObject1.getDouble("reqsub_auc_unit_price")));
                            tv_technical_doc.setText(jsonObject1.getString("reqsub_auc_technical_doc"));
                            technical_doc = jsonObject1.getString("reqsub_auc_technical_doc");
                            auction_qr_code = jsonObject1.getString("reqsub_auc_qr_code");

                            pay_emd = jsonObject1.getString("pay_emd");
                            add_money_into_wallet = jsonObject1.getString("add_money_into_wallet");
                            wallet_status = jsonObject1.getInt("wallet_status");
                            int reqsub_auc_vendor_id = jsonObject1.getInt("reqsub_auc_vendor_id");
                            emd = jsonObject1.getString("reqsub_auc_emd");
                            reqsub_auc_auction_id = jsonObject1.getString("reqsub_auc_auction_id");
                            auction_status = jsonObject1.getString("auction_status");

                            String current_time = jsonObject1.getString("current_time");
                            String end_date_time = jsonObject1.getString("end_date_time");
                            assigned_broker_name = jsonObject1.getString("assigned_broker_name");
                            String is_broker_assigned = jsonObject1.getString("is_broker_assigned");

                            if (is_broker_assigned.equals("NO")) {
                                tv_assign_broker.setClickable(true);
                                tv_assign_broker.setText("Assign Broker");
                                iv_assign_edit.setVisibility(View.GONE);
                            } else {
                                tv_assign_broker.setClickable(false);
                                tv_assign_broker.setText("Broker Assigned : - " + assigned_broker_name);
                                iv_assign_edit.setVisibility(VISIBLE);
                            }

                            if (auction_status.equals(Constants.STATUS_COMPLETED)) {
                                bt_participate.setVisibility(View.GONE);
                            } else {
                                bt_participate.setVisibility(View.VISIBLE);
                            }

                            if (pay_emd.equals("YES")) {
                                Log.d("TAG", "onClick: pay emd");
                                int status = wallet_status;
                                switch (status) {

                                    case Constants.WALLET_PENDING:
                                        bt_participate.setText("EMD paid waiting for approval");
                                        break;

                                    case Constants.WALLET_APPROVED:
                                        Log.d("TAG", "onClick: approved");
                                        if (reqsub_auc_status == Constants.AUCTION_LIVE) {
                                            bt_participate.setText("Participate");
                                        } else {
                                            bt_participate.setText("Auction Not Live");
                                        }

                                        break;

                                    case Constants.WALLET_REJECT:
                                        bt_participate.setText("EMD Rejected");
                                        payEMDAgain(String.valueOf(emd), String.valueOf(reqsub_auc_id));
                                        break;
                                }
                            } else {
                                Log.d("TAG", "onClick: above switch ");

                                if (add_money_into_wallet.equals("YES")) {
                                    bt_participate.setText("Add Money");
                                } else {
                                    bt_participate.setText("Participate");
                                    Log.d("TAG", "onClick: EMD = " + emd);
                                }

                                Log.d("TAG", "onClick: below switch ");
                            }

                         /*   if (VendorData.getInstance().getVendorId() == reqsub_auc_vendor_id) {
                                bt_participate.setVisibility(View.GONE);
                                tv_mine_auction.setVisibility(VISIBLE);
                                ll_assign_layout.setVisibility(View.GONE);
                            } else {
                                tv_mine_auction.setVisibility(View.GONE);
                                bt_participate.setVisibility(View.VISIBLE);
                            }*/

                            if (reqsub_auc_status == Constants.AUCTION_LIVE) {
                                Log.d(TAG, "onResponse: timer visible");
                                tv_timer.setVisibility(View.VISIBLE);
                                timer_calculation(current_time, end_date_time + ":00");
                            } else {
                                Log.d(TAG, "onResponse: timer gone");
                                tv_timer.setVisibility(View.GONE);
                            }


                            if (technical_doc.equals("") || technical_doc.isEmpty()) {
                                ll_technical_doc.setVisibility(View.GONE);
                                bt_download.setVisibility(View.GONE);
                            } else {
                                Log.d(TAG, "onResponse: technical_doc" + technical_doc);
                            }

                            if (!checkDocDownloadedOrNot(technical_doc)) {
                                bt_download.setText("Download");
                            } else {
                                bt_download.setText("Downloaded");
                            }

                        }

                    } else {
                        CustomToast.showToast(ScrSubAuctionDetails.this, "Failed To Add Bid");

                    }

                } catch (
                        JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                Log.d("TAG", "onFailure: " + t.getMessage());
                CustomToast.showToast(ScrSubAuctionDetails.this, Constants.WENT_WRONG);
            }
        });
    }

    private void getAuctionEndTime() {
        Call<String> call = apiInterface.get_auction_end_time(auction_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(TAG, "onResponse: getAuctionEndTime " + response.body());
                Log.d(TAG, "onResponse: getAuctionEndTime url" + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        JSONObject object = jsonArray.getJSONObject(0);
                        final String current_time = object.getString("current_time");
                        String auction_start_date = object.getString("auction_start_date");
                        String auction_start_time = object.getString("auction_start_time");
                        String end_date_time = object.getString("end_date_time");


                        Log.d(TAG, "onResponse: current_time : " + current_time + " end_date_time : " + end_date_time);
                        if (new_end_date_time == null) {
                            new_end_date_time = end_date_time;
                        } else {
                            if (new_end_date_time.equals(end_date_time)) {
                                Log.d(TAG, "onResponse: both equal");
                                if (isFirstTimeForTimer) {
                                    isFirstTimeForTimer = false;

                                    final Handler handler = new Handler(Looper.getMainLooper());
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //Do something after 1s
                                            Log.d("TAG", "Updating");
                                            timer_calculation(current_time, new_end_date_time + ":00");
                                            handler.postDelayed(this, 1000);
                                        }
                                    }, 1000);
                                }
                                return;
                            } else {
                                new_end_date_time = end_date_time;
                                isFirstTimeForTimer = true;
                            }


                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSON Exception : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void timer_calculation(String start_date_time, String end_date_time) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {

                futureDate = dateFormat.parse(start_date_time);
                futureDate1 = dateFormat.parse(end_date_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                // tv_timer.setText("" + String.format("%02d", days));
                int minute = Integer.parseInt(String.format("%02d", minutes));
                int second = Integer.parseInt(String.format("%02d", seconds));
                Log.d("TAG", "updateDateTime: minutes : " + minute + " seconds : " + second);
                tv_timer.setText("Starts in " + String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                //tv_timer.setText(""+ String.format("%02d", minutes));
                //tv_timer.setText(""+ String.format("%02d", seconds));
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    Log.d("TAG", "updateDateTime: Hours : " + hours + " minutes : " + minutes + " seconds : " + seconds);
                    if (hours == 0 && minutes < 10) {
                        tv_time_ending.setVisibility(VISIBLE);
                    } else {
                        tv_time_ending.setVisibility(View.GONE);
                    }
                    tv_timer.setText("Ends In " + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                } else {
                    tv_timer.setText("The Auction has finished!");

                    if (isFirstTimeForDialoge) {
                        isFirstTimeForDialoge = false;

                        if (tv_timer.getText().toString().equals("The Auction has finished!")) {
                            if (tv_timer.getVisibility() == VISIBLE) {
                                showDialog();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDialog() {
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.text_black));
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ScrSubAuctionDetails.this);

        builder1.setTitle("Auction Ends");
        builder1.setMessage("Better luck next time. Auction has finished");

        builder1.setCancelable(false);
        builder1.setPositiveButton("Try Next Time", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.setCancelable(false);
        alert11.setCanceledOnTouchOutside(false);
        alert11.show();
    }


    public void showDialog(final String emd_amount, final String reqsub_auc_id) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("Participate");
        tv_text.setText("Pay EMD to participate in this auction");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Pay Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call_pay_emd_api(emd_amount, reqsub_auc_id);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public static boolean isStoragePermissionGranted(Activity activity) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSION_REQUEST_CODE);
                return false;
            }
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 1 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    FileUtils.createFolder(this);
                }
                break;
            default:
                break;
        }
    }

    private boolean isFileExists(String document_name) {
        String path = GRFile.getDOCFilePath() + document_name;
        File file = new File(path);
        return file.exists();
    }

    public boolean checkDocDownloadedOrNot(String image_name) {
        boolean flag;
        if (!isFileExists(image_name)) {
            flag = false;
            Log.d(TAG, "checkImageDownloadedOrNot: download image");
        } else {
            flag = true;
            Log.d(TAG, "checkImageDownloadedOrNot: already download image");
            // tv_download_image.setText("Downloaded");
        }
        return flag;
    }

    private void startDownload(String image_name, Button bt_download) {

        String path = GRFile.getDOCFilePath() + image_name;
        if (!isFileExists(image_name)) {
            bt_download.setText("Download");
            call_api_for_downloadPDF(image_name, path, bt_download);
        } else {
            bt_download.setText("Downloaded");
            Log.d(TAG, "startDownload: image already downloaded");
            CustomToast.showToast(ScrSubAuctionDetails.this, "Already Downloaded");
        }
    }

    public void call_api_for_downloadPDF(final String file_name, final String path, final Button bt_download) {
        CustomDialog.showDialog(ScrSubAuctionDetails.this, "Downloading Image...");
        Log.d(TAG, "call_api_for_downloadPDF: image url : " + CommunicationConstant.TECHNICAL_DOC + file_name);
        Call<ResponseBody> call = apiInterface.dowloadAnyFile(CommunicationConstant.TECHNICAL_DOC + file_name);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                if (response.isSuccessful()) {
                    CustomToast.showToast(ScrSubAuctionDetails.this, "Download Success..!");
                    Log.d(TAG, "onResponse: server contacted and has file");

                    new AsyncTask<Void, Void, Void>() {
                        @SuppressLint("StaticFieldLeak")
                        @Override
                        protected Void doInBackground(Void... voids) {
                            boolean writtenToDisk = writeResponseBodyToDisk(path, response.body());

                            Log.d(TAG, "onResponse: file download was a success? " + writtenToDisk);
                            CustomToast.showToast(ScrSubAuctionDetails.this, "File Downloaded Successfully");
                            if (writtenToDisk) {

                            }
                            return null;
                        }
                    }.execute();

                    if (isFileExists(file_name)) {
                        bt_download.setText("Downloaded");
                    } else {
                        bt_download.setText("Download");
                    }
                } else {
                    CustomToast.showToast(ScrSubAuctionDetails.this, "Download Failed ..Try again!");
                    Log.d(TAG, "onResponse:server contact failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d(TAG, "onResponse: failed");
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
            }
        });
    }

    private boolean writeResponseBodyToDisk(String path, ResponseBody body) {
        try {
            // todo change the file location/name according to your needs

            File futureStudioIconFile = new File(path);

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try {
                byte[] fileReader = new byte[4096];

                long fileSize = body.contentLength();
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(futureStudioIconFile);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
                }

                outputStream.flush();

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public void payEMDAgain(final String emd_amount, final String reqsub_auc_id) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("EMD Rejected");
        tv_text.setText("EMD is rejected , pay again to continue");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Pay Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call_pay_emd_api(emd_amount, reqsub_auc_id);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void showAddMoneyDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("Insufficient Fund");
        tv_text.setText("Add money to participate in the auction");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Add Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomIntent.startActivity(ScrSubAuctionDetails.this, ScrAddMoneyToWallet.class, false);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void showSuccessDialog(final String reqsub_auc_auction_id, final String reqsub_auc_id) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_login_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_header = dialog.findViewById(R.id.tv_header);
        TextView success_message = dialog.findViewById(R.id.success_message);
        success_message.setText("Emd Paid Successfully amount deducted from wallet");
        tv_header.setText("Success");
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(ScrSubAuctionDetails.this, ScrBidding.class);
                intent.putExtra("reqsub_auc_auction_id", reqsub_auc_auction_id);
                intent.putExtra("reqsub_auc_id", reqsub_auc_id);
                startActivity(intent);
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    private void call_pay_emd_api(String emd_amount, final String reqsub_auc_id) {
        CustomDialog.showDialog(ScrSubAuctionDetails.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", VendorData.getInstance().getVendorId() + "");
        params.put("reqsub_auc_id", reqsub_auc_id);
        params.put("sub_auction_emd", emd_amount);
        Call<String> call = apiInterface.pay_emd(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
                Log.d("TAG", "onResponse: call_pay_emd_api " + response.body());
                Log.d("TAG", "onResponse: call_pay_emd_api url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(response.body());
                    int message = object.getInt("message");
                    switch (message) {

                        case Messages.SUCCESS:
                            CustomToast.showToast(ScrSubAuctionDetails.this, "Emd paid successfully.");

                            /*Intent rejected = new Intent(ScrSubAuctionDetails.this, ScrPendingRequestTab.class);
                            rejected.putExtra("position", Constants.PENDING_EMD);
                            startActivity(rejected);
                            finish();*/
                            showSuccessDialog(reqsub_auc_auction_id, reqsub_auc_id);
                            break;

                        case Messages.FAILED:
                            CustomToast.showToast(ScrSubAuctionDetails.this, "Failed to pay emd.");
                            break;

                        case Messages.INSUFFICIENT_WALLET_AMOUNT:
                            CustomToast.showToast(ScrSubAuctionDetails.this, "Insufficient wallet amount.");
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrSubAuctionDetails.this);
            }
        });

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_participate:

                if (pay_emd.equals("YES")) {
                    Log.d("TAG", "onClick: pay emd");
                    int status = wallet_status;
                    switch (status) {

                        case Constants.WALLET_PENDING:
                            MyCustomDialog.showValidationDialog("Your EMD is pending, please wait for approval.", "Pending", ScrSubAuctionDetails.this);
                            break;

                        case Constants.WALLET_APPROVED:
                            Log.d("TAG", "onClick: approved");
                            if (reqsub_auc_status == Constants.AUCTION_LIVE) {

                                Intent intent = new Intent(ScrSubAuctionDetails.this, ScrBidding.class);
                                intent.putExtra("reqsub_auc_id", String.valueOf(reqsub_auc_id));
                                intent.putExtra("reqsub_auc_auction_id", String.valueOf(reqsub_auc_auction_id));
                                startActivity(intent);
                            } else {
                                // bt_participate.setText("Auction Not Live");
                                MyCustomDialog.showValidationDialog("Auction is not live.Please wait till it is live.", "Alert!", ScrSubAuctionDetails.this);
                            }

                            break;

                        case Constants.WALLET_REJECT:
                            payEMDAgain(String.valueOf(emd), String.valueOf(reqsub_auc_id));
                            break;
                    }
                } else {
                    Log.d("TAG", "onClick: above switch ");

                    if (add_money_into_wallet.equals("YES")) {

                        showAddMoneyDialog();
                    } else {
                        bt_participate.setText("Participate");
                        Log.d("TAG", "onClick: EMD = " + emd);
                        showDialog(String.valueOf(emd), String.valueOf(reqsub_auc_id));
                    }

                    Log.d("TAG", "onClick: below switch ");
                }

                break;

            case R.id.iv_click_qr:
                Intent intent = new Intent(ScrSubAuctionDetails.this, ScrViewImage.class);
                intent.putExtra("image", auction_qr_code);
                intent.putExtra("title", "QR Code");
                startActivity(intent);
                break;

            case R.id.bt_download:

                startDownload(technical_doc, bt_download);
                break;

            case R.id.tv_assign_broker:
                showDialogBroker();
                break;

            case R.id.iv_assign_edit:
                showupdateDialogBroker();
                break;
        }
    }

}