package com.tons.srkauction.view;

import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.model.UMO;
import com.tons.srkauction.model.UMOList;
import com.tons.srkauction.model.Vendor;
import com.tons.srkauction.model.VendorList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrAddBrokerAuctionRequest extends AppCompatActivity implements View.OnClickListener {

    private String TAG = ScrAddBrokerAuctionRequest.class.getName();
    private ArrayList<String> auction_type_list;
    private ArrayAdapter<String> auction_type_adapter;

    private ArrayList<String> auction_commodity_list;
    private ArrayAdapter<String> auction_commodity_adapter;

    //private AppCompatSpinner sp_auction_type;
    private SearchableSpinner sp_auction_commodity, sp_vendor;
    private SearchableSpinner sp_uom;
    private EditText et_unit_price;
    private EditText et_location;
    private EditText et_commodity_desc;
    private EditText et_qty;
    private Button bt_submit;

    private ApiInterface apiInterface;

    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_add_broker_auction_request);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        //initAuctiontype();
        initAuctioncommodity();
        initVendorList();
        initUom();
    }


    private void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Add Auction Request");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sp_uom = findViewById(R.id.sp_uom);
        et_qty = findViewById(R.id.et_qty);
        et_unit_price = findViewById(R.id.et_unit_price);
        et_location = findViewById(R.id.et_location);
        et_commodity_desc = findViewById(R.id.et_commodity_desc);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        sp_auction_commodity = findViewById(R.id.sp_auction_commodity);
        //sp_auction_type = findViewById(R.id.sp_auction_type);
        sp_vendor = findViewById(R.id.sp_vendor);


    }

    private void initAuctiontype() {

        auction_type_list = new ArrayList<>();
        auction_type_list.add(0, "Select auction type");
        auction_type_list.add(1, "Forward");
        auction_type_list.add(2, "Reverse");

        auction_type_adapter = new ArrayAdapter<String>(ScrAddBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, auction_type_list);
        //sp_auction_type.setAdapter(auction_type_adapter);
        auction_type_adapter.notifyDataSetChanged();
    }


    public void initAuctioncommodity() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getCommodityList : " + response.body());
                Log.d(TAG, "onResponse: getCommodityList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            auction_commodity_adapter = new ArrayAdapter<>(ScrAddBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, CommodityList.getInstance().getCommodityNames());
                            sp_auction_commodity.setAdapter(auction_commodity_adapter);
                            auction_commodity_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    public void initVendorList() {

        Call<String> call = apiInterface.ven_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getVendorList : " + response.body());
                Log.d(TAG, "onResponse: getVendorList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    VendorList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int vendor_id = jsonObject1.getInt("vendor_id");
                            String vendor_name = jsonObject1.getString("vendor_name");
                            Vendor Vendor = new Vendor(vendor_id, vendor_name);
                            VendorList.getInstance().add(Vendor);
                            ArrayAdapter<String> vendor_adapter = new ArrayAdapter<>(ScrAddBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, VendorList.getInstance().getNames());
                            sp_vendor.setAdapter(vendor_adapter);
                            vendor_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "KG");
        uom_list.add(2, "LTR");

        uom_adapter = new ArrayAdapter<String>(ScrAddBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);
        uom_adapter.notifyDataSetChanged();
    }

    public void initUmoList() {

        Call<String> call = apiInterface.get_uom();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d(TAG, "onResponse: getUmoList : " + response.body());
                Log.d(TAG, "onResponse: getUmo url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    UMOList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: UMO " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int master_id = jsonObject1.getInt("master_id");
                            String master_item_name = jsonObject1.getString("master_item_name");

                            UMO umo = new UMO(master_id, master_item_name);
                            UMOList.getInstance().add(umo);
                            ArrayAdapter<String> umo_adapter = new ArrayAdapter<>(ScrAddBrokerAuctionRequest.this, android.R.layout.simple_list_item_1, UMOList.getInstance().getmaster_item_name());
                            sp_uom.setAdapter(umo_adapter);
                            umo_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: UMO " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void validate() {

        String qty = et_qty.getText().toString();
        String unit_price = et_unit_price.getText().toString();
        String location = et_location.getText().toString();
        String et_com_desc = et_commodity_desc.getText().toString();

        if (sp_vendor.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select vendor", "Oop's", this);
            return;
        }

        if (sp_auction_commodity.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select auction commodity", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(qty)) {
            MyCustomDialog.showValidationDialog("Please enter quantity", "Oop's", this);
            return;
        }
        if (qty.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter quantity greater than zero", "Oop's", this);
            return;
        }


        if (TextUtils.isEmpty(unit_price)) {
            MyCustomDialog.showValidationDialog("Please enter unit price", "Oop's", this);
            return;
        }

        if (unit_price.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter unit price greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(location)) {
            MyCustomDialog.showValidationDialog("Please enter pick up location", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(et_com_desc)) {
            MyCustomDialog.showValidationDialog("Please enter commodity description", "Oop's", this);
            return;
        }

        //CustomToast.showToast(ScrAddBrokerAuctionRequest.this, "Call API.");
        addAuctionRequest();
    }

    public void addAuctionRequest() {

        CustomDialog.showDialog(ScrAddBrokerAuctionRequest.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        /*if (sp_auction_type.getSelectedItem().toString().equals("Forward")) {
            params.put("request_auction_type", "F");
        } else {
            params.put("request_auction_type", "R");
        }*/
        params.put("request_auction_type", "FORWARD");
        params.put("request_auction_commodity", sp_auction_commodity.getSelectedItem().toString());
        params.put("request_auction_uom", sp_uom.getSelectedItem().toString());
        params.put("request_auction_commodity_desc", et_commodity_desc.getText().toString());
        params.put("request_auction_qty", et_qty.getText().toString());
        params.put("request_auction_unit_price", et_unit_price.getText().toString());
        params.put("request_auction_pickup_location", et_location.getText().toString());
        params.put("request_auction_vendor", String.valueOf(VendorList.getInstance().getVendorsID(sp_vendor.getSelectedItem().toString())));
        params.put("request_auction_broker", String.valueOf(BrokerData.getInstance().getBrokerId()));

        Call<String> call = apiInterface.add_auction_request(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrAddBrokerAuctionRequest.this);
                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        CustomToast.showToast(ScrAddBrokerAuctionRequest.this, "Auction added successfully");
                        finish();
                    } else {
                        CustomToast.showToast(ScrAddBrokerAuctionRequest.this, "Failed to add auction request");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onResponse: Vendor add Auction " + "Failed");
                CustomToast.showToast(ScrAddBrokerAuctionRequest.this, Constants.WENT_WRONG);
                CustomDialog.closeDialog(ScrAddBrokerAuctionRequest.this);
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_submit:
                validate();
                break;
        }
    }
}