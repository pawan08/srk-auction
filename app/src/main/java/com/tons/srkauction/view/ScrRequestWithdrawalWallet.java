package com.tons.srkauction.view;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestWithdrawWalletModel;
import com.tons.srkauction.model.RequestedWithdrawWalletModelList;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.view.adapter.RequestWithdrawalFromWalletAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrRequestWithdrawalWallet extends AppCompatActivity implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    private TextView headerText;
    private ImageView headerImage;
    private FloatingActionButton fb_add;
    private RecyclerView rv_withdraw_wallet;
    private RequestWithdrawalFromWalletAdapter requestWithdrawalFromWalletAdapter;
    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipe_refresh_layout;

    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_requested_withdraw_from_wallet);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        initUI();
        headerText.setText("Request Withdrawal From Wallet");
    }

    private void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        headerText = toolbar.findViewById(R.id.tv_header);
        headerImage = toolbar.findViewById(R.id.iv_back);
        fb_add = findViewById(R.id.fb_add);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        rv_withdraw_wallet = findViewById(R.id.rv_request_wallet);
        requestWithdrawalFromWalletAdapter = new RequestWithdrawalFromWalletAdapter(ScrRequestWithdrawalWallet.this);
        rv_withdraw_wallet.setAdapter(requestWithdrawalFromWalletAdapter);
        rv_withdraw_wallet.setLayoutManager(new LinearLayoutManager(this));

        fb_add.setOnClickListener(this);
        headerImage.setOnClickListener(this);

        ll_no_data = findViewById(R.id.ll_no_data);

        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

    }

    public void showEmptyLayout() {
        if (requestWithdrawalFromWalletAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        call_withdraw_request_wallet();

    }

    private void call_withdraw_request_wallet() {
        Map<String, String> params = new HashMap<>();
        //params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        params.put("vendor_id", "8");

        Call<String> call = apiInterface.get_withdrawal_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Wallet_Request_List" + response.body());
                Log.d("TAG", "Wallet_Request_List url " + call.request().url().toString());
                RequestedWithdrawWalletModelList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String w_amount = jsonObject1.getString("w_amount");
                            String w_added_time = jsonObject1.getString("w_added_time");

                            RequestWithdrawWalletModel requestWithdrawWalletModel = new RequestWithdrawWalletModel(w_added_time, w_amount);

                            RequestedWithdrawWalletModelList.getInstance().add(requestWithdrawWalletModel);


                        }
                    }
                    requestWithdrawalFromWalletAdapter.setList(RequestedWithdrawWalletModelList.getInstance().getList());
                    requestWithdrawalFromWalletAdapter.notifyDataSetChanged();
                    swipe_refresh_layout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.fb_add:
                CustomIntent.startActivity(ScrRequestWithdrawalWallet.this, ScrWithdrawMoneyFromWallet.class, false);
                break;

            case R.id.iv_back:
                finish();
                break;
        }
    }

    @Override
    public void onRefresh() {
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);

                                          call_withdraw_request_wallet();


                                      }
                                  }
        );

    }
}
