package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import com.tons.srkauction.R;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomIntent;

public class ScrSplash extends AppCompatActivity {

    private static final int TIME = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.WHITE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            }
        }
        setContentView(R.layout.activity_scr_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startNextScreen();
    }

    public void startNextScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //CustomIntent.startActivity(ScrSplash.this, ScrSelectRole.class, true);
                if (VendorData.getInstance().getVendorMobile() != null || BrokerData.getInstance().getBrokerMobile() != null) {
                    CustomIntent.startActivity(ScrSplash.this, ScrHome.class, true);
                } else {
                    CustomIntent.startActivity(ScrSplash.this, ScrSelectRole.class, true);
                }
            }
        }, TIME);
    }

}