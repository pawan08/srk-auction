package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgApprovedAuctionRequest;
import com.tons.srkauction.view.fragment.FrgNonNegotiableApprovedListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiablePendingListing;
import com.tons.srkauction.view.fragment.FrgNonNegotiableRejectedListing;
import com.tons.srkauction.view.fragment.FrgPendingAuctionRequest;
import com.tons.srkauction.view.fragment.FrgRejectedAuctionRequest;

public class NonNegotiableTabAdapter extends FragmentStatePagerAdapter {


    public NonNegotiableTabAdapter(FragmentManager fm){
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FrgNonNegotiableApprovedListing();
        } else if (position == 1){
            return new FrgNonNegotiablePendingListing();
        }else{
            return new FrgNonNegotiableRejectedListing();
        }
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){

            case 0:
                return "Approved";

            case 1:
                return "Pending";

            case 2:
                return "Rejected";
            default:
                return null;
        }
    }
}
