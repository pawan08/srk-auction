package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.model.PendingWalletHistory;
import com.tons.srkauction.model.RejectedWalletHistory;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrBidding;
import com.tons.srkauction.view.ScrViewProfileImage;
import com.tons.srkauction.view.fragment.FrgWallet;
import com.tons.srkauction.view.fragment.FrgWithdrawal;

import java.util.ArrayList;

public class PendingWithdrawalAdapter extends RecyclerView.Adapter<PendingWithdrawalAdapter.ViewHolderBid> {


    private ArrayList<PendingWalletHistory> list;
    private FrgWithdrawal context;
    String wallet_vendor_attachment;

    public PendingWithdrawalAdapter(FrgWithdrawal context) {
        this.context = context;

    }

    public void setList(ArrayList<PendingWalletHistory> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_pending_wallet, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBid holder, int position) {
        PendingWalletHistory pendingWalletHistory = list.get(position);
        holder.tv_desc.setText(pendingWalletHistory.getWallet_vendor_desc());
        holder.tv_date.setText(DateToTimeStamp.getDateForNotification(pendingWalletHistory.getWallet_added_time()));

        Log.d("TAG", "onBindViewHolder: tv_amount" + pendingWalletHistory.getWallet_credit());
        Log.d("TAG", "onBindViewHolder: tv_amount" + pendingWalletHistory.getWallet_debit());

        if (Float.parseFloat(pendingWalletHistory.getWallet_credit()) != 0) {
            holder.tv_amount.setText("" + pendingWalletHistory.getWallet_credit());
        } else {
            holder.tv_amount.setText("" + pendingWalletHistory.getWallet_debit());
        }

        if (pendingWalletHistory.getWallet_vendor_attachment().equals("") && pendingWalletHistory.getWallet_vendor_attachment() == null) {
            holder.tv_view_attachment.setVisibility(View.GONE);
        } else {
            holder.tv_view_attachment.setTag(pendingWalletHistory);
            holder.tv_view_attachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    PendingWalletHistory rejectedWalletHistory = (PendingWalletHistory) view.getTag();
                    Intent intent = new Intent(context.getActivity(), ScrViewProfileImage.class);
                    intent.putExtra("image", CommunicationConstant.WALLET_IMAGE + rejectedWalletHistory.getWallet_vendor_attachment());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_amount, tv_desc, tv_date, tv_view_attachment;


        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);
            tv_desc = itemView.findViewById(R.id.tv_desc);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_view_attachment = itemView.findViewById(R.id.tv_view_attachment);
            tv_date = itemView.findViewById(R.id.tv_date);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }
}
