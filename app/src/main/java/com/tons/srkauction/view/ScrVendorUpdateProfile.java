package com.tons.srkauction.view;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.sayantan.advancedspinner.MultiSpinner;
import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.communication.volley_communication.VolleyMultipartRequest;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.communication.volley_communication.datapart.DataPart;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.model.StateList;
import com.tons.srkauction.model.StateModel;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrVendorUpdateProfile extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener, AdapterView.OnItemSelectedListener {

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;
    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    private String image_type = null;

    private ArrayList<String> gender_list;
    private ArrayAdapter<String> gender_adapter;

    private ArrayList<String> b_type_list;
    private ArrayAdapter<String> b_type_adapter;

    private ArrayAdapter<String> commodity_adapter;
    private MultiSpinner sp_vendor_dealing_in;

    private EditText et_name;
    private EditText et_email;
    private EditText et_address;
    private EditText et_password;
    private EditText et_company_name;
    private EditText et_company_address;
    private EditText et_pan_card_shop_act;
    private EditText et_gst_no;
    private EditText et_mandi_license_no;
    private EditText et_vendor_dealing_in;
    private EditText et_contact_whtsapp;
    private EditText et_request_commodityy;


    private Button bt_submit;

    private CircleImageView civ_vendor_profile_image;
    private ImageView civ_aadhar_card;
    private ImageView civ_pan_card;
    private ImageView civ_gst_card;
    private ImageView civ_mandilicense_card;
    private ImageView iv_camera;
    private MultiSpinner sp_business_type;

    private Bitmap aadhar_bitmap, pan_bitmap, gst_bitmap, mandi_bitmap, profile_bitmap;
    private Uri aadharimageUri, panimageUri, profileimageUri, gstimageUri, mandiimageUri;
    private String x;
    private String y;
    private String pancard_pattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";

    private ApiInterface apiInterface;

    String name, vendor_state, whtsappno, requestcommodity, email, address, comp_name, comp_address, pan_card, gst_no, mandi_license_no, vendor_dealing_in, vendor_business_type;
    private String getUOM;
    MultipartBody.Part vendor_pic_profile;
    MultipartBody.Part vendor_pic_gst;
    MultipartBody.Part vendor_pic_aadhar;
    MultipartBody.Part vendor_pic_pan;
    MultipartBody.Part vendor_pic_mandi_license;
    private SearchableSpinner sp_select_state;
    private SearchableSpinner sp_uom;
    String get_vendor_dealing_in;
    String vendor_pic_aadhar_ = null;
    String vendor_pic_pan_ = null;
    String vendor_pic_gst_ = null;
    String vendor_pic_mandi_license_ = null;
    String vendor_pic_profile_ = null;
    String state_name;
    private boolean[] selected;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
                //  getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_vendor_update_profile);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        initUI();
        initBusinessType();
        getVendorProfileDetails();
        getStateList();
        initUom();
    }

    public void initUI() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Update Profile");


        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        et_company_name = findViewById(R.id.et_company_name);
        et_company_address = findViewById(R.id.et_company_address);
        et_pan_card_shop_act = findViewById(R.id.et_pan_card_shop_act);
        et_gst_no = findViewById(R.id.et_gst_no);
        et_mandi_license_no = findViewById(R.id.et_mandi_license_no);

        et_contact_whtsapp = findViewById(R.id.et_contact_whtsapp);
        et_request_commodityy = findViewById(R.id.et_req);
        //et_vendor_dealing_in = findViewById(R.id.et_vendor_dealing_in);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);


        sp_select_state = findViewById(R.id.sp_select_state);
        sp_uom = findViewById(R.id.sp_uom);
        iv_camera = findViewById(R.id.iv_camera);
        iv_camera.setOnClickListener(this);
        civ_aadhar_card = findViewById(R.id.civ_aadhar_card);
        civ_aadhar_card.setOnClickListener(this);

        civ_vendor_profile_image = findViewById(R.id.civ_vendor_profile_image);
        civ_vendor_profile_image.setOnClickListener(this);


        civ_pan_card = findViewById(R.id.civ_pan_card);
        civ_pan_card.setOnClickListener(this);


        civ_gst_card = findViewById(R.id.civ_gst_card);
        civ_gst_card.setOnClickListener(this);


        civ_mandilicense_card = findViewById(R.id.civ_mandilicense_card);
        civ_mandilicense_card.setOnClickListener(this);

        sp_vendor_dealing_in = findViewById(R.id.sp_vendor_dealing_in);
        sp_vendor_dealing_in.setOnItemSelectedListener(this);


        // called other init functions blow


    }

    @Override
    protected void onResume() {
        super.onResume();
        /*initBusinessType();
        getVendorProfileDetails();*/
        // getVendorDealingIn();
    }

    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "1 KG");
        uom_list.add(2, "QUINTAL");
        uom_list.add(3, "METRIC TON");
        uom_list.add(4, "20 KG PRICE");
        uom_adapter = new ArrayAdapter<String>(ScrVendorUpdateProfile.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);

        uom_adapter.notifyDataSetChanged();


    }

    public void initBusinessType() {

        sp_business_type = findViewById(R.id.sp_business_type);
        b_type_list = new ArrayList<>();
        //  b_type_list.add(, "Select Business Type");
        b_type_list.add(0, "Trader");
        b_type_list.add(1, "Retailer");
        b_type_list.add(2, "Millers / Processor");
        b_type_list.add(3, "FPO / FPC");
        b_type_list.add(4, "Financial Institution");
        b_type_list.add(5, "Importer / Exporter");
        b_type_adapter = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, R.id.tv_text, b_type_list);
        sp_business_type.setSpinnerList(b_type_list);
        sp_business_type.setAdapter(b_type_adapter);
        b_type_adapter.notifyDataSetChanged();

    }

    private void getStateList() {

        Call<String> call = apiInterface.get_states();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                Log.d("TAG", "onResponse: success : " + response.body().toString());

                try {
                    JSONObject object = new JSONObject(response.body());
                    int message = object.getInt("message");

                    switch (message) {

                        case Messages.SUCCESS:
                            StateList.getInstance().clearList();
                            JSONArray jsonArray = object.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String s_name = jsonObject1.getString("state_name");
                                int s_id = jsonObject1.getInt("state_id");
                                StateModel stateModel = new StateModel(s_name, s_id);
                                StateList.getInstance().add(stateModel);
                            }
                            ArrayAdapter<String> state_adapter = new ArrayAdapter<String>(ScrVendorUpdateProfile.this, android.R.layout.simple_spinner_item, StateList.getInstance().getStateName());
                            sp_select_state.setAdapter(state_adapter);
                            state_adapter.notifyDataSetChanged();

                            int position = state_adapter.getPosition(state_name);
                            sp_select_state.setSelection(position);

                            break;

                        case Messages.FAILED:
                            // CustomToast.showToast(ScrAddCase.this, "DATA FETCH ERROR");
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // CustomDialog.closeDialog(ScrAddCase.this);

            }
        });

    }

    public void getVendorProfileDetails() {

        CustomDialog.showDialog(ScrVendorUpdateProfile.this, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.ven_profile_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrVendorUpdateProfile.this);

                Log.d("TAG", "onResponse: getVendorDetails : " + response.body());
                Log.d("TAG", "onResponse: getVendorDetails url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String vendor_name = jsonObject1.getString("vendor_name");
                            String vendor_mobile = jsonObject1.getString("vendor_mobile");
                            String vendor_email = jsonObject1.getString("vendor_email");
                            String vendor_address = jsonObject1.getString("vendor_address");
                            String vendor_business_type = jsonObject1.getString("vendor_business_type");
                            String vendor_company_shop_name = jsonObject1.getString("vendor_company_shop_name");
                            String vendor_company_shop_address = jsonObject1.getString("vendor_company_shop_address");
                            String vendor_pan_shopact = jsonObject1.getString("vendor_pan_shopact");
                            String vendor_gst = jsonObject1.getString("vendor_gst");
                            String vendor_mandi = jsonObject1.getString("vendor_mandi");
                            get_vendor_dealing_in = jsonObject1.getString("vendor_dealing_in");
                            vendor_pic_aadhar_ = jsonObject1.getString("vendor_pic_aadhar");
                            vendor_pic_pan_ = jsonObject1.getString("vendor_pic_pan");
                            vendor_pic_gst_ = jsonObject1.getString("vendor_pic_gst");
                            vendor_pic_mandi_license_ = jsonObject1.getString("vendor_pic_mandi_license");
                            vendor_pic_profile_ = jsonObject1.getString("vendor_pic_profile");
                            String vendor_ref_name = jsonObject1.getString("vendor_ref_name");
                            String vendor_ref_mobile = jsonObject1.getString("vendor_ref_mobile");
                            String vendor_ref_address = jsonObject1.getString("vendor_ref_address");
                            state_name = jsonObject1.getString("vendor_state");
                            String uom = jsonObject1.getString("vendor_uom");

                            String[] array = vendor_business_type.split(", ");
                            boolean[] business_type = new boolean[b_type_list.size()];
                            Log.d("TAG", "business_type array " + Arrays.toString(array));
                            for (String value : array) {
                                if (b_type_list.contains(value)) {
                                    Log.d("TAG", "onResponse: value exits " + "true");
                                    int position = b_type_list.indexOf(value);
                                    Array.setBoolean(business_type, position, true);
                                    sp_business_type.setSelected(business_type);
                                    Log.d("TAG", "business_type: position " + Arrays.toString(business_type));
                                    Log.d("TAG", "business_type: position " + position);
                                } else {
                                    Log.d("TAG", "business_type: value exits " + "false");
                                }
                            }


                            int spinnerPosition = uom_adapter.getPosition(uom);
                            Log.d("TAG", "initUom: " + spinnerPosition);
                            Log.d("TAG", "initUom: " + uom);
                            sp_uom.setSelection(spinnerPosition);

                            String whatsappnumber = jsonObject1.getString("vendor_whatsapp_no");

                            String requestedcommodity = jsonObject1.getString("vendor_requested_commodity");

                            et_contact_whtsapp.setText(whatsappnumber);
                            et_request_commodityy.setText(requestedcommodity);

                            et_name.setText(vendor_name);
                            et_email.setText(vendor_email);
                            et_address.setText(vendor_address);
                            et_company_name.setText(vendor_company_shop_name);
                            et_company_address.setText(vendor_company_shop_address);
                            et_pan_card_shop_act.setText(vendor_pan_shopact);
                            et_gst_no.setText(vendor_gst);
                            et_mandi_license_no.setText(vendor_mandi);
                            //   et_vendor_dealing_in.setText(vendor_dealing_in);


                            Log.d("TAG", "onResponse: Url " + CommunicationConstant.VENDOR_PAN + vendor_pic_pan_);

                            Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_AADHAR + vendor_pic_aadhar_)
                                    .placeholder(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_aadhar_card);

                            Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_PAN + vendor_pic_pan_)
                                    .placeholder(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_pan_card);

                            Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_GST + vendor_pic_gst_)
                                    .placeholder(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_gst_card);


                            Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_MANDI_LICENSE + vendor_pic_mandi_license_)
                                    .placeholder(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .error(ScrVendorUpdateProfile.this.getResources().getDrawable(R.drawable.ic_baseline_image))
                                    .into(civ_mandilicense_card);

                            // Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_MANDI_LICENSE+vendor_pic_mandi_license).into(civ_mandilicense_card);

                            Picasso.with(ScrVendorUpdateProfile.this).load(CommunicationConstant.VENDOR_PROFILE + vendor_pic_profile_)
                                    .placeholder(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .error(getResources().getDrawable(R.drawable.ic_error_profile_2))
                                    .into(civ_vendor_profile_image);

                            getVendorDealingIn();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void getVendorDealingIn() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            ArrayList<String> dummy_list = new ArrayList<>();
                            dummy_list.add(0, "Select Vendor Dealing In");
                            commodity_adapter = new ArrayAdapter<>(ScrVendorUpdateProfile.this, android.R.layout.simple_list_item_1, dummy_list);
                            sp_vendor_dealing_in.setSpinnerList(CommodityList.getInstance().getNames());
                            sp_vendor_dealing_in.setAdapter(commodity_adapter);
                            commodity_adapter.notifyDataSetChanged();

                            String vendor_dealing_in = get_vendor_dealing_in;
                            Log.d("TAG", "vendor_dealing_in  " + vendor_dealing_in);

                            String[] array = vendor_dealing_in.split(", ");
                            selected = new boolean[CommodityList.getInstance().getNames().size()];
                            Log.d("TAG", "vendor_dealing_in array " + Arrays.toString(array));
                            for (String value : array) {
                                if (CommodityList.getInstance().getNames().contains(value)) {
                                    Log.d("TAG", "onResponse: value exits " + "true");
                                    position = CommodityList.getInstance().getNames().indexOf(value);
                                    Array.setBoolean(selected, position, true);
                                    sp_vendor_dealing_in.setSelected(selected);
                                    Log.d("TAG", "onResponse: position " + Arrays.toString(selected));
                                    Log.d("TAG", "onResponse: position " + position);
                                } else {
                                    Log.d("TAG", "onResponse: value exits " + "false");
                                }
                            }
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void doValidate() {

        if (vendor_pic_profile_ != null && !vendor_pic_profile_.equals("") && !vendor_pic_profile_.equals("null")) {
            civ_vendor_profile_image.invalidate();
            BitmapDrawable drawable = (BitmapDrawable) civ_vendor_profile_image.getDrawable();
            profile_bitmap = drawable.getBitmap();
        }
        if (vendor_pic_aadhar_ != null && !vendor_pic_aadhar_.equals("") && !vendor_pic_aadhar_.equals("null")) {
            civ_aadhar_card.invalidate();
            BitmapDrawable drawable1 = (BitmapDrawable) civ_aadhar_card.getDrawable();
            aadhar_bitmap = drawable1.getBitmap();
        }
        if (vendor_pic_pan_ != null && !vendor_pic_pan_.equals("") && !vendor_pic_pan_.equals("null")) {
            civ_pan_card.invalidate();
            BitmapDrawable drawable2 = (BitmapDrawable) civ_pan_card.getDrawable();
            pan_bitmap = drawable2.getBitmap();
        }
        if (vendor_pic_gst_ != null && !vendor_pic_gst_.equals("") && !vendor_pic_gst_.equals("null")) {
            civ_gst_card.invalidate();
            BitmapDrawable drawable3 = (BitmapDrawable) civ_gst_card.getDrawable();
            gst_bitmap = drawable3.getBitmap();
        }
        if (vendor_pic_mandi_license_ != null && !vendor_pic_mandi_license_.equals("") && !vendor_pic_mandi_license_.equals("null")) {
            civ_mandilicense_card.invalidate();
            BitmapDrawable drawable4 = (BitmapDrawable) civ_mandilicense_card.getDrawable();
            mandi_bitmap = drawable4.getBitmap();
        }
        name = et_name.getText().toString();
        whtsappno = et_contact_whtsapp.getText().toString();
        requestcommodity = et_request_commodityy.getText().toString();
        email = et_email.getText().toString();
        address = et_address.getText().toString();
        comp_name = et_company_name.getText().toString();
        comp_address = et_company_address.getText().toString();
        pan_card = et_pan_card_shop_act.getText().toString();
        gst_no = et_gst_no.getText().toString();
        mandi_license_no = et_mandi_license_no.getText().toString();
        vendor_state = sp_select_state.getSelectedItem().toString();
        vendor_dealing_in = sp_vendor_dealing_in.getSelectedItem().toString();
        vendor_business_type = sp_business_type.getSelectedItem().toString();
        //   vendor_dealing_in = et_vendor_dealing_in.getText().toString();

        if (TextUtils.isEmpty(name)) {
            MyCustomDialog.showValidationDialog("Please enter name", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(email)) {
            MyCustomDialog.showValidationDialog("Please enter your email", "Oop's", this);
            return;
        } else if (!email.matches(String.valueOf(Patterns.EMAIL_ADDRESS))) {
            MyCustomDialog.showValidationDialog("Please enter valid email", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(address)) {
            MyCustomDialog.showValidationDialog("Please enter address", "Oop's", this);
            return;
        }

        if (sp_select_state.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please Select  State", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }


        if (TextUtils.isEmpty(comp_name)) {
            MyCustomDialog.showValidationDialog("Please enter company / shop name", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(comp_address)) {
            MyCustomDialog.showValidationDialog("Please enter company / shop address", "Oop's", this);
            return;
        }


        if (TextUtils.isEmpty(requestcommodity)) {
            MyCustomDialog.showValidationDialog("Please enter request commodity", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(pan_card)) {
            MyCustomDialog.showValidationDialog("Please enter your pan number", "Oop's", this);
            return;
        }

        if (sp_business_type.getSelectedItemPosition() == -1) {
            MyCustomDialog.showValidationDialog("Please select business type", "Oop's", this);
            return;
        }


        if (sp_vendor_dealing_in.getSelectedItemPosition() == -1) {
            MyCustomDialog.showValidationDialog("Please select vendor dealing in", "Oop's", this);
            return;
        }

        if (profile_bitmap == null) {
            CustomToast.showToast(ScrVendorUpdateProfile.this, "Upload profile photo");
            return;
        } else {
            Log.d("TAG", "NoProfile: " + "Yes");
        }

        if (aadhar_bitmap == null) {
            CustomToast.showToast(ScrVendorUpdateProfile.this, "Upload Aadhar photo");
            return;
        } else {
            Log.d("c", "NoAadhar: " + "Yes");
        }

        if (pan_bitmap == null) {
            CustomToast.showToast(ScrVendorUpdateProfile.this, "Upload Pan photo");
            return;
        } else {
            Log.d("TAG", "NoPan: " + "Yes");
        }


      /*  if (gst_bitmap == null) {
            CustomToast.showToast(ScrVendorUpdateProfile.this, "Upload GST photo");
            return;
        } else {
            Log.d("TAG", "NoGST: " + "Yes");
        }

        if (mandi_bitmap == null) {
            CustomToast.showToast(ScrVendorUpdateProfile.this, "Upload Mandi Licence photo");
            return;
        } else {
            Log.d("TAG", "NoMandi: " + "Yes");
        }*/


        String vendor_id = String.valueOf(VendorData.getInstance().getVendorId());

        final Map<String, String> params = new HashMap<>();
        params.put("vendor_id", vendor_id);
        params.put("vendor_name", name);
        params.put("vendor_dealing_in", vendor_dealing_in);
        params.put("vendor_email", email);
        params.put("vendor_whatsapp_no", "9623669343");
        params.put("vendor_address", address);
        params.put("vendor_requested_commodity", requestcommodity);
        params.put("vendor_business_type", vendor_business_type);
        params.put("vendor_company_shop_name", comp_name);
        params.put("vendor_company_shop_address", comp_address);
        params.put("vendor_pan_shopact", pan_card);
        params.put("vendor_gst", gst_no);
        params.put("vendor_mandi", mandi_license_no);
        params.put("vendor_state", sp_select_state.getSelectedItem().toString());
        params.put("vendor_uom", sp_uom.getSelectedItem().toString());
        Log.d("TAG", "doValidate: params " + sp_select_state.getSelectedItem().toString());
        call_Vendor_Update_Profile(params);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.civ_aadhar_card:
                image_type = Constants.AADHAR_IMAGE;
                RenameBottomSheetDialog bottomSheet1 = new RenameBottomSheetDialog();
                bottomSheet1.show(getSupportFragmentManager(), "ADHAR");
                break;
            case R.id.civ_pan_card:
                image_type = Constants.PAN_IMAGE;
                RenameBottomSheetDialog bottomSheet2 = new RenameBottomSheetDialog();
                bottomSheet2.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.civ_gst_card:
                image_type = Constants.GST_IMAGE;
                RenameBottomSheetDialog bottomSheet3 = new RenameBottomSheetDialog();
                bottomSheet3.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.civ_mandilicense_card:
                image_type = Constants.MANDI_LICENSE_IMAGE;
                RenameBottomSheetDialog bottomSheet4 = new RenameBottomSheetDialog();
                bottomSheet4.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.civ_vendor_profile_image:
            case R.id.iv_camera:
                image_type = Constants.PROFILE_IMAGE;
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.bt_submit:
                doValidate();
                break;
        }
    }


    private void call_Vendor_Update_Profile(final Map<String, String> params) {


        final String url = CommunicationConstant.UPDATE_VENDOR_PROFILE;

        CustomDialog.showDialog(ScrVendorUpdateProfile.this, Constants.PROGRESS_MSG);

        VolleyMultipartRequest postRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        CustomDialog.closeDialog(ScrVendorUpdateProfile.this);
                        Log.d("TAG", "onResponse: vendor profile " + new String(response.data));
                        Log.d("TAG", "onResponse: vendor profile url " + url);
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            boolean status = jsonObject.getBoolean("status");
                            int message = jsonObject.getInt("message");
                            if (message == Messages.SUCCESS) {

                                VendorData.getInstance().setVendorName(name);
                                VendorData.getInstance().setVendorEmail(email);

                                JSONArray jsonArray = jsonObject.getJSONArray("data");
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    String aadahar_pic = jsonObject1.getString("vendor_pic_aadhar");
                                    String pan_pic = jsonObject1.getString("vendor_pic_pan");
                                    String gst_pic = jsonObject1.getString("vendor_pic_gst");
                                    String mandi_pic = jsonObject1.getString("vendor_pic_mandi_license");
                                    String profile_pic = jsonObject1.getString("vendor_pic_profile");

                                }

                                CustomToast.showToast(ScrVendorUpdateProfile.this, "Profile Updated Successfully");
                                /*CustomIntent.startActivity(ScrVendorUpdateProfile.this,ScrHome.class);*/
                                finish();

                            } else {

                                CustomToast.showToast(ScrVendorUpdateProfile.this, "Profile Update Failed");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("REGISTER_RESPONSE", response.data.toString());

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomDialog.closeDialog(ScrVendorUpdateProfile.this);
                        Log.d("TAG", String.valueOf(error));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("vendor_pic_profile", new DataPart("prfile_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(profile_bitmap)));
                params.put("vendor_pic_aadhar", new DataPart("aadhar_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(aadhar_bitmap)));
                params.put("vendor_pic_pan", new DataPart("pan_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(pan_bitmap)));
                if (gst_bitmap != null) {
                    params.put("vendor_pic_gst", new DataPart("gst_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(gst_bitmap)));
                }
                if (mandi_bitmap != null) {
                    params.put("vendor_pic_mandi_license", new DataPart("mandi_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(mandi_bitmap)));
                }
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        long lengthbmp = imageInByte.length;
        Log.d("SIZETAG", String.valueOf(lengthbmp));
        return byteArrayOutputStream.toByteArray();
    }


    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                y = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    switch (image_type) {
                        case Constants.AADHAR_IMAGE:
                            launchAadharCamera();
                            break;

                        case Constants.PAN_IMAGE:
                            launchPanCamera();
                            break;

                        case Constants.GST_IMAGE:
                            launchGSTCamera();
                            break;

                        case Constants.MANDI_LICENSE_IMAGE:
                            launchMandiCamera();
                            break;

                        case Constants.PROFILE_IMAGE:
                            launchProfileCamera();
                            break;
                    }
                }
                break;

            case 2:
                y = "2";
                launchGalleryIntent();
                break;
        }
    }

    private void launchAadharCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        aadharimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, aadharimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchPanCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        panimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, panimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchGSTCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        gstimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, gstimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchMandiCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        mandiimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mandiimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchProfileCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        profileimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, profileimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: y = " + y);
                switch (y) {
                    case "1":
                        switch (image_type) {
                            case Constants.AADHAR_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url1 = null;
                                try {

                                    aadhar_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), aadharimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input = this.getContentResolver().openInputStream(aadharimageUri);
                                    ExifInterface exif = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif = new ExifInterface(input);
                                            int orientation2 = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            aadhar_bitmap = MyBitmap.rotateBitmap(aadhar_bitmap, orientation2);
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                        } else {
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.PAN_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url2 = null;
                                try {

                                    pan_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), panimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input2 = this.getContentResolver().openInputStream(panimageUri);
                                    ExifInterface exif2 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif2 = new ExifInterface(input2);
                                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            pan_bitmap = MyBitmap.rotateBitmap(pan_bitmap, orientation2);
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                        } else {
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url2);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.GST_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url3 = null;
                                try {

                                    gst_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), gstimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input3 = this.getContentResolver().openInputStream(gstimageUri);
                                    ExifInterface exif3 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif3 = new ExifInterface(input3);
                                            int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            gst_bitmap = MyBitmap.rotateBitmap(gst_bitmap, orientation2);
                                            civ_gst_card.setImageBitmap(gst_bitmap);
                                        } else {
                                            civ_gst_card.setImageBitmap(gst_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url3);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.MANDI_LICENSE_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url4 = null;
                                try {

                                    mandi_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mandiimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input4 = this.getContentResolver().openInputStream(mandiimageUri);
                                    ExifInterface exif4 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif4 = new ExifInterface(input4);
                                            int orientation2 = exif4.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            mandi_bitmap = MyBitmap.rotateBitmap(mandi_bitmap, orientation2);
                                            civ_mandilicense_card.setImageBitmap(mandi_bitmap);
                                        } else {
                                            civ_mandilicense_card.setImageBitmap(mandi_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url4);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.PROFILE_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url5 = null;
                                try {

                                    profile_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), profileimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input5 = this.getContentResolver().openInputStream(profileimageUri);
                                    ExifInterface exif5 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif5 = new ExifInterface(input5);
                                            int orientation2 = exif5.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            profile_bitmap = MyBitmap.rotateBitmap(profile_bitmap, orientation2);
                                            civ_vendor_profile_image.setImageBitmap(profile_bitmap);
                                        } else {
                                            civ_vendor_profile_image.setImageBitmap(profile_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url5);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;
                        }
                        break;

                    case "2":
                        try {
                            switch (image_type) {
                                case Constants.AADHAR_IMAGE:
                                    final Uri imageUri = data.getData();
                                    final InputStream imageStream;
                                    imageStream = getContentResolver().openInputStream(imageUri);
                                    aadhar_bitmap = BitmapFactory.decodeStream(imageStream);
                                    ExifInterface exif1 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif1 = new ExifInterface(imageStream);
                                            assert exif1 != null;
                                            int orientation2 = exif1.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            aadhar_bitmap = MyBitmap.rotateBitmap(aadhar_bitmap, orientation2);
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id p0roof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    break;

                                case Constants.PAN_IMAGE:
                                    final Uri imageUri2 = data.getData();
                                    InputStream imageStream2 = null;
                                    imageStream2 = getContentResolver().openInputStream(imageUri2);
                                    pan_bitmap = BitmapFactory.decodeStream(imageStream2);
                                    ExifInterface exif2 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif2 = new ExifInterface(imageStream2);
                                            assert exif2 != null;
                                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            pan_bitmap = MyBitmap.rotateBitmap(pan_bitmap, orientation2);
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.GST_IMAGE:
                                    final Uri imageUri3 = data.getData();
                                    InputStream imageStream3 = null;
                                    imageStream3 = getContentResolver().openInputStream(imageUri3);
                                    gst_bitmap = BitmapFactory.decodeStream(imageStream3);
                                    ExifInterface exif3 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif3 = new ExifInterface(imageStream3);
                                            assert exif3 != null;
                                            int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            gst_bitmap = MyBitmap.rotateBitmap(gst_bitmap, orientation2);
                                            civ_gst_card.setImageBitmap(gst_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_gst_card.setImageBitmap(gst_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.MANDI_LICENSE_IMAGE:
                                    final Uri imageUri4 = data.getData();
                                    InputStream imageStream4 = null;
                                    imageStream4 = getContentResolver().openInputStream(imageUri4);
                                    mandi_bitmap = BitmapFactory.decodeStream(imageStream4);
                                    ExifInterface exif4 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif4 = new ExifInterface(imageStream4);
                                            assert exif4 != null;
                                            int orientation2 = exif4.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            mandi_bitmap = MyBitmap.rotateBitmap(mandi_bitmap, orientation2);
                                            civ_mandilicense_card.setImageBitmap(mandi_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_mandilicense_card.setImageBitmap(mandi_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.PROFILE_IMAGE:
                                    final Uri imageUri5 = data.getData();
                                    InputStream imageStream5 = null;
                                    imageStream5 = getContentResolver().openInputStream(imageUri5);
                                    profile_bitmap = BitmapFactory.decodeStream(imageStream5);
                                    ExifInterface exif5 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif5 = new ExifInterface(imageStream5);
                                            assert exif5 != null;
                                            int orientation2 = exif5.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            profile_bitmap = MyBitmap.rotateBitmap(profile_bitmap, orientation2);
                                            civ_vendor_profile_image.setImageBitmap(profile_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_vendor_profile_image.setImageBitmap(profile_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                            }
                            break;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: file not found exception ; " + e.getMessage());
                            Toast.makeText(ScrVendorUpdateProfile.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                }

            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}