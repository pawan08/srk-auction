package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgAuctionCompleted;
import com.tons.srkauction.view.fragment.FrgAuctionLive;
import com.tons.srkauction.view.fragment.FrgAuctionUpcoming;

public class AuctionTabAdapter extends FragmentStatePagerAdapter {


    public AuctionTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new FrgAuctionLive();
        } else if (position == 1) {
            return new FrgAuctionUpcoming();
        } else if (position == 2) {
            return new FrgAuctionCompleted();
        } else {
            return new FrgAuctionCompleted();
        }
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

            case 0:
                return "Live";

            case 1:
                return "Upcoming";

            case 2:
                return "Completed";

            case 3:
                return "Finish";
            default:
                return null;
        }
    }
}
