package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.UpcomingAuction;
import com.tons.srkauction.model.ViewSample;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrViewDetailsSample;
import com.tons.srkauction.view.ScrViewSample;
import com.tons.srkauction.view.fragment.FrgAuctionUpcoming;

import java.util.ArrayList;

public class ViewSampleAdapter extends RecyclerView.Adapter<ViewSampleAdapter.ViewHolderBid> {

    private ArrayList<ViewSample> list;
    private ScrViewSample context;

    public ViewSampleAdapter(ScrViewSample context) {
        this.context = context;

    }

    public void setList(ArrayList<ViewSample> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_view_sample, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        ViewSample viewSample = list.get(i);
        holder.tv_vendor_name.setText(viewSample.getVendor_name());
        holder.tv_commodity.setText(viewSample.getRequest_auction_commodity());
        holder.tv_auction_subcode.setText(viewSample.getReqsub_auc_code());
        holder.tv_location.setText(viewSample.getReqsub_auc_location());
        holder.tv_warehouse.setText(viewSample.getReqsub_auc_warehouse());
        holder.tv_qty.setText(viewSample.getReqsub_auc_qty());
        holder.tv_price.setText("₹" + viewSample.getReqsub_auc_unit_price());

        holder.bt_view_details.setTag(viewSample);
        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ViewSample viewSample1 = (ViewSample) view.getTag();
                Intent intent = new Intent(context, ScrViewDetailsSample.class);
                intent.putExtra("reqsub_auc_id", String.valueOf(viewSample1.getReqsub_auc_id()));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

   /* public void filteredList(ArrayList<UpcomingAuction> filteredList)
    {
        list = filteredList;
        notifyDataSetChanged();
    }*/

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_vendor_name, tv_commodity, tv_auction_subcode, tv_location, tv_warehouse, tv_qty, tv_price;
        private Button bt_view_details;
        private CardView cv_main;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            tv_commodity = itemView.findViewById(R.id.tv_commodity);
            tv_auction_subcode = itemView.findViewById(R.id.tv_auction_subcode);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_warehouse = itemView.findViewById(R.id.tv_warehouse);
            tv_qty = itemView.findViewById(R.id.tv_unit_qty);
            tv_price = itemView.findViewById(R.id.tv_req_price);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
            cv_main = itemView.findViewById(R.id.cv_main);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
