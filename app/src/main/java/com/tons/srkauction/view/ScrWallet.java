package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.VendorNotification;
import com.tons.srkauction.model.VendorNotificationList;
import com.tons.srkauction.model.Wallet;
import com.tons.srkauction.model.WalletList;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.view.adapter.VendorNotificationAdapter;
import com.tons.srkauction.view.adapter.WalletAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrWallet extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rv_transactions;
    private WalletAdapter walletAdapter;
    private ApiInterface apiInterface;
    private Button bt_pending,bt_rejected;
    private LinearLayout ll_no_data;
    private TextView tv_wallet_amt;
    //  private SwipeRefreshLayout swipeRefreshLayout;

    private Button bt_add_money,bt_withdraw_money;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_wallet);
        initUI();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }

    public void initUI() {


        ll_no_data = findViewById(R.id.ll_no_data);
        tv_wallet_amt = findViewById(R.id.tv_wallet_amt);

        rv_transactions = findViewById(R.id.rv_transactions);
        rv_transactions.setLayoutManager(new GridLayoutManager(this, 1));


        walletAdapter = new WalletAdapter(this);
        rv_transactions.setAdapter(walletAdapter);

          /*  swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
            swipeRefreshLayout.setOnRefreshListener(this);
*/
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Wallet");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        bt_add_money = findViewById(R.id.bt_add_money);
        bt_pending = findViewById(R.id.bt_pending);
        bt_rejected = findViewById(R.id.bt_rejected);
        bt_withdraw_money=findViewById(R.id.bt_withdraw_money);
        bt_withdraw_money.setOnClickListener(this);
        bt_add_money.setOnClickListener(this);
        bt_pending.setOnClickListener(this);
        bt_rejected.setOnClickListener(this);

    }

    public void showEmptyLayout() {
        if (walletAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        call_Wallet_hitory_Api();
    }


    private void call_Wallet_hitory_Api() {
        Map<String, String> params = new HashMap<>();
    //    params.put("vendor_id", "4");
       params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_wallet_history_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                WalletList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetWalletHistory" + response.body());
                    Log.d("TAG", "GetWalletHistory url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONObject objectZero = jsonObject.getJSONObject("data1");
                        String wallet_amount = objectZero.getString("wallet_amount");
                        tv_wallet_amt.setText(wallet_amount);

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String wallet_id = jsonObject1.getString("wallet_id");
                            String wallet_credit = jsonObject1.getString("wallet_credit");
                            String wallet_debit = jsonObject1.getString("wallet_debit");
                            String wallet_added_time = jsonObject1.getString("wallet_added_time");
                            String wallet_type = jsonObject1.getString("wallet_type");
                            String wallet_changed_time = jsonObject1.getString("wallet_changed_time");
                            Wallet wallet = new Wallet(wallet_id, wallet_credit, wallet_debit, "", wallet_added_time,wallet_type,wallet_changed_time);
                            WalletList.getInstance().add(wallet);
                        }
                        walletAdapter.setList(WalletList.getInstance().getList());
                        walletAdapter.notifyDataSetChanged();
                    }
                    showEmptyLayout();

//                    swipeRefreshLayout.setRefreshing(false);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //swipeRefreshLayout.setRefreshing(false);

            }

        });
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.bt_add_money:
                CustomIntent.startActivity(ScrWallet.this, ScrAddMoneyToWallet.class, false);
                break;

            case R.id.bt_pending:
                CustomIntent.startActivity(ScrWallet.this, ScrPendingRequestTab.class, false);
                break;

            case R.id.bt_rejected:
                CustomIntent.startActivity(ScrWallet.this, ScrRejectedRequestsTab.class, false);
                break;

            case R.id.bt_withdraw_money:
                CustomIntent.startActivity(ScrWallet.this, ScrWithdrawMoneyFromWallet.class, false);
                break;

        }
    }

/*    @Override
    public void onRefresh() {

        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_Wallet_hitory_Api();
                                    }
                                }
        );

    }*/
}