package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.BrokerNotification;
import com.tons.srkauction.model.BrokerNotificationList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrForgotPassword extends AppCompatActivity implements View.OnClickListener {


    private Button bt_submit_mob_num;
    private EditText et_contact;
    private TextView tv_login;
    String mobile;

    private ApiInterface apiInterface;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.cross_line));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_forgot_password);

        initUI();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

    }

    public void initUI() {
        et_contact = findViewById(R.id.et_contact);

        tv_login = findViewById(R.id.tv_login);
        tv_login.setOnClickListener(this);

        bt_submit_mob_num = findViewById(R.id.bt_submit_mob_num);
        bt_submit_mob_num.setOnClickListener(this);

    }

    public void validate() {
        mobile = et_contact.getText().toString();
        boolean isValid = true;

        if (mobile.isEmpty()) {
            isValid = false;
            et_contact.setError("Enter Mobile Number");
        } else if (mobile.length() < 10) {
            isValid = false;
            et_contact.setError("Enter Valid Mobile Number");
        } else {
            isValid = isValid && true;
            et_contact.setError(null);
        }

        if (isValid) {

        }
    }


    private void call_forgot_password_Api() {
        Map<String, String> params = new HashMap<>();
       if(UserData.getInstance().getUserType()==1 ){
            params.put("type", String.valueOf(UserData.getInstance().getUserType()));
            params.put("mobile",et_contact.getText().toString());

        }else if(UserData.getInstance().getUserType()==2){
            params.put("type", String.valueOf(UserData.getInstance().getUserType()));
            params.put("mobile", et_contact.getText().toString());
        }

    /*  params.put("type","1");
        params.put("mobile", "8208020681");*/
        Call<String> call = apiInterface.forget_password(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "GetForgotPassword" + response.body());
                Log.d("TAG", "GetForgotPassword url " + call.request().url().toString());


                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        String data = jsonObject.getString("data");
                        CustomToast.showToast(ScrForgotPassword.this, "your password is: "+ data);

                        }else {
                        CustomToast.showToast(ScrForgotPassword.this, "Inactive User");
                    }
                    /*else if (Messages.INACTIVE_USER==24){
                        CustomToast.showToast(ScrForgotPassword.this, "Inactive User");


                    } else if (Messages.NOT_REGISTERED==25) {
                        CustomToast.showToast(ScrForgotPassword.this, "User Not Registered");

                    }*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }

        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.bt_submit_mob_num:
                validate();
                call_forgot_password_Api();
                break;

            case R.id.tv_login:
                finish();
                break;
        }
    }
}