package com.tons.srkauction.view.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.RequestSubAuction;
import com.tons.srkauction.model.SubAuction;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.ScrAddMoneyToWallet;
import com.tons.srkauction.view.ScrRequestSubAuctionListing;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionListing;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class RequestSubAuctionAdapter extends RecyclerView.Adapter<RequestSubAuctionAdapter.ViewHolderBid> {

    private Timer timer;
    private ArrayList<RequestSubAuction> list;
    private ScrRequestSubAuctionListing context;
    private ApiInterface apiInterface;


    public RequestSubAuctionAdapter(ScrRequestSubAuctionListing context) {
        this.context = context;

    }

    public void setList(ArrayList<RequestSubAuction> list) {
        this.list = list;
        timer = new Timer();
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sub_auction, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        final RequestSubAuction subAuction = list.get(i);
        holder.tv_location.setText(subAuction.getReqsub_auc_location());
        holder.tv_unit_price.setText(subAuction.getReqsub_auc_unit_price());
        holder.tv_quantity.setText(subAuction.getReqsub_auc_qty());
        holder.tv_emd_amount.setText(subAuction.getReqsub_auc_emd());
        holder.tv_vendor_name.setText(subAuction.getVendor_name());

            holder.bt_participate.setTag(subAuction);
            holder.bt_participate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RequestSubAuction subAuction1 = (RequestSubAuction) view.getTag();
                    Intent intent = new Intent(context, ScrSubAuctionDetails.class);
                    intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                    intent.putExtra("vendor_id", subAuction1.getReqsub_auc_vendor_id());
                    context.startActivity(intent);

                }
            });


        TimerTask timerTask = new TimerTask() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            public void run() {
                Log.d("TAG", "Updating");
                upcomingTimerCalculation(holder.chronometer, subAuction.getCurrent_time(), subAuction.getEnd_date_time());
            }
        };
        timer.schedule(timerTask, 1, 1000);
        upcomingTimerCalculation(holder.chronometer, subAuction.getCurrent_time(), subAuction.getEnd_date_time());

    }

    public void showDialog(final String emd_amount, final String reqsub_auc_id) {


        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("Participate");
        tv_text.setText("Pay EMD to participate in this auction");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Pay Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call_pay_emd_api(emd_amount, reqsub_auc_id);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void payEMDAgain(final String emd_amount, final String reqsub_auc_id) {


        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("EMD Rejected");
        tv_text.setText("EMD is rejected , pay again to continue");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Pay Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                call_pay_emd_api(emd_amount, reqsub_auc_id);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void showAddMoneyDialog() {


        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText("Insufficient Fund");
        tv_text.setText("Add money to participate in the auction");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Add Now");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomIntent.startActivity(context, ScrAddMoneyToWallet.class, false);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    private void call_pay_emd_api(String emd_amount, String reqsub_auc_id) {
        CustomDialog.showDialog(context, Constants.PROGRESS_MSG);
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", VendorData.getInstance().getVendorId() + "");
        params.put("reqsub_auc_id", reqsub_auc_id);
        params.put("sub_auction_emd", emd_amount);
        Call<String> call = apiInterface.pay_emd(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(context);
                Log.d("TAG", "onResponse: call_pay_emd_api " + response.body());
                Log.d("TAG", "onResponse: call_pay_emd_api url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(response.body());
                    int message = object.getInt("message");
                    switch (message) {

                        case Messages.SUCCESS:
                            CustomToast.showToast(context, "Emd paid successfully.");
                            break;

                        case Messages.FAILED:
                            CustomToast.showToast(context, "Failed to pay emd.");
                            break;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(context);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    private void upcomingTimerCalculation(final TextView txtCurrentTime, final String current_time, final String end_date_time) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {
                //Date currentTime = Calendar.getInstance().getTime();
                futureDate = dateFormat.parse(current_time);
                //futureDate = dateFormat.parse(CurrentTime.getInstance().getKEY_CURRENT_TIME());
                futureDate1 = dateFormat.parse(end_date_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                txtCurrentTime.setText(String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                    txtCurrentTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                } else {
                    txtCurrentTime.setVisibility(VISIBLE);
                    txtCurrentTime.setText("Finished!");

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG", "upcomingTimerCalculation: Exception " + e.getMessage());
        }

    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView chronometer;
        private TextView tv_location, tv_quantity, tv_unit_price, tv_view_details, tv_emd_amount, tv_vendor_name;
        private Button bt_participate;
        private CardView card_main;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            chronometer = itemView.findViewById(R.id.chronometer);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_view_details = itemView.findViewById(R.id.tv_view_details);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_emd_amount = itemView.findViewById(R.id.tv_emd_amount);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            bt_participate = itemView.findViewById(R.id.bt_participate);
            card_main = itemView.findViewById(R.id.card_main);

        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
