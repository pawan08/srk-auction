package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.LiveAuction;
import com.tons.srkauction.model.AuctionList;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrAuctionDetails;
import com.tons.srkauction.view.fragment.FrgAuctionLive;

import java.util.ArrayList;

public class LiveAuctionAdapter extends RecyclerView.Adapter<LiveAuctionAdapter.ViewHolderBid> {


    private ArrayList<LiveAuction> list;
    private FrgAuctionLive context;

    public LiveAuctionAdapter(FrgAuctionLive context) {
        this.context = context;

    }

    public void setList(ArrayList<LiveAuction> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_auction2, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        LiveAuction requestedLiveAuction = list.get(i);
        holder.tv_auction_type.setText(requestedLiveAuction.getAuction_type());
        holder.tv_auction_commomditity.setText(requestedLiveAuction.getAuction_code());
        holder.tv_uom.setText("Commodity : " + requestedLiveAuction.getAuction_commodity());
        holder.tv_date.setText(DateToTimeStamp.getDate(requestedLiveAuction.getDate())+" "+DateToTimeStamp.getTime(requestedLiveAuction.getAuction_start_time()+":00"));

        holder.bt_view_details.setTag(requestedLiveAuction);
        holder.bt_view_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LiveAuction liveAuction =(LiveAuction) view.getTag();
                AuctionList.getInstance().setSelectedList(liveAuction);
                Intent intent=new Intent(context.getActivity(), ScrAuctionDetails.class);
                intent.putExtra("auction_id", String.valueOf(liveAuction.getAuction_id()));
                context.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public void filteredList(ArrayList<LiveAuction> filteredList)
    {
        list=filteredList;
        notifyDataSetChanged();
    }

    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView tv_date, tv_unit_price, tv_uom;
        private TextView tv_auction_commomditity, tv_auction_type;
        private CardView cv_main;
        private Button bt_view_details;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_auction_type = itemView.findViewById(R.id.tv_auction_type);
            tv_auction_commomditity = itemView.findViewById(R.id.tv_auction_commomditity);
            tv_uom = itemView.findViewById(R.id.tv_uom);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_date = itemView.findViewById(R.id.tv_date);
            cv_main = itemView.findViewById(R.id.cv_main);
            bt_view_details = itemView.findViewById(R.id.bt_view_details);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {

            }
        }
    }


}
