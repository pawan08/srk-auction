package com.tons.srkauction.view;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrAddRequestOfflineForm extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener {

    private SearchableSpinner sp_auction_commodity;
    private SearchableSpinner sp_uom;
    private EditText et_unit_price;
    private EditText et_location;
    private EditText et_commodity_desc;
    private LinearLayout ll_image;
    private EditText et_qty;
    private Button bt_submit;
    private ImageView iv_image;
    private  ApiInterface apiInterface;

    private ArrayList<String> uom_list;
    private ArrayAdapter<String> uom_adapter;

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;
    private String x;
    private Uri imageUri;
    private Bitmap bitmap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scr_add_request_offline_form);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        initAuctioncommodity();
        initUom();
    }

    private void initUi(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Add Offline Request");
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        sp_uom = findViewById(R.id.sp_uom);
        et_qty = findViewById(R.id.et_qty);
        et_unit_price = findViewById(R.id.et_unit_price);
        et_location = findViewById(R.id.et_location);
        et_commodity_desc = findViewById(R.id.et_commodity_desc);
        iv_image = findViewById(R.id.iv_image);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        sp_auction_commodity = findViewById(R.id.sp_auction_commodity);
        iv_image = findViewById(R.id.iv_image);
        ll_image = findViewById(R.id.ll_image);
        bt_submit.setOnClickListener(this);
        ll_image.setOnClickListener(this);
    }

    private void initUom() {

        uom_list = new ArrayList<>();
        uom_list.add(0, "Select UOM");
        uom_list.add(1, "1 KG");
        uom_list.add(2, "QUINTAL");
        uom_list.add(3, "METRIC TON");
        uom_list.add(4, "20 KG PRICE");


        uom_adapter = new ArrayAdapter<String>(ScrAddRequestOfflineForm.this, android.R.layout.simple_list_item_1, uom_list);
        sp_uom.setAdapter(uom_adapter);
        uom_adapter.notifyDataSetChanged();
    }

    public void initAuctioncommodity() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("TAG", "onResponse: getCommodityList : " + response.body());
                Log.d("TAG", "onResponse: getCommodityList url : " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            ArrayAdapter<String> auction_commodity_adapter = new ArrayAdapter<>(ScrAddRequestOfflineForm.this, android.R.layout.simple_list_item_1, CommodityList.getInstance().getCommodityNames());
                            sp_auction_commodity.setAdapter(auction_commodity_adapter);
                            auction_commodity_adapter.notifyDataSetChanged();
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: " + t.getMessage());
            }
        });
    }

    private void validate() {
        String unit_price = et_unit_price.getText().toString();
        String qty = et_qty.getText().toString();
        String location = et_location.getText().toString();
        String et_com_desc = et_commodity_desc.getText().toString();


        if (sp_auction_commodity.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select  commodity", "Oop's", this);
            return;
        }

        if (sp_uom.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select UOM", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(qty)) {
            MyCustomDialog.showValidationDialog("Please enter quantity", "Oop's", this);
            return;
        }

        if (qty.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter quantity greater than zero", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(unit_price)) {
            MyCustomDialog.showValidationDialog("Please enter unit price", "Oop's", this);
            return;
        }

        if (unit_price.equals("0")) {
            MyCustomDialog.showValidationDialog("Please enter unit price greater than zero", "Oop's", this);
            return;
        }


        if (TextUtils.isEmpty(location)) {
            MyCustomDialog.showValidationDialog("Please enter  location", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(et_com_desc)) {
            MyCustomDialog.showValidationDialog("Please enter  description", "Oop's", this);
            return;
        }

        if (bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select image", "Oop's", this);
            return;
        }

        //Map<String, String> params = new HashMap<>();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.bt_submit:
                validate();
                break;

            case R.id.ll_image:
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "image");
                break;
        }
    }

    private void launchCameraIntent1() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        imageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }

    @Override
    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                x = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    launchCameraIntent1();
                }
                break;

            case 2:
                x = "2";
                launchGalleryIntent();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: x = " + x);
                switch (x) {

                    case "1":
                        Log.d("TAG", "onActivityResult: inside case 1 ");
                        String url1 = null;
                        try {
                            bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                            // url1 = getRealPathFromURI(imageUri1);
                            InputStream input2 = this.getContentResolver().openInputStream(imageUri);
                            ExifInterface exif2 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif2 = new ExifInterface(input2);
                                    int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: camera id proof ");
                            break;

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                            Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                        }
                        break;

                    case "2":
                        Log.d("TAG", "onActivityResult: inside case 2 ");
                        try {

                            imageUri = data.getData();
                            final InputStream imageStream = getContentResolver().openInputStream(imageUri);
                            bitmap = BitmapFactory.decodeStream(imageStream);
                            ExifInterface exif3 = null;
                            try {
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    exif3 = new ExifInterface(imageStream);
                                    int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                    bitmap = MyBitmap.rotateBitmap(bitmap, orientation2);
                                    iv_image.setImageBitmap(bitmap);
                                } else {
                                    iv_image.setImageBitmap(bitmap);
                                }
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                            Log.d("TAG", "onActivityResult: gallery id proof ");

                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrAddRequestOfflineForm.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                        break;
                }
            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }
}