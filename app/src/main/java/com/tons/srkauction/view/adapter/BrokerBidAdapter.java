package com.tons.srkauction.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.BidModel;
import com.tons.srkauction.model.BrokerBidModel;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrBidding;
import com.tons.srkauction.view.ScrBrokerBidding;

import java.util.ArrayList;

public class BrokerBidAdapter extends RecyclerView.Adapter<BrokerBidAdapter.ViewHolderBid> {


    private ArrayList<BrokerBidModel> list;
    private ScrBrokerBidding context;

    public BrokerBidAdapter(ScrBrokerBidding context) {
        this.context = context;

    }

    public void setList(ArrayList<BrokerBidModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bid, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBid holder, int position) {
        BrokerBidModel bidModel = list.get(position);
        if (bidModel.getBid_broker_id() == BrokerData.getInstance().getBrokerId()) {
            holder.tv_tittle.setText("Your Bid");
        } else if (bidModel.getReqsub_auc_vendor_id() == bidModel.getBid_vendor_id()) {
            holder.tv_tittle.setText("Your Bid ( " + bidModel.getVendor_name() + " )");
        } else {
            holder.tv_tittle.setText("Others Bid");
        }
        //  holder.tv_tittle.setText(bidModel.getTitle());
        holder.tv_amount.setText(bidModel.getBid_amount());
        holder.tv_time.setText(DateToTimeStamp.getDateForNotification(bidModel.getBid_added_time()+":00"));


    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_tittle, tv_amount, tv_time;


        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_tittle = itemView.findViewById(R.id.tv_tittle);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_time = itemView.findViewById(R.id.tv_time);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }
}
