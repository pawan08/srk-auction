package com.tons.srkauction.view;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.VendorNotification;
import com.tons.srkauction.model.VendorNotificationList;
import com.tons.srkauction.model.ViewSample;
import com.tons.srkauction.model.ViewSampleList;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.view.adapter.VendorNotificationAdapter;
import com.tons.srkauction.view.adapter.ViewSampleAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrViewSample extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private RecyclerView rv_vendor_notification;
    private ViewSampleAdapter viewSampleAdapter;
    private ApiInterface apiInterface;

    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ImageView iv_back;
    private TextView tv_header;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_vendor_view_sample);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }

    public void initUi() {
        ll_no_data = findViewById(R.id.ll_no_data);

        rv_vendor_notification = findViewById(R.id.rv_vendor_notification);
        rv_vendor_notification.setLayoutManager(new GridLayoutManager(this, 1));

        viewSampleAdapter = new ViewSampleAdapter(this);
        rv_vendor_notification.setAdapter(viewSampleAdapter);

        swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);

        Toolbar toolbar = findViewById(R.id.toolbar);
        iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Sample Listing");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);
    }

    public void showEmptyLayout() {
        if (viewSampleAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_View_Sample_Api();
                                    }
                                }
        );
    }


    private void call_View_Sample_Api() {
        Map<String, String> params = new HashMap<>();
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));
        Call<String> call = apiInterface.get_samples_data(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                ViewSampleList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetVendorSample" + response.body());
                    Log.d("TAG", "GetVendorSample url : " + call.request().url().toString());

                    boolean status = jsonObject.getBoolean("status");

                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            String sample_id = jsonObject1.getString("sample_id");
                            String reqsub_auc_code = jsonObject1.getString("reqsub_auc_code");
                            String reqsub_auc_warehouse = jsonObject1.getString("reqsub_auc_warehouse");
                            String reqsub_auc_location = jsonObject1.getString("reqsub_auc_location");
                            String reqsub_auc_qty = jsonObject1.getString("reqsub_auc_qty");
                            String reqsub_auc_unit_price = jsonObject1.getString("reqsub_auc_unit_price");
                            int reqsub_auc_id = jsonObject1.getInt("reqsub_auc_id");
                            String vendor_name = jsonObject1.getString("vendor_name");
                            String request_auction_commodity = jsonObject1.getString("request_auction_commodity");

                            Log.d("TAG", "onResponse: vendor_name : " + vendor_name + " Commodity : " + request_auction_commodity);

                            ViewSample viewSample = new ViewSample(sample_id, reqsub_auc_code, reqsub_auc_warehouse, reqsub_auc_location, reqsub_auc_qty, reqsub_auc_unit_price, reqsub_auc_id, vendor_name, request_auction_commodity);
                            ViewSampleList.getInstance().add(viewSample);
                        }

                    }
                    swipeRefreshLayout.setRefreshing(false);
                    viewSampleAdapter.setList(ViewSampleList.getInstance().getList());
                    viewSampleAdapter.notifyDataSetChanged();
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);

            }

        });
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        call_View_Sample_Api();
                                    }
                                }
        );
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                onBackPressed();
                break;
        }
    }
}
