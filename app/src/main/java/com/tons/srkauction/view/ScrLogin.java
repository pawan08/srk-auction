package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.common.api.Api;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.FCMData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class ScrLogin extends AppCompatActivity implements View.OnClickListener {

    private ApiInterface apiInterface;


    private EditText et_contact;
    private EditText et_password;
    private Button bt_submit;

    private TextView tv_header;
    private TextView tv_register;
    private TextView tv_forgot_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.cross_line));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_login);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        initUI();
        setHeader();
    }

    private void initUI() {
        tv_header = findViewById(R.id.tv_header);
        tv_register = findViewById(R.id.tv_register);
        tv_register.setOnClickListener(this);

        tv_forgot_password = findViewById(R.id.tv_forgot_password);
        tv_forgot_password.setOnClickListener(this);

        et_contact = findViewById(R.id.et_contact);
        et_password = findViewById(R.id.et_password);


        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

    }

    public void setHeader() {
        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            tv_header.setText("Vendor account");
        } else if (UserData.getInstance().getUserType() == UserData.ROLE_BROKER) {
            tv_header.setText("Broker account");
        } else {
            tv_header.setVisibility(View.GONE);
            tv_header.setText("");
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_register:
                Class register_class;
                if (UserData.getInstance().getUserType() == 1) {
                    register_class = ScrVendorRegistration.class;
                } else {
                    register_class = ScrBrokerRegistration.class;
                }
                CustomIntent.startActivity(ScrLogin.this, register_class, false);
                break;

            case R.id.tv_forgot_password:
                CustomIntent.startActivity(ScrLogin.this, ScrForgotPassword.class, false);
                break;

            case R.id.bt_submit:
                //CustomIntent.startActivity(ScrLogin.this, ScrHome.class);
                doValidate();
                break;
        }
    }

    public void doValidate() {
        String moible = et_contact.getText().toString().trim();
        String password = et_password.getText().toString();
        if (TextUtils.isEmpty(moible)) {
            showDialog("Enter contact number.", "Oop's");
            return;
        } else if (!moible.matches("[0-9]{10}")) {
            showDialog("Contact number should have 10 digits.", "Oop's");
            return;
        }

        if (TextUtils.isEmpty(password)) {
            showDialog("Enter password.", "Oop's");
            return;
        }

        Call<String> call = null;
        String type = null;
        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            call = apiInterface.ven_login(moible, password, FCMData.getInstance().getFCMToken());
            type = "Vendor";
        } else if (UserData.getInstance().getUserType() == UserData.ROLE_BROKER) {
            call = apiInterface.bro_login(moible, password, FCMData.getInstance().getFCMToken());
            type = "Broker";
        }
        UIUtil.hideKeyboard(this);
        call_Login_API(call, type);
    }

    public void call_Login_API(Call<String> call, final String type) {
        CustomDialog.showDialog(ScrLogin.this, Constants.PROGRESS_MSG);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "onResponse: login " + type + " : " + response.body());
                Log.d("TAG", "onResponse: login " + type + " url : " + call.request().url().toString());
                CustomDialog.closeDialog(ScrLogin.this);
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(response.body());
                    boolean status = object.getBoolean("status");
                    int message = object.getInt("message");


                    if (message == Messages.SUCCESS) {
                        JSONArray array = object.getJSONArray("data");
                        JSONObject object1 = array.getJSONObject(0);

                        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
                            Log.d("TAG", "onResponse: inside vendor ");
                            int vendor_id = object1.getInt("vendor_id");
                            int vendor_status = object1.getInt("vendor_status");
                            String vendor_name = object1.getString("vendor_name");
                            String vendor_mobile = object1.getString("vendor_mobile");
                            String vendor_mobile_whtsapp = object1.getString("vendor_whatsapp_no");
                            String vendor_rqst_commodity = object1.getString("vendor_requested_commodity");
                            String vendor_email = object1.getString("vendor_email");
                            String vendor_password = object1.getString("vendor_password");
                            String vendor_pic_profile = object1.getString("vendor_pic_profile");
                            String vendor_address = object1.getString("vendor_address");
                            String vendor_business_type = object1.getString("vendor_business_type");
                            String vendor_company_shop_name = object1.getString("vendor_company_shop_name");
                            String vendor_company_shop_address = object1.getString("vendor_company_shop_address");
                            String vendor_pan_shopact = object1.getString("vendor_pan_shopact");
                            String vendor_gst = object1.getString("vendor_gst");
                            String vendor_mandi = object1.getString("vendor_mandi");
                            String vendor_pic_aadhar = object1.getString("vendor_pic_aadhar");
                            String vendor_pic_pan = object1.getString("vendor_pic_pan");
                            String vendor_pic_gst = object1.getString("vendor_pic_gst");
                            String vendor_pic_mandi_license = object1.getString("vendor_pic_mandi_license");
                            String vendor_dealing_in = object1.getString("vendor_dealing_in");
                            String vendor_uom = object1.getString("vendor_uom");

                            VendorData.getInstance().setVendorId(vendor_id);
                            VendorData.getInstance().setVendorName(vendor_name);
                            VendorData.getInstance().setVendorMobile(vendor_mobile);
                            VendorData.getInstance().setVENDOR_MOBILE_WHTSAPP(vendor_mobile_whtsapp);
                            VendorData.getInstance().setVendorRequestCommoddity(vendor_rqst_commodity);
                            VendorData.getInstance().setVendorEmail(vendor_email);
                            VendorData.getInstance().setVendorPassword(vendor_password);
                            VendorData.getInstance().setVendorProfile(vendor_pic_profile);
                            VendorData.getInstance().setAadharPic(vendor_pic_aadhar);
                            VendorData.getInstance().setPanpic(vendor_pic_pan);
                            VendorData.getInstance().setGSTPic(vendor_pic_gst);
                            VendorData.getInstance().setMandiPic(vendor_pic_mandi_license);
                            VendorData.getInstance().setVendorAddress(vendor_address);
                            VendorData.getInstance().setVendorBusinessType(vendor_business_type);
                            VendorData.getInstance().setVendorCompanyName(vendor_company_shop_name);
                            VendorData.getInstance().setVendorCompanyAddress(vendor_company_shop_address);
                            VendorData.getInstance().setVendorPanCard(vendor_pan_shopact);
                            VendorData.getInstance().setGSTNO(vendor_gst);
                            VendorData.getInstance().setMandiLicenseNumber(vendor_mandi);
                            VendorData.getInstance().setVendorDealingIn(vendor_dealing_in);
                            VendorData.getInstance().setUOM(vendor_uom);

                            CustomIntent.startActivity(ScrLogin.this, ScrHome.class);

                            CustomToast.showToast(ScrLogin.this, vendor_name + "");

                        } else if (UserData.getInstance().getUserType() == UserData.ROLE_BROKER) {
                            Log.d("TAG", "onResponse: inside broker ");

                            int broker_id = object1.getInt("broker_id");
                            int broker_status = object1.getInt("broker_status");
                            String broker_name = object1.getString("broker_name");
                            String broker_mobile = object1.getString("broker_mobile");
                            String broker_email = object1.getString("broker_email");
                            String broker_password = object1.getString("broker_password");
                            String broker_profile_pic = object1.getString("broker_profile_pic");
                            String broker_gender = object1.getString("broker_gender");
                            String broker_age = object1.getString("broker_age");
                            String broker_address = object1.getString("broker_address");
                            String broker_work_exp = object1.getString("broker_work_exp");
                            String broker_dealing_in = object1.getString("broker_dealing_in");
                            String broker_uom = object1.getString("broker_uom");

                            BrokerData.getInstance().setBrokerId(broker_id);
                            BrokerData.getInstance().setBrokerName(broker_name);
                            BrokerData.getInstance().setBrokerMobile(broker_mobile);
                            BrokerData.getInstance().setBrokerEmail(broker_email);
                            BrokerData.getInstance().setBrokerPassword(broker_password);
                            BrokerData.getInstance().setBrokerProfile(broker_profile_pic);
                            BrokerData.getInstance().setBrokerGender(broker_gender);
                            BrokerData.getInstance().setBrokerAge(broker_age);
                            BrokerData.getInstance().setBrokerAddress(broker_address);
                            BrokerData.getInstance().setBrokerWorkExperience(broker_work_exp);
                            BrokerData.getInstance().setBROKER_DEALING_IN(broker_dealing_in);
                            BrokerData.getInstance().setBrokerUOM(broker_uom);

                            CustomIntent.startActivity(ScrLogin.this, ScrHome.class);

                            CustomToast.showToast(ScrLogin.this, broker_name + "");
                        }

                    } else if (message == Messages.FAILED) {
                        CustomToast.showToast(ScrLogin.this, "Wrong Credentials.");
                    } else if (message == Messages.INACTIVE_USER) {
                        CustomToast.showToast(ScrLogin.this, "This account is inactive.");
                    } else if (message == Messages.NOT_REGISTERED) {
                        CustomToast.showToast(ScrLogin.this, "This contact is not registered with us.");
                    } else if (message == Messages.ALREADY_EXIST) {
                        CustomToast.showToast(ScrLogin.this, "User already logged in with this credentials.");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: JSONException : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: login " + type + " : " + t.getMessage());
                CustomDialog.closeDialog(ScrLogin.this);
            }
        });
    }

    private void showDialog(String message, final String title) {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_validation);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText(title + "");
        tv_text.setText(message + "");

        Button bt_no = dialog.findViewById(R.id.bt_no);
        bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("Okay");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.setCancelable(true);
        dialog.show();

    }
}