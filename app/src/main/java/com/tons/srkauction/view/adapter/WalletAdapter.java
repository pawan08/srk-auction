package com.tons.srkauction.view.adapter;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.Wallet;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrWallet;

import java.util.ArrayList;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.ViewHolderBid> {


    private ArrayList<Wallet> list;
    private ScrWallet context;

    public WalletAdapter(ScrWallet context) {
        this.context = context;

    }

    public void setList(ArrayList<Wallet> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_wallet_transactions, null);
        return new ViewHolderBid(view);
    }



    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        Wallet wallet = list.get(i);
        holder.tv_date.setText((wallet.getWallet_added_time()));
        holder.tv_change_time.setText(DateToTimeStamp.getDateForNotification(wallet.getWallet_changed_time()));
        if (Float.parseFloat(wallet.getWallet_credit()) != 0) {
            holder.tv_tittle.setText("Amount added to wallet");
            holder.tv_amount.setText("+ " + wallet.getWallet_credit());
            holder.tv_amount.setTextColor(Color.GREEN);
        } else {
            holder.tv_tittle.setText("Amount debited from wallet");
            holder.tv_amount.setText("- " + wallet.getWallet_debit());
            holder.tv_amount.setTextColor(Color.RED);
        }
    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder {
        private TextView tv_tittle, tv_date, tv_amount, tv_change_time;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);
            tv_tittle = itemView.findViewById(R.id.tv_tittle);
            tv_date = itemView.findViewById(R.id.tv_date);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_change_time = itemView.findViewById(R.id.tv_change_time);
        }

    }
}
