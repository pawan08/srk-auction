package com.tons.srkauction.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.model.RejectedWalletHistory;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrViewProfileImage;

import java.util.ArrayList;

public class RejetedWalletHistoryAdapter extends RecyclerView.Adapter<RejetedWalletHistoryAdapter.RejectedViewHolder> {

    private Context context;
    private ArrayList<RejectedWalletHistory> list;
    private RejectedWalletHistory rejectedWalletHistory;

    public RejetedWalletHistoryAdapter(Context context) {

        this.context = context;
    }

    public void setList(ArrayList<RejectedWalletHistory> list) {

        this.list = list;
    }

    @NonNull
    @Override
    public RejectedViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_reject, parent, false);
        return new RejectedViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RejectedViewHolder holder, int position) {

        rejectedWalletHistory = list.get(position);
        //   holder.tv_amount.setText(rejectedWalletHistory.getWallet_debit());
        holder.tv_date.setText(DateToTimeStamp.getDateForNotification(rejectedWalletHistory.getWallet_added_time()));
        holder.tv_remark.setText(rejectedWalletHistory.getWallet_admin_remark());


        if (Float.parseFloat(rejectedWalletHistory.getWallet_credit()) != 0) {
            holder.tv_amount.setText("" + rejectedWalletHistory.getWallet_credit());
        } else {
            holder.tv_amount.setText("" + rejectedWalletHistory.getWallet_debit());
        }

        if (rejectedWalletHistory.getWallet_vendor_attachment().equals("") && rejectedWalletHistory.getWallet_vendor_attachment() == null) {
            holder.tv_view_attachment.setVisibility(View.GONE);
        } else {
            holder.tv_view_attachment.setTag(rejectedWalletHistory);
            holder.tv_view_attachment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RejectedWalletHistory rejectedWalletHistory = (RejectedWalletHistory) view.getTag();
                    Intent intent = new Intent(context, ScrViewProfileImage.class);
                    intent.putExtra("image", CommunicationConstant.WALLET_IMAGE + rejectedWalletHistory.getWallet_vendor_attachment());
                    context.startActivity(intent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {

        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class RejectedViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_amount, tv_remark, tv_view_attachment, tv_date;

        public RejectedViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_remark = itemView.findViewById(R.id.tv_remark);
            tv_view_attachment = itemView.findViewById(R.id.tv_view_attachment);
            tv_date = itemView.findViewById(R.id.tv_date);
        }
    }

}
