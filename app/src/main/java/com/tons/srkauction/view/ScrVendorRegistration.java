package com.tons.srkauction.view;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.sayantan.advancedspinner.MultiSpinner;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.communication.volley_communication.VolleyMultipartRequest;
import com.tons.srkauction.communication.volley_communication.VolleySingleton;
import com.tons.srkauction.communication.volley_communication.datapart.DataPart;
import com.tons.srkauction.imagecrop.ImagePickerActivity;
import com.tons.srkauction.model.CommodityList;
import com.tons.srkauction.model.Comodity;
import com.tons.srkauction.model.StateList;
import com.tons.srkauction.model.StateModel;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.utils.MyBitmap;
import com.tons.srkauction.view.custom.MyCustomDialog;
import com.tons.srkauction.view.custom.RenameBottomSheetDialog;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrVendorRegistration extends AppCompatActivity implements View.OnClickListener, RenameBottomSheetDialog.BottomSheetListener, AdapterView.OnItemSelectedListener {

    String[] permissionArrays = new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};
    public static final int RequestPermissionCode = 1;
    public static final int RequestPermissionCode2 = 2;
    public static final int REQUEST_IMAGE = 100;
    private  SearchableSpinner sp_select_state;
    private String image_type = null;
    // private ArrayList<SpinnerBusinessType> b_type_list;
    private ArrayList<String> b_type_list;
    private ArrayAdapter<String> b_type_adapter;

    private ArrayAdapter<String> commodity_adapter;

    private EditText et_name;
    private EditText et_contact;
    private EditText et_email;
    private EditText et_address;
    private EditText et_password;
    private EditText et_company_name;
    private EditText et_company_address;
    private EditText et_pan_card_shop_act;
    private EditText et_gst_no;
    private EditText et_mandi_license_no;
    private EditText et_request_commodity;
    private EditText et_ref_name;
    private EditText et_ref_contact;
    private EditText et_ref_address;
    private EditText et_whatsapp;

    private Button bt_submit;

    private LinearLayout ll_profile_image;
    private LinearLayout ll_pan_card;
    private LinearLayout ll_aadhar_card;
    private LinearLayout ll_gst;
    private LinearLayout ll_mandi_license;
    private CircleImageView civ_profile_image;
    private ImageView civ_aadhar_card;
    private ImageView civ_pan_card;
    private ImageView civ_gst;
    private ImageView civ_mandi_license;
    private TextView tv_profile_image;
    private TextView tv_aadhar_card;
    private TextView tv_pan_card;
    private TextView tv_gst;
    private TextView tv_mandi_license;

    private Bitmap bitmap, aadhar_bitmap, pan_bitmap, gst_bitmap, mandi_bitmap, profile_bitmap;
    private Uri aadharimageUri, panimageUri, profileimageUri, gstimageUri, mandiimageUri, imageUri;
    private String y;
    private String pancard_pattern = "[A-Z]{5}[0-9]{4}[A-Z]{1}";
    private MultiSpinner sp_business_type;
    private MultiSpinner sp_vendor_dealing_in;
    private SearchableSpinner sp_uom;
    private ApiInterface apiInterface;
    MultipartBody.Part vendor_pic_gst;
    MultipartBody.Part vendor_pic_mandi_license;
    private EditText et_account_no,et_branch_name,et_bank_name,et_ifsc_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.cross_line));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_vendor_registration);

        initUI();
        getVendorDealingIn();
        getStateList();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //getVendorDealingIn();
    }

    public void initUI() {
        et_name = findViewById(R.id.et_name);
        et_contact = findViewById(R.id.et_contact);
        et_email = findViewById(R.id.et_email);
        et_address = findViewById(R.id.et_address);
        et_password = findViewById(R.id.et_password);
        et_company_name = findViewById(R.id.et_company_name);
        et_company_address = findViewById(R.id.et_company_address);
        et_pan_card_shop_act = findViewById(R.id.et_pan_card_shop_act);
        et_gst_no = findViewById(R.id.et_gst_no);
        et_mandi_license_no = findViewById(R.id.et_mandi_license_no);
        et_ref_name = findViewById(R.id.et_ref_name);

        et_ref_contact = findViewById(R.id.et_ref_contact);
        et_request_commodity = findViewById(R.id.et_request_commodity);
        et_whatsapp = findViewById(R.id.et_whatsapp);

        et_ref_address = findViewById(R.id.et_ref_address);

        et_branch_name = findViewById(R.id.et_branch_name);
        et_bank_name = findViewById(R.id.et_bank_name);
        et_account_no = findViewById(R.id.et_account_no);
        et_ifsc_code = findViewById(R.id.et_ifsc_code);

        sp_vendor_dealing_in = findViewById(R.id.sp_vendor_dealing_in);
        sp_uom = findViewById(R.id.sp_uom);
        sp_vendor_dealing_in.setOnItemSelectedListener(this);

        bt_submit = findViewById(R.id.bt_submit);
        bt_submit.setOnClickListener(this);

        ll_profile_image = findViewById(R.id.ll_profile_image);
        ll_profile_image.setOnClickListener(this);
        civ_profile_image = findViewById(R.id.civ_profile_image);
        civ_profile_image.setOnClickListener(this);
        tv_profile_image = findViewById(R.id.tv_profile_image);
        tv_profile_image.setOnClickListener(this);

        ll_aadhar_card = findViewById(R.id.ll_aadhar_card);
        ll_aadhar_card.setOnClickListener(this);
        civ_aadhar_card = findViewById(R.id.civ_aadhar_card);
        civ_aadhar_card.setOnClickListener(this);
        tv_aadhar_card = findViewById(R.id.tv_aadhar_card);
        tv_aadhar_card.setOnClickListener(this);

        sp_select_state = findViewById(R.id.sp_select_state);
        ll_pan_card = findViewById(R.id.ll_pan_card);
        ll_pan_card.setOnClickListener(this);
        civ_pan_card = findViewById(R.id.civ_pan_card);
        civ_pan_card.setOnClickListener(this);
        tv_pan_card = findViewById(R.id.tv_pan_card);
        tv_pan_card.setOnClickListener(this);

        ll_gst = findViewById(R.id.ll_gst);
        ll_gst.setOnClickListener(this);
        civ_gst = findViewById(R.id.civ_gst);
        civ_gst.setOnClickListener(this);
        tv_gst = findViewById(R.id.tv_gst);
        tv_gst.setOnClickListener(this);

        ll_mandi_license = findViewById(R.id.ll_mandi_license);
        ll_mandi_license.setOnClickListener(this);
        civ_mandi_license = findViewById(R.id.civ_mandi_license);
        civ_mandi_license.setOnClickListener(this);
        tv_mandi_license = findViewById(R.id.tv_mandi_license);
        tv_mandi_license.setOnClickListener(this);

        // called other init functions blow
        initBusinessType();


        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
    }


    public void initBusinessType() {

        sp_business_type = findViewById(R.id.sp_business_type);
        b_type_list = new ArrayList<>();
        //b_type_list.add("Select Business Type");
        b_type_adapter = new ArrayAdapter<String>(this, R.layout.item_custom_spinner, R.id.tv_text, b_type_list);
        sp_business_type.setAdapter(b_type_adapter);
        b_type_adapter.notifyDataSetChanged();

    }

    private void getStateList() {

        Call<String> call = apiInterface.get_states();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                Log.d("TAG", "onResponse: success : " + response.body().toString());

                try {
                    JSONObject object = new JSONObject(response.body());
                    int message = object.getInt("message");

                    switch (message) {

                        case Messages.SUCCESS:
                            StateList.getInstance().clearList();
                            JSONArray jsonArray = object.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String s_name = jsonObject1.getString("state_name");
                                int s_id = jsonObject1.getInt("state_id");
                                StateModel stateModel = new StateModel(s_name, s_id);
                                StateList.getInstance().add(stateModel);
                            }
                            ArrayAdapter<String> state_adapter = new ArrayAdapter<String>(ScrVendorRegistration.this, android.R.layout.simple_spinner_item, StateList.getInstance().getStateName());
                            sp_select_state.setAdapter(state_adapter);
                            state_adapter.notifyDataSetChanged();

                            break;

                        case Messages.FAILED:
                           // CustomToast.showToast(ScrAddCase.this, "DATA FETCH ERROR");
                            break;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                // CustomDialog.closeDialog(ScrAddCase.this);

            }
        });

    }


    public void getVendorDealingIn() {

        Call<String> call = apiInterface.commodity_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    CommodityList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    String status = jsonObject.getString("status");
                    if (status.equals("true")) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int commodity_id = jsonObject1.getInt("commodity_id");
                            String commodity_name = jsonObject1.getString("commodity_name");
                            Comodity comodity = new Comodity(commodity_id, commodity_name);
                            CommodityList.getInstance().add(comodity);
                            try {
                                sp_vendor_dealing_in.setSpinnerList(CommodityList.getInstance().getNames());
                                sp_vendor_dealing_in.setAdapter(commodity_adapter);
                                commodity_adapter.notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    } else {
                        Log.d("TAG", "onResponse: Vendor " + "Failed");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    public void showSuccessDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_login_success);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /*CustomIntent.startActivity(ScrVendorRegistration.this,ScrLogin.class,true);
                dialog.dismiss();*/
                finish();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void doValidate() {
        String name = et_name.getText().toString();
        String contact = et_contact.getText().toString();
        String email = et_email.getText().toString();
        String address = et_address.getText().toString();
        String passwword = et_password.getText().toString();
        String comp_name = et_company_name.getText().toString();
        String comp_address = et_company_address.getText().toString();
        String pan_card = et_pan_card_shop_act.getText().toString();
        String f = et_request_commodity.getText().toString();
        String whats_app_no = et_whatsapp.getText().toString();
        String gst_no = et_gst_no.getText().toString();
        String mandi_license_no = et_mandi_license_no.getText().toString();
        String branch_name = et_branch_name.getText().toString();
        String bank_name = et_bank_name.getText().toString();
        String acount_no = et_account_no.getText().toString();
        String ifsc_code = et_ifsc_code.getText().toString();
        // String vendor_dealing_in = et_vendor_dealing_in.getText().toString();

        if (TextUtils.isEmpty(name)) {
            MyCustomDialog.showValidationDialog("Please enter your name", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(contact)) {
            MyCustomDialog.showValidationDialog("Please enter your mobile number", "Oop's", this);
            return;
        } else if (!contact.matches("[0-9]{10}")) {
            MyCustomDialog.showValidationDialog("Mobile number should have 10 digits.", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(whats_app_no)) {
            MyCustomDialog.showValidationDialog("Please enter your What's app  number", "Oop's", this);
            return;
        } else if (!whats_app_no.matches("[0-9]{10}")) {
            MyCustomDialog.showValidationDialog(" What's app number should have 10 digits.", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(email)) {
            MyCustomDialog.showValidationDialog("Please enter your email", "Oop's", this);
            return;
        } else if (!email.matches(String.valueOf(Patterns.EMAIL_ADDRESS))) {
            MyCustomDialog.showValidationDialog("Please enter valid email", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(passwword)) {
            MyCustomDialog.showValidationDialog("Please enter password", "Oop's", this);
            return;
        }
        if (TextUtils.isEmpty(address)) {
            MyCustomDialog.showValidationDialog("Please enter address", "Oop's", this);
            return;
        }

        if (sp_select_state.getSelectedItemPosition() == 0) {
            MyCustomDialog.showValidationDialog("Please select state", "Oop's", this);
            return;
        }

        if (sp_business_type.getSelectedItems().size() == 0) {
            MyCustomDialog.showValidationDialog("Please select business type", "Oop's", this);
            return;
        }

        /*if (sp_vendor_dealing_in.getSelectedItemPosition() == -1) {
            MyCustomDialog.showValidationDialog("Please select Vendor Dealing In", "Oop's", this);
            return;
        }*/

        if (TextUtils.isEmpty(comp_name)) {
            MyCustomDialog.showValidationDialog("Please enter company / shop name", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(comp_address)) {
            MyCustomDialog.showValidationDialog("Please enter company / shop address", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(pan_card)) {
            MyCustomDialog.showValidationDialog("Please enter your Pan or Shop act license no", "Oop's", this);
            return;
        }

        if (sp_vendor_dealing_in.getSelectedItems().size() == 0) {
            MyCustomDialog.showValidationDialog("Please select vendor dealing in", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(bank_name)) {
            MyCustomDialog.showValidationDialog("Please enter your bank name", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(branch_name)) {
            MyCustomDialog.showValidationDialog("Please enter your branch name", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(acount_no)) {
            MyCustomDialog.showValidationDialog("Please enter your account number", "Oop's", this);
            return;
        }

        if (TextUtils.isEmpty(ifsc_code)) {
            MyCustomDialog.showValidationDialog("Please enter your ifsc code", "Oop's", this);
            return;
        }

        if (aadhar_bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select aadhar card photo", "Oop's", this);
            return;
        }

        if (pan_bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select pan card photo", "Oop's", this);
            return;
        }

        if (profile_bitmap == null) {
            MyCustomDialog.showValidationDialog("Please select profile photo", "Oop's", this);
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("vendor_name", et_name.getText().toString());
        params.put("vendor_dealing_in", sp_vendor_dealing_in.getSelectedItem().toString());
        params.put("vendor_mobile", et_contact.getText().toString());
        params.put("vendor_whatsapp_no", et_whatsapp.getText().toString());
        params.put("vendor_email", et_email.getText().toString());
        params.put("vendor_address", et_address.getText().toString());
        params.put("vendor_password", et_password.getText().toString());
        params.put("vendor_business_type", sp_business_type.getSelectedItem().toString());
        params.put("vendor_state", sp_select_state.getSelectedItem().toString());
        params.put("vendor_requested_commodity", et_request_commodity.getText().toString());
        params.put("vendor_company_shop_name", et_company_name.getText().toString());
        params.put("vendor_company_shop_address", et_company_address.getText().toString());
        params.put("vendor_pan_shopact", et_pan_card_shop_act.getText().toString());
        params.put("vendor_gst", et_gst_no.getText().toString());
        params.put("vendor_mandi", et_mandi_license_no.getText().toString());
        params.put("vendor_ref_name", et_ref_name.getText().toString());
        params.put("vendor_ref_mobile", et_ref_contact.getText().toString());
        params.put("vendor_ref_address", et_ref_address.getText().toString());
        params.put("vendor_uom", sp_uom.getSelectedItem().toString());
        params.put("vendor_bank_name", et_bank_name.getText().toString());
        params.put("vendor_branch_name", et_branch_name.getText().toString());
        params.put("vendor_ac_no", et_account_no.getText().toString());
        params.put("vendor_ifsc_code", et_ifsc_code.getText().toString());
        call_Vendor_Update_Profile(params);
    }


    private void call_Vendor_Update_Profile(final Map<String, String> params) {


        final String url = CommunicationConstant.VENDOR_REGISTER;

        CustomDialog.showDialog(ScrVendorRegistration.this, Constants.PROGRESS_MSG);

        VolleyMultipartRequest postRequest = new VolleyMultipartRequest(Request.Method.POST, url,
                new com.android.volley.Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {
                        CustomDialog.closeDialog(ScrVendorRegistration.this);
                        Log.d("TAG", "onResponse: vendor register " + new String(response.data));
                        Log.d("TAG", "onResponse: vendor register url " + url);
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            boolean status = jsonObject.getBoolean("status");
                            int message = jsonObject.getInt("message");
                            if (message == Messages.SUCCESS) {
                                showSuccessDialog();
                                //CustomToast.showToast(ScrVendorRegistration.this, "Registered Successfully");
                                //finish();

                            } else if (message == Messages.ALREADY_EXIST) {

                                CustomToast.showToast(ScrVendorRegistration.this, "User Already Exist");
                            } else {
                                CustomToast.showToast(ScrVendorRegistration.this, "Failed to register");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("REGISTER_RESPONSE", response.data.toString());

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        CustomDialog.closeDialog(ScrVendorRegistration.this);
                        Log.d("TAG", String.valueOf(error));
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                params.put("vendor_pic_profile", new DataPart("prfile_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(profile_bitmap)));
                params.put("vendor_pic_aadhar", new DataPart("aadhar_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(aadhar_bitmap)));
                params.put("vendor_pic_pan", new DataPart("pan_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(pan_bitmap)));

                if (gst_bitmap != null) {
                    Log.d("TAG", "getByteData:gst bitmap " + "not null");
                    params.put("vendor_pic_gst", new DataPart("gst_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(gst_bitmap)));
                } else {
                    Log.d("TAG", "getByteData:gst bitmap " + " null");
                }
                if (mandi_bitmap != null) {
                    Log.d("TAG", "getByteData:mandi bitmap " + "not null");
                    params.put("vendor_pic_mandi_license", new DataPart("mandi_" + VendorData.getInstance().getVendorId() + ".png", getFileDataFromDrawable(mandi_bitmap)));
                } else {
                    Log.d("TAG", "getByteData:mandi bitmap " + " null");
                }

                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(postRequest);
    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 30, byteArrayOutputStream);
        byte[] imageInByte = byteArrayOutputStream.toByteArray();
        long lengthbmp = imageInByte.length;
        Log.d("SIZETAG", String.valueOf(lengthbmp));
        return byteArrayOutputStream.toByteArray();
    }


    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.ll_aadhar_card:
            case R.id.civ_aadhar_card:
            case R.id.tv_aadhar_card:
                image_type = Constants.AADHAR_IMAGE;
                RenameBottomSheetDialog bottomSheet1 = new RenameBottomSheetDialog();
                bottomSheet1.show(getSupportFragmentManager(), "ADHAR");
                break;
            case R.id.ll_pan_card:
            case R.id.civ_pan_card:
            case R.id.tv_pan_card:
                image_type = Constants.PAN_IMAGE;
                RenameBottomSheetDialog bottomSheet2 = new RenameBottomSheetDialog();
                bottomSheet2.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.ll_gst:
            case R.id.civ_gst:
            case R.id.tv_gst:
                image_type = Constants.GST_IMAGE;
                RenameBottomSheetDialog bottomSheet3 = new RenameBottomSheetDialog();
                bottomSheet3.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.ll_mandi_license:
            case R.id.civ_mandi_license:
            case R.id.tv_mandi_license:
                image_type = Constants.MANDI_LICENSE_IMAGE;
                RenameBottomSheetDialog bottomSheet4 = new RenameBottomSheetDialog();
                bottomSheet4.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.ll_profile_image:
            case R.id.civ_profile_image:
            case R.id.tv_profile_image:
                image_type = Constants.PROFILE_IMAGE;
                RenameBottomSheetDialog bottomSheet = new RenameBottomSheetDialog();
                bottomSheet.show(getSupportFragmentManager(), "profile_photo");
                break;

            case R.id.bt_submit:
                doValidate();
                break;
        }
    }


    @Override
    public void onButtonClicked(int button_type) {
        switch (button_type) {
            case 1:
                y = "1";
                if ((ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(permissionArrays, RequestPermissionCode);
                        Log.d("TAG", "onClick: camera permission called");
                    }
                } else {
                    switch (image_type) {
                        case Constants.AADHAR_IMAGE:
                            launchAadharCamera();
                            break;

                        case Constants.PAN_IMAGE:
                            launchPanCamera();
                            break;

                        case Constants.GST_IMAGE:
                            launchGSTCamera();
                            break;

                        case Constants.MANDI_LICENSE_IMAGE:
                            launchMandiCamera();
                            break;

                        case Constants.PROFILE_IMAGE:
                            launchProfileCamera();
                            break;
                    }
                }
                break;

            case 2:
                y = "2";
                launchGalleryIntent();
                break;
        }
    }

    private void launchAadharCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        aadharimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, aadharimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchPanCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        panimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, panimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchGSTCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        gstimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, gstimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchMandiCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        mandiimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, mandiimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchProfileCamera() {
        Log.d("TAG", "camera 1");
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        profileimageUri = getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, profileimageUri);
        startActivityForResult(intent, REQUEST_IMAGE);

    }

    private void launchGalleryIntent() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_IMAGE);
    }

    private void launchCameraForProfile() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_IMAGE_CAPTURE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);

        // setting maximum bitmap width and height
        intent.putExtra(ImagePickerActivity.INTENT_SET_BITMAP_MAX_WIDTH_HEIGHT, true);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_WIDTH, 1000);
        intent.putExtra(ImagePickerActivity.INTENT_BITMAP_MAX_HEIGHT, 1000);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(intent, REQUEST_IMAGE);
    }

    private void launchGalleryForProfile() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        intent.putExtra(ImagePickerActivity.INTENT_IMAGE_PICKER_OPTION, ImagePickerActivity.REQUEST_GALLERY_IMAGE);

        // setting aspect ratio
        intent.putExtra(ImagePickerActivity.INTENT_LOCK_ASPECT_RATIO, true);
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_X, 1); // 16x9, 1x1, 3:4, 3:2
        intent.putExtra(ImagePickerActivity.INTENT_ASPECT_RATIO_Y, 1);
        startActivityForResult(intent, REQUEST_IMAGE);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("TAG", "onActivityResult: on start of onactivityresult");
        if (requestCode == REQUEST_IMAGE) {
            if (resultCode == Activity.RESULT_OK) {
                assert data != null;
                Log.d("TAG", "onActivityResult: y = " + y);
                switch (y) {
                    case "1":
                        switch (image_type) {
                            case Constants.AADHAR_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url1 = null;
                                try {

                                    aadhar_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), aadharimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input = this.getContentResolver().openInputStream(aadharimageUri);
                                    ExifInterface exif = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif = new ExifInterface(input);
                                            int orientation2 = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            aadhar_bitmap = MyBitmap.rotateBitmap(aadhar_bitmap, orientation2);
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                        } else {
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url1);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.PAN_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url2 = null;
                                try {

                                    pan_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), panimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input2 = this.getContentResolver().openInputStream(panimageUri);
                                    ExifInterface exif2 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif2 = new ExifInterface(input2);
                                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            pan_bitmap = MyBitmap.rotateBitmap(pan_bitmap, orientation2);
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                        } else {
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url2);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.GST_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url3 = null;
                                try {

                                    gst_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), gstimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input3 = this.getContentResolver().openInputStream(gstimageUri);
                                    ExifInterface exif3 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif3 = new ExifInterface(input3);
                                            int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            gst_bitmap = MyBitmap.rotateBitmap(gst_bitmap, orientation2);
                                            civ_gst.setImageBitmap(gst_bitmap);
                                        } else {
                                            civ_gst.setImageBitmap(gst_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url3);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.MANDI_LICENSE_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url4 = null;
                                try {

                                    mandi_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), mandiimageUri);
                                    // url1 = getRealPathFromURI(imageUri1);
                                    InputStream input4 = this.getContentResolver().openInputStream(mandiimageUri);
                                    ExifInterface exif4 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif4 = new ExifInterface(input4);
                                            int orientation2 = exif4.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            mandi_bitmap = MyBitmap.rotateBitmap(mandi_bitmap, orientation2);
                                            civ_mandi_license.setImageBitmap(mandi_bitmap);
                                        } else {
                                            civ_mandi_license.setImageBitmap(mandi_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url4);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;

                            case Constants.PROFILE_IMAGE:
                                Log.d("TAG", "onActivityResult: inside case 1 ");
                                String url5 = null;
                                try {

                                    profile_bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), profileimageUri);
                                    InputStream input5 = this.getContentResolver().openInputStream(profileimageUri);
                                    ExifInterface exif5 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif5 = new ExifInterface(input5);
                                            int orientation2 = exif5.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            profile_bitmap = MyBitmap.rotateBitmap(profile_bitmap, orientation2);
                                            civ_profile_image.setImageBitmap(profile_bitmap);
                                        } else {
                                            civ_profile_image.setImageBitmap(profile_bitmap);
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    Log.d("TAG", "onActivityResult: camera id proof ");
                                    break;

                                } catch (Exception e) {
                                    e.printStackTrace();
                                    Log.d("TAG", "onActivityResult: x = 1 : " + url5);
                                    Log.d("TAG", "onActivityResult: Exception : " + e.getMessage());
                                }
                                break;
                        }
                        break;

                    case "2":
                        try {
                            switch (image_type) {
                                case Constants.AADHAR_IMAGE:
                                    final Uri imageUri = data.getData();
                                    final InputStream imageStream;
                                    imageStream = getContentResolver().openInputStream(imageUri);
                                    aadhar_bitmap = BitmapFactory.decodeStream(imageStream);
                                    ExifInterface exif1 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif1 = new ExifInterface(imageStream);
                                            assert exif1 != null;
                                            int orientation2 = exif1.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            aadhar_bitmap = MyBitmap.rotateBitmap(aadhar_bitmap, orientation2);
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_aadhar_card.setImageBitmap(aadhar_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id p0roof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }

                                    break;

                                case Constants.PAN_IMAGE:
                                    final Uri imageUri2 = data.getData();
                                    InputStream imageStream2 = null;
                                    imageStream2 = getContentResolver().openInputStream(imageUri2);
                                    pan_bitmap = BitmapFactory.decodeStream(imageStream2);
                                    ExifInterface exif2 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif2 = new ExifInterface(imageStream2);
                                            assert exif2 != null;
                                            int orientation2 = exif2.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            pan_bitmap = MyBitmap.rotateBitmap(pan_bitmap, orientation2);
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_pan_card.setImageBitmap(pan_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.GST_IMAGE:
                                    final Uri imageUri3 = data.getData();
                                    InputStream imageStream3 = null;
                                    imageStream3 = getContentResolver().openInputStream(imageUri3);
                                    gst_bitmap = BitmapFactory.decodeStream(imageStream3);
                                    ExifInterface exif3 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif3 = new ExifInterface(imageStream3);
                                            assert exif3 != null;
                                            int orientation2 = exif3.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            gst_bitmap = MyBitmap.rotateBitmap(gst_bitmap, orientation2);
                                            civ_gst.setImageBitmap(gst_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_gst.setImageBitmap(gst_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.MANDI_LICENSE_IMAGE:
                                    final Uri imageUri4 = data.getData();
                                    InputStream imageStream4 = null;
                                    imageStream4 = getContentResolver().openInputStream(imageUri4);
                                    mandi_bitmap = BitmapFactory.decodeStream(imageStream4);
                                    ExifInterface exif4 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif4 = new ExifInterface(imageStream4);
                                            assert exif4 != null;
                                            int orientation2 = exif4.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            mandi_bitmap = MyBitmap.rotateBitmap(mandi_bitmap, orientation2);
                                            civ_mandi_license.setImageBitmap(mandi_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_mandi_license.setImageBitmap(mandi_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;

                                case Constants.PROFILE_IMAGE:
                                    final Uri imageUri5 = data.getData();
                                    InputStream imageStream5 = null;
                                    imageStream5 = getContentResolver().openInputStream(imageUri5);
                                    profile_bitmap = BitmapFactory.decodeStream(imageStream5);
                                    ExifInterface exif5 = null;
                                    try {
                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                            exif5 = new ExifInterface(imageStream5);
                                            assert exif5 != null;
                                            int orientation2 = exif5.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
                                            profile_bitmap = MyBitmap.rotateBitmap(profile_bitmap, orientation2);
                                            civ_profile_image.setImageBitmap(profile_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N+");
                                        } else {
                                            civ_profile_image.setImageBitmap(profile_bitmap);
                                            Log.d("TAG", "onActivityResult: gallery id proof N-");
                                        }
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    break;
                            }
                            break;
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                            Toast.makeText(ScrVendorRegistration.this, "Something went wrong", Toast.LENGTH_LONG).show();
                        }
                }

            }
        }
        Log.d("TAG", "onActivityResult: on end of onactivityresult");
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}