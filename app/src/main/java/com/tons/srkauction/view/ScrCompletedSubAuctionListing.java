package com.tons.srkauction.view;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CompletedSubAuction;
import com.tons.srkauction.model.CompletedSubAuctionList;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.adapter.CompletedSubAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrCompletedSubAuctionListing extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private CompletedSubAuctionAdapter subAuctionAdapter;
    private RecyclerView rv_sub_auction;
    private LinearLayout ll_no_data;
    private SwipeRefreshLayout swipe_refresh_layout;
    private ApiInterface apiInterface;
    private String auction_id,auction_status;
    private TextView tv_auction_date, tv_commodity, tv_qty, tv_unit_price, tv_auction_code;
    private LinearLayout ll_root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_src_sub_auction_details);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);

        Intent intent = getIntent();
        auction_id = intent.getStringExtra("auction_id");
    }

    public void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Completed Sub-Auction");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);


        rv_sub_auction = findViewById(R.id.rv_sub_auction);
        subAuctionAdapter = new CompletedSubAuctionAdapter(ScrCompletedSubAuctionListing.this);
        rv_sub_auction.setAdapter(subAuctionAdapter);
        rv_sub_auction.setLayoutManager(new LinearLayoutManager(this));

        ll_no_data = findViewById(R.id.ll_no_data);
        tv_auction_date = findViewById(R.id.tv_auction_date);
        tv_commodity = findViewById(R.id.tv_commodity);
        tv_qty = findViewById(R.id.tv_qty);
        tv_unit_price = findViewById(R.id.tv_unit_price);
        tv_auction_code = findViewById(R.id.tv_auction_code);
        ll_root = findViewById(R.id.ll_root);

        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          call_SubAuctionDetailsApi();
                                      }
                                  }
        );
    }

    public void showEmptyLayout() {
        if (subAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }

    public void call_SubAuctionDetailsApi() {
        Map<String, String> params = new HashMap<>();
        params.put("auction_id", String.valueOf(auction_id));
        params.put("vendor_id", String.valueOf(VendorData.getInstance().getVendorId()));

        Call<String> call = apiInterface.get_completed_sub_auction_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Sub Auction Details" + response.body());
                Log.d("TAG", "Sub Auction Details Url " + call.request().url().toString());

                CompletedSubAuctionList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        ll_root.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            //auction_id  = jsonObject1.getInt("auction_id");
                            String auction_code = jsonObject1.getString("auction_code");
                            String auction_commodity = jsonObject1.getString("auction_commodity");
                            String auction_start_date = jsonObject1.getString("auction_start_date");
                            String auction_start_time = jsonObject1.getString("auction_start_time");
                            auction_status = jsonObject1.getString("auction_status");

                            tv_auction_code.setText("CODE : " + auction_code);
                            tv_auction_date.setText("Auction Date : " + DateToTimeStamp.getDate(auction_start_date) + " " + DateToTimeStamp.getTime(auction_start_time+":00"));
                            tv_commodity.setText("Commodity : " + auction_commodity);

                        }

                        JSONArray jsonArray1 = jsonObject.getJSONArray("data1");
                        for (int i = 0; i < jsonArray1.length(); i++) {

                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);
                            int reqsub_auc_id = jsonObject1.getInt("reqsub_auc_id");
                            String reqsub_auc_qty = jsonObject1.getString("reqsub_auc_qty");
                            String reqsub_auc_location = jsonObject1.getString("reqsub_auc_location");
                            String reqsub_auc_unit_price = jsonObject1.getString("reqsub_auc_unit_price");
                            String reqsub_auc_emd = jsonObject1.getString("reqsub_auc_emd");
                            String current_time = jsonObject1.getString("current_time");
                            String end_date_time = jsonObject1.getString("end_date_time");
                            String vendor_name = jsonObject1.getString("vendor_name");
                            int reqsub_auc_status = jsonObject1.getInt("reqsub_auc_status");
                            String balance_wallet_amt = jsonObject1.getString("balance_wallet_amt");
                            String pay_emd = jsonObject1.getString("pay_emd");
                            String add_money_into_wallet = jsonObject1.getString("add_money_into_wallet");
                            int wallet_status = jsonObject1.getInt("wallet_status");
                            int reqsub_auc_bid_winner_acceptation = jsonObject1.getInt("reqsub_auc_bid_winner_acceptation");
                            int reqsub_auc_vendor_acceptation = jsonObject1.getInt("reqsub_auc_vendor_acceptation");
                            String is_winner = jsonObject1.getString("is_winner");
                            String is_mine_auction = jsonObject1.getString("is_mine_auction");

                            String reqsub_auc_end_time = jsonObject1.getString("reqsub_auc_end_time");
                            String acceptation_time = jsonObject1.getString("acceptation_time");

                            CompletedSubAuction subAuction = new CompletedSubAuction(Integer.parseInt(auction_id), reqsub_auc_id, reqsub_auc_qty, reqsub_auc_location, reqsub_auc_unit_price, reqsub_auc_emd, current_time, end_date_time, vendor_name, reqsub_auc_status, balance_wallet_amt, pay_emd, add_money_into_wallet, wallet_status,reqsub_auc_bid_winner_acceptation,reqsub_auc_vendor_acceptation,is_winner,is_mine_auction,reqsub_auc_end_time,acceptation_time);
                            CompletedSubAuctionList.getInstance().add(subAuction);
                            subAuctionAdapter.setList(CompletedSubAuctionList.getInstance().getList());
                            subAuctionAdapter.notifyDataSetChanged();
                        }


                    }
                    swipe_refresh_layout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse:JSON exception " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }


    @Override
    public void onRefresh() {
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          call_SubAuctionDetailsApi();
                                      }
                                  }
        );
    }
}
