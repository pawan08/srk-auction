package com.tons.srkauction.view;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class ScrSelectRole extends AppCompatActivity implements View.OnClickListener {

    private Button bt_continue;

    private LinearLayout ll_vendor;
    private LinearLayout ll_broker;

    private ImageView iv_vendor;
    private ImageView iv_broker;

    private TextView tv_vendor;
    private TextView tv_broker;
    private TextView tv_support;

    private TextView tv_error_header;
    private ApiInterface apiInterface;
    private TextView tv_title, tv_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.cross_line));
                getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_select_role);
        initUI();
        setSelectedvendor();
    }

    private void initUI() {
        ll_vendor = findViewById(R.id.ll_vendor);
        ll_vendor.setOnClickListener(this);
        ll_broker = findViewById(R.id.ll_broker);
        ll_broker.setOnClickListener(this);

        iv_broker = findViewById(R.id.iv_broker);
        iv_vendor = findViewById(R.id.iv_vendor);

        tv_error_header = findViewById(R.id.tv_error_header);

        tv_vendor = findViewById(R.id.tv_vendor);
        tv_broker = findViewById(R.id.tv_broker);

        tv_support = findViewById(R.id.tv_support);
        tv_support.setOnClickListener(this);

        bt_continue = findViewById(R.id.bt_continue);
        bt_continue.setOnClickListener(this);

        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);


    }

    public void setSelectedvendor() {
        UserData.getInstance().setUserType(UserData.ROLE_VENDOR);
        iv_vendor.setImageResource(R.drawable.ic_tick_blue);
        tv_vendor.setTextColor(getResources().getColor(R.color.colorPrimary));

        iv_broker.setImageResource(R.drawable.ic_tick_light);
        tv_broker.setTextColor(getResources().getColor(R.color.grey));
        tv_error_header.setText("Continue as a Vendor.");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_continue:
                if (UserData.getInstance().getUserType() == -1) {
                    CustomToast.showToast(ScrSelectRole.this, "Please select Role");
                    return;
                }
                Log.d("TAG", "onClick: user type = " + UserData.getInstance().getUserType());
                CustomIntent.startActivity(ScrSelectRole.this, ScrLogin.class, false);
                break;

            case R.id.ll_vendor:
                UserData.getInstance().setUserType(UserData.ROLE_VENDOR);
                iv_vendor.setImageResource(R.drawable.ic_tick_blue);
                tv_vendor.setTextColor(getResources().getColor(R.color.colorPrimary));

                iv_broker.setImageResource(R.drawable.ic_tick_light);
                tv_broker.setTextColor(getResources().getColor(R.color.grey));

                tv_error_header.setText("Continue as a Vendor.");

                break;

            case R.id.ll_broker:
                UserData.getInstance().setUserType(UserData.ROLE_BROKER);

                iv_broker.setImageResource(R.drawable.ic_tick_blue);
                tv_broker.setTextColor(getResources().getColor(R.color.colorPrimary));

                iv_vendor.setImageResource(R.drawable.ic_tick_light);
                tv_vendor.setTextColor(getResources().getColor(R.color.grey));

                tv_error_header.setText("Continue as a Broker.");

                break;

            case R.id.tv_support:
                showBiddingDialog();
        }
    }

    private void showBiddingDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_support);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tv_title = dialog.findViewById(R.id.tv_sup_title);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        tv_desc = dialog.findViewById(R.id.tv_sup_desc);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        call_support_api();
        dialog.show();
    }

    public void call_support_api() {
        Call<String> call = apiInterface.get_support_details();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrSelectRole.this);
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject object = new JSONObject(response.body());
                    Log.d("TAG", "onResponseSupport" + response.body());
                    boolean status = object.getBoolean("status");
                    int message = object.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONArray array = object.getJSONArray("data");
                        JSONObject object1 = array.getJSONObject(0);
                        String title = object1.getString("sup_title");
                        String desc = object1.getString("sup_desc");
                        tv_title.setText(title);
                        tv_desc.setText(Html.fromHtml(desc));

                    } else if (message == Messages.FAILED) {
                        CustomToast.showToast(ScrSelectRole.this, "Error in fetching data");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d("TAG", "onResponse: JSONException : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrSelectRole.this);
                Log.d("TAG", "onFailure: res " + t.getMessage());
            }
        });
    }
}