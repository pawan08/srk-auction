package com.tons.srkauction.view;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.transition.AutoTransition;
import android.transition.TransitionManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.BidList;
import com.tons.srkauction.model.BidModel;
import com.tons.srkauction.model.Broker;
import com.tons.srkauction.model.BrokerBidList;
import com.tons.srkauction.model.BrokerBidModel;
import com.tons.srkauction.model.BrokerList;
import com.tons.srkauction.model.Vendor;
import com.tons.srkauction.model.VendorList;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.adapter.BidAdapter;
import com.tons.srkauction.view.adapter.BrokerBidAdapter;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

public class ScrBrokerBidding extends AppCompatActivity implements View.OnClickListener {

    private String TAG = ScrBrokerBidding.class.getName();

    private ApiInterface apiInterface;

    private RecyclerView rv_bid;
    private BrokerBidAdapter brokerBidAdapter;

    private CardView base_cardview;
    private LinearLayout ll_show_more;
    private LinearLayout ll_no_data;

    private TextView headerText;
    private TextView tv_timer;
    private TextView tv_commodity, tv_view_details, tv_auction_code, tv_bag_type, tv_no_of_bags, tv_qty, tv_location, tv_warehouse, tv_auction_date, tv_unit_price;
    private ImageButton ib_expand_more;
    private ImageView headerImage;

    private Button bt_bid_now;

    public Timer timer;
    String auction_date, location, commodity, no_of_bag, warehouse, bag_type;
    private String bid_amount;
    private String reqsub_auc_id, reqsub_auc_auction_id;
    int quantity, unit_price, amount;
    private int reqsub_auc_vendor_id;
    boolean isFirstTimeForDialoge = true;
    boolean isFirstTimeForTimer = true;

    private String new_end_date_time = null;
    String last_bid = null;
    FloatingActionButton fb_add_bid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
            } else {
                getWindow().setStatusBarColor(Color.WHITE);
            }
        }
        setContentView(R.layout.activity_scr_bidding);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        getIntentData();
        initIUI();
        getSubAuctionDetails();
    }

    public void getIntentData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            reqsub_auc_auction_id = bundle.getString("reqsub_auc_auction_id");
            reqsub_auc_id = bundle.getString("reqsub_auc_id");
            reqsub_auc_vendor_id = bundle.getInt("vendor_id");

        }

        Log.d(TAG, "getIntentData: vendor_id" + reqsub_auc_vendor_id);
        Log.d(TAG, "getIntentData: reqsub_auc_auction_id" + reqsub_auc_auction_id);
        Log.d(TAG, "getIntentData: reqsub_auc_id" + reqsub_auc_id);
    }

    public void initIUI() {
        ll_no_data = findViewById(R.id.ll_no_data);
        Toolbar toolbar = findViewById(R.id.toolbar);
        headerText = toolbar.findViewById(R.id.tv_header);
        headerImage = toolbar.findViewById(R.id.iv_back);
        headerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        headerText.setText("Bidding");

        tv_timer = findViewById(R.id.tv_timer);
        base_cardview = findViewById(R.id.base_cardview);
        fb_add_bid = findViewById(R.id.fb_add_bid);
        fb_add_bid.setOnClickListener(this);
        tv_auction_code = findViewById(R.id.tv_auction_code);
        tv_view_details = findViewById(R.id.tv_view_details);
        tv_view_details.setOnClickListener(this);
        rv_bid = findViewById(R.id.rv_bid);
        rv_bid.setLayoutManager(new GridLayoutManager(this, 1));
        brokerBidAdapter = new BrokerBidAdapter(this);
        rv_bid.setAdapter(brokerBidAdapter);


    }

    public void showEmptyLayout() {
        if (brokerBidAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopTimer();
    }

    private void startTimer() {
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            public void run() {
                call_bidd_list();
                getAuctionEndTime();
                Log.d("TIMER", "Updating");
            }
        };
        timer.schedule(timerTask, 2, 1000);
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            Log.d("TIMER", "stopTimer: timer stopped.");
        }
        Log.d("TIMER", "stopTimer: end line");
    }

    private void call_bidd_list() {
        Map<String, String> params = new HashMap<>();
        Log.d(TAG, "getIntentData: vendor_id" + reqsub_auc_vendor_id);
        params.put("vendor_id", String.valueOf(reqsub_auc_vendor_id));
        params.put("auction_id", reqsub_auc_auction_id);
        params.put("sub_auction_id", reqsub_auc_id);

        Call<String> call = apiInterface.get_bid_list(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                BrokerBidList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    Log.d("TAG", "GetWalletHistory" + response.body());
                    Log.d("TAG", "GetWalletHistory url : " + call.request().url().toString());


                    int message = jsonObject.getInt("message");

                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            int bid_id = jsonObject1.getInt("bid_id");
                            int bid_vendor_id = jsonObject1.getInt("bid_vendor_id");
                            int bid_broker_id = jsonObject1.getInt("bid_broker_id");
                            String bid_added_time = jsonObject1.getString("bid_added_time");
                            bid_amount = jsonObject1.getString("bid_amount");
                            String vendor_name = jsonObject1.getString("vendor_name");
                            String broker_name = jsonObject1.getString("broker_name");


                            BrokerBidModel bidModel = new BrokerBidModel(bid_id, bid_vendor_id, bid_broker_id, bid_added_time, bid_amount, vendor_name, broker_name, reqsub_auc_vendor_id);
                            BrokerBidList.getInstance().add(bidModel);
                        }
                        brokerBidAdapter.setList(BrokerBidList.getInstance().getList());
                        brokerBidAdapter.notifyDataSetChanged();
                    } else {

                    }
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //swipeRefreshLayout.setRefreshing(false);

            }

        });
    }

    private void getSubAuctionDetails() {
        CustomDialog.showDialog(ScrBrokerBidding.this, "Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("reqsub_auc_id", reqsub_auc_id);
        Call<String> call = apiInterface.get_sub_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrBrokerBidding.this);
                Log.d("TAG", "onResponse: Add_bid url  " + call.request().url());
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            tv_auction_code.setText(jsonObject1.getString("auction_code"));
                            auction_date = jsonObject1.getString("auction_start_date");
                            bag_type = jsonObject1.getString("reqsub_auc_bag_type");
                            commodity = jsonObject1.getString("auction_commodity");
                            no_of_bag = jsonObject1.getString("reqsub_auc_no_of_bag");
                            warehouse = jsonObject1.getString("reqsub_auc_warehouse");
                            location = jsonObject1.getString("reqsub_auc_location");
                            unit_price = jsonObject1.getInt("reqsub_auc_unit_price");
                            quantity = jsonObject1.getInt("reqsub_auc_qty");
                        }

                    } else {
                        CustomToast.showToast(ScrBrokerBidding.this, "Failed To Add Bid");

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrBrokerBidding.this);
                Log.d("TAG", "onFailure: " + t.getMessage());
                CustomToast.showToast(ScrBrokerBidding.this, Constants.WENT_WRONG);
            }
        });
    }

    private void getAuctionEndTime() {
        Call<String> call = apiInterface.get_auction_end_time(Integer.parseInt(reqsub_auc_auction_id));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d(TAG, "onResponse: getAuctionEndTime " + response.body());
                Log.d(TAG, "onResponse: getAuctionEndTime url" + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        JSONObject object = jsonArray.getJSONObject(0);
                        final String current_time = object.getString("current_time");
                        String auction_start_date = object.getString("auction_start_date");
                        String auction_start_time = object.getString("auction_start_time");
                        String end_date_time = object.getString("end_date_time");


                        Log.d(TAG, "onResponse: current_time : " + current_time + " end_date_time : " + end_date_time);
                        if (new_end_date_time == null) {
                            new_end_date_time = end_date_time;
                        } else {
                            if (new_end_date_time.equals(end_date_time)) {
                                Log.d(TAG, "onResponse: both equal");
                                if (isFirstTimeForTimer) {
                                    isFirstTimeForTimer = false;

                                    final Handler handler = new Handler(Looper.getMainLooper());
                                    handler.postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            //Do something after 1s
                                            Log.d("TAG", "Updating");
                                            timer_calculation(current_time, new_end_date_time + ":00");
                                            handler.postDelayed(this, 1000);
                                        }
                                    }, 1000);
                                }
                                return;
                            } else {
                                new_end_date_time = end_date_time;
                                isFirstTimeForTimer = true;
                            }
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSON Exception : " + e.getMessage());
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d(TAG, "onFailure: " + t.getMessage());
            }
        });
    }

    private void timer_calculation(String start_date_time, String end_date_time) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {

                futureDate = dateFormat.parse(start_date_time);
                futureDate1 = dateFormat.parse(end_date_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                // tv_timer.setText("" + String.format("%02d", days));
                int minute = Integer.parseInt(String.format("%02d", minutes));
                int second = Integer.parseInt(String.format("%02d", seconds));
                Log.d("TAG", "updateDateTime: minutes : " + minute + " seconds : " + second);
                tv_timer.setText("Starts in " + String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                //tv_timer.setText(""+ String.format("%02d", minutes));
                //tv_timer.setText(""+ String.format("%02d", seconds));
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    Log.d("TAG", "updateDateTime: Hours : " + hours + " minutes : " + minutes + " seconds : " + seconds);
                    // tv_timer.setText("" + String.format("%02d", days));
                    if (hours == 0 && minutes < 10) {
                        tv_timer.setText("Hurry Time is Ending");
                    } else {
                        tv_timer.setText("Ends In " + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                    }
                    //tv_timer.setText(""+ String.format("%02d", minutes));
                    //tv_timer.setText(""+ String.format("%02d", seconds));
                } else {
                    tv_timer.setText("The Auction has finished!");

                    if (isFirstTimeForDialoge) {
                        isFirstTimeForDialoge = false;

                        if (tv_timer.getText().toString().equals("The Auction has finished!")) {
                            if (tv_timer.getVisibility() == VISIBLE) {
                                showDialog();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDialog() {
        ForegroundColorSpan foregroundColorSpan = new ForegroundColorSpan(getResources().getColor(R.color.text_black));
        AlertDialog.Builder builder1 = new AlertDialog.Builder(ScrBrokerBidding.this);

        builder1.setTitle("Auction Ends");
        builder1.setMessage("Better luck next time. Auction has finished");

        builder1.setCancelable(false);
        builder1.setPositiveButton("Try Next Time", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        AlertDialog alert11 = builder1.create();
        alert11.setCancelable(false);
        alert11.setCanceledOnTouchOutside(false);
        alert11.show();
    }


    private void callAddBidApi(final Dialog dialog, final TextView tv_error, String amount, String min_bid, int vendor_id) {
        CustomDialog.showDialog(ScrBrokerBidding.this, "Please Wait");
        Map<String, String> params = new HashMap<>();
        params.put("reqsub_auc_auction_id", reqsub_auc_auction_id);
        params.put("reqsub_auc_id", reqsub_auc_id);
        params.put("vendor_id", String.valueOf(vendor_id));
        params.put("broker_id", String.valueOf(BrokerData.getInstance().getBrokerId()));
        params.put("unit_bid_amount", amount);
        params.put("bid_amount", amount);
        Call<String> call = apiInterface.add_bid(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrBrokerBidding.this);
                Log.d("TAG", "onResponse: Add_bid " + response.body());
                Log.d("TAG", "onResponse: Add_bid url  " + call.request().url());
                if (!response.isSuccessful()) {
                    return;
                }

                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        CustomToast.showToast(ScrBrokerBidding.this, "Bid Added Successfully");
                        dialog.dismiss();
                    } else if (message == Messages.ALREADY_EXIST) {
                        JSONObject data = jsonObject.getJSONObject("data");
                        last_bid = data.getString("last_bid");
                        tv_error.setText("Please enter amount greater than the last bid amount (" + last_bid + ")");
                        tv_error.setVisibility(VISIBLE);
                    } else {
                        CustomToast.showToast(ScrBrokerBidding.this, "Failed To Add Bid");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSON Exeception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(ScrBrokerBidding.this);
                Log.d("TAG", "onFailure: " + t.getMessage());
                CustomToast.showToast(ScrBrokerBidding.this, Constants.WENT_WRONG);
            }
        });
    }

    private void showSubAuctionDetails() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dlg_sub_auction_details);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        tv_commodity = dialog.findViewById(R.id.tv_commodity);
        tv_bag_type = dialog.findViewById(R.id.tv_bag_type);
        tv_no_of_bags = dialog.findViewById(R.id.tv_no_of_bags);
        tv_qty = dialog.findViewById(R.id.tv_qty);
        tv_location = dialog.findViewById(R.id.tv_location);
        tv_warehouse = dialog.findViewById(R.id.tv_warehouse);
        tv_auction_date = dialog.findViewById(R.id.tv_auction_date);
        tv_unit_price = dialog.findViewById(R.id.tv_unit_price);

        tv_auction_date.setText(auction_date);
        tv_bag_type.setText(bag_type);
        tv_no_of_bags.setText(no_of_bag);
        tv_qty.setText("" + quantity);
        tv_location.setText(location);
        tv_warehouse.setText(warehouse);
        tv_unit_price.setText("" + unit_price);

        Button tv_submit = dialog.findViewById(R.id.tv_submit);
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialog.dismiss();
            }
        });

        dialog.show();
    }

    private void showBiddingDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.dialog_bid_now_broker);
        dialog.setCancelable(false);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        final SearchableSpinner sp_vendor = dialog.findViewById(R.id.sp_vendor);
        initVendorList(sp_vendor);

        Button tv_submit = dialog.findViewById(R.id.tv_submit);
        final TextView tv_error = dialog.findViewById(R.id.tv_error);
        TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
        TextView tv_min_bid = dialog.findViewById(R.id.tv_min_bid);
        final TextInputLayout til_amount = dialog.findViewById(R.id.til_amount);
        final EditText et_amount = dialog.findViewById(R.id.et_amount);

        final int min_bid = quantity * unit_price;
        tv_min_bid.setText("" + min_bid);
//9595148544  Fardeen Bhai

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                int vendor_id = 0;
                if (sp_vendor.getSelectedItemPosition() == 0) {
                    CustomToast.showToast(ScrBrokerBidding.this, "Select Vendor");
                    return;
                } else {
                    vendor_id = VendorList.getInstance().getVendorsID(sp_vendor.getSelectedItem().toString());
                }

                if (TextUtils.isEmpty(et_amount.getText().toString())) {
                    tv_error.setText("Enter Amount");
                    tv_error.setVisibility(VISIBLE);
                    return;
                } else {
                    tv_error.setText(null);
                    tv_error.setVisibility(View.GONE);
                }

                if (et_amount.getText().toString().equals("0")) {
                    tv_error.setText("Enter Valid Amount");
                    tv_error.setVisibility(VISIBLE);
                    return;
                } else {
                    tv_error.setText(null);
                    tv_error.setVisibility(View.GONE);
                }

                if (Float.parseFloat(et_amount.getText().toString()) <= Float.parseFloat(min_bid + "")) {
                    tv_error.setText("Amount should be greater than minimum bid amount");
                    tv_error.setVisibility(VISIBLE);
                    return;
                } else {
                    tv_error.setText(null);
                    tv_error.setVisibility(View.GONE);
                }
                callAddBidApi(dialog, tv_error, et_amount.getText().toString(), min_bid + "", vendor_id);
            }
        });

        dialog.show();
    }


    public void initVendorList(final SearchableSpinner sp_vendor) {

        Call<String> call = apiInterface.get_vendor_name_assigned_by(BrokerData.getInstance().getBrokerId() + "", reqsub_auc_id);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "onResponse: initVendorList : " + response.body());
                Log.d("TAG", "onResponse: initVendorList url : " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    VendorList.getInstance().clearList();
                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        Log.d("TAG", "onResponse: Vendor " + response.body());
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);

                            int vendor_id = jsonObject1.getInt("vendor_id");
                            String vendor_name = jsonObject1.getString("vendor_name");


                            Vendor broker = new Vendor(vendor_id, vendor_name);
                            VendorList.getInstance().add(broker);

                        }

                    } else {
                        Log.d("TAG", "onResponse: initVendorList " + "Failed");
                    }
                    ArrayAdapter<String> vendor_adapter = new ArrayAdapter<>(ScrBrokerBidding.this, android.R.layout.simple_list_item_1, VendorList.getInstance().getNames());
                    sp_vendor.setAdapter(vendor_adapter);
                    vendor_adapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.d(TAG, "onResponse: JSON Exception : " + e.getMessage());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "onFailure: initVendorList " + t.getMessage());
                ArrayAdapter<String> vendor_adapter = new ArrayAdapter<>(ScrBrokerBidding.this, android.R.layout.simple_list_item_1, VendorList.getInstance().getNames());
                sp_vendor.setAdapter(vendor_adapter);
                vendor_adapter.notifyDataSetChanged();

            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.tv_view_details:
                showSubAuctionDetails();
                break;

            case R.id.fb_add_bid:
                showBiddingDialog();
                break;
        }
    }
}