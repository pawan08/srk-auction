package com.tons.srkauction.view;

import android.app.Dialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.squareup.picasso.Picasso;
import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.Broker;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.DateToTimeStamp;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrAuctionDetails extends AppCompatActivity implements View.OnClickListener {

    private Button bt_view_batch;
    private ApiInterface apiInterface;
    private String auction_id;
    private TextView tv_job_id, tv_service, tv_service_provider, tv_commodity_des, tv_terms_conditions;
    private ImageView iv_commodity_image;
    private LinearLayout ll_root_layout;
    private TextView tv_assign_broker;
    private Dialog dialog;
    private ArrayList<Broker> list = new ArrayList<>();
    private int auction_status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_src_auction_details);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);


        Intent intent = getIntent();
        auction_id = intent.getStringExtra("auction_id");


    }

    @Override
    protected void onResume() {
        super.onResume();
        call_get_Auction_Details_api();
    }

    public void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Auction Details");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);


        tv_job_id = findViewById(R.id.tv_job_id);
        tv_service = findViewById(R.id.tv_service);
        tv_service_provider = findViewById(R.id.tv_service_provider);
        tv_commodity_des = findViewById(R.id.tv_commodity_des);
        tv_terms_conditions = findViewById(R.id.tv_terms_conditions);
        iv_commodity_image = findViewById(R.id.iv_commodity_image);
        ll_root_layout = findViewById(R.id.ll_root_layout);

        bt_view_batch = findViewById(R.id.bt_view_batch);
        bt_view_batch.setOnClickListener(this);
    }


    private void call_get_Auction_Details_api() {
        CustomDialog.showDialog(ScrAuctionDetails.this, "Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("auction_id", String.valueOf(auction_id));
        Call<String> call = apiInterface.get_auction_details(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(ScrAuctionDetails.this);
                Log.d("TAG", "Get Auction Details" + response.body());
                Log.d("TAG", "Get_Auction_Details url " + call.request().url().toString());

                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {

                        ll_root_layout.setVisibility(View.VISIBLE);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String auction_code = jsonObject1.getString("auction_code");
                            String auction_commodity = jsonObject1.getString("auction_commodity");
                            String auction_start_date = jsonObject1.getString("auction_start_date");
                            String auction_start_time = jsonObject1.getString("auction_start_time");
                            String auction_commodity_desc = jsonObject1.getString("auction_commodity_desc");
                            String auction_payment_tc = jsonObject1.getString("auction_payment_tc");
                            String auction_commodity_image = jsonObject1.getString("auction_commodity_image");
                            String broker_name = jsonObject1.getString("broker_name");
                            auction_status = jsonObject1.getInt("auction_status");
                            String auction_payment_tc_converted = Html.fromHtml(auction_payment_tc).toString();
                            String auction_commodity_desc_converted = Html.fromHtml(auction_commodity_desc).toString();

                            tv_job_id.setText(auction_code);
                            tv_service.setText(auction_commodity);
                            tv_service_provider.setText(DateToTimeStamp.getDate(auction_start_date) + " " + DateToTimeStamp.getTime(auction_start_time+":00"));
                            tv_commodity_des.setText(auction_commodity_desc_converted);
                            tv_terms_conditions.setText(auction_payment_tc_converted);

                            if (auction_commodity_image.isEmpty()) {
                                iv_commodity_image.setImageResource(R.drawable.ic_modern_camera);
                            } else {
                                Picasso.with(ScrAuctionDetails.this).load(auction_commodity_image).error(R.drawable.ic_location_on_black_24dp).into(iv_commodity_image);

                            }


                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("TAG", "Failed to get Auction Details");
                CustomDialog.closeDialog(ScrAuctionDetails.this);
            }

        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.bt_view_batch:
                if (auction_status == Integer.parseInt(Constants.STATUS_COMPLETED)){

                    Intent intent = new Intent(ScrAuctionDetails.this, ScrCompletedSubAuctionListing.class);
                    intent.putExtra("auction_id", String.valueOf(auction_id));
                    startActivity(intent);

                }else {
                    Intent intent = new Intent(ScrAuctionDetails.this, ScrSubAuctionListing.class);
                    intent.putExtra("auction_id", String.valueOf(auction_id));
                    startActivity(intent);
                }
                break;


        }

    }
}
