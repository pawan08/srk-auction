package com.tons.srkauction.view;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.tons.srkauction.R;

public class ScrNonnegotiableAuctionDetails extends AppCompatActivity implements OnClickListener {

    private Button bt_enquiry;
    private Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
            setContentView(R.layout.activity_scr_offfline_auction_details);
        }

        initUI();
    }

    private void initUI(){

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView headerText = toolbar.findViewById(R.id.tv_header);
        ImageView headerImage = toolbar.findViewById(R.id.iv_back);
        headerImage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        headerText.setText("NonNegotiable Auction Details");
        bt_enquiry = findViewById(R.id.bt_enquiry);
        bt_enquiry.setOnClickListener(this);
    }


        public void showNonNegotiableDialog() {

            dialog = new Dialog(this);
            dialog.setContentView(R.layout.dialog_enquire);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            Window window = dialog.getWindow();
            window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            dialog.setCancelable(true);
            TextView tv_cancel = dialog.findViewById(R.id.tv_cancel);
            Button bt_submit = dialog.findViewById(R.id.bt_submit);

            tv_cancel.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                }
            });
            bt_submit.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            dialog.setCancelable(true);
            dialog.show();
        }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.bt_enquiry:
                showNonNegotiableDialog();
                break;
        }
    }
}