package com.tons.srkauction.view;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.LiveAuction;
import com.tons.srkauction.model.AuctionList;
import com.tons.srkauction.view.adapter.LiveAuctionAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScrAuction extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    private RecyclerView rv_requested_auction;
    private LiveAuctionAdapter liveAuctionAdapter;
    private SwipeRefreshLayout swipe_refresh_layout;
    private LinearLayout ll_no_data;
    private EditText et_search;
    private ImageButton iv_search;
    private ApiInterface apiInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                //getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);
                getWindow().setStatusBarColor(getColor(R.color.colorPrimary));
            }
        }
        setContentView(R.layout.activity_scr_auction);

        initUi();
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        runLayoutAnimation(rv_requested_auction);

    }

    public void initUi() {

        Toolbar toolbar = findViewById(R.id.toolbar);
        ImageView iv_back = toolbar.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        TextView tv_header = toolbar.findViewById(R.id.tv_header);
        tv_header.setText("Auction List");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setElevation(2);

        rv_requested_auction = findViewById(R.id.rv_requested_auction);
       // liveAuctionAdapter = new LiveAuctionAdapter(this);
        rv_requested_auction.setAdapter(liveAuctionAdapter);
        rv_requested_auction.setLayoutManager(new GridLayoutManager(this, 1));

        ll_no_data = findViewById(R.id.ll_no_data);
        et_search = findViewById(R.id.et_search);
        iv_search = findViewById(R.id.iv_search);
        swipe_refresh_layout = findViewById(R.id.swipe_refresh_layout);
        swipe_refresh_layout.setOnRefreshListener(this);

      /*  et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                runLayoutAnimation(rv_requested_auction);
                filter(editable.toString());
            }
        });*/
        iv_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                filter(et_search.getText().toString());
            }
        });

    }

    private void filter(String text) {
        ArrayList<LiveAuction> arrayList = new ArrayList<>();
        for (LiveAuction item : AuctionList.getInstance().getList()) {
            if (item.getAuction_code().toLowerCase().contains(text.toLowerCase()) || item.getAuction_commodity().toLowerCase().contains(text.toLowerCase())) {
                arrayList.add(item);
                runLayoutAnimation(rv_requested_auction);

            }
        }
        liveAuctionAdapter.filteredList(arrayList);

    }

    private void runLayoutAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.layout_animation);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          //call_get_Auction_List_Api();
                                      }
                                  }
        );
    }

    public void showEmptyLayout() {
        if (liveAuctionAdapter.getItemCount() == 0) {
            ll_no_data.setVisibility(View.VISIBLE);
        } else {
            ll_no_data.setVisibility(View.GONE);
        }
    }


   /* private void call_get_Auction_List_Api() {

        Call<String> call = apiInterface.get_auction_list();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("TAG", "Get Auction List" + response.body());
                Log.d("TAG", "Get Auction List url " + call.request().url().toString());

                AuctionList.getInstance().clearList();
                if (!response.isSuccessful()) {
                    return;
                }
                try {
                    JSONObject jsonObject = new JSONObject(response.body());
                    boolean status = jsonObject.getBoolean("status");
                    int message = jsonObject.getInt("message");
                    if (message == Messages.SUCCESS) {
                        runLayoutAnimation(rv_requested_auction);
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            int auction_id = jsonObject1.getInt("auction_id");
                            String auction_code = jsonObject1.getString("auction_code");
                            String auction_type = jsonObject1.getString("auction_type");
                            String auction_commodity = jsonObject1.getString("auction_commodity");
                            String auction_uom = jsonObject1.getString("auction_uom");
                           // String auction_unit_price = jsonObject1.getString("auction_unit_price");
                            String auction_start_date = jsonObject1.getString("auction_start_date");
                            String auction_start_time = jsonObject1.getString("auction_start_time");

                            LiveAuction liveAuction = new LiveAuction(auction_id, auction_code, auction_type, auction_commodity, auction_uom, auction_start_date, auction_start_time);

                            AuctionList.getInstance().add(liveAuction);

                            liveAuctionAdapter.setList(AuctionList.getInstance().getList());
                            liveAuctionAdapter.notifyDataSetChanged();
                        }
                    }
                    swipe_refresh_layout.setRefreshing(false);
                    showEmptyLayout();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                swipe_refresh_layout.setRefreshing(false);

            }

        });
    }
*/
    @Override
    public void onRefresh() {
        swipe_refresh_layout.post(new Runnable() {
                                      @Override
                                      public void run() {
                                          swipe_refresh_layout.setRefreshing(true);
                                          //call_get_Auction_List_Api();

                                      }
                                  }
        );
    }
}
