package com.tons.srkauction.view.adapter;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.CommunicationConstant;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.model.PendingWalletEMD;
import com.tons.srkauction.model.PendingWalletHistory;
import com.tons.srkauction.model.RejectedWalletHistory;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrViewProfileImage;
import com.tons.srkauction.view.fragment.FrgEmd;
import com.tons.srkauction.view.fragment.FrgWithdrawal;

import java.util.ArrayList;

public class PendingWalletEMDAdapter extends RecyclerView.Adapter<PendingWalletEMDAdapter.ViewHolderBid> {


    private ArrayList<PendingWalletEMD> list;
    private FrgEmd context;
    public PendingWalletEMDAdapter(FrgEmd context) {
        this.context = context;

    }

    public void setList(ArrayList<PendingWalletEMD> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context.getActivity()).inflate(R.layout.item_pending_emd, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBid holder, int position) {
        final PendingWalletEMD pendingWalletEMD = list.get(position);

        holder.tv_date.setText(DateToTimeStamp.getDateForNotification(pendingWalletEMD.getWallet_added_time()));
        if (Float.parseFloat(pendingWalletEMD.getWallet_credit()) != 0) {
            holder.tv_amount.setText("" + pendingWalletEMD.getWallet_credit());
        } else {
            holder.tv_amount.setText("" + pendingWalletEMD.getWallet_debit());
        }

        holder.tv_request_code.setText("Code: "+pendingWalletEMD.getReqsub_auc_code());

        holder.tv_request_code.setTag(pendingWalletEMD);
        holder.tv_request_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PendingWalletEMD pendingWalletEMD = (PendingWalletEMD) view.getTag();
                Intent intent = new Intent(context.getActivity(), ScrSubAuctionDetails.class);
                intent.putExtra("reqsub_auc_id", String.valueOf(pendingWalletEMD.getReqsub_auc_id()));
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_amount,tv_remark,tv_date,tv_request_code ;


        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);
            tv_remark=itemView.findViewById(R.id.tv_remark);
            tv_amount=itemView.findViewById(R.id.tv_amount);
            tv_date=itemView.findViewById(R.id.tv_date);
            tv_request_code=itemView.findViewById(R.id.tv_request_code);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }
}
