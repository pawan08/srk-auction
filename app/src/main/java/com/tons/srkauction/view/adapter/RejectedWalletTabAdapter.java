package com.tons.srkauction.view.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.tons.srkauction.view.fragment.FrgRejectedEmd;
import com.tons.srkauction.view.fragment.FrgRejectedWallet;
import com.tons.srkauction.view.fragment.FrgRejectedWithdrawal;

public class RejectedWalletTabAdapter extends FragmentStatePagerAdapter {


    public RejectedWalletTabAdapter(FragmentManager fm) {
        super(fm);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        /*if (position == 0) {
            return new FrgRejectedEmd();
        } else */
        if (position == 0) {
            return new FrgRejectedWallet();
        } else {
            return new FrgRejectedWithdrawal();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {

          /*  case 0:
                return "EMD";*/

            case 0:
                return "Wallet";

            case 1:
                return "Withdrawal";
            default:
                return null;
        }
    }
}
