package com.tons.srkauction.view.adapter;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.communication.ApiInterface;
import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.communication.Messages;
import com.tons.srkauction.communication.RetrofitBase;
import com.tons.srkauction.model.CompletedSubAuction;
import com.tons.srkauction.model.SubAuction;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.CustomDialog;
import com.tons.srkauction.utils.CustomIntent;
import com.tons.srkauction.utils.CustomToast;
import com.tons.srkauction.view.ScrAddMoneyToWallet;
import com.tons.srkauction.view.ScrCompletedSubAuctionListing;
import com.tons.srkauction.view.ScrSubAuctionDetails;
import com.tons.srkauction.view.ScrSubAuctionListing;
import com.tons.srkauction.view.ScrViewDetailsSample;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class CompletedSubAuctionAdapter extends RecyclerView.Adapter<CompletedSubAuctionAdapter.ViewHolderBid> {

    private Timer timer;
    private ArrayList<CompletedSubAuction> list;
    private ScrCompletedSubAuctionListing context;
    private ApiInterface apiInterface;


    public CompletedSubAuctionAdapter(ScrCompletedSubAuctionListing context) {
        this.context = context;

    }

    public void setList(ArrayList<CompletedSubAuction> list) {
        this.list = list;
        timer = new Timer();
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_completed_sub_auction, parent, false);
        apiInterface = RetrofitBase.getInstance().create(ApiInterface.class);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolderBid holder, final int i) {
        final CompletedSubAuction subAuction = list.get(i);
        holder.tv_location.setText(subAuction.getReqsub_auc_location());
        holder.tv_unit_price.setText(subAuction.getReqsub_auc_unit_price());
        holder.tv_quantity.setText(subAuction.getReqsub_auc_qty());
        holder.tv_emd_amount.setText(subAuction.getReqsub_auc_emd());
        holder.tv_vendor_name.setText(subAuction.getVendor_name());

        Log.d("TAG", "request_auction_status: "+ subAuction.getReqsub_auc_status());


        holder.bt_participate.setTag(subAuction);
        holder.bt_participate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubAuction subAuction1 = (SubAuction) view.getTag();
                Intent intent = new Intent(context, ScrViewDetailsSample.class);
                intent.putExtra("reqsub_auc_id", String.valueOf(subAuction1.getReqsub_auc_id()));
                context.startActivity(intent);
            }
        });




        if (subAuction.getIs_mine_auction().equals("YES")){

            if (subAuction.getReqsub_auc_vendor_acceptation() == Constants.PENDING){

                holder.ll_yes_no.setVisibility(VISIBLE);
                holder.bt_accept_auction.setVisibility(GONE);
                holder.bt_reject_auction.setVisibility(GONE);
                holder.ll_accept_timer.setVisibility(VISIBLE);
                TimerTask timerTask = new TimerTask() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    public void run() {
                        Log.d("TAG", "Updating");
                        Log.d("TAG", "run:current " + subAuction.getReqsub_auc_end_time());
                        Log.d("TAG", "run:end " + subAuction.getAcceptation_time());

                        upcomingTimerCalculation(holder.tv_timer_5, subAuction.getReqsub_auc_end_time(),
                                subAuction.getAcceptation_time(),holder.bt_accept_auction,holder.bt_reject_auction,holder.ll_yes_no,holder.tv_text_info,holder.ll_accept_timer);

                    }
                };
                timer.schedule(timerTask, 1, 1000);

                if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT){
                    holder.tv_text_info.setVisibility(VISIBLE);
                    holder.tv_text_info.setText("Bidder accepted the request");
                }else{
                    holder.tv_text_info.setVisibility(GONE);
                }

                holder.bt_yes.setTag(subAuction);
                holder.bt_yes.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CompletedSubAuction completedSubAuction = (CompletedSubAuction)view.getTag();
                        showDialog("Confirm Auction","Are you sure you want to sell this auction ?",Constants.SELLER,Constants.ACCEPT,completedSubAuction.getReqsub_auc_id(),i,completedSubAuction.getIs_mine_auction());
                    }
                });

                holder.bt_no.setTag(subAuction);
                holder.bt_no.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        CompletedSubAuction completedSubAuction = (CompletedSubAuction)view.getTag();
                        showDialog("Decline Auction","Are you sure you don't want to sell this auction ?",Constants.SELLER,Constants.REJECT,completedSubAuction.getReqsub_auc_id(),i,completedSubAuction.getIs_mine_auction());
                    }
                });
            }else{
                Log.d("TAG", "onBindViewHolder: Vendor already performed an action");
                holder.ll_yes_no.setVisibility(GONE);
                holder.tv_text_info.setVisibility(VISIBLE);
                holder.ll_accept_timer.setVisibility(GONE);
                holder.tv_text_info.setText("Not Accepted in 5 minuted");
                holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));

                if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT && subAuction.getReqsub_auc_vendor_acceptation()== Constants.ACCEPT){

                    holder.tv_text_info.setText("Auction accepted from both ends");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.button_green));
                }
                else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.PENDING && subAuction.getReqsub_auc_vendor_acceptation()==Constants.ACCEPT){

                    holder.tv_text_info.setText("Pending From Bidder");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                }else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation()==Constants.ACCEPT){

                    holder.tv_text_info.setText("Rejected From Bidder");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                } else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.REJECT){

                    holder.tv_text_info.setText("You rejected the request");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                } else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.REJECT){

                    holder.tv_text_info.setText("You rejected the request");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                }else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.REJECT){

                    holder.tv_text_info.setText("Both Vendor and Bidder rejected");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                }

            }


        }else{
            Log.d("TAG", "onBindViewHolder:isMineAuction " +subAuction.getIs_mine_auction() );
            holder.ll_yes_no.setVisibility(GONE);
            if (subAuction.getIs_winner().equals("YES")){

                if ((subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.PENDING)){

                    holder.bt_accept_auction.setVisibility(VISIBLE);
                    holder.bt_reject_auction.setVisibility(VISIBLE);
                    holder.ll_accept_timer.setVisibility(VISIBLE);
                    TimerTask timerTask = new TimerTask() {
                        @RequiresApi(api = Build.VERSION_CODES.O)
                        public void run() {
                            Log.d("TAG", "Updating");
                            Log.d("TAG", "run:current " + subAuction.getReqsub_auc_end_time());
                            Log.d("TAG", "run:end " + subAuction.getAcceptation_time());

                            upcomingTimerCalculation(holder.tv_timer_5, subAuction.getReqsub_auc_end_time(),
                                    subAuction.getAcceptation_time(),holder.bt_accept_auction,holder.bt_reject_auction,holder.ll_yes_no,holder.tv_text_info,holder.ll_accept_timer);
                        }
                    };
                    timer.schedule(timerTask, 1, 1000);

                    if (subAuction.getReqsub_auc_vendor_acceptation() == Constants.ACCEPT){
                        holder.tv_text_info.setVisibility(VISIBLE);
                        holder.tv_text_info.setText("Vendor accepted the request");
                    }else{
                        holder.tv_text_info.setVisibility(GONE);
                    }

                    holder.bt_accept_auction.setTag(subAuction);
                    holder.bt_accept_auction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CompletedSubAuction completedSubAuction = (CompletedSubAuction)view.getTag();
                            showDialog("Accept Auction","Are you sure you want to accept this auction ?",Constants.BIDDER,Constants.ACCEPT,completedSubAuction.getReqsub_auc_id(),i,completedSubAuction.getIs_mine_auction());
                        }
                    });

                    holder.bt_reject_auction.setTag(subAuction);
                    holder.bt_reject_auction.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            CompletedSubAuction completedSubAuction = (CompletedSubAuction)view.getTag();
                            showDialog("Reject Auction","Are you sure you want to reject this auction ?",Constants.BIDDER,Constants.REJECT,completedSubAuction.getReqsub_auc_id(),i,completedSubAuction.getIs_mine_auction());
                        }
                    });
                }else{

                    Log.d("TAG", "onBindViewHolder: Bidder already performed an action");
                    holder.bt_accept_auction.setVisibility(GONE);
                    holder.bt_reject_auction.setVisibility(GONE);
                    holder.ll_accept_timer.setVisibility(GONE);
                    holder.tv_text_info.setVisibility(VISIBLE);
                    holder.tv_text_info.setText("Not Accepted in 5 minutes");
                    holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));

                    if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT && subAuction.getReqsub_auc_vendor_acceptation()== Constants.ACCEPT){

                        holder.tv_text_info.setText("Auction accepted from both ends");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.button_green));
                    }
                    else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT && subAuction.getReqsub_auc_vendor_acceptation()==Constants.PENDING){

                        holder.tv_text_info.setText("Pending From Vendor");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    }else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.ACCEPT && subAuction.getReqsub_auc_vendor_acceptation()==Constants.REJECT){

                        holder.tv_text_info.setText("Rejected From Vendor");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                    } else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.ACCEPT){

                        holder.tv_text_info.setText("You rejected the request");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                    } else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.PENDING){

                        holder.tv_text_info.setText("You rejected the request");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                    }else if (subAuction.getReqsub_auc_bid_winner_acceptation() == Constants.REJECT && subAuction.getReqsub_auc_vendor_acceptation() == Constants.REJECT){

                        holder.tv_text_info.setText("Both Vendor and Bidder rejected");
                        holder.tv_text_info.setTextColor(context.getResources().getColor(R.color.viewfinder_laser));
                    }
                }

            }else{
                Log.d("TAG", "onBindViewHolder:isWinner " +subAuction.getIs_winner() );
                holder.bt_accept_auction.setVisibility(GONE);
                holder.bt_reject_auction.setVisibility(GONE);
            }
        }



    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public void showDialog(String title, String message, final int type, final int acceptation_value, final int reqsub_auc_id, final int position,final String is_mine_auction) {

        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.dlg_pay_emd);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        TextView tv_title = dialog.findViewById(R.id.tv_title);
        TextView tv_text = dialog.findViewById(R.id.tv_text);

        tv_title.setText(title);
        tv_text.setText(message);

        Button bt_no = dialog.findViewById(R.id.bt_no);
        //bt_no.setVisibility(View.GONE);
        Button bt_yes = dialog.findViewById(R.id.bt_yes);
        bt_yes.setText("YES");
        bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        bt_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callAcceptBidApi(type,acceptation_value,reqsub_auc_id,position,is_mine_auction);
                dialog.dismiss();

            }
        });
        dialog.setCancelable(true);
        dialog.show();
    }

    public void callAcceptBidApi(final int type, final int acceptation_value, int reqsub_auc_id, final int position, final String is_mine_auction){

        CustomDialog.showDialog(context,"Please Wait..!");
        Map<String, String> params = new HashMap<>();
        params.put("type",  String.valueOf(type));
        params.put("acceptation_value",  String.valueOf(acceptation_value));
        params.put("reqsub_auc_id", String.valueOf(reqsub_auc_id));
        Call<String> call = apiInterface.accept_sub_auction_for_bid_winner(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                CustomDialog.closeDialog(context);
                Log.d("TAG", "onResponse: callAcceptBidApi " + response.body());
                Log.d("TAG", "onResponse: callAcceptBidApi url " + call.request().url().toString());
                if (!response.isSuccessful()) {
                    return;
                }

                try{

                    JSONObject jsonObject = new JSONObject(response.body());
                    int message = jsonObject.getInt("message");
                    switch (message) {

                        case Messages.SUCCESS:
                            //context.call_SubAuctionDetailsApi();

                                    CustomToast.showToast(context, "Success");
                                    CompletedSubAuction completedSubAuction = list.get(position);
                                    if (is_mine_auction.equals("YES")){

                                        if (type == Constants.SELLER && acceptation_value == Constants.ACCEPT) {
                                            Log.d("TAG", "run: in seller accept if block");
                                            completedSubAuction.setReqsub_auc_vendor_acceptation(Constants.ACCEPT);
                                        } else {
                                            Log.d("TAG", "run: in seller accept else block");
                                            completedSubAuction.setReqsub_auc_vendor_acceptation(Constants.REJECT);
                                        }

                                    }else {
                                        if (type == Constants.BIDDER && acceptation_value == Constants.ACCEPT) {
                                            Log.d("TAG", "run: in bidder if block");
                                            completedSubAuction.setReqsub_auc_bid_winner_acceptation(Constants.ACCEPT);
                                        } else {
                                            Log.d("TAG", "run: in bidder else block");
                                            completedSubAuction.setReqsub_auc_bid_winner_acceptation(Constants.REJECT);
                                        }
                                    }
                                    notifyDataSetChanged();

                            break;

                        case Messages.FAILED:
                            CustomToast.showToast(context, "Failed.");
                            break;
                    }

                }catch (JSONException e){
                    e.printStackTrace();;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                CustomDialog.closeDialog(context);
            }
        });

    }

    private void upcomingTimerCalculation(final TextView txtCurrentTime, final String current_time, final String end_date_time,Button bt_accept,Button bt_Reject,LinearLayout ll_yes_no,TextView txtMsg,LinearLayout ll_accept_timer) {

        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            // Please here set your event date//YYYY-MM-DD
            Date futureDate = null;
            Date futureDate1 = null;
            try {
                //Date currentTime = Calendar.getInstance().getTime();
                futureDate = dateFormat.parse(current_time);
                //futureDate = dateFormat.parse(CurrentTime.getInstance().getKEY_CURRENT_TIME());
                futureDate1 = dateFormat.parse(end_date_time);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Date currentDate = new Date();
            if (!currentDate.after(futureDate)) {
                long diff = futureDate.getTime()
                        - currentDate.getTime();
                long days = diff / (24 * 60 * 60 * 1000);
                diff -= days * (24 * 60 * 60 * 1000);
                long hours = diff / (60 * 60 * 1000);
                diff -= hours * (60 * 60 * 1000);
                long minutes = diff / (60 * 1000);
                diff -= minutes * (60 * 1000);
                long seconds = diff / 1000;
                txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                txtCurrentTime.setText(String.format("%02d", days) + ":" + String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
            } else {
                if (!currentDate.after(futureDate1)) {
                    long diff = futureDate1.getTime()
                            - currentDate.getTime();
                    long days = diff / (24 * 60 * 60 * 1000);
                    diff -= days * (24 * 60 * 60 * 1000);
                    long hours = diff / (60 * 60 * 1000);
                    diff -= hours * (60 * 60 * 1000);
                    long minutes = diff / (60 * 1000);
                    diff -= minutes * (60 * 1000);
                    long seconds = diff / 1000;
                    Log.d("TAG", "updateDateTime: seconds  : " + seconds);
                    txtCurrentTime.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                    txtCurrentTime.setText(String.format("%02d", hours) + ":" + String.format("%02d", minutes) + ":" + String.format("%02d", seconds));
                } else {

                    bt_accept.setVisibility(GONE);
                    bt_Reject.setVisibility(GONE);
                    ll_yes_no.setVisibility(GONE);
                    ll_accept_timer.setVisibility(GONE);
                    txtMsg.setVisibility(VISIBLE);
                    txtMsg.setText("Not Accepted in 5 minutes");
                    txtMsg.setTextColor(context.getResources().getColor(android.R.color.holo_red_dark));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            Log.d("TAG", "upcomingTimerCalculation: Exception " + e.getMessage());
        }

    }

    class ViewHolderBid extends RecyclerView.ViewHolder{

        private TextView chronometer;
        private TextView tv_location, tv_quantity, tv_unit_price, tv_view_details, tv_emd_amount, tv_vendor_name,tv_text_info;
        private Button bt_participate;
        private CardView card_main;
        private LinearLayout ll_yes_no,ll_accept_timer;
        private Button bt_accept_auction,bt_reject_auction,bt_yes,bt_no;
        private TextView tv_timer_5;

        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            chronometer = itemView.findViewById(R.id.chronometer);
            tv_timer_5 = itemView.findViewById(R.id.tv_timer_5);
            tv_location = itemView.findViewById(R.id.tv_location);
            tv_text_info = itemView.findViewById(R.id.tv_text_info);
            tv_view_details = itemView.findViewById(R.id.tv_view_details);
            tv_quantity = itemView.findViewById(R.id.tv_quantity);
            tv_unit_price = itemView.findViewById(R.id.tv_unit_price);
            tv_emd_amount = itemView.findViewById(R.id.tv_emd_amount);
            tv_vendor_name = itemView.findViewById(R.id.tv_vendor_name);
            bt_participate = itemView.findViewById(R.id.bt_participate);
            card_main = itemView.findViewById(R.id.card_main);
            ll_yes_no = itemView.findViewById(R.id.ll_yes_no);
            ll_accept_timer = itemView.findViewById(R.id.ll_accept_timer);
            bt_accept_auction = itemView.findViewById(R.id.bt_accept_auction);
            bt_reject_auction = itemView.findViewById(R.id.bt_reject_auction);
            bt_yes = itemView.findViewById(R.id.bt_yes);
            bt_no = itemView.findViewById(R.id.bt_no);

        }

    }
}
