package com.tons.srkauction.view.adapter;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tons.srkauction.R;
import com.tons.srkauction.model.BidModel;
import com.tons.srkauction.storage.VendorData;
import com.tons.srkauction.utils.DateToTimeStamp;
import com.tons.srkauction.view.ScrBidding;

import java.util.ArrayList;

public class BidAdapter extends RecyclerView.Adapter<BidAdapter.ViewHolderBid> {


    private ArrayList<BidModel> list;
    private ScrBidding context;

    public BidAdapter(ScrBidding context) {
        this.context = context;

    }

    public void setList(ArrayList<BidModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolderBid onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bid, null);
        return new ViewHolderBid(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderBid holder, int position) {
        BidModel bidModel = list.get(position);
        if (bidModel.getBid_vendor_id() != VendorData.getInstance().getVendorId()) {
            holder.tv_tittle.setText("Others Bid");
        } else {
            if (bidModel.getBid_broker_id() > 0 && bidModel.getBid_vendor_id() == VendorData.getInstance().getVendorId()) {
                holder.tv_tittle.setText("Your Bid ( " + bidModel.getBroker_name() + " )");
            } else if (bidModel.getBid_vendor_id() == VendorData.getInstance().getVendorId()) {
                holder.tv_tittle.setText("Your Bid");
            }
        }

        Log.d("TAG", "onBindViewHolder: vendor id shared : " + VendorData.getInstance().getVendorId());
        Log.d("TAG", "onBindViewHolder: vendor id server : " + bidModel.getBid_vendor_id());
        Log.d("TAG", "onBindViewHolder: broker id server : " + bidModel.getBid_broker_id());
        Log.d("TAG", "onBindViewHolder: broker name server : " + bidModel.getBroker_name());

        //  holder.tv_tittle.setText(bidModel.getTitle());
        holder.tv_amount.setText(bidModel.getBid_amount());
        holder.tv_time.setText(DateToTimeStamp.getDateForNotification(bidModel.getBid_added_time() + ":00"));


    }


    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }


    class ViewHolderBid extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView tv_tittle, tv_amount, tv_time;


        private ViewHolderBid(@NonNull View itemView) {
            super(itemView);

            tv_tittle = itemView.findViewById(R.id.tv_tittle);
            tv_amount = itemView.findViewById(R.id.tv_amount);
            tv_time = itemView.findViewById(R.id.tv_time);
        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {


            }
        }
    }
}
