package com.tons.srkauction.communication;

public class CommunicationConstant {

    public static final String ROOT_URL = "https://www.georeachtech.com/srk/";

    public static final String BASE_URL = ROOT_URL + "api/";

    public static final String ATTACHMENTS = ROOT_URL + "attachments/";

    public static final String VENDOR_AADHAR = ATTACHMENTS + "vendor-aadhar/";
    public static final String VENDOR_GST = ATTACHMENTS + "vendor-gst/";
    public static final String VENDOR_MANDI_LICENSE = ATTACHMENTS + "vendor-mandi-license/";
    public static final String VENDOR_PAN = ATTACHMENTS + "vendor-pan/";
    public static final String VENDOR_PROFILE = ATTACHMENTS + "vendor-profile/";

    public static final String BROKER_PROFILE = ATTACHMENTS + "broker-profile/";

    public static final String AUCTION_COMMODITY_IMAGE = ATTACHMENTS + "commodity-images/";
    public static final String WALLET_IMAGE = ATTACHMENTS + "emd/";
    public static final String QR_CODE = ATTACHMENTS + "qr-code/";


    // api url
    public static final String VENDOR_REGISTER = BASE_URL + "ven-register.php";
    public static final String UPDATE_VENDOR_PROFILE = BASE_URL + "update-vendor-profile.php";
    public static final String ADD_MONEY_INTO_WALLET = BASE_URL + "add_money_to_wallet.php";

    public static final String UPDATE_BROKER_PROFILE = BASE_URL + "update-broker-profile.php";
    public static final String ADD_REQUEST = BASE_URL + "add_auction_request.php";
    public static final String EDIT_REQUET = BASE_URL + "update-requested-auction.php";
    public static final String EDIT_REQUEST_ATTACHMENT = ATTACHMENTS + "request_auction/";

    public static final String TECHNICAL_DOC = ATTACHMENTS + "auction-technical-documents/";

}

