package com.tons.srkauction.communication;

import static com.tons.srkauction.communication.CommunicationConstant.BASE_URL;

public class Constants<KG> {

    public static final String PROGRESS_MSG = "Please Wait...";
    public static final String WENT_WRONG = "Something went wrong please try again later";
    public static final String FETCH_ERROR = "Data fetch error";
    public static final String SERVER_ERROR = "Server error";
    public static final String FETCH_CURRENT_LOCATION_MSG = "Fetching Location...";


    public static final String AUCTION_LIST = "com.tons.srkauction.ScrAuction";
    public static final String AUCTION_DETAILS = "com.tons.srkauction.ScrAuctionDetails";
    public static final String REQUESTED_AUCTION_LIST = "com.tons.srkauction.ScrAuctionRequest";
    public static final String REQUESTED_AUCTION_DETAILS = "com.tons.srkauction.ScrRequestAuctionDetails";
    public static final String BROKER_NOTIFICATION = "com.tons.srkauction.ScrBrokerNotification";
    public static final String VENDOR_NOTIFICATION = "com.tons.srkauction.ScrVendorNotification";


    public static final String STATUS_INACTIVE = "0";
    public static final String STATUS_ACTIVE = "1";
    public static final String STATUS_REJECT = "2";
    public static final String STATUS_FINAL = "3";
    public static final String STATUS_UPCOMING = "4";
    public static final String STATUS_LIVE = "5";
    public static final String STATUS_COMPLETED = "6";

    public static final String AADHAR_IMAGE = "1";
    public static final String PAN_IMAGE = "2";
    public static final String GST_IMAGE = "3";
    public static final String MANDI_LICENSE_IMAGE = "4";
    public static final String PROFILE_IMAGE = "5";

    //-----wallet Status-----
    public static final int WALLET_PENDING = 0;
    public static final int WALLET_APPROVED = 1;
    public static final int WALLET_REJECT = 2;

    //-----wallet Type-----
    public static final String WT_ADD = "ADD";
    public static final String WT_WITHDRAWAL = "WITHDRAWAL";
    public static final String WT_EMD = "EMD";
    public static final String WT_SCHEDULE = "SCHEDULE";

    public static final int BIDDER = 1;
    public static final int SELLER = 2;

    public static final int ACCEPT = 1;
    public static final int REJECT = 2;
    public static final int PENDING = 0;

    //// PAGE POSITION FOR APP


    public static final int AUCTION_LIVE = 1;
    public static final int AUCTION_UPCOMING = 2;
    public static final int AUCTION_COMPLETED = 3;

    public static final int AUCTION_PENDING = 1;
    public static final int AUCTION_APPROVED = 2;
    public static final int AUCTION_REJECTED = 3;

    public static final int PENDING_EMD = 1;
    public static final int PENDING_WALLET = 2;
    public static final int PENDING_WITHDRAWAL = 3;

    public static final int REJECTED_EMD = 1;
    public static final int REJECTED_WALLET = 2;
    public static final int REJECTED_WITHDRAWAL = 3;

    public static final int SUB_AUCTION_PENDING = 0;
    public static final int SUB_AUCTION_LIVE = 1;
    public static final int SUB_AUCTION_COMPLETED = 2;

    //UOM and its Calculations
    public static final String UOM_ONE_KG = "1 KG";
    public static final String UOM_QUINTAL = "QUINTAL";
    public static final String UOM_METRIC_TON = "METRIC TON";
    public static final String UOM_TWENTY_KG_PRICE = "20 KG PRICE";

    public static final int CAL_ONE_KG = 1;
    public static final int CAL_QUINTAL = 100;
    public static final int CAL_METRIC_TON = 1000;
    public static final int CAL_TWENTY_KG_PRICE = 20;


    ////  APP CLICK ACTIONS
    public static final String AUCTION_SCR = "com.tons.srkauction.ScrAuctionTab";

}
