package com.tons.srkauction.communication;

public class Messages {

    public static final int SUCCESS = 20;
    public static final int FAILED = 21;
    public static final int ALREADY_EXIST = 22;
    public static final int INVALID_WALLET_AMOUNT = 23;
    public static final int INACTIVE_USER = 24;
    public static final int NOT_REGISTERED = 25;
    public static final int INSUFFICIENT_WALLET_AMOUNT = 26;



}
