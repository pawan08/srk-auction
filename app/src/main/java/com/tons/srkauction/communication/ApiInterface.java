package com.tons.srkauction.communication;

import java.util.HashMap;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET("ven-login.php")
    Call<String> ven_login(@Query("vendor_mobile") String vendor_mobile, @Query("vendor_password") String vendor_password, @Query("vendor_token") String vendor_token);

    @GET("bro-login.php")
    Call<String> bro_login(@Query("broker_mobile") String vendor_mobile, @Query("broker_password") String vendor_password, @Query("broker_token") String broker_token);

    @GET("notification-vendor-list.php")
    Call<String> notification_vendor_list(@QueryMap Map<String, String> params);

    @GET("notification-broker-list.php")
    Call<String> notification_broker_list(@QueryMap Map<String, String> params);


    @Multipart
    @POST("ven-register.php")
    Call<String> ven_register(@Part("vendor_name") String vendor_name, @Part("vendor_dealing_in") String vendor_dealing_in, @Part("vendor_email") String vendor_email
            , @Part("vendor_password") String vendor_passwor, @Part("vendor_mobile") String vendor_mobile, @Part("vendor_address") String vendor_address, @Part("vendor_business_type") String vendor_business_type, @Part("vendor_company_shop_name") String vendor_company_shop_name
            , @Part("vendor_company_shop_address") String vendor_company_shop_address, @Part("vendor_pan_shopact") String vendor_pan_shopact, @Part("vendor_gst") String vendor_gst, @Part("vendor_mandi") String vendor_mandi
            , @Part("vendor_ref_name") String vendor_ref_name, @Part("vendor_ref_mobile") String vendor_ref_mobile, @Part("vendor_ref_address") String vendor_ref_address, @Part MultipartBody.Part vendor_pic_aadhar
            , @Part MultipartBody.Part vendor_pic_pan, @Part MultipartBody.Part vendor_pic_gst, @Part MultipartBody.Part vendor_pic_mandi_license, @Part MultipartBody.Part vendor_pic_profile);


    @Multipart
    @POST("bro-register.php")
    Call<String> bro_register(@PartMap Map<String, String> params, @Part MultipartBody.Part broker_profile_pic);


    @GET("commodity-list.php")
    Call<String> commodity_list();

    @GET("ven_list.php")
    Call<String> ven_list();

    @GET("add_auction_request.php")
    Call<String> add_auction_request(@QueryMap Map<String, String> params);

    @GET("ven-profile-details.php")
    Call<String> ven_profile_details(@QueryMap Map<String, String> params);

    @GET("bro-auction-request-list.php")
    Call<String> bro_auction_request_list(@QueryMap Map<String, String> params);

    @GET("ven-auction-request-list.php")
    Call<String> ven_auction_request_list(@QueryMap Map<String, String> params);

    @GET("get-auction-list.php")
    Call<String> get_auction_list(@QueryMap Map<String, String> params);

    @GET("bro-profile-details.php")
    Call<String> bro_profile_details(@QueryMap Map<String, String> params);

    @GET("get-uom.php")
    Call<String> get_uom();

    @GET("get-auction-details.php")
    Call<String> get_auction_details(@QueryMap Map<String, String> params);

   /* @GET("update-vendor-profile.php")
    Call<String> update_vendor_profile(@QueryMap Map<String,String> params);*/


    /*@GET("update-broker-profile.php")
    Call<String> (@QueryMap Map<String,String> params);*/

    @Multipart
    @POST("update-broker-profile.php")
    Call<String> update_broker_profile(@PartMap Map<String, String> params, @Part MultipartBody.Part broker_profile_pic);

  /*  @Multipart
    @POST("update-vendor-profile.php")
    Call<String> update_vendor_profile(@Part("vendor_id") String vendor_id, @Part("vendor_name") String vendor_name, @Part("vendor_dealing_in") String vendor_dealing_in, @Part("vendor_email") String vendor_email
            , @Part("vendor_mobile") String vendor_mobile, @Part("vendor_address") String vendor_address, @Part("vendor_business_type") String vendor_business_type, @Part("vendor_company_shop_name") String vendor_company_shop_name
            , @Part("vendor_company_shop_address") String vendor_company_shop_address, @Part("vendor_pan_shopact") String vendor_pan_shopact, @Part("vendor_gst") String vendor_gst, @Part("vendor_mandi") String vendor_mandi
            , @Part("vendor_ref_name") String vendor_ref_name, @Part("vendor_ref_mobile") String vendor_ref_mobile, @Part("vendor_ref_address") String vendor_ref_address, @Part MultipartBody.Part vendor_pic_aadhar
            , @Part MultipartBody.Part vendor_pic_pan, @Part MultipartBody.Part vendor_pic_gst, @Part MultipartBody.Part vendor_pic_mandi_license, @Part MultipartBody.Part vendor_pic_profile);
*/

    @Multipart
    @POST("update-vendor-profile.php")
    Call<String> update_vendor_profile(@Part("vendor_id") String vendor_id, @Part("vendor_name") String vendor_name, @Part("vendor_dealing_in") String vendor_dealing_in, @Part("vendor_email") String vendor_email
            , @Part("vendor_mobile") String vendor_mobile, @Part("vendor_whatsapp_no") String vendor_whtsapp_no, @Part("vendor_address") String vendor_address, @Part("vendor_requested_commodity") String requestedcommodity, @Part("vendor_business_type") String vendor_business_type, @Part("vendor_company_shop_name") String vendor_company_shop_name
            , @Part("vendor_company_shop_address") String vendor_company_shop_address, @Part("vendor_pan_shopact") String vendor_pan_shopact, @Part("vendor_gst") String vendor_gst, @Part("vendor_mandi") String vendor_mandi
            , @Part("vendor_ref_name") String vendor_ref_name, @Part("vendor_ref_mobile") String vendor_ref_mobile, @Part MultipartBody.Part vendor_pic_aadhar
            , @Part MultipartBody.Part vendor_pic_pan, @Part MultipartBody.Part vendor_pic_gst, @Part MultipartBody.Part vendor_pic_mandi_license, @Part MultipartBody.Part vendor_pic_profile);

    @GET("get-requested-auction-details.php")
    Call<String> get_requested_auction_details(@QueryMap Map<String, String> params);

    @Multipart
    @POST("add_money_to_wallet.php")
    Call<String> add_money_to_wallet(@PartMap Map<String, String> params, @Part MultipartBody.Part wallet_vendor_attachment);


    @GET(" get_sub_auction_list.php")
    Call<String> get_sub_auction_list(@QueryMap Map<String, String> params);

    @GET(" get_completed_sub_auction_list.php")
    Call<String> get_completed_sub_auction_list(@QueryMap Map<String, String> params);

    @GET(" get-wallet-history.php")
    Call<String> get_wallet_history_list(@QueryMap Map<String, String> params);

    @GET("add_bid.php")
    Call<String> add_bid(@QueryMap Map<String, String> params);

    @GET("get_bidding.php")
    Call<String> get_bid_list(@QueryMap Map<String, String> params);

    @GET("logout.php")
    Call<String> logout(@QueryMap Map<String, String> params);

    @GET("delete-requested-auction.php")
    Call<String> delete_requested_auction(@QueryMap Map<String, String> params);

    @GET("update-requested-auction.php")
    Call<String> update_requested_auction(@QueryMap Map<String, String> params);

    @GET("bro-list.php")
    Call<String> bro_list();

    @GET("assign-broker-to-auction.php")
    Call<String> assign_broker_to_auction(@QueryMap Map<String, String> params);

    @GET("update_assigned_broker.php")
    Call<String> update_assigned_broker(@QueryMap Map<String, String> params);

    @GET("request_wallet_amount_withdrawal.php")
    Call<String> wallet_withdrwl(@QueryMap Map<String, String> params);

    @GET("get_withdrawal_requests.php")
    Call<String> get_withdrawal_list(@QueryMap Map<String, String> params);

    @GET("get_sub_auction_details.php")
    Call<String> get_sub_auction_details(@QueryMap Map<String, String> params);

    @GET("pay_emd.php")
    Call<String> pay_emd(@QueryMap Map<String, String> params);

    @GET("get_pending_reject_requests.php")
    Call<String> get_pending_reject_requests(@QueryMap Map<String, String> params);

    @GET("get_pending_reject_requests.php")
    Call<String> get_pending_list(@QueryMap Map<String, String> params);

    @GET("get_sub_auc_list_by_request_id.php")
    Call<String> get_sub_auc_list_by_request_id(@QueryMap Map<String, String> params);

    @Streaming
    @GET()
    Call<ResponseBody> dowloadAnyFile(@Url String pad_url);

    @GET("get-dashboard.php")
    Call<String> get_dashboard_data(@QueryMap Map<String, String> params);

    @GET("get_dashboard_data_for_broker.php")
    Call<String> get_dashboard_data_for_broker(@QueryMap Map<String, String> params);

    @GET("get_samples.php")
    Call<String> get_samples_data(@QueryMap Map<String, String> params);

    @GET("get_sample_details.php")
    Call<String> get_sample_details(@QueryMap Map<String, String> params);

    @GET("accept_sub_auction_for_bid_winner.php")
    Call<String> accept_sub_auction_for_bid_winner(@QueryMap Map<String, String> params);

    @GET("get_sub_auction_details_for_broker.php")
    Call<String> get_sub_auction_details_for_broker(@QueryMap Map<String, String> params);

    @GET("get_auction_end_time.php")
    Call<String> get_auction_end_time(@Query("auction_id") int auction_id);

    @GET("get_wallet_available_balance.php")
    Call<String> get_available_amt(@QueryMap Map<String, String> params);

    @GET("get_states.php")
    Call<String> get_states();

    @GET("get_support_details.php")
    Call<String> get_support_details();

    @GET("forget.php")
    Call<String> forget_password(@QueryMap Map<String, String> params);

    @GET("get_vendor_name_assigned_by.php")
    Call<String> get_vendor_name_assigned_by(@Query("broker_id") String broker_id, @Query("reqsub_auc_id") String reqsub_auc_id);


}

