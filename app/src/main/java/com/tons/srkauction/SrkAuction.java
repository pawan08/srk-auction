package com.tons.srkauction;

import android.content.Context;

import androidx.multidex.MultiDexApplication;

public class SrkAuction extends MultiDexApplication {

    public static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();

        appContext = this;
    }
}
