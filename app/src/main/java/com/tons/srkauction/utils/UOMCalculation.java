package com.tons.srkauction.utils;

import com.tons.srkauction.communication.Constants;
import com.tons.srkauction.storage.BrokerData;
import com.tons.srkauction.storage.UserData;
import com.tons.srkauction.storage.VendorData;

public class UOMCalculation {

    public static String calculateUOM(double api_unit_price) {

        double unit_price = api_unit_price;
        double calculated_uom = 0;
        String uom_type = null;
        String user_uom = null;
        if (UserData.getInstance().getUserType() == UserData.ROLE_VENDOR) {
            user_uom = VendorData.getInstance().getUOM();
        } else {
            user_uom = BrokerData.getInstance().getBrokerUOM();
        }
        switch (user_uom) {
            case Constants.UOM_ONE_KG:
                calculated_uom = unit_price / Constants.CAL_ONE_KG;
                uom_type = "per 1 KG";
                break;
            case Constants.UOM_QUINTAL:
                calculated_uom = unit_price / Constants.CAL_QUINTAL;
                uom_type = "per Quintal";
                break;
            case Constants.UOM_METRIC_TON:
                calculated_uom = unit_price / Constants.CAL_METRIC_TON;
                uom_type = "per Metric Ton";
                break;
            case Constants.UOM_TWENTY_KG_PRICE:
                calculated_uom = unit_price / Constants.CAL_TWENTY_KG_PRICE;
                uom_type = "per 20 KG";
                break;
        }

        return calculated_uom + " / " + uom_type;

    }

}

