package com.tons.srkauction.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;

import com.tons.srkauction.R;


/**
 * Created by admin on 2/2/2018.
 */

public class CustomToast {

    @SuppressLint("RestrictedApi")
    public static void showToast(final Context activity, final String msg) {
        if (activity == null) {
            return;
        }
        try {

            Toast toast = Toast.makeText(activity, msg, Toast.LENGTH_SHORT);
            LinearLayout toastLayout = (LinearLayout) toast.getView();
            TextView toastTV = (TextView) toastLayout.getChildAt(0);
            Typeface font = ResourcesCompat.getFont(activity, R.font.roboto);
            toastTV.setTypeface(font);
                toastTV.setTextSize(TypedValue.COMPLEX_UNIT_PX, activity.getResources().getDimensionPixelSize(R.dimen.snack_bar));
            toast.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


}
