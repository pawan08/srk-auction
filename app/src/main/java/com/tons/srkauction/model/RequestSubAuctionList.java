package com.tons.srkauction.model;

import java.util.ArrayList;

public class RequestSubAuctionList {

    private ArrayList<RequestSubAuction> list = new ArrayList<>();
    private static RequestSubAuctionList _instance = null;

    private RequestSubAuctionList() {

    }

    public static RequestSubAuctionList getInstance() {

        if (_instance == null) {
            _instance = new RequestSubAuctionList();
        }
        return _instance;
    }

    public void add(RequestSubAuction subAuction) {
        list.add(subAuction);
    }

    public ArrayList<RequestSubAuction> getList() {
        return list;
    }



    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
