package com.tons.srkauction.model;

import java.util.ArrayList;

public class CommodityList {

    private ArrayList<Comodity> list = new ArrayList<>();
    private static CommodityList _instance = null;

    private CommodityList() {

    }

    public static CommodityList getInstance() {

        if (_instance == null) {
            _instance = new CommodityList();
        }
        return _instance;
    }

    public void add(Comodity comodity) {
        list.add(comodity);
    }

    public ArrayList<Comodity> getList() {
        return list;
    }

    public ArrayList<String> getNames() {

        ArrayList<String> names = new ArrayList<>();
        // names.add(0, "Select Vendor Dealing In");
        for (Comodity comodity : list) {
            names.add(comodity.getCommodity_name());
        }
        return names;
    }

    public ArrayList<String> getCommodityNames() { // used to just differentiate the 0th value

        ArrayList<String> names = new ArrayList<>();
        names.add(0, "Select Auction Commodity");
        for (Comodity comodity : list) {
            names.add(comodity.getCommodity_name());
        }
        return names;
    }


    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }


}


