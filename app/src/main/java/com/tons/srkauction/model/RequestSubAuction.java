package com.tons.srkauction.model;

public class RequestSubAuction {

    private int reqsub_auc_id;
    private int reqsub_auc_vendor_id;
    private String reqsub_auc_qty;
    private String reqsub_auc_location;
    private String reqsub_auc_unit_price;
    private String reqsub_auc_emd;
    private String current_time;
    private String end_date_time;
    private String vendor_name;
    private int reqsub_auc_status;

    public RequestSubAuction(int reqsub_auc_id, int reqsub_auc_vendor_id, String reqsub_auc_qty, String reqsub_auc_location, String reqsub_auc_unit_price, String reqsub_auc_emd, String current_time, String end_date_time, String vendor_name, int reqsub_auc_status) {
        this.reqsub_auc_id = reqsub_auc_id;
        this.reqsub_auc_vendor_id = reqsub_auc_vendor_id;
        this.reqsub_auc_qty = reqsub_auc_qty;
        this.reqsub_auc_location = reqsub_auc_location;
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
        this.reqsub_auc_emd = reqsub_auc_emd;
        this.current_time = current_time;
        this.end_date_time = end_date_time;
        this.vendor_name = vendor_name;
        this.reqsub_auc_status = reqsub_auc_status;
    }

    public int getReqsub_auc_id() {
        return reqsub_auc_id;
    }

    public void setReqsub_auc_id(int reqsub_auc_id) {
        this.reqsub_auc_id = reqsub_auc_id;
    }

    public int getReqsub_auc_vendor_id() {
        return reqsub_auc_vendor_id;
    }

    public void setReqsub_auc_vendor_id(int reqsub_auc_vendor_id) {
        this.reqsub_auc_vendor_id = reqsub_auc_vendor_id;
    }

    public String getReqsub_auc_qty() {
        return reqsub_auc_qty;
    }

    public void setReqsub_auc_qty(String reqsub_auc_qty) {
        this.reqsub_auc_qty = reqsub_auc_qty;
    }

    public String getReqsub_auc_location() {
        return reqsub_auc_location;
    }

    public void setReqsub_auc_location(String reqsub_auc_location) {
        this.reqsub_auc_location = reqsub_auc_location;
    }

    public String getReqsub_auc_unit_price() {
        return reqsub_auc_unit_price;
    }

    public void setReqsub_auc_unit_price(String reqsub_auc_unit_price) {
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
    }

    public String getReqsub_auc_emd() {
        return reqsub_auc_emd;
    }

    public void setReqsub_auc_emd(String reqsub_auc_emd) {
        this.reqsub_auc_emd = reqsub_auc_emd;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public int getReqsub_auc_status() {
        return reqsub_auc_status;
    }

    public void setReqsub_auc_status(int reqsub_auc_status) {
        this.reqsub_auc_status = reqsub_auc_status;
    }
}
