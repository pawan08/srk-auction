package com.tons.srkauction.model;

public class UMO {

    int master_id;
    String master_item_name;

    public UMO(int master_id, String master_item_name) {
        this.master_id = master_id;
        this.master_item_name = master_item_name;
    }


    public int getMaster_id() {
        return master_id;
    }

    public void setMaster_id(int master_id) {
        this.master_id = master_id;
    }

    public String getMaster_item_name() {
        return master_item_name;
    }

    public void setMaster_item_name(String master_item_name) {
        this.master_item_name = master_item_name;
    }
}
