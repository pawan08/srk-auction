package com.tons.srkauction.model;

public class RejectedWalletHistory {

    private int  wallet_id;
    private int  wallet_vendor_id;
    private String  wallet_vendor_attachment;
    private String  wallet_credit;
    private String  wallet_debit;
    private String  wallet_admin_remark;
    private String  wallet_added_time;

    public RejectedWalletHistory(int wallet_id, int wallet_vendor_id, String wallet_vendor_attachment, String wallet_credit, String wallet_debit, String wallet_admin_remark, String wallet_added_time) {
        this.wallet_id = wallet_id;
        this.wallet_vendor_id = wallet_vendor_id;
        this.wallet_vendor_attachment = wallet_vendor_attachment;
        this.wallet_credit = wallet_credit;
        this.wallet_debit = wallet_debit;
        this.wallet_admin_remark = wallet_admin_remark;
        this.wallet_added_time = wallet_added_time;
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
    }

    public int getWallet_vendor_id() {
        return wallet_vendor_id;
    }

    public void setWallet_vendor_id(int wallet_vendor_id) {
        this.wallet_vendor_id = wallet_vendor_id;
    }

    public String getWallet_vendor_attachment() {
        return wallet_vendor_attachment;
    }

    public void setWallet_vendor_attachment(String wallet_vendor_attachment) {
        this.wallet_vendor_attachment = wallet_vendor_attachment;
    }

    public String getWallet_credit() {
        return wallet_credit;
    }

    public void setWallet_credit(String wallet_credit) {
        this.wallet_credit = wallet_credit;
    }

    public String getWallet_debit() {
        return wallet_debit;
    }

    public void setWallet_debit(String wallet_debit) {
        this.wallet_debit = wallet_debit;
    }

    public String getWallet_admin_remark() {
        return wallet_admin_remark;
    }

    public void setWallet_admin_remark(String wallet_admin_remark) {
        this.wallet_admin_remark = wallet_admin_remark;
    }

    public String getWallet_added_time() {
        return wallet_added_time;
    }

    public void setWallet_added_time(String wallet_added_time) {
        this.wallet_added_time = wallet_added_time;
    }
}
