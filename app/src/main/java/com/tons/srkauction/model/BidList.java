package com.tons.srkauction.model;

import java.util.ArrayList;

public class BidList {

    private ArrayList<BidModel> list = new ArrayList<>();
    private static BidList _instance = null;

    private BidList() {

    }

    public static BidList getInstance() {

        if (_instance == null) {
            _instance = new BidList();
        }
        return _instance;
    }

    public void add(BidModel Bid) {
        list.add(Bid);
    }

    public ArrayList<BidModel> getList() {
        return list;
    }

    public BidModel getItemAtPosition(int position){
        return list.get(position);
    }


    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
