package com.tons.srkauction.model;


import java.util.ArrayList;

public class VendorNotificationList {

    private ArrayList<VendorNotification> list = new ArrayList<>();
    private static VendorNotificationList _instance = null;
    private VendorNotification vendorNotification;

    private VendorNotificationList(){

    }

    public static VendorNotificationList getInstance(){
        if (_instance == null){
            _instance = new VendorNotificationList();
        }return _instance;
    }

    public void add(VendorNotification vendorNotification){
        list.add(vendorNotification);
    }

    public ArrayList<VendorNotification> getList(){
        return list;
    }

    public void setSelectedList(VendorNotification vendorNotification){
        this.vendorNotification = vendorNotification;
    }

    public VendorNotification getSelectedList(){
        return vendorNotification;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
