package com.tons.srkauction.model;

import java.util.ArrayList;

public class RequestAuctionList {

    private ArrayList<RequestAuction> list = new ArrayList<>();
    private static RequestAuctionList _instance = null;
    private RequestAuction auction;

    private RequestAuctionList(){

    }

    public static RequestAuctionList getInstance(){
        if (_instance == null){
            _instance = new RequestAuctionList();
        }return _instance;
    }

    public void add(RequestAuction auction){
        list.add(auction);
    }

    public ArrayList<RequestAuction> getList(){
        return list;
    }

    public void setSelectedList(RequestAuction auction){
        this.auction = auction;
    }

    public RequestAuction getSelectedList(){
        return auction;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
