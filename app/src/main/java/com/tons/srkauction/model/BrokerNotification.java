package com.tons.srkauction.model;

public class BrokerNotification {
    int nb_id;
    String nb_title;
    String nb_desc;
    String nb_added_time;
    boolean clicked;

    public BrokerNotification(int nb_id, String nb_title, String nb_desc, String nb_added_time) {
        this.nb_id = nb_id;
        this.nb_title = nb_title;
        this.nb_desc = nb_desc;
        this.nb_added_time = nb_added_time;
        this.clicked = false;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public int getNb_id() {
        return nb_id;
    }

    public void setNb_id(int nb_id) {
        this.nb_id = nb_id;
    }

    public String getNb_title() {
        return nb_title;
    }

    public void setNb_title(String nb_title) {
        this.nb_title = nb_title;
    }

    public String getNb_desc() {
        return nb_desc;
    }

    public void setNb_desc(String nb_desc) {
        this.nb_desc = nb_desc;
    }

    public String getNb_added_time() {
        return nb_added_time;
    }

    public void setNb_added_time(String nb_added_time) {
        this.nb_added_time = nb_added_time;
    }
}
