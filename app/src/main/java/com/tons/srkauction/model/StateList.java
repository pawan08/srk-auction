package com.tons.srkauction.model;

import java.util.ArrayList;

public class StateList {

    private ArrayList<StateModel> list = new ArrayList<>();
    private StateModel stateModel;
    private static StateList _instance = null;

    private StateList() {

    }

    public static StateList getInstance() {
        if (_instance == null) {
            _instance = new StateList();
        }
        return _instance;
    }

    public void add(StateModel stateModel) {
        list.add(stateModel);
    }

    public ArrayList<StateModel> getList() {
        return list;
    }

    public ArrayList<String> getStateName() {
        ArrayList<String> names = new ArrayList<String>();
        names.add(0, "Select State");
        for (StateModel stateModel : list) {
            names.add(stateModel.getS_name());
        }
        return names;
    }

    public int getStateID(String state_name) {
        for (StateModel stateModel : list) {
            if (stateModel.getS_name().equals(state_name)) {
                return stateModel.getS_id();
            }
        }
        return -1;
    }

    public String getSName(int state_id) {
        for (StateModel stateModel : list) {
            if (stateModel.getS_id() == state_id) {
                return stateModel.getS_name();
            }
        }
        return null;
    }

    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
