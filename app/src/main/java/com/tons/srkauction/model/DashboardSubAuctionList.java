package com.tons.srkauction.model;

import java.util.ArrayList;

public class DashboardSubAuctionList {

    private ArrayList<DashboardSubAuction> list = new ArrayList<>();
    private static DashboardSubAuctionList _instance = null;
    private DashboardSubAuction dashboardSubAuction;

    private DashboardSubAuctionList() {

    }

    public static DashboardSubAuctionList getInstance() {

        if (_instance == null) {
            _instance = new DashboardSubAuctionList();
        }
        return _instance;
    }

    public void add(DashboardSubAuction dashboardSubAuction) {
        list.add(dashboardSubAuction);
    }

    public ArrayList<DashboardSubAuction> getList() {
        return list;
    }

    public void setSelectedList(DashboardSubAuction dashboardSubAuction){
        this.dashboardSubAuction = dashboardSubAuction;
    }

    public DashboardSubAuction getSelectedList(){
        return dashboardSubAuction;
    }

    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
