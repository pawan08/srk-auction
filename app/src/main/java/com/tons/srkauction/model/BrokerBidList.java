package com.tons.srkauction.model;

import java.util.ArrayList;

public class BrokerBidList {

    private ArrayList<BrokerBidModel> list = new ArrayList<>();
    private static BrokerBidList _instance = null;

    private BrokerBidList() {

    }

    public static BrokerBidList getInstance() {

        if (_instance == null) {
            _instance = new BrokerBidList();
        }
        return _instance;
    }

    public void add(BrokerBidModel Bid) {
        list.add(Bid);
    }

    public ArrayList<BrokerBidModel> getList() {
        return list;
    }

    public BrokerBidModel getItemAtPosition(int position){
        return list.get(position);
    }


    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
