package com.tons.srkauction.model;

import java.util.ArrayList;

public class AuctionList {

    private ArrayList<LiveAuction> list = new ArrayList<>();
    private static AuctionList _instance = null;
    private LiveAuction liveAuction;

    private AuctionList(){

    }

    public static AuctionList getInstance(){
        if (_instance == null){
            _instance = new AuctionList();
        }return _instance;
    }

    public void add(LiveAuction liveAuction){
        list.add(liveAuction);
    }

    public ArrayList<LiveAuction> getList(){
        return list;
    }

    public void setSelectedList(LiveAuction liveAuction){
        this.liveAuction = liveAuction;
    }

    public LiveAuction getSelectedList(){
        return liveAuction;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
