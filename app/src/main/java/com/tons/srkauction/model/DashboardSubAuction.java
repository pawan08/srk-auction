package com.tons.srkauction.model;

public class DashboardSubAuction {

    private String auction_code;
    private String sub_auction_code;
    private String commodity;
    private String quantity;
    private String vendor_name;
    private String location;
    private String emd_amount;

    public DashboardSubAuction(String auction_code, String sub_auction_code, String commodity, String quantity, String vendor_name, String location, String emd_amount) {
        this.auction_code = auction_code;
        this.sub_auction_code = sub_auction_code;
        this.commodity = commodity;
        this.quantity = quantity;
        this.vendor_name = vendor_name;
        this.location = location;
        this.emd_amount = emd_amount;
    }

    public String getAuction_code() {
        return auction_code;
    }

    public void setAuction_code(String auction_code) {
        this.auction_code = auction_code;
    }

    public String getSub_auction_code() {
        return sub_auction_code;
    }

    public void setSub_auction_code(String sub_auction_code) {
        this.sub_auction_code = sub_auction_code;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getEmd_amount() {
        return emd_amount;
    }

    public void setEmd_amount(String emd_amount) {
        this.emd_amount = emd_amount;
    }
}
