package com.tons.srkauction.model;

public class RequestWithdrawWalletModel {
    int id;
    private String date;
    private String amt;

    public RequestWithdrawWalletModel(String date, String amt) {
        this.date = date;
        this.amt = amt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmt() {
        return amt;
    }

    public void setAmt(String amt) {
        this.amt = amt;
    }
}
