package com.tons.srkauction.model;

public class BrokerBidModel {
    int bid_id;
    int bid_vendor_id;
    int bid_broker_id;
    String bid_added_time;
    String bid_amount;
    String vendor_name;
    String broker_name;
    int reqsub_auc_vendor_id;

    public BrokerBidModel(int bid_id, int bid_vendor_id, int bid_broker_id, String bid_added_time, String bid_amount, String vendor_name, String broker_name, int reqsub_auc_vendor_id) {
        this.bid_id = bid_id;
        this.bid_vendor_id = bid_vendor_id;
        this.bid_broker_id = bid_broker_id;
        this.bid_added_time = bid_added_time;
        this.bid_amount = bid_amount;
        this.vendor_name = vendor_name;
        this.broker_name = broker_name;
        this.reqsub_auc_vendor_id = reqsub_auc_vendor_id;
    }

    public int getBid_id() {
        return bid_id;
    }

    public void setBid_id(int bid_id) {
        this.bid_id = bid_id;
    }

    public int getBid_vendor_id() {
        return bid_vendor_id;
    }

    public void setBid_vendor_id(int bid_vendor_id) {
        this.bid_vendor_id = bid_vendor_id;
    }

    public int getBid_broker_id() {
        return bid_broker_id;
    }

    public void setBid_broker_id(int bid_broker_id) {
        this.bid_broker_id = bid_broker_id;
    }

    public String getBid_added_time() {
        return bid_added_time;
    }

    public void setBid_added_time(String bid_added_time) {
        this.bid_added_time = bid_added_time;
    }

    public String getBid_amount() {
        return bid_amount;
    }

    public void setBid_amount(String bid_amount) {
        this.bid_amount = bid_amount;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getBroker_name() {
        return broker_name;
    }

    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name;
    }

    public int getReqsub_auc_vendor_id() {
        return reqsub_auc_vendor_id;
    }

    public void setReqsub_auc_vendor_id(int reqsub_auc_vendor_id) {
        this.reqsub_auc_vendor_id = reqsub_auc_vendor_id;
    }
}
