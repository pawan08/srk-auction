package com.tons.srkauction.model;

import android.content.BroadcastReceiver;

import java.util.ArrayList;

public class BrokerNotificationList {

    private ArrayList<BrokerNotification> list = new ArrayList<>();
    private static BrokerNotificationList _instance = null;
    private BrokerNotification brokerNotification;

    private BrokerNotificationList(){

    }

    public static BrokerNotificationList getInstance(){
        if (_instance == null){
            _instance = new BrokerNotificationList();
        }return _instance;
    }

    public void add(BrokerNotification brokerNotification){
        list.add(brokerNotification);
    }

    public ArrayList<BrokerNotification> getList(){
        return list;
    }

    public void setSelectedList(BrokerNotification brokerNotification){
        this.brokerNotification = brokerNotification;
    }

    public BrokerNotification getSelectedList(){
        return brokerNotification;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
