package com.tons.srkauction.model;

public class VendorNotification {
    int nv_id;
    String nv_title;
    String nv_desc;
    String nv_added_time;

    public int getNv_id() {
        return nv_id;
    }

    public void setNv_id(int nv_id) {
        this.nv_id = nv_id;
    }

    public String getNv_title() {
        return nv_title;
    }

    public void setNv_title(String nv_title) {
        this.nv_title = nv_title;
    }

    public String getNv_desc() {
        return nv_desc;
    }

    public void setNv_desc(String nv_desc) {
        this.nv_desc = nv_desc;
    }

    public String getNv_added_time() {
        return nv_added_time;
    }

    public void setNv_added_time(String nv_added_time) {
        this.nv_added_time = nv_added_time;
    }

    public VendorNotification(int nv_id, String nv_title, String nv_desc, String nv_added_time) {
        this.nv_id = nv_id;
        this.nv_title = nv_title;
        this.nv_desc = nv_desc;
        this.nv_added_time = nv_added_time;
    }
}
