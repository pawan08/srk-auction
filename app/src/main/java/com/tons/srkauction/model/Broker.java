package com.tons.srkauction.model;

public class Broker {

    private int broker_id;
    private String broker_name;

    public Broker(int broker_id, String broker_name) {
        this.broker_id = broker_id;
        this.broker_name = broker_name;
    }

    public int getBroker_id() {
        return broker_id;
    }

    public void setBroker_id(int broker_id) {
        this.broker_id = broker_id;
    }

    public String getBroker_name() {
        return broker_name;
    }

    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name;
    }
}
