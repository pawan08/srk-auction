package com.tons.srkauction.model;

import java.util.ArrayList;

public class PendingWalletHistoryList {

    private ArrayList<PendingWalletHistory> list = new ArrayList<>();
    private static PendingWalletHistoryList _instance = null;
    private PendingWalletHistory pendingWalletHistory;

    private PendingWalletHistoryList(){

    }

    public static PendingWalletHistoryList getInstance(){
        if (_instance == null){
            _instance = new PendingWalletHistoryList();
        }return _instance;
    }

    public void add(PendingWalletHistory pendingWalletHistory){
        list.add(pendingWalletHistory);
    }

    public ArrayList<PendingWalletHistory> getList(){
        return list;
    }

    public void setSelectedList(PendingWalletHistory pendingWalletHistory){
        this.pendingWalletHistory = pendingWalletHistory;
    }

    public PendingWalletHistory getSelectedList(){
        return pendingWalletHistory;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
