package com.tons.srkauction.model;

import java.util.ArrayList;

public class VendorList {

    private ArrayList<Vendor> list = new ArrayList<>();
    private static VendorList _instance = null;

    private VendorList() {

    }

    public static VendorList getInstance() {

        if (_instance == null) {
            _instance = new VendorList();
        }
        return _instance;
    }

    public void add(Vendor vendor) {
        list.add(vendor);
    }

    public ArrayList<Vendor> getList() {
        return list;
    }

    public ArrayList<String> getNames() {

        ArrayList<String> names = new ArrayList<>();
        names.add(0, "Select Vendor");
        for (Vendor vendor : list) {
            names.add(vendor.getVendor_name());
        }
        return names;
    }

    public int getVendorsID(String vendor_name) {
        for (Vendor vendor : list) {
            if (vendor.getVendor_name().equals(vendor_name)) {
                return vendor.getVendor_id();
            }
        }
        return -1;
    }

    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
