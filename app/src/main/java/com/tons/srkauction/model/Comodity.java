package com.tons.srkauction.model;

public class Comodity {

    private int commodity_id;
    private String commodity_name;

    public Comodity(int commodity_id, String commodity_name) {
        this.commodity_id = commodity_id;
        this.commodity_name = commodity_name;
    }

    public int getCommodity_id() {
        return commodity_id;
    }

    public void setCommodity_id(int commodity_id) {
        this.commodity_id = commodity_id;
    }

    public String getCommodity_name() {
        return commodity_name;
    }

    public void setCommodity_name(String commodity_name) {
        this.commodity_name = commodity_name;
    }
}
