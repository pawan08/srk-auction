package com.tons.srkauction.model;

public class SubAuction {

    private int auction_id;
    private int reqsub_auc_id;
    private int reqsub_auc_id_vendor_id;
    private String reqsub_auc_qty;
    private String reqsub_auc_location;
    private String reqsub_auc_unit_price;
    private String reqsub_auc_emd;
    private String current_time;
    private String end_date_time;
    private String vendor_name;
    private int reqsub_auc_status;
    private String balance_wallet_amt;
    private String pay_emd;
    private String add_money_into_wallet;
    private int wallet_status;
    private String reqsub_auc_code;

    public SubAuction(int auction_id, int reqsub_auc_id, int reqsub_auc_id_vendor_id, String reqsub_auc_qty, String reqsub_auc_location, String reqsub_auc_unit_price, String reqsub_auc_emd, String current_time, String end_date_time, String vendor_name, int reqsub_auc_status, String balance_wallet_amt, String pay_emd, String add_money_into_wallet, int wallet_status, String reqsub_auc_code) {
        this.auction_id = auction_id;
        this.reqsub_auc_id = reqsub_auc_id;
        this.reqsub_auc_id_vendor_id = reqsub_auc_id_vendor_id;
        this.reqsub_auc_qty = reqsub_auc_qty;
        this.reqsub_auc_location = reqsub_auc_location;
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
        this.reqsub_auc_emd = reqsub_auc_emd;
        this.current_time = current_time;
        this.end_date_time = end_date_time;
        this.vendor_name = vendor_name;
        this.reqsub_auc_status = reqsub_auc_status;
        this.balance_wallet_amt = balance_wallet_amt;
        this.pay_emd = pay_emd;
        this.add_money_into_wallet = add_money_into_wallet;
        this.wallet_status = wallet_status;
        this.reqsub_auc_code = reqsub_auc_code;
    }

    public int getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(int auction_id) {
        this.auction_id = auction_id;
    }

    public int getReqsub_auc_id() {
        return reqsub_auc_id;
    }

    public void setReqsub_auc_id(int reqsub_auc_id) {
        this.reqsub_auc_id = reqsub_auc_id;
    }

    public int getReqsub_auc_id_vendor_id() {
        return reqsub_auc_id_vendor_id;
    }

    public void setReqsub_auc_id_vendor_id(int reqsub_auc_id_vendor_id) {
        this.reqsub_auc_id_vendor_id = reqsub_auc_id_vendor_id;
    }

    public String getReqsub_auc_qty() {
        return reqsub_auc_qty;
    }

    public void setReqsub_auc_qty(String reqsub_auc_qty) {
        this.reqsub_auc_qty = reqsub_auc_qty;
    }

    public String getReqsub_auc_location() {
        return reqsub_auc_location;
    }

    public void setReqsub_auc_location(String reqsub_auc_location) {
        this.reqsub_auc_location = reqsub_auc_location;
    }

    public String getReqsub_auc_unit_price() {
        return reqsub_auc_unit_price;
    }

    public void setReqsub_auc_unit_price(String reqsub_auc_unit_price) {
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
    }

    public String getReqsub_auc_emd() {
        return reqsub_auc_emd;
    }

    public void setReqsub_auc_emd(String reqsub_auc_emd) {
        this.reqsub_auc_emd = reqsub_auc_emd;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public int getReqsub_auc_status() {
        return reqsub_auc_status;
    }

    public void setReqsub_auc_status(int reqsub_auc_status) {
        this.reqsub_auc_status = reqsub_auc_status;
    }

    public String getBalance_wallet_amt() {
        return balance_wallet_amt;
    }

    public void setBalance_wallet_amt(String balance_wallet_amt) {
        this.balance_wallet_amt = balance_wallet_amt;
    }

    public String getPay_emd() {
        return pay_emd;
    }

    public void setPay_emd(String pay_emd) {
        this.pay_emd = pay_emd;
    }

    public String getAdd_money_into_wallet() {
        return add_money_into_wallet;
    }

    public void setAdd_money_into_wallet(String add_money_into_wallet) {
        this.add_money_into_wallet = add_money_into_wallet;
    }

    public int getWallet_status() {
        return wallet_status;
    }

    public void setWallet_status(int wallet_status) {
        this.wallet_status = wallet_status;
    }

    public String getReqsub_auc_code() {
        return reqsub_auc_code;
    }

    public void setReqsub_auc_code(String reqsub_auc_code) {
        this.reqsub_auc_code = reqsub_auc_code;
    }
}

