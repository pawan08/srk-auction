package com.tons.srkauction.model;


import java.util.ArrayList;

public class ViewSampleList {

    private ArrayList<ViewSample> list = new ArrayList<>();
    private static ViewSampleList _instance = null;
    private ViewSample viewSample;

    private ViewSampleList(){

    }

    public static ViewSampleList getInstance(){
        if (_instance == null){
            _instance = new ViewSampleList();
        }return _instance;
    }

    public void add(ViewSample viewSample){
        list.add(viewSample);
    }

    public ArrayList<ViewSample> getList(){
        return list;
    }

    public void setSelectedList(ViewSample viewSample){
        this.viewSample = viewSample;
    }

    public ViewSample getSelectedList(){
        return viewSample;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
