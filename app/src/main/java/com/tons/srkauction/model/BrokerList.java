package com.tons.srkauction.model;

import java.util.ArrayList;

public class BrokerList {

    private ArrayList<Broker> list = new ArrayList<>();
    private static BrokerList _instance = null;

    private BrokerList() {

    }

    public static BrokerList getInstance() {

        if (_instance == null) {
            _instance = new BrokerList();
        }
        return _instance;
    }

    public void add(Broker broker) {
        list.add(broker);
    }

    public ArrayList<Broker> getList() {
        return list;
    }

    public ArrayList<String> getNames() {

        ArrayList<String> names = new ArrayList<>();
        names.add(0, "Select Broker");
        for (Broker broker : list) {
            names.add(broker.getBroker_name());
        }
        return names;
    }

    public int getVendorsID(String broker_name) {
        for (Broker broker : list) {
            if (broker.getBroker_name().equals(broker_name)) {
                return broker.getBroker_id();
            }
        }
        return -1;
    }

    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
