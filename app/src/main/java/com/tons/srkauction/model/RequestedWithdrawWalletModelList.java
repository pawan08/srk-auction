package com.tons.srkauction.model;

import java.util.ArrayList;

public class RequestedWithdrawWalletModelList {


    private ArrayList<RequestWithdrawWalletModel> list = new ArrayList<>();
    private static RequestedWithdrawWalletModelList _instance = null;
    private RequestWithdrawWalletModel requestWithdrawWalletModel;

    private RequestedWithdrawWalletModelList(){

    }

    public static RequestedWithdrawWalletModelList getInstance(){
        if (_instance == null){
            _instance = new RequestedWithdrawWalletModelList();
        }return _instance;
    }

    public void add(RequestWithdrawWalletModel requestWithdrawWalletModel){
        list.add(requestWithdrawWalletModel);
    }

    public ArrayList<RequestWithdrawWalletModel> getList(){
        return list;
    }

    public void setSelectedList(RequestWithdrawWalletModel requestWithdrawWalletModel){
        this.requestWithdrawWalletModel = requestWithdrawWalletModel;
    }

    public RequestWithdrawWalletModel getSelectedList(){
        return requestWithdrawWalletModel;
    }

    public void clearList(){
        if (list.size() != 0){
            list.clear();
        }
    }
}
