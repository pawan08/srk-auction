package com.tons.srkauction.model;

import java.util.ArrayList;

public class UMOList {

    private ArrayList<UMO> list = new ArrayList<>();
    private static UMOList _instance = null;

    private UMOList() {

    }

    public static UMOList getInstance() {

        if (_instance == null) {
            _instance = new UMOList();
        }
        return _instance;
    }

    public void add(UMO umo) {
        list.add(umo);
    }

    public ArrayList<UMO> getList() {
        return list;
    }

    public ArrayList<String> getmaster_item_name() {

        ArrayList<String> names = new ArrayList<>();
        names.add(0, "Select UMO");
        for (UMO umo : list) {
            names.add(umo.getMaster_item_name());
        }
        return names;
    }

    public int getmaster_id(String master_id) {
        for (UMO umo : list) {
            if (umo.getMaster_item_name().equals(master_id)) {
                return umo.getMaster_id();
            }
        }
        return -1;
    }

    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
