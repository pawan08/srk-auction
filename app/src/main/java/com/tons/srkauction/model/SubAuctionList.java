package com.tons.srkauction.model;

import java.util.ArrayList;

public class SubAuctionList {

    private ArrayList<SubAuction> list = new ArrayList<>();
    private static SubAuctionList _instance = null;

    private SubAuctionList() {

    }

    public static SubAuctionList getInstance() {

        if (_instance == null) {
            _instance = new SubAuctionList();
        }
        return _instance;
    }

    public void add(SubAuction subAuction) {
        list.add(subAuction);
    }

    public ArrayList<SubAuction> getList() {
        return list;
    }



    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
