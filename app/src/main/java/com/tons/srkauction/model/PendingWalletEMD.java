package com.tons.srkauction.model;

public class PendingWalletEMD {

    private int  wallet_id;
    private int  wallet_vendor_id;
    private String  wallet_credit;
    private String  wallet_debit;
    private String  wallet_added_time;
    private int reqsub_auc_id;
    private String  reqsub_auc_code;


    public PendingWalletEMD(int wallet_id, int wallet_vendor_id, String wallet_credit, String wallet_debit, String wallet_added_time, int reqsub_auc_id, String reqsub_auc_code) {
        this.wallet_id = wallet_id;
        this.wallet_vendor_id = wallet_vendor_id;
        this.wallet_credit = wallet_credit;
        this.wallet_debit = wallet_debit;
        this.wallet_added_time = wallet_added_time;
        this.reqsub_auc_id = reqsub_auc_id;
        this.reqsub_auc_code = reqsub_auc_code;
    }

    public int getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(int wallet_id) {
        this.wallet_id = wallet_id;
    }

    public int getWallet_vendor_id() {
        return wallet_vendor_id;
    }

    public void setWallet_vendor_id(int wallet_vendor_id) {
        this.wallet_vendor_id = wallet_vendor_id;
    }

    public String getWallet_credit() {
        return wallet_credit;
    }

    public void setWallet_credit(String wallet_credit) {
        this.wallet_credit = wallet_credit;
    }

    public String getWallet_debit() {
        return wallet_debit;
    }

    public void setWallet_debit(String wallet_debit) {
        this.wallet_debit = wallet_debit;
    }

    public String getWallet_added_time() {
        return wallet_added_time;
    }

    public void setWallet_added_time(String wallet_added_time) {
        this.wallet_added_time = wallet_added_time;
    }

    public int getReqsub_auc_id() {
        return reqsub_auc_id;
    }

    public void setReqsub_auc_id(int reqsub_auc_id) {
        this.reqsub_auc_id = reqsub_auc_id;
    }

    public String getReqsub_auc_code() {
        return reqsub_auc_code;
    }

    public void setReqsub_auc_code(String reqsub_auc_code) {
        this.reqsub_auc_code = reqsub_auc_code;
    }
}
