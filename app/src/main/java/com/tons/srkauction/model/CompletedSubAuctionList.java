package com.tons.srkauction.model;

import java.util.ArrayList;

public class CompletedSubAuctionList {

    private ArrayList<CompletedSubAuction> list = new ArrayList<>();
    private static CompletedSubAuctionList _instance = null;

    private CompletedSubAuctionList() {

    }

    public static CompletedSubAuctionList getInstance() {

        if (_instance == null) {
            _instance = new CompletedSubAuctionList();
        }
        return _instance;
    }

    public void add(CompletedSubAuction subAuction) {
        list.add(subAuction);
    }

    public ArrayList<CompletedSubAuction> getList() {
        return list;
    }



    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
