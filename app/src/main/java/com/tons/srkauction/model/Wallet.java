package com.tons.srkauction.model;

public class Wallet {
    private String wallet_id;
    private String wallet_credit;
    private String wallet_debit;
    private String wallet_desc;
    private String wallet_type;
    private String wallet_added_time;
    private String wallet_changed_time;


    public Wallet(String wallet_id, String wallet_credit, String wallet_debit, String wallet_desc, String wallet_type, String wallet_added_time,String wallet_changed_time) {
        this.wallet_id = wallet_id;
        this.wallet_credit = wallet_credit;
        this.wallet_debit = wallet_debit;
        this.wallet_desc = wallet_desc;
        this.wallet_type = wallet_type;
        this.wallet_added_time = wallet_added_time;
        this.wallet_changed_time=wallet_changed_time;
    }

    public String getWallet_id() {
        return wallet_id;
    }

    public void setWallet_id(String wallet_id) {
        this.wallet_id = wallet_id;
    }

    public String getWallet_credit() {
        return wallet_credit;
    }

    public void setWallet_credit(String wallet_credit) {
        this.wallet_credit = wallet_credit;
    }

    public String getWallet_debit() {
        return wallet_debit;
    }

    public void setWallet_debit(String wallet_debit) {
        this.wallet_debit = wallet_debit;
    }

    public String getWallet_desc() {
        return wallet_desc;
    }

    public void setWallet_desc(String wallet_desc) {
        this.wallet_desc = wallet_desc;
    }

    public String getWallet_type() {
        return wallet_type;
    }

    public void setWallet_type(String wallet_type) {
        this.wallet_type = wallet_type;
    }

    public String getWallet_added_time() {
        return wallet_added_time;
    }

    public void setWallet_added_time(String wallet_added_time) {
        this.wallet_added_time = wallet_added_time;
    }


    public String getWallet_changed_time() {
        return wallet_changed_time;
    }

    public void setWallet_changed_time(String wallet_changed_time) {
        this.wallet_changed_time = wallet_changed_time;
    }
}
