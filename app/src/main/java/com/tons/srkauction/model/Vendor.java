package com.tons.srkauction.model;

public class Vendor {

    private int vendor_id;
    private String vendor_name;

    public Vendor(int vendor_id, String vendor_name) {
        this.vendor_id = vendor_id;
        this.vendor_name = vendor_name;
    }

    public int getVendor_id() {
        return vendor_id;
    }

    public void setVendor_id(int vendor_id) {
        this.vendor_id = vendor_id;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }
}
