package com.tons.srkauction.model;

public class StateModel {

    private  String s_name;
    private  int s_id;

    public StateModel(String s_name, int s_id) {
        this.s_name = s_name;
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }
}
