package com.tons.srkauction.model;

public class CompletedAuction {

    int auction_id;
    String auction_code;
    String auction_type;
    String auction_commodity;
    String uom;
    String date;
    String auction_start_time;
    String is_winner;
    String is_mine_auction;

    public CompletedAuction(int auction_id, String auction_code, String auction_type, String auction_commodity, String uom, String date, String auction_start_time, String is_winner, String is_mine_auction) {
        this.auction_id = auction_id;
        this.auction_code = auction_code;
        this.auction_type = auction_type;
        this.auction_commodity = auction_commodity;
        this.uom = uom;
        this.date = date;
        this.auction_start_time = auction_start_time;
        this.is_winner = is_winner;
        this.is_mine_auction = is_mine_auction;
    }

    public int getAuction_id() {
        return auction_id;
    }

    public void setAuction_id(int auction_id) {
        this.auction_id = auction_id;
    }

    public String getAuction_code() {
        return auction_code;
    }

    public void setAuction_code(String auction_code) {
        this.auction_code = auction_code;
    }

    public String getAuction_type() {
        return auction_type;
    }

    public void setAuction_type(String auction_type) {
        this.auction_type = auction_type;
    }

    public String getAuction_commodity() {
        return auction_commodity;
    }

    public void setAuction_commodity(String auction_commodity) {
        this.auction_commodity = auction_commodity;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuction_start_time() {
        return auction_start_time;
    }

    public void setAuction_start_time(String auction_start_time) {
        this.auction_start_time = auction_start_time;
    }

    public String getIs_winner() {
        return is_winner;
    }

    public void setIs_winner(String is_winner) {
        this.is_winner = is_winner;
    }

    public String getIs_mine_auction() {
        return is_mine_auction;
    }

    public void setIs_mine_auction(String is_mine_auction) {
        this.is_mine_auction = is_mine_auction;
    }
}
