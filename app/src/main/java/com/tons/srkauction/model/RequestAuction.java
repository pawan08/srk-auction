package com.tons.srkauction.model;

public class RequestAuction {

    int request_auction_id;
    String auction_type;
    String auction_commodity;
    String uom;
    String unit_price;
    String qty;
    String date;
    String request_auction_vendor;
    String request_auction_broker;
    String broker_name;
    String request_auction_status;
    String request_auction_remark;
    String sub_auction_count;

    public RequestAuction(int request_auction_id, String auction_type, String auction_commodity, String uom, String unit_price, String qty, String date, String request_auction_vendor, String request_auction_broker, String broker_name, String request_auction_status, String request_auction_remark, String sub_auction_count) {
        this.request_auction_id = request_auction_id;
        this.auction_type = auction_type;
        this.auction_commodity = auction_commodity;
        this.uom = uom;
        this.unit_price = unit_price;
        this.qty = qty;
        this.date = date;
        this.request_auction_vendor = request_auction_vendor;
        this.request_auction_broker = request_auction_broker;
        this.broker_name = broker_name;
        this.request_auction_status = request_auction_status;
        this.request_auction_remark = request_auction_remark;
        this.sub_auction_count = sub_auction_count;
    }

    public int getRequest_auction_id() {
        return request_auction_id;
    }

    public void setRequest_auction_id(int request_auction_id) {
        this.request_auction_id = request_auction_id;
    }

    public String getAuction_type() {
        return auction_type;
    }

    public void setAuction_type(String auction_type) {
        this.auction_type = auction_type;
    }

    public String getAuction_commodity() {
        return auction_commodity;
    }

    public void setAuction_commodity(String auction_commodity) {
        this.auction_commodity = auction_commodity;
    }

    public String getUom() {
        return uom;
    }

    public void setUom(String uom) {
        this.uom = uom;
    }

    public String getUnit_price() {
        return unit_price;
    }

    public void setUnit_price(String unit_price) {
        this.unit_price = unit_price;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getRequest_auction_vendor() {
        return request_auction_vendor;
    }

    public void setRequest_auction_vendor(String request_auction_vendor) {
        this.request_auction_vendor = request_auction_vendor;
    }

    public String getRequest_auction_broker() {
        return request_auction_broker;
    }

    public void setRequest_auction_broker(String request_auction_broker) {
        this.request_auction_broker = request_auction_broker;
    }

    public String getBroker_name() {
        return broker_name;
    }

    public void setBroker_name(String broker_name) {
        this.broker_name = broker_name;
    }

    public String getRequest_auction_status() {
        return request_auction_status;
    }

    public void setRequest_auction_status(String request_auction_status) {
        this.request_auction_status = request_auction_status;
    }

    public String getRequest_auction_remark() {
        return request_auction_remark;
    }

    public void setRequest_auction_remark(String request_auction_remark) {
        this.request_auction_remark = request_auction_remark;
    }

    public String getSub_auction_count() {
        return sub_auction_count;
    }

    public void setSub_auction_count(String sub_auction_count) {
        this.sub_auction_count = sub_auction_count;
    }
}
