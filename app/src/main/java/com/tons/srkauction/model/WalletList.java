package com.tons.srkauction.model;

import java.util.ArrayList;

public class WalletList {

    private ArrayList<Wallet> list = new ArrayList<>();
    private static WalletList _instance = null;

    private WalletList() {

    }

    public static WalletList getInstance() {

        if (_instance == null) {
            _instance = new WalletList();
        }
        return _instance;
    }

    public void add(Wallet Wallet) {
        list.add(Wallet);
    }

    public ArrayList<Wallet> getList() {
        return list;
    }



    public void clearList() {
        if (list.size() > 0) {
            list.clear();
        }
    }
}
