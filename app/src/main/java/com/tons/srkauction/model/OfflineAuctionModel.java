package com.tons.srkauction.model;

public class OfflineAuctionModel {

    private String auction_code;
    private String commodity;
    private String price;
    private String quantity;

    public OfflineAuctionModel(String auction_code, String commodity, String price, String quantity) {
        this.auction_code = auction_code;
        this.commodity = commodity;
        this.price = price;
        this.quantity = quantity;
    }

    public String getAuction_code() {
        return auction_code;
    }

    public void setAuction_code(String auction_code) {
        this.auction_code = auction_code;
    }

    public String getCommodity() {
        return commodity;
    }

    public void setCommodity(String commodity) {
        this.commodity = commodity;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
}
