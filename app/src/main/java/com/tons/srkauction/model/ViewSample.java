package com.tons.srkauction.model;

public class ViewSample {

    String sample_id;
    String reqsub_auc_code;
    String reqsub_auc_warehouse;
    String reqsub_auc_location;
    String reqsub_auc_qty;
    String reqsub_auc_unit_price;
    int reqsub_auc_id;
    String vendor_name;
    String request_auction_commodity;

    public ViewSample(String sample_id, String reqsub_auc_code, String reqsub_auc_warehouse, String reqsub_auc_location, String reqsub_auc_qty, String reqsub_auc_unit_price, int reqsub_auc_id, String vendor_name, String request_auction_commodity) {
        this.sample_id = sample_id;
        this.reqsub_auc_code = reqsub_auc_code;
        this.reqsub_auc_warehouse = reqsub_auc_warehouse;
        this.reqsub_auc_location = reqsub_auc_location;
        this.reqsub_auc_qty = reqsub_auc_qty;
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
        this.reqsub_auc_id = reqsub_auc_id;
        this.vendor_name = vendor_name;
        this.request_auction_commodity = request_auction_commodity;
    }

    public String getSample_id() {
        return sample_id;
    }

    public void setSample_id(String sample_id) {
        this.sample_id = sample_id;
    }

    public String getReqsub_auc_code() {
        return reqsub_auc_code;
    }

    public void setReqsub_auc_code(String reqsub_auc_code) {
        this.reqsub_auc_code = reqsub_auc_code;
    }

    public String getReqsub_auc_warehouse() {
        return reqsub_auc_warehouse;
    }

    public void setReqsub_auc_warehouse(String reqsub_auc_warehouse) {
        this.reqsub_auc_warehouse = reqsub_auc_warehouse;
    }

    public String getReqsub_auc_location() {
        return reqsub_auc_location;
    }

    public void setReqsub_auc_location(String reqsub_auc_location) {
        this.reqsub_auc_location = reqsub_auc_location;
    }

    public String getReqsub_auc_qty() {
        return reqsub_auc_qty;
    }

    public void setReqsub_auc_qty(String reqsub_auc_qty) {
        this.reqsub_auc_qty = reqsub_auc_qty;
    }

    public String getReqsub_auc_unit_price() {
        return reqsub_auc_unit_price;
    }

    public void setReqsub_auc_unit_price(String reqsub_auc_unit_price) {
        this.reqsub_auc_unit_price = reqsub_auc_unit_price;
    }

    public int getReqsub_auc_id() {
        return reqsub_auc_id;
    }

    public void setReqsub_auc_id(int reqsub_auc_id) {
        this.reqsub_auc_id = reqsub_auc_id;
    }

    public String getVendor_name() {
        return vendor_name;
    }

    public void setVendor_name(String vendor_name) {
        this.vendor_name = vendor_name;
    }

    public String getRequest_auction_commodity() {
        return request_auction_commodity;
    }

    public void setRequest_auction_commodity(String request_auction_commodity) {
        this.request_auction_commodity = request_auction_commodity;
    }
}
